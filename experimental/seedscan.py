"""Seed scanning procedure."""
VS_MODULE_PATH = "../vaccine_strains/"
import sys
sys.path.append(VS_MODULE_PATH)
import sequence_functions as seqf

# from ..vaccine_strains import sequence_functions as seqf # use packaging

import copy

class Sequence:
    def __init__(self, string, alphabet):
        if not alphabet.adheresAlphabet(string)[0]:
            raise ValueError("@Sequence.__init__: string does not adhere to alphabet")
        self.string = string
        self.alphabet = alphabet


def indexKmers(seq, k):
    """Index kmers of length k in seq into a dictionary, showing counts and locations."""
    kmer_counts, kmer_locs = {}, {}
    for i in range(len(seq) - k + 1):
        kmer = seq[i:i+k]
        kmer_counts[kmer] = kmer_counts.get(kmer, 0) + 1
        if kmer in kmer_locs: # other method would be inefficient
            kmer_locs[kmer].append(i)
        else:
            kmer_locs[kmer] = [i]
    return kmer_counts, kmer_locs


def allKmers(alphabet):
    """Return list of kmers possible with alphabet."""
    klist = []
    for a in alphabet:
        if len(klist) == 0:
            klist = a
        else:
            for i in range(len(klist)):
                klist[i] += a
    return klist


def mergeListIntoDict(list, dict, default_value):
    """Merge list items into dict with value default_value if not in dict. Mutates dict."""
    for l in list:
        if l not in dict: # only perform get check in case key in dict
            dict[l] = default_value
    return dict


def mergeListIntoDictFunctional(list, dict, func):
    """Merge list items into dict with value func(list item) if not in dict. Mutates dict."""
    for l in list:
        if l not in dict: # only perform get check in case key in dict
            dict[l] = func(l)
    return dict


RNA_COMPLEMENTS_INCL_GU = {"A" : [("A", 1), ("U", 0), ("C", 1), ("G", 1)],
                           "U" : [("A", 0), ("U", 1), ("C", 1), ("G", 0.5)],
                           "C" : [("A", 1), ("U", 1), ("C", 1), ("G", 0)],
                           "G" : [("A", 1), ("U", 0.5), ("C", 0), ("G", 1)]}
def getComplements(kmers, compl_dict, max_penalty):
    """Get list of complementaries per kmer in kmers, computed with compl_dict
    with maximum score/penalty of max_penalty."""
    complements = [] # [(complement string, complement score)]
    for kmer in kmers: # assume no inefficient duplicates
        # optimization possible: overlapping sub-kmers have same sub-kmer complements
        kmer_compl = [("", 0)] # [(complement string, complement score)]
        for c in kmer: # loop over kmer chars
            kmer_compl_new = []
            for compl_char in compl_dict[c]: # loop over complement chars to c; throw KeyError
                for compl in kmer_compl: # loop over complement strings so far
                    nscore = compl[1] + compl_char[1]
                    if nscore <= max_penalty: # add to complements
                        kmer_compl_new.append((compl[0] + compl_char[0], nscore))
            if len(kmer_compl_new) > 0:
                kmer_compl = kmer_compl_new # points to old list address?
            else:
                kmer_compl = [("", 0)]

        if kmer_compl == [("", 0)]:
            complements.append([]) # empty; no matching complements
        else:
            complements.append(copy.copy(kmer_compl)) # all that pass max_penalty threshold
    return complements


def matchComplementaries(kmer_counts1, kmer_locs1,
                         kmer_counts2, kmer_locs2,
                         complementation_dict, compl_max_penalty,
                         kmer_count_cutoff=0):
    """Matching algorithm for kmer complementation in sequence. Between two sequences' kmer contents.
    kmer_count_cutoff gives threshold of scan-strand kmers to consider for complementation.
        Improves speed, decreases reliability."""
    complements_computed = {} # efficiency
    locs_computed = {} # efficiecny
    for kmer1, kmer_count1 in kmer_counts1.items():
        if kmer_count1 < kmer_count_cutoff:
            continue

        target_seq_locs = [] # indices of kmer starts of complementary kmers in target seq (2)
        if kmer1 not in complements_computed:
            complements_computed[kmer1] = getComplements(kmer1, complementation_dict, compl_max_penalty)
        k1_complements = complements_computed[kmer1]
        for kc in k1_complements:
            target_seq_locs.extend(kmer_locs2[kc]) # store complement locations


    return

# seed scan --> overlap removal ? --> pre-extension + pre-rejection --> extension + rejection --> evaluation
if __name__ == "__main__":
    seq_Q = "AAAABBABABAAAABABABABBABABABAAAAC"
    seq_T = ""
    print(indexKmers(seq_Q, 3))