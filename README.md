MSc Thesis Project

Leiden University

Ruben Walen

Supervisors: Alexandre P. Goultiaev, Katherine J. Wolstencroft

Date: 30 June 2021

## Summary
Inter-RNA interactions location data in the influenza genome is imported from the datasets of Dadonaite et al. (2019) or Le Sage et al. (2020). These interactions are then extrapolated to various sets of influenza A strains using multiple sequence alignment (genome.jp _CLUSTALW_) (sequence_eda.py). Interaction structures are predicted using the Freiburg University _intaRNA_ software (process_interaction_sites.py). Various measurements are performed on the resulting extrapolated interaction data. The pipeline uses Python, but requires some external binaries (see below). All computations were performed on Windows 10.

## Input
Input data is mostly provided in ./input. Important required data: Dadonaite et al. or Le Sage et al.-derived interaction sites, reference strain sequences; extrapolation strain sequences (can be incomplete in terms of segments). Configuration files are stored in ./input, e.g. config_H1N1.txt. Comments in the config files describe their elements. These are used throughout the pipeline. Specific programs have their own parameters, often at the beginning of the script parts (after function declarations). External binaries are required (tested on Windows): _clustal-omega-1.2.2_ (pointer in config txt); _IntaRNA-3.2.0_ (pointer in config txt); _ViennaRNA_ full install (pointer in interaction_results_analysis.py) for version 2.4.X (installed November 2020). An _intaRNA_ config can be provided, see e.g. intarna_config.txt in ./vaccine_strains/.

## Packages
- requires Biopython (https://biopython.org/)
- requires numpy
- requires matplotlib

## Pipeline
./vaccine_strains:

executables:
1. sequence_eda.py: perform MSA on reference strain and extrapolation strains
2. process_alignment_sites.py: derive interaction sites for each strain based on location dataset, use _intaRNA_ for structure prediction
3. all of the following:
- interaction_global_analysis.py: derive global statistics for interactions
- interaction_results_analysis.py: exact analysis of single interactions: consensus profiles, covariation, cross-strain-interaction, structural imposition, etc.
- interaction_segments_analysis.py: segment-centric interaction analysis; interaction profiles + heatmaps for each segment
- mutation_analysis.py: reference-strain-oriented mutation analysis, including interaction site comparison to whole segments
- covariation_analysis.py: some covariation statistics, unused

standalone:
- circos.py: Circos plot recreation, cf. Dadonaite et al. (2019) (requires https://pypi.org/project/pycircos/) (place in ./vaccine_strains/misc/pyCircos-master)
- segments_consensus_plot.py: whole-segment consensus profiles

libraries:
- get_interaction_sites.py: interaction retrieval functions
- intarna_functions.py: core functions for calling intaRNA and retrieving and processing interactions
- sequence_functions.py: core functions for handling and processing RNA sequences
- vienna_rna_functions.py: functions for handling the _ViennaRNA_ package

./experimental:

K-mer analysis idea, mostly unused.

./results:

Various results of pipeline runs. Some of the Excel documents contain further analysis. The folders ending in 'WSN' contain analyses of individual interactions. The 'segment_distributions' folder contains segment consensus plots, segment interaction distributions, and segment interaction heatmaps.

## References
Dadonaite, B., Gilbertson, B., Knight, M.L. et al. The structure of the influenza A virus genome. _Nat Microbiol_ 4, 1781–1789 (2019). https://doi.org/10.1038/s41564-019-0513-7

Le Sage, V., Kanarek, J. P., Snyder, D. J., Cooper, V. S., Lakdawala, S. S., & Lee, N. (2020). Mapping of influenza virus RNA-RNA interactions reveals a flexible network. _Cell reports_, 31(13), 107823.