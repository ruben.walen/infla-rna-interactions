>A/WSN/1933
UCAUUCAAAUUGGAGUGCCAGAUCAUCAUGUGAGUCA------------------
                  ||||                                 
------------GGGUUCGUCUUUCACCGUCCGGGAGAAACAUAGUCUUACCUGG

>A/Moscow/10/1999
----------------------UCAUUCAAAUUGGAAUGCCAGAUCAUCAUGUGAGUUA
                              ||::|||||   ||:|             
GGGUUCGUCUUUCACCUUCCUGGAGAAACGUAGUCUUAC---CUGG-------------

>A/Bangkok/1/1979
----------------------UCAUUCAAAUUGGAAUGCCAGAUCAUCAUGUGAGUUA
                              ||::|||||   ||:|             
GGGUUCGUCUUUCACCUUCCUGGAGAAACGUAGUCUUAC---CUGG-------------

>A/Beijing/353/1989
----------------------UCAUUCAAAUUGGAAUGCCAGAUCAUCAUGUGAGUUA
                              ||::|||||   ||:|             
GGGUUCGUCUUUCACCUUCCUGGAGAAACGUAGUCUUAC---CUGG-------------

>A/Leningrad/360/1986
----------------------UCAUUCAAAUUGGAAUGCCAGAUCAUCAUGUGAGUUA
                              ||::|||||   ||:|             
GGGUUUGUCUUUCGCCUUCCUGGAGAAACGUAGUCUUAC---CUGG-------------

>A/Wellington/01/2004
----------------------UCAUUCAAAUUGGAAUGCCAGAUCAUUAUGUGAGUUA
                              ||::|||||   ||:|             
GGGUUCGUCUUUCACCUUCCUGGAGAAACGUAGUCUUAC---CUGG-------------

>A/Beijing/32/1992
----------------------UCAUUCAAAUUGGAAUGCCAGAUCAUCAUGUGAGUUA
                              ||::|||||   ||:|             
GGGUUCGUCUUUCACCUUCCUGGAGAAACGUAGUCUUAC---CUGG-------------

>A/Wisconsin/67/2005
-------------------UCAUUCAAAUUUGAAUGCCAGAUCAUUAUGUGAGUUA
                                       ||:|             
GGGUUCGUCUUUCACCUUCCUGGAGAAACGUAGUCUUACCUGG-------------

>A/Brisbane/10/2007
----------------------UCAUUCAAAUUGGAAUG--CCAAAUCAUUAUGUGAGUUA
                              ||::|||||  ||                  
GGGUUCGUCUUUCACCUUCCUGGAGAAACGUAGUCUUACCUGG------------------

>A/Shandong/9/1993
----------------------UCAUUCAAAUUGGAAUGCCAGAUCAUCAUGUGAGUUA
                              ||::|||||   ||:|             
GGGUUCGUCUUUCACCUUCCUGGAGAAACGUAGUCUUAC---CUGG-------------

>A/Philippines/2/1982
----------------------UCAUUUAAAUUGGAAUGCCAGAUCAUUAUGUGAGUUA
                              ||::|||||   ||:|             
GGGUUCGUCUUUCACCUUCCUGGAGAAACGUAGUCUUAC---CUGG-------------

>A/Victoria/210/2009
----------------------UCAUUCAAAUUGGAAUG--CCAAAUCAUUAUGUGAGUUA
                              ||::|||||  ||                  
GGGUUCGUCUUUUACCUUCCUGGAGAAACGUAGUCUUACCUGG------------------

>A/Texas/50/2012
----------------------UCAUUCAAAUUGGAAUG--CCAAAUCAUUAUGUGAGUUA
                              ||::|||||  ||                  
GGGUUCGUCUUUCACCUUCCUGGAGAAACGUAGUCUUACCUGG------------------

>A/Victoria/361/2011
----------------------UCAUUCAAAUUGGAAUG--CCAAAUCAUUAUGUGAGUUA
                              ||::|||||  ||                  
GGGUUCGUCUUUCACCUUCCUGGAGAAACGUAGUCUUACCUGG------------------

>A/Ohio/02/2012
----------------------UCAUUCAAAUUGGAAUG--CCAAAUCAUUAUGUGAGUUA
                              ||::|||||  ||                  
GGGUUCGUCUUUCACCUUCCUGGAGAAACGUAGUCUUACCUGG------------------

>A/Colorado/06/2017
----------------------UCAUUCAAAUUGGAAUG--CCAAAUCAUUAUGUGAGUUA
                              ||::|||||  ||                  
GGGUUCGUCUUUCACCUUCCUGGAGAAACGUAGUCUUACCUGG------------------

>A/Brisbane/1/2012
----------------------UCAUUCAAAUUGGAAUG--CCAAAUCAUUAUGUGAGUUA
                              ||::|||||  ||                  
GGGUUCGUCUUUCACCUUCCUGGAGAAACGUAGUCUUACCUGG------------------

>A/Brisbane/6/2012
----------------------UCAUUCAAAUUGGAAUG--CCAAAUCAUUAUGUGAGUUA
                              ||::|||||  ||                  
GGGUUCGUCUUUCACCUUCCUGGAGAAACGUAGUCUUACCUGG------------------

>A/South Australia/55/2014
----------------------UCAUUCAAAUUGGAAUG--CCAAAUCAUUAUGUGAGUUA
                              ||::|||||  ||                  
GGGUUCGUCUUUCACCUUCCUGGAGAAACGUAGUCUUACCUGG------------------

>A/Kansas/14/2017
----------------------UCAUUCAAAUUGGAAUG--CCAAAUCAUUAUGUGAGUUA
                              ||::|||||  ||                  
GGGUUCGUCUUUCACCUUCCUGGAGAAACGUAGUCUUACCUGG------------------

