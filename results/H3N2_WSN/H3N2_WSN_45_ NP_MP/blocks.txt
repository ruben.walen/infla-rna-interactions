45 NP MP

Strains found:
A/WSN/1933
A/Moscow/10/1999
A/Bangkok/1/1979
A/Beijing/353/1989
A/Leningrad/360/1986
A/Wellington/01/2004
A/Beijing/32/1992
A/Wisconsin/67/2005
A/Brisbane/10/2007
A/Shandong/9/1993
A/Philippines/2/1982
A/Victoria/210/2009
A/Texas/50/2012
A/Victoria/361/2011
A/Ohio/02/2012
A/Colorado/06/2017
A/Brisbane/1/2012
A/Brisbane/6/2012
A/South Australia/55/2014
A/Kansas/14/2017

forna copy-able:
>A/WSN/1933 47-7 2-31
ACUCCCUGCCGACUAAGUCUUGUCGAAUUGUUAUCUCUCUUAAGAGAGAUAGCAGGGCAGUCCGGGGGAGU
((((((..((((((..(.(.((.........(((((((((()))))))))).)).).))))).))))))))
>A/Moscow/10/1999 47-7 2-31
ACUUCCCGCCAACUAGGUCUUGUCGAACUGUUAUCUCUUUUAAGAGAGAUAGCAAGGUAGUCCGGGGGAGU
(((.(((.(..((((...((........((.(((((((.(()).))))))).)))).))))..)))).)))
>A/Bangkok/1/1979 45-7 3-31
UUCCCGCCAACUAGGUCUUGUCGAAUUGUUAUCUCUCUUAAGAGAGAUAGCAAGGCAGUCCGGGGGAG
.((((.((.....((...((.(...(((.(((((((((()))))))))).)))).))..)))))))).
>A/Beijing/353/1989 47-7 2-31
ACUUCCCGCCAACUAGGUCUUGUCGAACUGUUAUCUCUCUUAAGAGAGAUAGCAAGGUAGUCCGGGGGAGU
(((.(((.(..((((...((........((.(((((((((()))))))))).)))).))))..)))).)))
>A/Leningrad/360/1986 47-7 2-31
ACUUCCCGCCAACUAGGUCUUGUCGAACUGUUAUCUCUCUUAAGAGAGAUAGCAAGGUAGUCCGGGGGAGU
(((.(((.(..((((...((........((.(((((((((()))))))))).)))).))))..)))).)))
>A/Wellington/01/2004 47-7 2-31
ACUUCCCGCCAACUAGGUCUUGUCGAACUGUUAUCUCUUUUAAGAGAGAUAGCAAGGUAGUCCGGGGGAGU
(((.(((.(..((((...((........((.(((((((.(()).))))))).)))).))))..)))).)))
>A/Beijing/32/1992 47-7 2-31
ACUUCCCGCCAACUAGGUCUUGUCGAACUGUUAUCUCUCUUAAGAGAGAUAGCAAGGUAGUCCGGGGGAGU
(((.(((.(..((((...((........((.(((((((((()))))))))).)))).))))..)))).)))
>A/Wisconsin/67/2005 47-7 2-31
ACUUCCCGCCAACUAGGUCUUGUCGAACUGUUAUCUCUUUUAAGAGAGAUAGCAAGGUAGUCCGGGGGAGU
(((.(((.(..((((...((........((.(((((((.(()).))))))).)))).))))..)))).)))
>A/Brisbane/10/2007 47-7 2-31
ACUUCCCGCCAACUAGGUCUUGUCGAACUGUUAUCUCUUUUAAGAGAGAUAGCAAGGUAGUCCGGGGGAGU
(((.(((.(..((((...((........((.(((((((.(()).))))))).)))).))))..)))).)))
>A/Shandong/9/1993 47-7 2-31
ACUUCCCGCCAACUAGGUCUUGUCGAACUGUUAUCUCUCUUAAGAGAGAUAGCAAGGUAGUCCGGGGGAGU
(((.(((.(..((((...((........((.(((((((((()))))))))).)))).))))..)))).)))
>A/Philippines/2/1982 47-7 2-31
ACUUCCCGCUAACUAGGUCUUGUCGAAUUGUUAUCUCUCUUAAGAGAGAUAGCAAGGUAGCCCGGGGGAGU
(((.(((((((................(((.(((((((((()))))))))).)))..))))...))).)))
>A/Victoria/210/2009 47-1 2-36
ACUUCCCGCCAACUAGGUCUUGUCGAACUGUUAUCUCUUUUACCAUGCAUACAAGAGAGAUAGCAAGGUAGUCCGGGGGAGU
(((.(((.(..((((...((........((.(((((((.((...((()))..)).))))))).)))).))))..)))).)))
>A/Texas/50/2012 47-1 2-36
ACUUCCCGCCAACUAGGUCUUGUCGAACUGUUAUCUCUUUUACCAUGCAUACAAGAGAGAUAGCAAGGUAGUCCGGGGGAGU
(((.(((.(..((((...((........((.(((((((.((...((()))..)).))))))).)))).))))..)))).)))
>A/Victoria/361/2011 47-1 2-36
ACUUCCCGCCAACUAGGUCUUGUCGAACUGUUAUCUCUUUUACCAUGCAUACAAGAGAGAUAGCAAGGUAGUCCGGGGGAGU
(((.(((.(..((((...((........((.(((((((.((...((()))..)).))))))).)))).))))..)))).)))
>A/Ohio/02/2012 45-1 3-36
UUCCCGCCAACUAGGUUUUGUCGAACUGUUAUCUCUUUUACCAUGCAUACAAGAGAGAUAGCAAGGCAGUCCGGGGGAG
.((((.((.....((...((.(....((.(((((((.((...((()))..)).))))))).)).).))..)))))))).
>A/Colorado/06/2017 47-1 2-36
ACUUCCUGCCAACUAGGUUUUGUCGAACUGUUAUCUCUUUUACCAUGCAUACAAGAGAGAUAGCAAGGUAGUCCGGGGGAGU
(((.((..((.....((...(..(....((.(((((((.((...((()))..)).))))))).)).)..)..)))))).)))
>A/Brisbane/1/2012 47-1 2-36
ACUUCCCGCCAACUAGGUCUUGUCGAACUGUUAUCUCUUUUACCAUGCAUACAAGAGAGAUAGCAAGGUAGUCCGGGGGAGU
(((.(((.(..((((...((........((.(((((((.((...((()))..)).))))))).)))).))))..)))).)))
>A/Brisbane/6/2012 47-1 2-36
ACUUCCCGCCAACUAGGUCUUGUCGAACUGUUAUCUCUUUUACCAUGCAUACAAGAGAGAUAGCAAGGUAGUCCGGGGGAGU
(((.(((.(..((((...((........((.(((((((.((...((()))..)).))))))).)))).))))..)))).)))
>A/South Australia/55/2014 47-1 2-36
ACUUCCCGCCAACUAGGUCUUGUCGAACUGUUAUCUCUUUUACCAUGCAUACAAGAGAGAUAGCAAGGUAGUCCGGGGGAGU
(((.(((.(..((((...((........((.(((((((.((...((()))..)).))))))).)))).))))..)))).)))
>A/Kansas/14/2017 47-1 2-36
ACUUCCUGCCAACUAGGUUUUGUCGAACUGUUAUCUCUUUUACCAUGCAUACAAGAGAGAUAGCAAGGUAGUCCGGGGGAGU
(((.((..((.....((...(..(....((.(((((((.((...((()))..)).))))))).)).)..)..)))))).)))

Mutation block
A/WSN/1933 45 NP 1319-1365 MP 941-983
v = genomic start position; $ = genomic end position
               	$                                             v v                                         $
A/WSN/1933	ACTCCCTGCCGACTAAGTCTTGTCGAATTGTTATCTCTCTTACCACG&TTGAGGGGGCCTGACGGGACGATAGAGAGAACGTACGTTTCGA
A/WSN/1933	                                               &                                           
A/Moscow/10/1..	   T  C   A    G           C          T        &              T  A              A          
A/Bangkok/1/1..	   T  C   A    G                               &                 A              A          
A/Beijing/353..	   T  C   A    G           C                   &              T  A              A          
A/Leningrad/3..	   T  C   A    G           C                   &              T  A              A          
A/Wellington/..	   T  C   A    G           C          T        &              T  A              A          
A/Beijing/32/..	   T  C   A    G           C                   &              T  A              A          
A/Wisconsin/6..	   T  C   A    G           C          T        &              T  A              A          
A/Brisbane/10..	   T  C   A    G           C          T        &              T  A              A          
A/Shandong/9/..	   T  C   A    G           C                   &              T  A              A          
A/Philippines..	   T  C  TA    G                               &           C  T  A              A          
A/Victoria/21..	   T  C   A    G           C          T      T &              T  A              A          
A/Texas/50/2012	   T  C   A    G           C          T      T &              T  A              A          
A/Victoria/36..	   T  C   A    G           C          T      T &              T  A              A          
A/Ohio/02/2012	   T  C   A    G  T        C          T      T &                 A              A          
A/Colorado/06..	   T      A    G  T        C          T      T &              T  A              A          
A/Brisbane/1/..	   T  C   A    G           C          T      T &              T  A              A          
A/Brisbane/6/..	   T  C   A    G           C          T      T &              T  A              A          
A/South Austr..	   T  C   A    G           C          T      T &              T  A              A          
A/Kansas/14/2..	   T      A    G  T        C          T      T &              T  A              A          

Per-nucleotide structure block
A/WSN/1933	ACUCCCUGCCGACUAAGUCUUGUCGAAUUGUUAUCUCUCUUACCACG&UUGAGGGGGCCUGACGGGACGAUAGAGAGAACGUACGUUUCGA
A/WSN/1933	||||||..||||||..|:|:||........:||||||||||......&.||||||||.|||||:|:||:||||||||||............
A/WSN/1933	                                               &                                           	-20.69 kcal/mol
A/Moscow/10/1..	   :  |  ..   | .  |..      ||        :        &    :     .      |          :              	-18.07 kcal/mol
A/Bangkok/1/1..	..:   |   .... | ..   :|   |||                 & .:      | .:    |                         	-21.30 kcal/mol
A/Beijing/353..	   :  |  ..   | .  |..      ||                 &    :     .      |                         	-20.68 kcal/mol
A/Leningrad/3..	   :  |  ..   | .  |..      ||                 &    :     .      |                         	-20.68 kcal/mol
A/Wellington/..	   :  |  ..   | .  |..      ||        :        &    :     .      |          :              	-18.07 kcal/mol
A/Beijing/32/..	   :  |  ..   | .  |..      ||                 &    :     .      |                         	-20.68 kcal/mol
A/Wisconsin/6..	   :  |  ..   | .  |..      ||        :        &    :     .      |          :              	-18.07 kcal/mol
A/Brisbane/10..	   :  |  ..   | .  |..      ||        :        &    :     .      |          :              	-18.07 kcal/mol
A/Shandong/9/..	   :  |  ..   | .  |..      ||                 &    :     .      |                         	-20.68 kcal/mol
A/Philippines..	   :  ||   ...  ......     |||                 &    :   . .    ..|                         	-20.86 kcal/mol
A/Victoria/21..	   :  |  ..   | .  |..      ||        :     |||&    :     .      |          :    |||       	-19.24 kcal/mol
A/Texas/50/2012	   :  |  ..   | .  |..      ||        :     |||&    :     .      |          :    |||       	-19.24 kcal/mol
A/Victoria/36..	   :  |  ..   | .  |..      ||        :     |||&    :     .      |          :    |||       	-19.24 kcal/mol
A/Ohio/02/2012	..:   |   .... | ..   :|    ||        :     |||& .:      | .:    .          :    |||       	-17.90 kcal/mol
A/Colorado/06..	   :      .... | ..  ::|    ||        :     |||&    :    | .: :  .          :    |||       	-14.06 kcal/mol
A/Brisbane/1/..	   :  |  ..   | .  |..      ||        :     |||&    :     .      |          :    |||       	-19.24 kcal/mol
A/Brisbane/6/..	   :  |  ..   | .  |..      ||        :     |||&    :     .      |          :    |||       	-19.24 kcal/mol
A/South Austr..	   :  |  ..   | .  |..      ||        :     |||&    :     .      |          :    |||       	-19.24 kcal/mol
A/Kansas/14/2..	   :      .... | ..  ::|    ||        :     |||&    :    | .: :  .          :    |||       	-14.06 kcal/mol