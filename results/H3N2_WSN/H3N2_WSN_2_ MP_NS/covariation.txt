Covariation report for: 2 MP NS

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/Moscow/10/1999
AGUUCAC-UCGUCCGUCGCCUCCGGUACCUCUAACGAUCA
      | ||||||: |:|||| ||||||           
------GAAGCAGGU-GUGGAGACCAUGGUCAUU------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn1', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'bb0>bb0', 7: 'bb0s>nb1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bb1>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bn0>bb1s', 15: 'nb0>bb1s', 16: 'bb0>bb1', 17: 'bb1>bb1', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bn0>bn1', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'nn1', 30: 'bn0>bn0', 31: 'bn0>bn0', 32: 'bn0>bn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bb0>bb0', 1: 'bn0>bb0s', 2: 'nb1>bb0s', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'bb0>bb1', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb1s>nb0', 10: 'bb1>bb0', 11: 'bb1>bb1', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bn1>bn0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'nn0', 23: 'nn0', 24: 'bn0>bn0', 25: 'bn0>bn0', 26: 'bn0>bn0'}

Covariance layer complex:
  *         *   1 2           *         
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
        1      * *2   *                 
  *         *   1 2           *         
AGUUCAC-UCGUCCGUCGCCUCCGGUACCUCUAACGAUCA
      | ||||||: |:|||| ||||||           
------GAAGCAGGU-GUGGAGACCAUGGUCAUU------
        1       **2   *                 

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/Bangkok/1/1979
AGUUCAC-UCGUCCGUCGUCUCCGGUACCUCCAACGAUCA
      | ||||||: |::||||||||||           
------GAAGCAGGU-GUGGAGGCCAUGGUCAUU------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn1', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'bb0>bb0', 7: 'bb0s>nb1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bb1>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bn0>bb1s', 15: 'nb0>bb1s', 16: 'bb0>bb1', 17: 'bb0>bb1', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'nn1', 30: 'bn1>bn0', 31: 'bn0>bn0', 32: 'bn0>bn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bb0>bb0', 1: 'bn0>bb0s', 2: 'nb1>bb0s', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'bb0>bb1', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb1s>nb0', 10: 'bb1>bb0', 11: 'bb1>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'nn0', 23: 'nn0', 24: 'bn0>bn1', 25: 'bn0>bn0', 26: 'bn0>bn0'}

Covariance layer complex:
  *         *   1             **        
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
        1      * **                     
  *         *   1             **        
AGUUCAC-UCGUCCGUCGUCUCCGGUACCUCCAACGAUCA
      | ||||||: |::||||||||||           
------GAAGCAGGU-GUGGAGGCCAUGGUCAUU------
        1       ***                     

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/Beijing/353/1989
AGUUCAC-UCGUCCGUCGCCUCCGGUACCUCUAACGAUCA
      | ||||||: |:|||||||||||           
------GAAGCAGGU-GUGGAGGCCAUGGUCAUU------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn1', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'bb0>bb0', 7: 'bb0s>nb1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bb1>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bn0>bb1s', 15: 'nb0>bb1s', 16: 'bb0>bb1', 17: 'bb1>bb1', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'nn1', 30: 'bn0>bn0', 31: 'bn0>bn0', 32: 'bn0>bn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bb0>bb0', 1: 'bn0>bb0s', 2: 'nb1>bb0s', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'bb0>bb1', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb1s>nb0', 10: 'bb1>bb0', 11: 'bb1>bb1', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'nn0', 23: 'nn0', 24: 'bn0>bn0', 25: 'bn0>bn0', 26: 'bn0>bn0'}

Covariance layer complex:
  *         *   1 2           *         
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
        1      * *2                     
  *         *   1 2           *         
AGUUCAC-UCGUCCGUCGCCUCCGGUACCUCUAACGAUCA
      | ||||||: |:|||||||||||           
------GAAGCAGGU-GUGGAGGCCAUGGUCAUU------
        1       **2                     

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/Leningrad/360/1986
AGUUCAC-UCGUCCGUCGUCUCCGGUACCUCCAACGAUCA
      | ||||||: |::||||||||||           
------GAAGCAGGU-GUGGAGGCCAUGGUCAUU------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn1', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'bb0>bb0', 7: 'bb0s>nb1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bb1>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bn0>bb1s', 15: 'nb0>bb1s', 16: 'bb0>bb1', 17: 'bb0>bb1', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'nn1', 30: 'bn1>bn0', 31: 'bn0>bn0', 32: 'bn0>bn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bb0>bb0', 1: 'bn0>bb0s', 2: 'nb1>bb0s', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'bb0>bb1', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb1s>nb0', 10: 'bb1>bb0', 11: 'bb1>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'nn0', 23: 'nn0', 24: 'bn0>bn1', 25: 'bn0>bn0', 26: 'bn0>bn0'}

Covariance layer complex:
  *         *   1             **        
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
        1      * **                     
  *         *   1             **        
AGUUCAC-UCGUCCGUCGUCUCCGGUACCUCCAACGAUCA
      | ||||||: |::||||||||||           
------GAAGCAGGU-GUGGAGGCCAUGGUCAUU------
        1       ***                     

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/Wellington/01/2004
AGUUCAC-UCGUCCGUCGCCUCCGGUACCUUUAACGAUCA
      | ||||||: |:|||| ||||||           
------GAAGCAGGU-GUGGAGACCAUGGUCAUU------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn1', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'bb0>bb0', 7: 'bb0s>nb1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bb1>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bn0>bb1s', 15: 'nb0>bb1s', 16: 'bb0>bb1', 17: 'bb1>bb1', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bn0>bn1', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'nn1', 30: 'bn0>bn0', 31: 'bn0>bn0', 32: 'bn0>bn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bb0>bb0', 1: 'bn0>bb0s', 2: 'nb1>bb0s', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'bb0>bb1', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb1s>nb0', 10: 'bb1>bb0', 11: 'bb1>bb1', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bn1>bn0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'nn0', 23: 'nn0', 24: 'bn0>bn0', 25: 'bn0>bn0', 26: 'bn0>bn0'}

Covariance layer complex:
  *         *   1 2           *         
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
        1      * *2   *                 
  *         *   1 2           *         
AGUUCAC-UCGUCCGUCGCCUCCGGUACCUUUAACGAUCA
      | ||||||: |:|||| ||||||           
------GAAGCAGGU-GUGGAGACCAUGGUCAUU------
        1       **2   *                 

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/Beijing/32/1992
AGUUCAC-UCGUCCGUCGCCUCCGGUACCUUUAACGAUCA
      | ||||||: |:|||||||||||           
------GAAGCAGGU-GUGGAGGCCAUGGUCAUU------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn1', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'bb0>bb0', 7: 'bb0s>nb1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bb1>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bn0>bb1s', 15: 'nb0>bb1s', 16: 'bb0>bb1', 17: 'bb1>bb1', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'nn1', 30: 'bn0>bn0', 31: 'bn0>bn0', 32: 'bn0>bn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bb0>bb0', 1: 'bn0>bb0s', 2: 'nb1>bb0s', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'bb0>bb1', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb1s>nb0', 10: 'bb1>bb0', 11: 'bb1>bb1', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'nn0', 23: 'nn0', 24: 'bn0>bn0', 25: 'bn0>bn0', 26: 'bn0>bn0'}

Covariance layer complex:
  *         *   1 2           *         
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
        1      * *2                     
  *         *   1 2           *         
AGUUCAC-UCGUCCGUCGCCUCCGGUACCUUUAACGAUCA
      | ||||||: |:|||||||||||           
------GAAGCAGGU-GUGGAGGCCAUGGUCAUU------
        1       **2                     

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/Wisconsin/67/2005
AGUUCAC-UCGUCCGUCGCCUCCGGUACCUCUAACGAUCA
      | ||||||: |:|||| ||||||           
------GAAGCAGGU-GUGGAGACCAUGGUCAUU------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn1', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'bb0>bb0', 7: 'bb0s>nb1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bb1>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bn0>bb1s', 15: 'nb0>bb1s', 16: 'bb0>bb1', 17: 'bb1>bb1', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bn0>bn1', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'nn1', 30: 'bn0>bn0', 31: 'bn0>bn0', 32: 'bn0>bn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bb0>bb0', 1: 'bn0>bb0s', 2: 'nb1>bb0s', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'bb0>bb1', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb1s>nb0', 10: 'bb1>bb0', 11: 'bb1>bb1', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bn1>bn0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'nn0', 23: 'nn0', 24: 'bn0>bn0', 25: 'bn0>bn0', 26: 'bn0>bn0'}

Covariance layer complex:
  *         *   1 2           *         
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
        1      * *2   *                 
  *         *   1 2           *         
AGUUCAC-UCGUCCGUCGCCUCCGGUACCUCUAACGAUCA
      | ||||||: |:|||| ||||||           
------GAAGCAGGU-GUGGAGACCAUGGUCAUU------
        1       **2   *                 

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/Brisbane/10/2007
AGUUCAC-UCGUCCGUCGCCUCCGGUACCUCUAACGAUCA
      | ||||||: |:|||| ||||||           
------GAAGCAGGU-GUGGAGACCAUGGUCAUU------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn1', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'bb0>bb0', 7: 'bb0s>nb1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bb1>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bn0>bb1s', 15: 'nb0>bb1s', 16: 'bb0>bb1', 17: 'bb1>bb1', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bn0>bn1', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'nn1', 30: 'bn0>bn0', 31: 'bn0>bn0', 32: 'bn0>bn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bb0>bb0', 1: 'bn0>bb0s', 2: 'nb1>bb0s', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'bb0>bb1', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb1s>nb0', 10: 'bb1>bb0', 11: 'bb1>bb1', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bn1>bn0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'nn0', 23: 'nn0', 24: 'bn0>bn0', 25: 'bn0>bn0', 26: 'bn0>bn0'}

Covariance layer complex:
  *         *   1 2           *         
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
        1      * *2   *                 
  *         *   1 2           *         
AGUUCAC-UCGUCCGUCGCCUCCGGUACCUCUAACGAUCA
      | ||||||: |:|||| ||||||           
------GAAGCAGGU-GUGGAGACCAUGGUCAUU------
        1       **2   *                 

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/Shandong/9/1993
AGUUCAC-UCGUCCGUCGCCUCCGGUACCUUUAACGAUCA
      | ||||||: |:|||||||||||           
------GAAGCAGGU-GUGGAGGCCAUGGUCAUU------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn1', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'bb0>bb0', 7: 'bb0s>nb1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bb1>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bn0>bb1s', 15: 'nb0>bb1s', 16: 'bb0>bb1', 17: 'bb1>bb1', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'nn1', 30: 'bn0>bn0', 31: 'bn0>bn0', 32: 'bn0>bn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bb0>bb0', 1: 'bn0>bb0s', 2: 'nb1>bb0s', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'bb0>bb1', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb1s>nb0', 10: 'bb1>bb0', 11: 'bb1>bb1', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'nn0', 23: 'nn0', 24: 'bn0>bn0', 25: 'bn0>bn0', 26: 'bn0>bn0'}

Covariance layer complex:
  *         *   1 2           *         
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
        1      * *2                     
  *         *   1 2           *         
AGUUCAC-UCGUCCGUCGCCUCCGGUACCUUUAACGAUCA
      | ||||||: |:|||||||||||           
------GAAGCAGGU-GUGGAGGCCAUGGUCAUU------
        1       **2                     

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/Philippines/2/1982
AGUUCAC-UCGUCCGUCGUCUCCGGUACCUCCAACGAUCA
      | ||||||: |::||||||||||           
------GAAGCAGGU-GUGGAGGCCAUGGUCAUU------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn1', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'bb0>bb0', 7: 'bb0s>nb1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bb1>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bn0>bb1s', 15: 'nb0>bb1s', 16: 'bb0>bb1', 17: 'bb0>bb1', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'nn1', 30: 'bn1>bn0', 31: 'bn0>bn0', 32: 'bn0>bn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bb0>bb0', 1: 'bn0>bb0s', 2: 'nb1>bb0s', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'bb0>bb1', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb1s>nb0', 10: 'bb1>bb0', 11: 'bb1>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'nn0', 23: 'nn0', 24: 'bn0>bn1', 25: 'bn0>bn0', 26: 'bn0>bn0'}

Covariance layer complex:
  *         *   1             **        
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
        1      * **                     
  *         *   1             **        
AGUUCAC-UCGUCCGUCGUCUCCGGUACCUCCAACGAUCA
      | ||||||: |::||||||||||           
------GAAGCAGGU-GUGGAGGCCAUGGUCAUU------
        1       ***                     

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/Victoria/210/2009
AGUUCAC-UUGUCCGUCGCCUCCGGUACCUCUAACGAUCA
      | |:||||: |:|||| ||||||           
------GAAGCAGGU-GUGGAGACCAUGGUCAUU------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn1', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'bb0>bb0', 7: 'bb0s>nb1', 8: 'bb1>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bb1>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bn0>bb1s', 15: 'nb0>bb1s', 16: 'bb0>bb1', 17: 'bb1>bb1', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bn0>bn1', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'nn1', 30: 'bn0>bn0', 31: 'bn0>bn0', 32: 'bn0>bn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bb0>bb0', 1: 'bn0>bb0s', 2: 'nb1>bb0s', 3: 'bb0>bb1', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'bb0>bb1', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb1s>nb0', 10: 'bb1>bb0', 11: 'bb1>bb1', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bn1>bn0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'nn0', 23: 'nn0', 24: 'bn0>bn0', 25: 'bn0>bn0', 26: 'bn0>bn0'}

Covariance layer complex:
  *      *  *   1 2           *         
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
        1      * *2   *                 
  *      *  *   1 2           *         
AGUUCAC-UUGUCCGUCGCCUCCGGUACCUCUAACGAUCA
      | |:||||: |:|||| ||||||           
------GAAGCAGGU-GUGGAGACCAUGGUCAUU------
        1       **2   *                 

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/Texas/50/2012
AGUUCAC-UCGUCCGUCGCCUCCGGUACCUCUAACGAUCA
      | ||||||: |: ||| ||||||           
------GAAGCAGGU-GUUGAGACCAUGGUCAUU------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn1', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'bb0>bb0', 7: 'bb0s>nb1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bb1>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bn0>bb1s', 15: 'nb0>bb1s', 16: 'bb0>bb1', 17: 'bn1>bn1', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bn0>bn1', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'nn1', 30: 'bn0>bn0', 31: 'bn0>bn0', 32: 'bn0>bn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bb0>bb0', 1: 'bn0>bb0s', 2: 'nb1>bb0s', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'bb0>bb1', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb1s>nb0', 10: 'bb1>bb0', 11: 'bn1>bn1', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bn1>bn0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'nn0', 23: 'nn0', 24: 'bn0>bn0', 25: 'bn0>bn0', 26: 'bn0>bn0'}

Covariance layer complex:
  *         *   1 *           *         
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
        1      * **   *                 
  *         *   1 *           *         
AGUUCAC-UCGUCCGUCGCCUCCGGUACCUCUAACGAUCA
      | ||||||: |: ||| ||||||           
------GAAGCAGGU-GUUGAGACCAUGGUCAUU------
        1       ***   *                 

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/Victoria/361/2011
AGUUCAC-UCGUCCGUCGCCUCCGGUACCUCUAACGAUCA
      | ||||||: |: ||| ||||||           
------GAAGCAGGU-GUUGAGACCAUGGUCAUU------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn1', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'bb0>bb0', 7: 'bb0s>nb1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bb1>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bn0>bb1s', 15: 'nb0>bb1s', 16: 'bb0>bb1', 17: 'bn1>bn1', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bn0>bn1', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'nn1', 30: 'bn0>bn0', 31: 'bn0>bn0', 32: 'bn0>bn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bb0>bb0', 1: 'bn0>bb0s', 2: 'nb1>bb0s', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'bb0>bb1', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb1s>nb0', 10: 'bb1>bb0', 11: 'bn1>bn1', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bn1>bn0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'nn0', 23: 'nn0', 24: 'bn0>bn0', 25: 'bn0>bn0', 26: 'bn0>bn0'}

Covariance layer complex:
  *         *   1 *           *         
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
        1      * **   *                 
  *         *   1 *           *         
AGUUCAC-UCGUCCGUCGCCUCCGGUACCUCUAACGAUCA
      | ||||||: |: ||| ||||||           
------GAAGCAGGU-GUUGAGACCAUGGUCAUU------
        1       ***   *                 

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/Ohio/02/2012
AGUUCAC-UCGUCCGUCGCCUCCGGUACCUCUAACGAUCA
      | ||||||: |:|||| ||||||           
------GAAGCAGGU-GUGGAGACCAUGGUCAUU------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn1', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'bb0>bb0', 7: 'bb0s>nb1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bb1>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bn0>bb1s', 15: 'nb0>bb1s', 16: 'bb0>bb1', 17: 'bb1>bb1', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bn0>bn1', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'nn1', 30: 'bn0>bn0', 31: 'bn0>bn0', 32: 'bn0>bn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bb0>bb0', 1: 'bn0>bb0s', 2: 'nb1>bb0s', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'bb0>bb1', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb1s>nb0', 10: 'bb1>bb0', 11: 'bb1>bb1', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bn1>bn0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'nn0', 23: 'nn0', 24: 'bn0>bn0', 25: 'bn0>bn0', 26: 'bn0>bn0'}

Covariance layer complex:
  *         *   1 2           *         
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
        1      * *2   *                 
  *         *   1 2           *         
AGUUCAC-UCGUCCGUCGCCUCCGGUACCUCUAACGAUCA
      | ||||||: |:|||| ||||||           
------GAAGCAGGU-GUGGAGACCAUGGUCAUU------
        1       **2   *                 

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/Colorado/06/2017
AGUUCAC-UCGUCCGUCGCCUCCGGUACCUCUAACGAUCA
      | ||||||: |: ||| ||||||           
------GAAGCAGGU-GUUGAGACCAUGGUCAUU------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn1', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'bb0>bb0', 7: 'bb0s>nb1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bb1>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bn0>bb1s', 15: 'nb0>bb1s', 16: 'bb0>bb1', 17: 'bn1>bn1', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bn0>bn1', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'nn1', 30: 'bn0>bn0', 31: 'bn0>bn0', 32: 'bn0>bn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bb0>bb0', 1: 'bn0>bb0s', 2: 'nb1>bb0s', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'bb0>bb1', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb1s>nb0', 10: 'bb1>bb0', 11: 'bn1>bn1', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bn1>bn0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'nn0', 23: 'nn0', 24: 'bn0>bn0', 25: 'bn0>bn0', 26: 'bn0>bn0'}

Covariance layer complex:
  *         *   1 *           *         
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
        1      * **   *                 
  *         *   1 *           *         
AGUUCAC-UCGUCCGUCGCCUCCGGUACCUCUAACGAUCA
      | ||||||: |: ||| ||||||           
------GAAGCAGGU-GUUGAGACCAUGGUCAUU------
        1       ***   *                 

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/Brisbane/1/2012
AGUUCACUCGUCCGUCGCCUCCGGUACCUCUAACGAUCA
      ||||| |: |: ||| ||||||           
-----GGAGCAAGU-GUUGAGACCAUGGUCAUU------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn1', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'bb0s>bb1s', 7: 'bb0s>nb1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bn1>bn1', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bn0>bb1s', 15: 'nb0>bb1s', 16: 'bb0>bb1', 17: 'bn1>bn1', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bn0>bn1', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'nn1', 30: 'bn0>bn0', 31: 'bn0>bn0', 32: 'bn0>bn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bn0>bb0s', 1: 'bb1s>bb0s', 2: 'nb1>bb0s', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'bn1>bn1', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb1s>nb0', 10: 'bb1>bb0', 11: 'bn1>bn1', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bn1>bn0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'nn0', 23: 'nn0', 24: 'bn0>bn0', 25: 'bn0>bn0', 26: 'bn0>bn0'}

Covariance layer complex:
  *         *   1 *           *         
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
       *1   *  * **   *                 
  *        *   1 *           *         
AGUUCACUCGUCCGUCGCCUCCGGUACCUCUAACGAUCA
      ||||| |: |: ||| ||||||           
-----GGAGCAAGU-GUUGAGACCAUGGUCAUU------
      *1   *   ***   *                 

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/Brisbane/6/2012
AGUUCAC-UCGUCCGUCGCCUCCGGUACCUCUAACGAUCA
      | ||||||: |: ||| ||||||           
------GAAGCAGGU-GUUGAGACCAUGGUCAUU------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn1', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'bb0>bb0', 7: 'bb0s>nb1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bb1>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bn0>bb1s', 15: 'nb0>bb1s', 16: 'bb0>bb1', 17: 'bn1>bn1', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bn0>bn1', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'nn1', 30: 'bn0>bn0', 31: 'bn0>bn0', 32: 'bn0>bn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bb0>bb0', 1: 'bn0>bb0s', 2: 'nb1>bb0s', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'bb0>bb1', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb1s>nb0', 10: 'bb1>bb0', 11: 'bn1>bn1', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bn1>bn0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'nn0', 23: 'nn0', 24: 'bn0>bn0', 25: 'bn0>bn0', 26: 'bn0>bn0'}

Covariance layer complex:
  *         *   1 *           *         
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
        1      * **   *                 
  *         *   1 *           *         
AGUUCAC-UCGUCCGUCGCCUCCGGUACCUCUAACGAUCA
      | ||||||: |: ||| ||||||           
------GAAGCAGGU-GUUGAGACCAUGGUCAUU------
        1       ***   *                 

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/South Australia/55/2014
AGUUCAC-UCGUCCGUCGCCUCCGGUACCUCUAUCGAUCA
      | ||||||: |: ||| ||||||           
------GAAGCAGGU-GUUGAGACCAUGGUCAUU------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn1', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'bb0>bb0', 7: 'bb0s>nb1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bb1>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bn0>bb1s', 15: 'nb0>bb1s', 16: 'bb0>bb1', 17: 'bn1>bn1', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bn0>bn1', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'nn1', 30: 'bn0>bn0', 31: 'bn0>bn0', 32: 'bn1>bn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bb0>bb0', 1: 'bn0>bb0s', 2: 'nb1>bb0s', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'bb0>bb1', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb1s>nb0', 10: 'bb1>bb0', 11: 'bn1>bn1', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bn1>bn0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'nn0', 23: 'nn0', 24: 'bn0>bn0', 25: 'bn0>bn0', 26: 'bn0>bn1'}

Covariance layer complex:
  *         *   1 *           *  *      
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
        1      * **   *                 
  *         *   1 *           *  *      
AGUUCAC-UCGUCCGUCGCCUCCGGUACCUCUAUCGAUCA
      | ||||||: |: ||| ||||||           
------GAAGCAGGU-GUUGAGACCAUGGUCAUU------
        1       ***   *                 

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/Kansas/14/2017
AGUUCAC-UCGUCCGUCGCCUCCGGUACCUCUAUCGAUCA
      | ||||||: |: ||| ||||||           
------GAAGCAGGU-GUUGAGACCAUGGUCAUU------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn1', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'bb0>bb0', 7: 'bb0s>nb1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bb1>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bn0>bb1s', 15: 'nb0>bb1s', 16: 'bb0>bb1', 17: 'bn1>bn1', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bn0>bn1', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'nn1', 30: 'bn0>bn0', 31: 'bn0>bn0', 32: 'bn1>bn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bb0>bb0', 1: 'bn0>bb0s', 2: 'nb1>bb0s', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'bb0>bb1', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb1s>nb0', 10: 'bb1>bb0', 11: 'bn1>bn1', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bn1>bn0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'nn0', 23: 'nn0', 24: 'bn0>bn0', 25: 'bn0>bn0', 26: 'bn0>bn1'}

Covariance layer complex:
  *         *   1 *           *  *      
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
        1      * **   *                 
  *         *   1 *           *  *      
AGUUCAC-UCGUCCGUCGCCUCCGGUACCUCUAUCGAUCA
      | ||||||: |: ||| ||||||           
------GAAGCAGGU-GUUGAGACCAUGGUCAUU------
        1       ***   *                 
