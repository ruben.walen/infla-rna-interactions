304 PB2 PA

Strains found:
A/WSN/1933
A/Anhui/1/2005[H5N1]
A/Vietnam/UT36282/2010[H5N1]
A/Egypt/MOH-NRC-8434/2014[H5N1]
A/Changsha/1/2014[H5N6]
A/Yunnan/0127/2015[H5N6]
A/duck/Mongolia/54+47/01[H5N1]
A/duck/Hokkaido/Vac-3/2007[H5N1]
A/duck/Moscow/4182-C/2017[H5N3]
A/duck/Zhejiang/6DK19-MA/2013[H5N2]
A/Anas Platyrhynchos/Belgium/10811-6/2019[H5N6]
A/swine/Banten/UT2071/2005[H5N1]
A/swine/Zhejiang/SW57/2015[H5N1]
A/Crow/Aghakhan/2017[H5N8]
A/swan/Krasnodar/44/2017[H5N8]

forna copy-able:
>A/WSN/1933 33-11 4-32
GAACCCUGUAAACUAUGGCGAGUACUCCCGUUCGAAAGAGUUUACAGGUUUC
(((.(((((((((((.((.(((()))))).).......)))))))))).)))
>A/Anhui/1/2005[H5N1] 28-20 9-17
CUGUAAACUAGUUUACAG
((((((((()))))))))
>A/Vietnam/UT36282/2010[H5N1] 44-42 25-27
ACGCGU
((()))
>A/Egypt/MOH-NRC-8434/2014[H5N1] 24-17 6-13
AAACUGUGUACAGUUU
(((((((..)))))))
>A/Changsha/1/2014[H5N6] 28-12 9-29
CUGUAAACUAUGACGGGCCCGUUCGAAAGAGUUUACAG
(((((((((..(((((())))).).....)))))))))
>A/Yunnan/0127/2015[H5N6] 24-15 6-16
AAGCUGUGACGUCUACAGUUU
((.((((((())).)))).))
>A/duck/Mongolia/54+47/01[H5N1] 40-37 8-11
ACUGCAGU
(((())))
>A/duck/Hokkaido/Vac-3/2007[H5N1] 23-17 7-13
AACUGUGUACAGUU
((((((..))))))
>A/duck/Moscow/4182-C/2017[H5N3] 28-18 9-19
CUGUAAACUGUAGAGUUUACAG
(((((((((.().)))))))))
>A/duck/Zhejiang/6DK19-MA/2013[H5N2] 28-12 9-29
CUGUAAACUAUGUCGGGCCCGUUCGAAAGAGUUUACAG
(((((((((....(((())))........)))))))))
>A/Anas Platyrhynchos/Belgium/10811-6/2019[H5N6] 28-20 9-17
UUGUAAACUAGUUUACAG
.(((((((()))))))).
>A/swine/Banten/UT2071/2005[H5N1] 28-20 9-17
CUGUAAACUAGUUUACAG
((((((((()))))))))
>A/swine/Zhejiang/SW57/2015[H5N1] 38-33 22-27
UGCACGCGUUCG
.(.((())).).
>A/Crow/Aghakhan/2017[H5N8] 23-17 7-13
AACUGUGUACAGUU
((((((..))))))
>A/swan/Krasnodar/44/2017[H5N8] 23-17 7-13
AACUGUGUACAGUU
((((((..))))))

Mutation block
A/WSN/1933 304 PB2 464-508 PA 1453-1489
v = genomic start position; $ = genomic end position
               	$                                           v v                                   $
A/WSN/1933	TACTCCCTACACGAACCCTGTAAACTATGGCGAGTCTATTATTTT&CTTCTTTGGACATTTGAGAAAGCTTGCCCTCAATGTA
A/WSN/1933	                                             &                                     
A/Anhui/1/200..	   G A  G  T  C              A AG           C&       T                           C 
A/Vietnam/UT3..	   G A  G  TA C              A AG           C&       T                             
A/Egypt/MOH-N..	   G A     T  C     C     G  A AG           C&       T                           C 
A/Changsha/1/..	   G A     T  T              A  G           C&    C  T                           C 
A/Yunnan/0127..	   G A     T  C        G  G  A A   T        C& C     T     C                     C 
A/duck/Mongol..	   G A  G  TA C              A AG           C&       T                           C 
A/duck/Hokkai..	   G A        C           G  A AG           C&       T                           C 
A/duck/Moscow..	   G A        C           G  A A            C&       T                           C 
A/duck/Zhejia..	   G A     T  T              T  G           C&    C  T                           C 
A/Anas Platyr..	   G A        C  T           A AG  T        C&       T                           C 
A/swine/Bante..	   G A  G  T  C              A A            C&       T                           C 
A/swine/Zheji..	   G A  G     C              A AG  T        C&    C  T                           C 
A/Crow/Aghakh..	   G A        C           G  A AG  T        C&       T                           C 
A/swan/Krasno..	   G A        C           G  A AG  T        C&       T                           C 

Per-nucleotide structure block
A/WSN/1933	UACUCCCUACACGAACCCUGUAAACUAUGGCGAGUCUAUUAUUUU&CUUCUUUGGACAUUUGAGAAAGCUUGCCCUCAAUGUA
A/WSN/1933	............|||.|||||||||||:||.||||..........&...|||.||||||||||.......|:||||||.....
A/WSN/1933	                                             &                                     	-12.40 kcal/mol
A/Anhui/1/200..	            ... .         .... ....          &   ... .                ........     	-3.72 kcal/mol
A/Vietnam/UT3..	 |||        ... .............. ....          &   ... ..........        | .....     	-2.70 kcal/mol
A/Egypt/MOH-N..	            ... .....      |:. ....          &   .. |     :....       ........     	-4.50 kcal/mol
A/Changsha/1/..	            ... .         .   |   .          &   ... .             :|  |   ...     	-7.79 kcal/mol
A/Yunnan/0127..	            ... .....  :   |  |....          &   .. |:    .   .       ........     	-2.34 kcal/mol
A/duck/Mongol..	     ||||   ... .............. ....          &   ...     ......       ........     	-3.14 kcal/mol
A/duck/Hokkai..	            ... ......     |:. ....          &   ...|     :....       ........     	-5.29 kcal/mol
A/duck/Moscow..	            ... .         .|.. ....          &   ... .          |     ........     	-4.99 kcal/mol
A/duck/Zhejia..	            ... .         ....|   .          &   ... .                .|   ...     	-7.82 kcal/mol
A/Anas Platyr..	            ... .:        .... ....          &   ... .:               ........     	-2.63 kcal/mol
A/swine/Bante..	            ... .         .... ....          &   ... .                ........     	-5.58 kcal/mol
A/swine/Zheji..	       :| || .. .............. ....          &   ... ..........    :|  | .....     	-3.10 kcal/mol
A/Crow/Aghakh..	            ... ......     |:. ....          &   ...|     :....       ........     	-5.28 kcal/mol
A/swan/Krasno..	            ... ......     |:. ....          &   ...|     :....       ........     	-5.28 kcal/mol