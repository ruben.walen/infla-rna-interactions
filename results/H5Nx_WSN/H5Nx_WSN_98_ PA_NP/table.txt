98 PA 612-652 (40) NP 689-731 (42)
Reference strain: A/WSN/1933
Strain	FE/interaction found	Seg. 1 span	Seg. 2 span	Full bonds/half bonds/no bonds	Mutations in whole site seg.1 / seg. 2
A/WSN/1933	-9.48	2-10 (8)	17-9 (8)	8/1/0	0/0
A/Anhui/1/2005[H5N1]	-4.24	3-17 (14)	41-24 (17)	12/1/5	7/8
A/Vietnam/UT36282/2010[H5N1]	-12.82	3-35 (32)	41-7 (34)	22/0/14	8/6
A/Egypt/MOH-NRC-8434/2014[H5N1]	-6.33	3-30 (27)	41-11 (30)	19/0/12	10/8
A/Changsha/1/2014[H5N6]	-7.37	11-35 (24)	23-7 (16)	14/2/9	6/7
A/Yunnan/0127/2015[H5N6]	-1.41	6-8 (2)	12-10 (2)	3/0/0	8/7
A/duck/Mongolia/54+47/01[H5N1]	-5.15	3-17 (14)	41-24 (17)	13/0/5	9/7
A/duck/Hokkaido/Vac-3/2007[H5N1]	-4.98	10-17 (7)	31-24 (7)	8/0/0	8/6
A/duck/Moscow/4182-C/2017[H5N3]	-7.54	3-35 (32)	41-7 (34)	20/2/14	7/7
A/duck/Zhejiang/6DK19-MA/2013[H5N2]	-2.48	3-17 (14)	41-24 (17)	12/1/5	8/6
A/Anas Platyrhynchos/Belgium/10811-6/2019[H5N6]	-8.81	10-35 (25)	31-7 (24)	17/1/8	4/7
A/swine/Banten/UT2071/2005[H5N1]	-4.36	5-17 (12)	22-8 (14)	7/4/4	9/7
A/swine/Zhejiang/SW57/2015[H5N1]	-5.62	3-35 (32)	41-7 (34)	20/1/15	8/7
A/Crow/Aghakhan/2017[H5N8]	-9.05	11-35 (24)	23-7 (16)	14/2/9	8/7
A/swan/Krasnodar/44/2017[H5N8]	-6.04	3-35 (32)	41-7 (34)	22/1/13	5/7