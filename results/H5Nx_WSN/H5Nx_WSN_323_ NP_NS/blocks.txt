323 NP NS

Strains found:
A/WSN/1933
A/Anhui/1/2005[H5N1]
A/Vietnam/UT36282/2010[H5N1]
A/Egypt/MOH-NRC-8434/2014[H5N1]
A/Changsha/1/2014[H5N6]
A/Yunnan/0127/2015[H5N6]
A/duck/Mongolia/54+47/01[H5N1]
A/duck/Hokkaido/Vac-3/2007[H5N1]
A/duck/Moscow/4182-C/2017[H5N3]
A/duck/Zhejiang/6DK19-MA/2013[H5N2]
A/Anas Platyrhynchos/Belgium/10811-6/2019[H5N6]
A/swine/Banten/UT2071/2005[H5N1]
A/swine/Zhejiang/SW57/2015[H5N1]
A/Crow/Aghakhan/2017[H5N8]
A/swan/Krasnodar/44/2017[H5N8]

forna copy-able:
>A/WSN/1933 19-22 37-34
CAGAUCUG
(((())))
>A/Anhui/1/2005[H5N1] 29-35 29-23
GGUGGGUACCUGCC
((..((()))..))
>A/Vietnam/UT36282/2010[H5N1] 29-35 29-23
GGUGGGUACCCGCC
((.(((()))).))
>A/Egypt/MOH-NRC-8434/2014[H5N1] 26-37 42-28
UCAGGUGGGUGAUCACUUUCUGCUUGG
.((.(..((((())))...)..).)).
>A/Changsha/1/2014[H5N6] 29-35 29-23
GGUGGGUACCCACC
((((((()))))))
>A/Yunnan/0127/2015[H5N6] 1-14 42-28
UCAUUCAGGUUGGAUCACUUUCUGUUUGG
.((..(((....(()).....)))..)).
>A/duck/Mongolia/54+47/01[H5N1] 26-36 42-29
UCAGGUGAGUGCACUUUCUGCUUGG
.((.(..(((()))).....).)).
>A/duck/Hokkaido/Vac-3/2007[H5N1] 8-30 29-8
GGUUGGAAUGCCAGAUCAUCAGAUCUGAUGCAAAGGGAGCCUGCC
((..((..(.((....((((((()))))))....)).).))..))
>A/duck/Moscow/4182-C/2017[H5N3] 9-24 13-1
GUUGGAAUGCCAGAUCGGUCCAUUCUGAU
.(..(((((...((.().)))))))..).
>A/duck/Zhejiang/6DK19-MA/2013[H5N2] 29-35 29-23
GGUGGGUACCCACC
((((((()))))))
>A/Anas Platyrhynchos/Belgium/10811-6/2019[H5N6] 9-24 13-1
AUUGGAAUGCCAGAUCGGUCCAUUCUGAU
((..(((((...((.().)))))))..))
>A/swine/Banten/UT2071/2005[H5N1] 29-35 29-23
GGUGGGUACCUGCC
((..((()))..))
>A/swine/Zhejiang/SW57/2015[H5N1] 29-35 29-23
GGUGGGUACCCACC
((((((()))))))
>A/Crow/Aghakhan/2017[H5N8] 18-35 42-27
CCAGAUCAUUAGGUGAGUGCCACUUUCUGUUUGG
(((.(.((..(.(((.(..)))).)..))).)))
>A/swan/Krasnodar/44/2017[H5N8] 18-36 42-29
CCAGAUCAUCAGGUGAGUGCACUUUCUGUUUGG
(((.(....(((...(((())))..)))).)))

Mutation block
A/WSN/1933 323 NP 1087-1123 NS 504-546
v = genomic start position; $ = genomic end position
               	v                                   $ $                                         v
A/WSN/1933	TCATTCAAATTGGAGTGCCAGATCATCATGTGAGTCA&GGGTTCGTCTTTCACCGTCCGGGAGAAACATAGTCTTACCTGG
A/WSN/1933	                                     &                                           
A/Anhui/1/200..	     T G      A     T       G   G  A &                    AA G     G    T        
A/Vietnam/UT3..	     T G      A     T       G   G  A &                 C  AA G     G    T        
A/Egypt/MOH-N..	     T G      A     T       G   G  G &               T    AA G     G    T        
A/Changsha/1/..	       G            T       G   G  A &                AC  AA G     G    T        
A/Yunnan/0127..	       GG     A     T       GA  G  A &     T         T    CA G     G  A          
A/duck/Mongol..	     T G      A             G      G &                    AA G     G             
A/duck/Hokkai..	       GG     A             GA     G &                     A G     G             
A/duck/Moscow..	     T GG     A             G      G &     T              AA G     G             
A/duck/Zhejia..	       GG           T       G   G  A &                AC  AA G     G    T        
A/Anas Platyr..	     T G      A             G      G &     T              AA G     G             
A/swine/Bante..	     T G      A     T       G   G  A &                    AA G     G    T        
A/swine/Zheji..	       G      A     T       G   G  A &            T   AC  AA G   T G    T        
A/Crow/Aghakh..	     T G      A           T G      G &     T              AA G     G    T        
A/swan/Krasno..	     T G      A             G      G &     T              AA T     G    T        

Per-nucleotide structure block
A/WSN/1933	UCAUUCAAAUUGGAGUGCCAGAUCAUCAUGUGAGUCA&GGGUUCGUCUUUCACCGUCCGGGAGAAACAUAGUCUUACCUGG
A/WSN/1933	..................||||...............&......||||.................................
A/WSN/1933	                                     &                                           	-1.81 kcal/mol
A/Anhui/1/200..	                  ....      ||::|||  &      ....    ||::|||                      	-4.83 kcal/mol
A/Vietnam/UT3..	                  ....      ||:||||  &      ....    ||:||||                      	-7.77 kcal/mol
A/Egypt/MOH-N..	                  ....   :||:|::|||||& :||:|:: .  ||||                           	-6.48 kcal/mol
A/Changsha/1/..	                  ....      |||||||  &      ....    |||||||                      	-7.89 kcal/mol
A/Yunnan/0127..	:||  |||:   ||    ....               & :||     :    ||                           	-4.37 kcal/mol
A/duck/Mongol..	                  ....   :||:|::|||| & :||:|:...:||||                            	-3.69 kcal/mol
A/duck/Hokkai..	       ||: ||  | | ... |||||||       &      ....    ||: || | ||    |||||||       	-5.77 kcal/mol
A/duck/Moscow..	        :|::||||| ..  :|             &      ....                    :|::|||||||:|	-4.86 kcal/mol
A/duck/Zhejia..	                  ....      |||||||  &      ....    |||||||                      	-7.78 kcal/mol
A/Anas Platyr..	        ||::||||| ..  :|             &      ....                    ||::|||||||:|	-4.36 kcal/mol
A/swine/Bante..	                  ....      ||::|||  &      ....    ||::|||                      	-4.83 kcal/mol
A/swine/Zheji..	                  ....      |||||||  &      ....    |||||||                      	-8.09 kcal/mol
A/Crow/Aghakh..	                 |  :  ||  |:||| |:  & |||:|  ..|:||||:                          	-3.60 kcal/mol
A/swan/Krasno..	                 |  :     |||: :|||| & |||:|   ::||||                            	-3.83 kcal/mol