>A/WSN/1933
UCAUUCAAAUUGGAGUGCCAGAUCAUCAUGUGAGUCA------------------
                  ||||                                 
------------GGGUUCGUCUUUCACCGUCCGGGAGAAACAUAGUCUUACCUGG

>A/Anhui/1/2005[H5N1]
UCAUUUAGAUUGGAAUGCCAUAUCAUCAGGUGGGUAA--------------------
                            ||::|||                      
--------------GGGUUCGUCUUUCACCGUCCAAGGGAAACGUAGUUUUACCUGG

>A/Vietnam/UT36282/2010[H5N1]
UCAUUUAGAUUGGAAUGCCAUAUCAUCAGGUGGGUAA--------------------
                            ||:||||                      
--------------GGGUUCGUCUUUCACCGCCCAAGGGAAACGUAGUUUUACCUGG

>A/Egypt/MOH-NRC-8434/2014[H5N1]
UCAUUUAGAUUGGAAUGCCAUAUCAUCAGGUGG---GUGA---------------------------
                         :||:|::|   ||||                           
------------------------GGGUUCGUCUUUCACUGUCCAAGGGAAACGUAGUUUUACCUGG

>A/Changsha/1/2014[H5N6]
UCAUUCAGAUUGGAGUGCCAUAUCAUCAGGUGGGUAA--------------------
                            |||||||                      
--------------GGGUUCGUCUUUCACCACCCAAGGGAAACGUAGUUUUACCUGG

>A/Yunnan/0127/2015[H5N6]
-UCAUUCAGGUUG-GAAUGCCAUAUCAUCAGAUGGGUAA----
 :||  |||:    ||                           
GGGUUUGUCUUUCACUGUCCCAGGGAAACGUAAUCUUACCUGG

>A/duck/Mongolia/54+47/01[H5N1]
UCAUUUAGAUUGGAAUGCCAGAUCAUCAGGU---GAGUGA---------------------------
                         :||:|:   :||||                            
------------------------GGGUUCGUCUUUCACCGUCCAAGGGAAACGUAGUCUUACCUGG

>A/duck/Hokkaido/Vac-3/2007[H5N1]
-------UCAUUCAGGUUGGAAUGCCAGAUCAUCAGAUGAGUGA
              ||: ||  | ||    |||||||       
GGGUUCGUCUUUCACCGUCCG-AGGGAAACGUAGUCUUACCUGG

>A/duck/Moscow/4182-C/2017[H5N3]
----------------------UCAUUUAGGUUGGAAUGCCAGAUCAUCAGGUGAGUGA
                              :|::|||||   ||:|             
GGGUUUGUCUUUCACCGUCCAAGGGAAACGUAGUCUUAC---CUGG-------------

>A/duck/Zhejiang/6DK19-MA/2013[H5N2]
UCAUUCAGGUUGGAGUGCCAUAUCAUCAGGUGGGUAA--------------------
                            |||||||                      
--------------GGGUUCGUCUUUCACCACCCAAGGGAAACGUAGUUUUACCUGG

>A/Anas Platyrhynchos/Belgium/10811-6/2019[H5N6]
----------------------UCAUUUAGAUUGGAAUGCCAGAUCAUCAGGUGAGUGA
                              ||::|||||   ||:|             
GGGUUUGUCUUUCACCGUCCAAGGGAAACGUAGUCUUAC---CUGG-------------

>A/swine/Banten/UT2071/2005[H5N1]
UCAUUUAGAUUGGAAUGCCAUAUCAUCAGGUGGGUAA--------------------
                            ||::|||                      
--------------GGGUUCGUCUUUCACCGUCCAAGGGAAACGUAGUUUUACCUGG

>A/swine/Zhejiang/SW57/2015[H5N1]
UCAUUCAGAUUGGAAUGCCAUAUCAUCAGGUGGGUAA--------------------
                            |||||||                      
--------------GGGUUCGUCUUUUACCACCCAAGGGAAUCGUAGUUUUACCUGG

>A/Crow/Aghakhan/2017[H5N8]
UCAUUUAGAUUGGAAUGCCAGAUCAUUAGGUGAGUGA------------------------
                 |||:| ||  |:||| |:                          
----------------GGGUUU-GUCUUUCAC-CGUCCAAGGGAAACGUAGUUUUACCUGG

>A/swan/Krasnodar/44/2017[H5N8]
UCAUUUAGAUUGGAAUGCCAGAUCAUCAGGUGAGUGA---------------------------
                 |||:|    |||: :||||                            
----------------GGGUUU----GUCU-UUCACCGUCCAAGUGAAACGUAGUUUUACCUGG

