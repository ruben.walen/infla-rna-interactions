103 PA 112-164 (52) MP 932-964 (32)
Reference strain: A/WSN/1933
Strain	FE/interaction found	Seg. 1 span	Seg. 2 span	Full bonds/half bonds/no bonds	Mutations in whole site seg.1 / seg. 2
A/WSN/1933	-15.49	30-16 (14)	10-23 (13)	13/1/1	0/0
A/Anhui/1/2005[H5N1]	-16.91	35-8 (27)	6-32 (26)	20/3/6	0/1
A/Vietnam/UT36282/2010[H5N1]	-16.91	35-8 (27)	6-32 (26)	20/3/6	0/1
A/Egypt/MOH-NRC-8434/2014[H5N1]	-15.72	45-6 (39)	6-30 (24)	22/1/17	1/3
A/Changsha/1/2014[H5N6]	-19.97	45-8 (37)	6-32 (26)	23/2/14	1/2
A/Yunnan/0127/2015[H5N6]	-17.6	35-8 (27)	6-32 (26)	20/4/5	3/2
A/duck/Mongolia/54+47/01[H5N1]	-19.37	45-8 (37)	6-32 (26)	22/2/15	3/0
A/duck/Hokkaido/Vac-3/2007[H5N1]	-18.1	35-8 (27)	6-32 (26)	21/2/6	2/1
A/duck/Moscow/4182-C/2017[H5N3]	-14.49	34-16 (18)	7-23 (16)	15/1/3	2/1
A/duck/Zhejiang/6DK19-MA/2013[H5N2]	-19.03	35-8 (27)	6-32 (26)	21/3/5	0/2
A/Anas Platyrhynchos/Belgium/10811-6/2019[H5N6]	-12.44	35-8 (27)	6-32 (26)	19/3/7	1/1
A/swine/Banten/UT2071/2005[H5N1]	-17.26	35-8 (27)	6-32 (26)	20/4/5	2/2
A/swine/Zhejiang/SW57/2015[H5N1]	-19.03	35-8 (27)	6-32 (26)	21/3/5	0/2
A/Crow/Aghakhan/2017[H5N8]	-15.49	30-16 (14)	10-23 (13)	13/1/1	0/0
A/swan/Krasnodar/44/2017[H5N8]	-17.35	35-8 (27)	6-32 (26)	20/3/6	1/0