Covariation report for: 53 MP NS

Reference strain:
A/WSN/1933
CUUCCGUAGA-------AGGCCCUCUUU---UCAAACCGUAUU
     |:|||       |||||||||||   |||         
----UCGUCUUUCACCGUCCGGGAGAAACAUAGUCUU------
Comparison strain:
A/Anhui/1/2005[H5N1]
CUUCCGUAGAAGG---------CCCUCUUU-UCAAACCGUAUU
     |:||||:|         ||||     |||||       
----UCGUCUUUCACCGUCCAAGGGAAACGUAGUUUU------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'bb0>bb0', 6: 'bb0>bb0', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0s>nb0', 11: 'bb0s>nb0', 12: 'bb0s>nb0', 13: 'bb0s>bb0s', 14: 'bb0s>bb1s', 15: 'bb0s>bb0s', 16: 'bb0s>bb0s', 17: 'bn0>bb0s', 18: 'bn0>bb0s', 19: 'bn0>bn0', 20: 'bn0>bn0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nb0>nb1', 25: 'nb0>nb0', 26: 'nn0', 27: 'nn0', 28: 'nn0', 29: 'nn0', 30: 'nn0', 31: 'nn0', 32: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'bb0>bb0', 2: 'bb0>bb0', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'nb0>bb0s', 7: 'nb0>bb0s', 8: 'nb0>bb0s', 9: 'nn0', 10: 'nn0', 11: 'nn0', 12: 'nn0', 13: 'bn0>bb0s', 14: 'bn0>bb0s', 15: 'bn0>bb0s', 16: 'bn1>bb0s', 17: 'bn1>bb0s', 18: 'bb0s>bb0s', 19: 'bb1s>bb0s', 20: 'bb0s>bb0s', 21: 'bb0s>bb0s', 22: 'bn0>bn0', 23: 'bn0>bn0', 24: 'nn0', 25: 'nn1', 26: 'nn0', 27: 'bb0>bb0', 28: 'bb0>bb0', 29: 'bb0>bb0', 30: 'nb1>nb0', 31: 'nb0>nb0', 32: 'nn0'}

Covariance layer complex:
                                  11       
CUUCCGUAGA-------AGGCCCUCUUU---UCAAACCGUAUU
     |:|||       |||||||||||   |||         
----UCGUCUUUCACCGUCCGGGAGAAACAUAGUCUU------
          111       ** *     *    11       
                                  11       
CUUCCGUAGAAGG---------CCCUCUUU-UCAAACCGUAUU
     |:||||:|         ||||     |||||       
----UCGUCUUUCACCGUCCAAGGGAAACGUAGUUUU------
          111       ** *     *    11       

Reference strain:
A/WSN/1933
CUUCCGUAGA-------AGGCCCUCUUU---UCAAACCGUAUU
     |:|||       |||||||||||   |||         
----UCGUCUUUCACCGUCCGGGAGAAACAUAGUCUU------
Comparison strain:
A/Vietnam/UT36282/2010[H5N1]
CUUCCGUAGAAGG---------CCCUCUUU-UCAAACCGUAUU
     |:||||:|         ||||     |||||       
----UCGUCUUUCACCGCCCAAGGGAAACGUAGUUUU------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'bb0>bb0', 6: 'bb0>bb0', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0s>nb0', 11: 'bb0s>nb0', 12: 'bb0s>nb0', 13: 'bb0s>bb0s', 14: 'bb0s>bb1s', 15: 'bb0s>bb0s', 16: 'bb0s>bb0s', 17: 'bn0>bb0s', 18: 'bn0>bb0s', 19: 'bn0>bn0', 20: 'bn0>bn0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nb0>nb1', 25: 'nb0>nb0', 26: 'nn0', 27: 'nn0', 28: 'nn0', 29: 'nn0', 30: 'nn0', 31: 'nn0', 32: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'bb0>bb0', 2: 'bb0>bb0', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'nb0>bb0s', 7: 'nb0>bb0s', 8: 'nb0>bb0s', 9: 'nn0', 10: 'nn0', 11: 'nn0', 12: 'nn0', 13: 'bn1>bb0s', 14: 'bn0>bb0s', 15: 'bn0>bb0s', 16: 'bn1>bb0s', 17: 'bn1>bb0s', 18: 'bb0s>bb0s', 19: 'bb1s>bb0s', 20: 'bb0s>bb0s', 21: 'bb0s>bb0s', 22: 'bn0>bn0', 23: 'bn0>bn0', 24: 'nn0', 25: 'nn1', 26: 'nn0', 27: 'bb0>bb0', 28: 'bb0>bb0', 29: 'bb0>bb0', 30: 'nb1>nb0', 31: 'nb0>nb0', 32: 'nn0'}

Covariance layer complex:
                                  11       
CUUCCGUAGA-------AGGCCCUCUUU---UCAAACCGUAUU
     |:|||       |||||||||||   |||         
----UCGUCUUUCACCGUCCGGGAGAAACAUAGUCUU------
          111    *  ** *     *    11       
                                  11       
CUUCCGUAGAAGG---------CCCUCUUU-UCAAACCGUAUU
     |:||||:|         ||||     |||||       
----UCGUCUUUCACCGCCCAAGGGAAACGUAGUUUU------
          111    *  ** *     *    11       

Reference strain:
A/WSN/1933
CUUCCGUAGA-------AGGCCCUCUUU---UCAAACCGUAUU
     |:|||       |||||||||||   |||         
----UCGUCUUUCACCGUCCGGGAGAAACAUAGUCUU------
Comparison strain:
A/Egypt/MOH-NRC-8434/2014[H5N1]
CUUCCGUAGAAGG---------CCCUCUUU-UCAAACCGUAUU
     |:||||:|         ||||     |||||       
----UCGUCUUUCACUGUCCAAGGGAAACGUAGUUUU------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'bb0>bb0', 6: 'bb0>bb0', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0s>nb0', 11: 'bb0s>nb0', 12: 'bb0s>nb0', 13: 'bb0s>bb0s', 14: 'bb0s>bb1s', 15: 'bb0s>bb0s', 16: 'bb0s>bb0s', 17: 'bn0>bb0s', 18: 'bn0>bb0s', 19: 'bn0>bn0', 20: 'bn0>bn0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nb0>nb1', 25: 'nb0>nb0', 26: 'nn0', 27: 'nn0', 28: 'nn0', 29: 'nn0', 30: 'nn0', 31: 'nn0', 32: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'bb0>bb0', 2: 'bb0>bb0', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'nb0>bb0s', 7: 'nb0>bb0s', 8: 'nb0>bb0s', 9: 'nn0', 10: 'nn0', 11: 'nn1', 12: 'nn0', 13: 'bn0>bb0s', 14: 'bn0>bb0s', 15: 'bn0>bb0s', 16: 'bn1>bb0s', 17: 'bn1>bb0s', 18: 'bb0s>bb0s', 19: 'bb1s>bb0s', 20: 'bb0s>bb0s', 21: 'bb0s>bb0s', 22: 'bn0>bn0', 23: 'bn0>bn0', 24: 'nn0', 25: 'nn1', 26: 'nn0', 27: 'bb0>bb0', 28: 'bb0>bb0', 29: 'bb0>bb0', 30: 'nb1>nb0', 31: 'nb0>nb0', 32: 'nn0'}

Covariance layer complex:
                                  11       
CUUCCGUAGA-------AGGCCCUCUUU---UCAAACCGUAUU
     |:|||       |||||||||||   |||         
----UCGUCUUUCACCGUCCGGGAGAAACAUAGUCUU------
          111  *    ** *     *    11       
                                  11       
CUUCCGUAGAAGG---------CCCUCUUU-UCAAACCGUAUU
     |:||||:|         ||||     |||||       
----UCGUCUUUCACUGUCCAAGGGAAACGUAGUUUU------
          111  *    ** *     *    11       

Reference strain:
A/WSN/1933
CUUCCGUAGA-------AGGCCCUCUUU---UCAAACCGUAUU
     |:|||       |||||||||||   |||         
----UCGUCUUUCACCGUCCGGGAGAAACAUAGUCUU------
Comparison strain:
A/Changsha/1/2014[H5N6]
CUUCCGUAGAAGG---------CCCUCUUU-UCAAACCGUAUU
     |:||||:|         ||||     |||||       
----UCGUCUUUCACCACCCAAGGGAAACGUAGUUUU------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'bb0>bb0', 6: 'bb0>bb0', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0s>nb0', 11: 'bb0s>nb0', 12: 'bb0s>nb0', 13: 'bb0s>bb0s', 14: 'bb0s>bb1s', 15: 'bb0s>bb0s', 16: 'bb0s>bb0s', 17: 'bn0>bb0s', 18: 'bn0>bb0s', 19: 'bn0>bn0', 20: 'bn0>bn0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nb0>nb1', 25: 'nb0>nb0', 26: 'nn0', 27: 'nn0', 28: 'nn0', 29: 'nn0', 30: 'nn0', 31: 'nn0', 32: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'bb0>bb0', 2: 'bb0>bb0', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'nb0>bb0s', 7: 'nb0>bb0s', 8: 'nb0>bb0s', 9: 'nn0', 10: 'nn0', 11: 'nn0', 12: 'nn1', 13: 'bn1>bb0s', 14: 'bn0>bb0s', 15: 'bn0>bb0s', 16: 'bn1>bb0s', 17: 'bn1>bb0s', 18: 'bb0s>bb0s', 19: 'bb1s>bb0s', 20: 'bb0s>bb0s', 21: 'bb0s>bb0s', 22: 'bn0>bn0', 23: 'bn0>bn0', 24: 'nn0', 25: 'nn1', 26: 'nn0', 27: 'bb0>bb0', 28: 'bb0>bb0', 29: 'bb0>bb0', 30: 'nb1>nb0', 31: 'nb0>nb0', 32: 'nn0'}

Covariance layer complex:
                                  11       
CUUCCGUAGA-------AGGCCCUCUUU---UCAAACCGUAUU
     |:|||       |||||||||||   |||         
----UCGUCUUUCACCGUCCGGGAGAAACAUAGUCUU------
          111   **  ** *     *    11       
                                  11       
CUUCCGUAGAAGG---------CCCUCUUU-UCAAACCGUAUU
     |:||||:|         ||||     |||||       
----UCGUCUUUCACCACCCAAGGGAAACGUAGUUUU------
          111   **  ** *     *    11       

Reference strain:
A/WSN/1933
CUUCCGUAGA-------AGGCCCUCUUU---UCAAACCGUAUU
     |:|||       |||||||||||   |||         
----UCGUCUUUCACCGUCCGGGAGAAACAUAGUCUU------
Comparison strain:
A/Yunnan/0127/2015[H5N6]
---CUUCCGUAGA-AGGCCCUCUUUUCAAACCGUAUU
        || || |||  |:|||| ||         
UUGUCUUUCA-CUGUCCCAGGGAAACGUAAUCUU---

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'bb0s>nb0', 6: 'bb0s>nb0', 7: 'bn0>bn0', 8: 'bb0s>nb0', 9: 'bb0s>nb1', 10: 'bb0>bb0', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bn0>bn1', 14: 'bn0>bn1', 15: 'bb0>bb0', 16: 'bb0>bb1', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bn0>bn0', 22: 'bb0s>nb1', 23: 'bb0s>nb0', 24: 'nn0', 25: 'nn0', 26: 'nn0', 27: 'nn0', 28: 'nn0', 29: 'nn0', 30: 'nn0', 31: 'nn0', 32: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'bn1>bb0s', 2: 'bn0>bb0s', 3: 'bn0>bn0', 4: 'bn0>bb0s', 5: 'bn0>bb0s', 6: 'nn0', 7: 'nn0', 8: 'nb0>bb0s', 9: 'nb0>bb0s', 10: 'nb0>bb0s', 11: 'nb1>bb0s', 12: 'nn0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bn1>bn0', 17: 'bn1>bn0', 18: 'bb0>bb0', 19: 'bb1>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nn0', 25: 'nb1>bb0s', 26: 'nb0>bb0s', 27: 'bn0>bn0', 28: 'bn1>bb0s', 29: 'bn0>bb0s', 30: 'nn0', 31: 'nn0', 32: 'nn0'}

Covariance layer complex:
                                           
CUUCCGUAGA-------AGGCCCUCUUU---UCAAACCGUAUU
     |:|||       |||||||||||   |||         
----UCGUCUUUCACCGUCCGGGAGAAACAUAGUCUU------
     *      1111    ** *     11 *          
                                     
---CUUCCGUAGA-AGGCCCUCUUUUCAAACCGUAUU
        || || |||  |:|||| ||         
UUGUCUUUCA-CUGUCCCAGGGAAACGUAAUCUU---
 *      11 11    ** *     11 *       

Reference strain:
A/WSN/1933
CUUCCGUAGA-------AGGCCCUCUUU---UCAAACCGUAUU
     |:|||       |||||||||||   |||         
----UCGUCUUUCACCGUCCGGGAGAAACAUAGUCUU------
Comparison strain:
A/duck/Mongolia/54+47/01[H5N1]
CUUCCGUAGAAGG---------CCCUCUUU-UCAAACCGUAUU
     |:||||:|         ||||     |||         
----UCGUCUUUCACCGUCCAAGGGAAACGUAGUCUU------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'bb0>bb0', 6: 'bb0>bb0', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0s>nb0', 11: 'bb0s>nb0', 12: 'bb0s>nb0', 13: 'bb0s>bb0s', 14: 'bb0s>bb1s', 15: 'bb0s>bb0s', 16: 'bb0s>bb0s', 17: 'bn0>bb0s', 18: 'bn0>bb0s', 19: 'bn0>bn0', 20: 'bn0>bn0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nn0', 25: 'nn0', 26: 'nn0', 27: 'nn0', 28: 'nn0', 29: 'nn0', 30: 'nn0', 31: 'nn0', 32: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'bb0>bb0', 2: 'bb0>bb0', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'nb0>bb0s', 7: 'nb0>bb0s', 8: 'nb0>bb0s', 9: 'nn0', 10: 'nn0', 11: 'nn0', 12: 'nn0', 13: 'bn0>bb0s', 14: 'bn0>bb0s', 15: 'bn0>bb0s', 16: 'bn1>bb0s', 17: 'bn1>bb0s', 18: 'bb0s>bb0s', 19: 'bb1s>bb0s', 20: 'bb0s>bb0s', 21: 'bb0s>bb0s', 22: 'bn0>bn0', 23: 'bn0>bn0', 24: 'nn0', 25: 'nn1', 26: 'nn0', 27: 'bb0>bb0', 28: 'bb0>bb0', 29: 'bb0>bb0', 30: 'nn0', 31: 'nn0', 32: 'nn0'}

Covariance layer complex:
                                           
CUUCCGUAGA-------AGGCCCUCUUU---UCAAACCGUAUU
     |:|||       |||||||||||   |||         
----UCGUCUUUCACCGUCCGGGAGAAACAUAGUCUU------
          111       ** *     *             
                                           
CUUCCGUAGAAGG---------CCCUCUUU-UCAAACCGUAUU
     |:||||:|         ||||     |||         
----UCGUCUUUCACCGUCCAAGGGAAACGUAGUCUU------
          111       ** *     *             

Reference strain:
A/WSN/1933
CUUCCGUAGA-------AGGCCCUCUUU---UCAAACCGUAUU
     |:|||       |||||||||||   |||         
----UCGUCUUUCACCGUCCGGGAGAAACAUAGUCUU------
Comparison strain:
A/duck/Hokkaido/Vac-3/2007[H5N1]
CUUCCGUAGA-------AGGCCCUCUUUUCAAACCGUAUU
     |:|||       |||| |:|||| ||         
----UCGUCUUUCACCGUCCGAGGGAAACGUAGUCUU---

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'bb0>bb0', 6: 'bb0>bb0', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bn0>bn1', 15: 'bb0>bb0', 16: 'bb0>bb1', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bn0>bn0', 22: 'bb0s>nb1', 23: 'bb0s>nb0', 24: 'nn0', 25: 'nn0', 26: 'nn0', 27: 'nn0', 28: 'nn0', 29: 'nn0', 30: 'nn0', 31: 'nn0', 32: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'bb0>bb0', 2: 'bb0>bb0', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nn0', 10: 'nn0', 11: 'nn0', 12: 'nn0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bn1>bn0', 18: 'bb0>bb0', 19: 'bb1>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nn0', 25: 'nb1>bb0s', 26: 'nb0>bb0s', 27: 'bn0>bn0', 28: 'bn0>bb0s', 29: 'bn0>bb0s', 30: 'nn0', 31: 'nn0', 32: 'nn0'}

Covariance layer complex:
                                           
CUUCCGUAGA-------AGGCCCUCUUU---UCAAACCGUAUU
     |:|||       |||||||||||   |||         
----UCGUCUUUCACCGUCCGGGAGAAACAUAGUCUU------
                     * *     11            
                                        
CUUCCGUAGA-------AGGCCCUCUUUUCAAACCGUAUU
     |:|||       |||| |:|||| ||         
----UCGUCUUUCACCGUCCGAGGGAAACGUAGUCUU---
                     * *     11         

Reference strain:
A/WSN/1933
CUUCCGUAGA-------AGGCCCUCUUU---UCAAACCGUAUU
     |:|||       |||||||||||   |||         
----UCGUCUUUCACCGUCCGGGAGAAACAUAGUCUU------
Comparison strain:
A/duck/Moscow/4182-C/2017[H5N3]
------CUUCCGUAGAAGGCCCUCUUU-UCAAACCGUAUU
           |:||    ||||     |||         
UUGUCUUUCACCGUCCAA-GGGAAACGUAGUCUU------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'bb0s>nb0', 6: 'bb0s>nb0', 7: 'bb0s>bb0s', 8: 'bb0s>bb0s', 9: 'bn0>bn0', 10: 'bn0>bb0s', 11: 'bn0>bb0s', 12: 'bn0>bn0', 13: 'bb0s>bb0s', 14: 'bb0s>bb1s', 15: 'bb0s>bb0s', 16: 'bb0s>bb0s', 17: 'bn0>bb0s', 18: 'bn0>bb0s', 19: 'bn0>bn0', 20: 'bn0>bn0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nn0', 25: 'nn0', 26: 'nn0', 27: 'nn0', 28: 'nn0', 29: 'nn0', 30: 'nn0', 31: 'nn0', 32: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'bn1>bb0s', 2: 'bn0>bb0s', 3: 'bn0>bb0s', 4: 'bn0>bb0s', 5: 'bn0>bn0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nn0', 10: 'nn0', 11: 'nb0>bb0s', 12: 'nb0>bb0s', 13: 'bb0s>bb0s', 14: 'bb0s>bb0s', 15: 'bn0>bn0', 16: 'bn1>bb0s', 17: 'bn1>bb0s', 18: 'bb0s>bb0s', 19: 'bb1s>bb0s', 20: 'bb0s>bb0s', 21: 'bb0s>bb0s', 22: 'bn0>bn0', 23: 'bn0>bn0', 24: 'nn0', 25: 'nn1', 26: 'nn0', 27: 'bb0>bb0', 28: 'bb0>bb0', 29: 'bb0>bb0', 30: 'nn0', 31: 'nn0', 32: 'nn0'}

Covariance layer complex:
                                           
CUUCCGUAGA-------AGGCCCUCUUU---UCAAACCGUAUU
     |:|||       |||||||||||   |||         
----UCGUCUUUCACCGUCCGGGAGAAACAUAGUCUU------
     *         11   ** *     *             
                                        
------CUUCCGUAGAAGGCCCUCUUU-UCAAACCGUAUU
           |:||    ||||     |||         
UUGUCUUUCACCGUCCAA-GGGAAACGUAGUCUU------
 *         11   **  *     *             

Reference strain:
A/WSN/1933
CUUCCGUAGA-------AGGCCCUCUUU---UCAAACCGUAUU
     |:|||       |||||||||||   |||         
----UCGUCUUUCACCGUCCGGGAGAAACAUAGUCUU------
Comparison strain:
A/duck/Zhejiang/6DK19-MA/2013[H5N2]
UUUCCGUAGAAGG---------CCCUCUUU-UCAAACCGUAUU
     |:||||:|         ||||     |||||       
----UCGUCUUUCACCACCCAAGGGAAACGUAGUUUU------

Strand 1 variance codes:
{0: 'nn1', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'bb0>bb0', 6: 'bb0>bb0', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0s>nb0', 11: 'bb0s>nb0', 12: 'bb0s>nb0', 13: 'bb0s>bb0s', 14: 'bb0s>bb1s', 15: 'bb0s>bb0s', 16: 'bb0s>bb0s', 17: 'bn0>bb0s', 18: 'bn0>bb0s', 19: 'bn0>bn0', 20: 'bn0>bn0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nb0>nb1', 25: 'nb0>nb0', 26: 'nn0', 27: 'nn0', 28: 'nn0', 29: 'nn0', 30: 'nn0', 31: 'nn0', 32: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'bb0>bb0', 2: 'bb0>bb0', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'nb0>bb0s', 7: 'nb0>bb0s', 8: 'nb0>bb0s', 9: 'nn0', 10: 'nn0', 11: 'nn0', 12: 'nn1', 13: 'bn1>bb0s', 14: 'bn0>bb0s', 15: 'bn0>bb0s', 16: 'bn1>bb0s', 17: 'bn1>bb0s', 18: 'bb0s>bb0s', 19: 'bb1s>bb0s', 20: 'bb0s>bb0s', 21: 'bb0s>bb0s', 22: 'bn0>bn0', 23: 'bn0>bn0', 24: 'nn0', 25: 'nn1', 26: 'nn0', 27: 'bb0>bb0', 28: 'bb0>bb0', 29: 'bb0>bb0', 30: 'nb1>nb0', 31: 'nb0>nb0', 32: 'nn0'}

Covariance layer complex:
*                                 11       
CUUCCGUAGA-------AGGCCCUCUUU---UCAAACCGUAUU
     |:|||       |||||||||||   |||         
----UCGUCUUUCACCGUCCGGGAGAAACAUAGUCUU------
          111   **  ** *     *    11       
*                                 11       
UUUCCGUAGAAGG---------CCCUCUUU-UCAAACCGUAUU
     |:||||:|         ||||     |||||       
----UCGUCUUUCACCACCCAAGGGAAACGUAGUUUU------
          111   **  ** *     *    11       

Reference strain:
A/WSN/1933
CUUCCGUAGA-------AGGCCCUCUUU---UCAAACCGUAUU
     |:|||       |||||||||||   |||         
----UCGUCUUUCACCGUCCGGGAGAAACAUAGUCUU------
Comparison strain:
A/Anas Platyrhynchos/Belgium/10811-6/2019[H5N6]
------CUUCCGUAGAAGGCCCUCUUU-UCAAACCGUAUU
           |:||    ||||     |||         
UUGUCUUUCACCGUCCAA-GGGAAACGUAGUCUU------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'bb0s>nb0', 6: 'bb0s>nb0', 7: 'bb0s>bb0s', 8: 'bb0s>bb0s', 9: 'bn0>bn0', 10: 'bn0>bb0s', 11: 'bn0>bb0s', 12: 'bn0>bn0', 13: 'bb0s>bb0s', 14: 'bb0s>bb1s', 15: 'bb0s>bb0s', 16: 'bb0s>bb0s', 17: 'bn0>bb0s', 18: 'bn0>bb0s', 19: 'bn0>bn0', 20: 'bn0>bn0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nn0', 25: 'nn0', 26: 'nn0', 27: 'nn0', 28: 'nn0', 29: 'nn0', 30: 'nn0', 31: 'nn0', 32: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'bn1>bb0s', 2: 'bn0>bb0s', 3: 'bn0>bb0s', 4: 'bn0>bb0s', 5: 'bn0>bn0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nn0', 10: 'nn0', 11: 'nb0>bb0s', 12: 'nb0>bb0s', 13: 'bb0s>bb0s', 14: 'bb0s>bb0s', 15: 'bn0>bn0', 16: 'bn1>bb0s', 17: 'bn1>bb0s', 18: 'bb0s>bb0s', 19: 'bb1s>bb0s', 20: 'bb0s>bb0s', 21: 'bb0s>bb0s', 22: 'bn0>bn0', 23: 'bn0>bn0', 24: 'nn0', 25: 'nn1', 26: 'nn0', 27: 'bb0>bb0', 28: 'bb0>bb0', 29: 'bb0>bb0', 30: 'nn0', 31: 'nn0', 32: 'nn0'}

Covariance layer complex:
                                           
CUUCCGUAGA-------AGGCCCUCUUU---UCAAACCGUAUU
     |:|||       |||||||||||   |||         
----UCGUCUUUCACCGUCCGGGAGAAACAUAGUCUU------
     *         11   ** *     *             
                                        
------CUUCCGUAGAAGGCCCUCUUU-UCAAACCGUAUU
           |:||    ||||     |||         
UUGUCUUUCACCGUCCAA-GGGAAACGUAGUCUU------
 *         11   **  *     *             

Reference strain:
A/WSN/1933
CUUCCGUAGA-------AGGCCCUCUUU---UCAAACCGUAUU
     |:|||       |||||||||||   |||         
----UCGUCUUUCACCGUCCGGGAGAAACAUAGUCUU------
Comparison strain:
A/swine/Banten/UT2071/2005[H5N1]
CUGCCGUAGAAGG---------CCCUCUUU-UCAAACCGUAUU
     |:||||:|         ||||     |||||       
----UCGUCUUUCACCGUCCAAGGGAAACGUAGUUUU------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn1', 3: 'nn0', 4: 'nn0', 5: 'bb0>bb0', 6: 'bb0>bb0', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0s>nb0', 11: 'bb0s>nb0', 12: 'bb0s>nb0', 13: 'bb0s>bb0s', 14: 'bb0s>bb1s', 15: 'bb0s>bb0s', 16: 'bb0s>bb0s', 17: 'bn0>bb0s', 18: 'bn0>bb0s', 19: 'bn0>bn0', 20: 'bn0>bn0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nb0>nb1', 25: 'nb0>nb0', 26: 'nn0', 27: 'nn0', 28: 'nn0', 29: 'nn0', 30: 'nn0', 31: 'nn0', 32: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'bb0>bb0', 2: 'bb0>bb0', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'nb0>bb0s', 7: 'nb0>bb0s', 8: 'nb0>bb0s', 9: 'nn0', 10: 'nn0', 11: 'nn0', 12: 'nn0', 13: 'bn0>bb0s', 14: 'bn0>bb0s', 15: 'bn0>bb0s', 16: 'bn1>bb0s', 17: 'bn1>bb0s', 18: 'bb0s>bb0s', 19: 'bb1s>bb0s', 20: 'bb0s>bb0s', 21: 'bb0s>bb0s', 22: 'bn0>bn0', 23: 'bn0>bn0', 24: 'nn0', 25: 'nn1', 26: 'nn0', 27: 'bb0>bb0', 28: 'bb0>bb0', 29: 'bb0>bb0', 30: 'nb1>nb0', 31: 'nb0>nb0', 32: 'nn0'}

Covariance layer complex:
  *                               11       
CUUCCGUAGA-------AGGCCCUCUUU---UCAAACCGUAUU
     |:|||       |||||||||||   |||         
----UCGUCUUUCACCGUCCGGGAGAAACAUAGUCUU------
          111       ** *     *    11       
  *                               11       
CUGCCGUAGAAGG---------CCCUCUUU-UCAAACCGUAUU
     |:||||:|         ||||     |||||       
----UCGUCUUUCACCGUCCAAGGGAAACGUAGUUUU------
          111       ** *     *    11       

Reference strain:
A/WSN/1933
CUUCCGUAGA-------AGGCCCUCUUU---UCAAACCGUAUU
     |:|||       |||||||||||   |||         
----UCGUCUUUCACCGUCCGGGAGAAACAUAGUCUU------
Comparison strain:
A/swine/Zhejiang/SW57/2015[H5N1]
CUUCCGUAGAAGG---------CCCUCUUU-UCAAACCGUAUU
     |:||||::         ||||     |||||       
----UCGUCUUUUACCACCCAAGGGAAUCGUAGUUUU------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'bb0>bb0', 6: 'bb0>bb0', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0s>nb0', 11: 'bb0s>nb0', 12: 'bb0s>nb1', 13: 'bb0s>bb0s', 14: 'bb0s>bb1s', 15: 'bb0s>bb0s', 16: 'bb0s>bb0s', 17: 'bn0>bb0s', 18: 'bn0>bb0s', 19: 'bn0>bn0', 20: 'bn0>bn1', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nb0>nb1', 25: 'nb0>nb0', 26: 'nn0', 27: 'nn0', 28: 'nn0', 29: 'nn0', 30: 'nn0', 31: 'nn0', 32: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'bb0>bb0', 2: 'bb0>bb0', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'nb0>bb0s', 7: 'nb0>bb0s', 8: 'nb1>bb0s', 9: 'nn0', 10: 'nn0', 11: 'nn0', 12: 'nn1', 13: 'bn1>bb0s', 14: 'bn0>bb0s', 15: 'bn0>bb0s', 16: 'bn1>bb0s', 17: 'bn1>bb0s', 18: 'bb0s>bb0s', 19: 'bb1s>bb0s', 20: 'bb0s>bb0s', 21: 'bb0s>bb0s', 22: 'bn0>bn0', 23: 'bn1>bn0', 24: 'nn0', 25: 'nn1', 26: 'nn0', 27: 'bb0>bb0', 28: 'bb0>bb0', 29: 'bb0>bb0', 30: 'nb1>nb0', 31: 'nb0>nb0', 32: 'nn0'}

Covariance layer complex:
                                  11       
CUUCCGUAGA-------AGGCCCUCUUU---UCAAACCGUAUU
     |:|||       |||||||||||   |||         
----UCGUCUUUCACCGUCCGGGAGAAACAUAGUCUU------
          111   **  ** *   * *    11       
                                  11       
CUUCCGUAGAAGG---------CCCUCUUU-UCAAACCGUAUU
     |:||||::         ||||     |||||       
----UCGUCUUUUACCACCCAAGGGAAUCGUAGUUUU------
          111   **  ** *   * *    11       

Reference strain:
A/WSN/1933
CUUCCGUAGA-------AGGCCCUCUUU---UCAAACCGUAUU
     |:|||       |||||||||||   |||         
----UCGUCUUUCACCGUCCGGGAGAAACAUAGUCUU------
Comparison strain:
A/Crow/Aghakhan/2017[H5N8]
------CUUCCGUAGAAGGCCCUCUUU-UCAAACCGUAUU
           |:||    ||||     |||||       
UUGUCUUUCACCGUCCAA-GGGAAACGUAGUUUU------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'bb0s>nb0', 6: 'bb0s>nb0', 7: 'bb0s>bb0s', 8: 'bb0s>bb0s', 9: 'bn0>bn0', 10: 'bn0>bb0s', 11: 'bn0>bb0s', 12: 'bn0>bn0', 13: 'bb0s>bb0s', 14: 'bb0s>bb1s', 15: 'bb0s>bb0s', 16: 'bb0s>bb0s', 17: 'bn0>bb0s', 18: 'bn0>bb0s', 19: 'bn0>bn0', 20: 'bn0>bn0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nb0>nb1', 25: 'nb0>nb0', 26: 'nn0', 27: 'nn0', 28: 'nn0', 29: 'nn0', 30: 'nn0', 31: 'nn0', 32: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'bn1>bb0s', 2: 'bn0>bb0s', 3: 'bn0>bb0s', 4: 'bn0>bb0s', 5: 'bn0>bn0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nn0', 10: 'nn0', 11: 'nb0>bb0s', 12: 'nb0>bb0s', 13: 'bb0s>bb0s', 14: 'bb0s>bb0s', 15: 'bn0>bn0', 16: 'bn1>bb0s', 17: 'bn1>bb0s', 18: 'bb0s>bb0s', 19: 'bb1s>bb0s', 20: 'bb0s>bb0s', 21: 'bb0s>bb0s', 22: 'bn0>bn0', 23: 'bn0>bn0', 24: 'nn0', 25: 'nn1', 26: 'nn0', 27: 'bb0>bb0', 28: 'bb0>bb0', 29: 'bb0>bb0', 30: 'nb1>nb0', 31: 'nb0>nb0', 32: 'nn0'}

Covariance layer complex:
                                  11       
CUUCCGUAGA-------AGGCCCUCUUU---UCAAACCGUAUU
     |:|||       |||||||||||   |||         
----UCGUCUUUCACCGUCCGGGAGAAACAUAGUCUU------
     *         11   ** *     *    11       
                               11       
------CUUCCGUAGAAGGCCCUCUUU-UCAAACCGUAUU
           |:||    ||||     |||||       
UUGUCUUUCACCGUCCAA-GGGAAACGUAGUUUU------
 *         11   **  *     *    11       

Reference strain:
A/WSN/1933
CUUCCGUAGA-------AGGCCCUCUUU---UCAAACCGUAUU
     |:|||       |||||||||||   |||         
----UCGUCUUUCACCGUCCGGGAGAAACAUAGUCUU------
Comparison strain:
A/swan/Krasnodar/44/2017[H5N8]
CUUCCGUAGAAGGCCCUCUUUUCAAACCGUAUU----
      :||||:|       ||||    |:||:    
----UUGUCUUUCACCGUCCAAGUGAAACGUAGUUUU

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'bn0>bn1', 6: 'bb0>bb0', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0s>nb0', 11: 'bb0s>nb0', 12: 'bb0s>nb0', 13: 'bn0>bb1s', 14: 'bn0>bb1s', 15: 'bn0>bb0s', 16: 'bn0>bb1s', 17: 'bn0>bn0', 18: 'bn0>bn0', 19: 'bn0>bn0', 20: 'bb0s>bb1s', 21: 'bb0s>bb1s', 22: 'bb0s>bb0s', 23: 'bb0s>bb1s', 24: 'nn0', 25: 'nn0', 26: 'nn0', 27: 'nn0', 28: 'nb0>nb0', 29: 'nb0>nb1', 30: 'nb0>nb0', 31: 'nb0>bb0s', 32: 'nb0>bb0s'}
Strand 2 variance codes:
{0: 'nn0', 1: 'bn1>bn0', 2: 'bb0>bb0', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'nb0>bb0s', 7: 'nb0>bb0s', 8: 'nb0>bb0s', 9: 'nn0', 10: 'nn0', 11: 'nn0', 12: 'nn0', 13: 'bn0>bb0s', 14: 'bn0>bb0s', 15: 'bn0>bb0s', 16: 'bb1s>bb0s', 17: 'bb1s>bb0s', 18: 'bb0s>bb0s', 19: 'bb1s>bb0s', 20: 'bn0>bn0', 21: 'bn0>bn0', 22: 'bn0>bn0', 23: 'bn0>bb0s', 24: 'nb0>nb0', 25: 'nb1>nb0', 26: 'nb0>nb0', 27: 'bb0s>nb0', 28: 'bb0s>nb0', 29: 'bn0>bb0s', 30: 'nn1', 31: 'nn0', 32: 'nn0'}

Covariance layer complex:
                                      11111
CUUCCGUAGA-------AGGCCCUCUUU---UCAAACCGUAUU
     |:|||       |||||||||||   |||         
----UCGUCUUUCACCGUCCGGGAGAAACAUAGUCUU------
     *    111       ** *    111   *        
                            11111    
CUUCCGUAGAAGGCCCUCUUUUCAAACCGUAUU----
      :||||:|       ||||    |:||:    
----UUGUCUUUCACCGUCCAAGUGAAACGUAGUUUU
     *    111       ** *    111   *  
