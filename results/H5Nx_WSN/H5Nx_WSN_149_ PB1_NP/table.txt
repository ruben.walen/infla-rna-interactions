149 PB1 1925-1959 (34) NP 672-706 (34)
Reference strain: A/WSN/1933
Strain	FE/interaction found	Seg. 1 span	Seg. 2 span	Full bonds/half bonds/no bonds	Mutations in whole site seg.1 / seg. 2
A/WSN/1933	-14.78	3-27 (24)	32-9 (23)	18/2/6	0/0
A/Anhui/1/2005[H5N1]	-3.33	7-17 (10)	35-25 (10)	10/0/1	6/8
A/Vietnam/UT36282/2010[H5N1]	-0.78	30-33 (3)	18-15 (3)	3/1/0	6/6
A/Egypt/MOH-NRC-8434/2014[H5N1]	-1.21	30-33 (3)	26-23 (3)	3/1/0	6/5
A/Changsha/1/2014[H5N6]	-5.03	30-35 (5)	26-21 (5)	4/2/0	5/6
A/Yunnan/0127/2015[H5N6]	-5.51	26-35 (9)	22-13 (9)	7/2/1	3/7
A/duck/Mongolia/54+47/01[H5N1]	-2.34	7-35 (28)	35-7 (28)	17/4/8	5/6
A/duck/Hokkaido/Vac-3/2007[H5N1]	-2.94	19-22 (3)	22-19 (3)	4/0/0	5/6
A/duck/Moscow/4182-C/2017[H5N3]	-7.58	7-35 (28)	35-13 (22)	18/1/11	7/7
A/duck/Zhejiang/6DK19-MA/2013[H5N2]	-5.58	26-35 (9)	22-13 (9)	7/2/1	4/5
A/Anas Platyrhynchos/Belgium/10811-6/2019[H5N6]	-3.96	11-15 (4)	22-18 (4)	4/1/0	8/7
A/swine/Banten/UT2071/2005[H5N1]	-1.85	21-24 (3)	17-14 (3)	4/0/0	6/6
A/swine/Zhejiang/SW57/2015[H5N1]	-5.03	30-35 (5)	26-21 (5)	4/2/0	5/6
A/Crow/Aghakhan/2017[H5N8]	-5.14	30-35 (5)	26-21 (5)	4/2/0	7/7
A/swan/Krasnodar/44/2017[H5N8]	-4.95	30-35 (5)	26-21 (5)	4/2/0	6/7