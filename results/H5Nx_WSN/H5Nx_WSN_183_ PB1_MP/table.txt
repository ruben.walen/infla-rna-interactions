183 PB1 345-393 (48) MP 399-445 (46)
Reference strain: A/WSN/1933
Strain	FE/interaction found	Seg. 1 span	Seg. 2 span	Full bonds/half bonds/no bonds	Mutations in whole site seg.1 / seg. 2
A/WSN/1933	-11.67	48-12 (36)	5-35 (30)	20/2/17	0/0
A/Anhui/1/2005[H5N1]	-12.85	37-17 (20)	8-30 (22)	17/1/5	10/7
A/Vietnam/UT36282/2010[H5N1]	-9.5	37-12 (25)	8-35 (27)	17/3/8	10/7
A/Egypt/MOH-NRC-8434/2014[H5N1]	-10.94	37-17 (20)	8-30 (22)	17/1/5	10/6
A/Changsha/1/2014[H5N6]	-12.24	48-17 (31)	5-30 (25)	20/1/13	12/5
A/Yunnan/0127/2015[H5N6]	-13.34	39-12 (27)	6-35 (29)	21/1/8	8/6
A/duck/Mongolia/54+47/01[H5N1]	-10.4	37-12 (25)	8-35 (27)	18/1/9	6/5
A/duck/Hokkaido/Vac-3/2007[H5N1]	-12.29	39-8 (31)	6-42 (36)	24/1/12	9/6
A/duck/Moscow/4182-C/2017[H5N3]	-9.99	37-12 (25)	8-35 (27)	18/2/8	7/4
A/duck/Zhejiang/6DK19-MA/2013[H5N2]	-11.63	37-12 (25)	8-35 (27)	19/0/9	9/5
A/Anas Platyrhynchos/Belgium/10811-6/2019[H5N6]	-9.94	39-12 (27)	6-35 (29)	19/2/9	8/4
A/swine/Banten/UT2071/2005[H5N1]	-12.16	37-17 (20)	8-30 (22)	17/1/5	9/6
A/swine/Zhejiang/SW57/2015[H5N1]	-8.84	47-22 (25)	17-44 (27)	19/0/11	9/8
A/Crow/Aghakhan/2017[H5N8]	-12.23	37-12 (25)	8-35 (27)	19/1/8	8/5
A/swan/Krasnodar/44/2017[H5N8]	-8.09	37-12 (25)	8-35 (27)	18/1/9	9/5