Covariation report for: 92 PB2 PA

Reference strain:
A/WSN/1933
CCGAUUACACGAUUAACCCGUUCCUCUGC-ACCACAACCAUU
            |:|| ||| :||||||  ||||   ||   
---------AGAAGUUUGGC-GGGAGACUUUGGUC--GG---
Comparison strain:
A/Anhui/1/2005[H5N1]
UCGAUUACACGACUAUCCUGUUCCUCUGC-ACCACAACCAUU
                ||   :||||||  ||||   ||   
--------AGAAGUUCGGU--GGGAGACUUUGGUC--GG---

Strand 1 variance codes:
{0: 'nn1', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nn0', 10: 'nn0', 11: 'nn0', 12: 'bn1>bn0', 13: 'bn0>bn0', 14: 'bn0>bn0', 15: 'bn1>bn0', 16: 'nb0>bb0s', 17: 'bb0s>bb0s', 18: 'bn1>bb0s', 19: 'bn0>bn1', 20: 'nn0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'bb0>bb0', 30: 'bb0>bb0', 31: 'bb0>bb0', 32: 'bb0>bb0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'bb0>bb0', 37: 'bb0>bb0', 38: 'nn0', 39: 'nn0', 40: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'bn0>bn1', 4: 'bn0>bn0', 5: 'bn0>bn0', 6: 'bn0>bn1', 7: 'nn1', 8: 'bb0s>nb0', 9: 'bb0s>bb0s', 10: 'bn1>bn0', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'nn0', 19: 'nn0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nn0', 25: 'bb0>bb0', 26: 'bb0>bb0'}

Covariance layer complex:
*           *  *1 *                       
CCGAUUACACGAUUAACCCGUUCCUCUGC-ACCACAACCAUU
            |:|| ||| :||||||  ||||   ||   
---------AGAAGUUUGGC-GGGAGACUUUGGUC--GG---
                *  *                      
*           *  *1 *                       
UCGAUUACACGACUAUCCUGUUCCUCUGC-ACCACAACCAUU
                ||   :||||||  ||||   ||   
--------AGAAGUUCGGU--GGGAGACUUUGGUC--GG---
               *  *                       

Reference strain:
A/WSN/1933
CCGAUUACACGAUUAACCCGUUCCUCUGC-ACCACAACCAUU
            |:|| ||| :||||||  ||||   ||   
---------AGAAGUUUGGC-GGGAGACUUUGGUC--GG---
Comparison strain:
A/Vietnam/UT36282/2010[H5N1]
UCGAUUACACGACUAUCCUGUUCCUCUGC-ACCACAACCAUU
                ||   :||||||  ||||        
--------AGAAAUUCGGU--GGGAGACUUUGGUCUG-----

Strand 1 variance codes:
{0: 'nn1', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nn0', 10: 'nn0', 11: 'nn0', 12: 'bn1>bn0', 13: 'bn0>bn1', 14: 'bn0>bn0', 15: 'bn1>bn0', 16: 'nb0>bb0s', 17: 'bb0s>bb0s', 18: 'bn1>bb0s', 19: 'bn0>bn1', 20: 'nn0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'bb0>bb0', 30: 'bb0>bb0', 31: 'bb0>bb0', 32: 'bb0>bb0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'bn0>bn1', 37: 'bn0>bn0', 38: 'nn0', 39: 'nn0', 40: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'bn0>bn1', 4: 'bn1>bn0', 5: 'bn0>bn0', 6: 'bn0>bn1', 7: 'nn1', 8: 'bb0s>nb0', 9: 'bb0s>bb0s', 10: 'bn1>bn0', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'nn0', 19: 'nn0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nn0', 25: 'bn1>bn0', 26: 'bn0>bn0'}

Covariance layer complex:
*           *  *1 *                       
CCGAUUACACGAUUAACCCGUUCCUCUGC-ACCACAACCAUU
            |:|| ||| :||||||  ||||   ||   
---------AGAAGUUUGGC-GGGAGACUUUGGUC--GG---
             *  *  *                 *    
*           *  *1 *                       
UCGAUUACACGACUAUCCUGUUCCUCUGC-ACCACAACCAUU
                ||   :||||||  ||||        
--------AGAAAUUCGGU--GGGAGACUUUGGUCUG-----
            *  *  *                *      

Reference strain:
A/WSN/1933
CCGAUUACACGAUUAACCCGUUCCUCUGC-ACCACAACCAUU
            |:|| ||| :||||||  ||||   ||   
---------AGAAGUUUGGC-GGGAGACUUUGGUC--GG---
Comparison strain:
A/Egypt/MOH-NRC-8434/2014[H5N1]
UCGAUUACACGACUAUCCCGUUCCUCUGCAUCACAACCAUU-
                 ||: :||||||      |||||   
---------AGAAGUUCGGU-GGGAGACU-----UUGGUCAG

Strand 1 variance codes:
{0: 'nn1', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nn0', 10: 'nn0', 11: 'nn0', 12: 'bn1>bn0', 13: 'bn0>bn0', 14: 'bn0>bn0', 15: 'bn1>bn0', 16: 'nn0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb1', 20: 'nn0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'bn0>bb0s', 30: 'bn1>bb0s', 31: 'bn0>bb0s', 32: 'bn0>bb0s', 33: 'nn0', 34: 'nb0>nb0', 35: 'nb0>bb0s', 36: 'bb0s>bb0s', 37: 'bb0s>bb0s', 38: 'nb0>bb0s', 39: 'nn0', 40: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'bn0>bn1', 4: 'bn0>bn0', 5: 'bn0>bn0', 6: 'bn0>bn1', 7: 'nn1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb1>bb0', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'nn0', 19: 'nb0>nb0', 20: 'bb0s>nb0', 21: 'bb0s>bb0s', 22: 'bb0s>bb0s', 23: 'bb0s>nb0', 24: 'nn0', 25: 'bn1>bb0s', 26: 'bn0>bb0s'}

Covariance layer complex:
*           *  *               *   11  1  
CCGAUUACACGAUUAACCCGUUCCUCUGC-ACCACAACCAUU
            |:|| ||| :||||||  ||||   ||   
---------AGAAGUUUGGC-GGGAGACUUUGGUC--GG---
                *  *         1       *    
*           *  *              *   11  1   
UCGAUUACACGACUAUCCCGUUCCUCUGCAUCACAACCAUU-
                 ||: :||||||      |||||   
---------AGAAGUUCGGU-GGGAGACU-----UUGGUCAG
                *  *              1     * 

Reference strain:
A/WSN/1933
CCGAUUACACGAUUAACCCGUUCCUCUGC-ACCACAACCAUU
            |:|| ||| :||||||  ||||   ||   
---------AGAAGUUUGGC-GGGAGACUUUGGUC--GG---
Comparison strain:
A/Changsha/1/2014[H5N6]
UCGAUUACACGACUAUCCCGUUCCUCUGC-ACCACAACCAUU
                 ||: :||||||  ||||        
---------AGAAGUUCGGU-GGGAGACUUUGGUCAG-----

Strand 1 variance codes:
{0: 'nn1', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nn0', 10: 'nn0', 11: 'nn0', 12: 'bn1>bn0', 13: 'bn0>bn0', 14: 'bn0>bn0', 15: 'bn1>bn0', 16: 'nn0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb1', 20: 'nn0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'bb0>bb0', 30: 'bb0>bb0', 31: 'bb0>bb0', 32: 'bb0>bb0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'bn0>bn1', 37: 'bn0>bn0', 38: 'nn0', 39: 'nn0', 40: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'bn0>bn1', 4: 'bn0>bn0', 5: 'bn0>bn0', 6: 'bn0>bn1', 7: 'nn1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb1>bb0', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'nn0', 19: 'nn0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nn0', 25: 'bn1>bn0', 26: 'bn0>bn0'}

Covariance layer complex:
*           *  *                          
CCGAUUACACGAUUAACCCGUUCCUCUGC-ACCACAACCAUU
            |:|| ||| :||||||  ||||   ||   
---------AGAAGUUUGGC-GGGAGACUUUGGUC--GG---
                *  *                 *    
*           *  *                          
UCGAUUACACGACUAUCCCGUUCCUCUGC-ACCACAACCAUU
                 ||: :||||||  ||||        
---------AGAAGUUCGGU-GGGAGACUUUGGUCAG-----
                *  *               *      

Reference strain:
A/WSN/1933
CCGAUUACACGAUUAACCCGUUCCUCUGC-ACCACAACCAUU
            |:|| ||| :||||||  ||||   ||   
---------AGAAGUUUGGC-GGGAGACUUUGGUC--GG---
Comparison strain:
A/Yunnan/0127/2015[H5N6]
CCGAUUGCACAACUAUCCCGUUCCUCUGC-ACCACAACCAUU
                 ||: :||||||  ||||   ||   
---------AGAAGUUCGGU-GGGAGACUUUGGUC--GG---

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'nn1', 7: 'nn0', 8: 'nn0', 9: 'nn0', 10: 'nn1', 11: 'nn0', 12: 'bn1>bn0', 13: 'bn0>bn0', 14: 'bn0>bn0', 15: 'bn1>bn0', 16: 'nn0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb1', 20: 'nn0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'bb0>bb0', 30: 'bb0>bb0', 31: 'bb0>bb0', 32: 'bb0>bb0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'bb0>bb0', 37: 'bb0>bb0', 38: 'nn0', 39: 'nn0', 40: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'bn0>bn1', 4: 'bn0>bn0', 5: 'bn0>bn0', 6: 'bn0>bn1', 7: 'nn1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb1>bb0', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'nn0', 19: 'nn0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nn0', 25: 'bb0>bb0', 26: 'bb0>bb0'}

Covariance layer complex:
      *   * *  *                          
CCGAUUACACGAUUAACCCGUUCCUCUGC-ACCACAACCAUU
            |:|| ||| :||||||  ||||   ||   
---------AGAAGUUUGGC-GGGAGACUUUGGUC--GG---
                *  *                      
      *   * *  *                          
CCGAUUGCACAACUAUCCCGUUCCUCUGC-ACCACAACCAUU
                 ||: :||||||  ||||   ||   
---------AGAAGUUCGGU-GGGAGACUUUGGUC--GG---
                *  *                      

Reference strain:
A/WSN/1933
CCGAUUACACGAUUAACCCGUUCCUCUGC-ACCACAACCAUU
            |:|| ||| :||||||  ||||   ||   
---------AGAAGUUUGGC-GGGAGACUUUGGUC--GG---
Comparison strain:
A/duck/Mongolia/54+47/01[H5N1]
UCGAUUACACGACUAUCCCGUUCCUCUGC-ACCACAACCAUU
                 ||: :||||||  ||||   ||   
---------AGAAGUUCGGU-GGGAGACUUUGGUC--GG---

Strand 1 variance codes:
{0: 'nn1', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nn0', 10: 'nn0', 11: 'nn0', 12: 'bn1>bn0', 13: 'bn0>bn0', 14: 'bn0>bn0', 15: 'bn1>bn0', 16: 'nn0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb1', 20: 'nn0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'bb0>bb0', 30: 'bb0>bb0', 31: 'bb0>bb0', 32: 'bb0>bb0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'bb0>bb0', 37: 'bb0>bb0', 38: 'nn0', 39: 'nn0', 40: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'bn0>bn1', 4: 'bn0>bn0', 5: 'bn0>bn0', 6: 'bn0>bn1', 7: 'nn1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb1>bb0', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'nn0', 19: 'nn0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nn0', 25: 'bb0>bb0', 26: 'bb0>bb0'}

Covariance layer complex:
*           *  *                          
CCGAUUACACGAUUAACCCGUUCCUCUGC-ACCACAACCAUU
            |:|| ||| :||||||  ||||   ||   
---------AGAAGUUUGGC-GGGAGACUUUGGUC--GG---
                *  *                      
*           *  *                          
UCGAUUACACGACUAUCCCGUUCCUCUGC-ACCACAACCAUU
                 ||: :||||||  ||||   ||   
---------AGAAGUUCGGU-GGGAGACUUUGGUC--GG---
                *  *                      

Reference strain:
A/WSN/1933
CCGAUUACACGAUUAACCCGUUCCUCUGC-ACCACAACCAUU
            |:|| ||| :||||||  ||||   ||   
---------AGAAGUUUGGC-GGGAGACUUUGGUC--GG---
Comparison strain:
A/duck/Hokkaido/Vac-3/2007[H5N1]
CCGAUUACACAACUAUCCCGUUCCUCUGC-ACCACAACCAUU
                 ||: :||||||  ||||   ||   
---------AGAAGUUCGGU-GGGAGACUUUGGUC--GG---

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nn0', 10: 'nn1', 11: 'nn0', 12: 'bn1>bn0', 13: 'bn0>bn0', 14: 'bn0>bn0', 15: 'bn1>bn0', 16: 'nn0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb1', 20: 'nn0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'bb0>bb0', 30: 'bb0>bb0', 31: 'bb0>bb0', 32: 'bb0>bb0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'bb0>bb0', 37: 'bb0>bb0', 38: 'nn0', 39: 'nn0', 40: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'bn0>bn1', 4: 'bn0>bn0', 5: 'bn0>bn0', 6: 'bn0>bn1', 7: 'nn1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb1>bb0', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'nn0', 19: 'nn0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nn0', 25: 'bb0>bb0', 26: 'bb0>bb0'}

Covariance layer complex:
          * *  *                          
CCGAUUACACGAUUAACCCGUUCCUCUGC-ACCACAACCAUU
            |:|| ||| :||||||  ||||   ||   
---------AGAAGUUUGGC-GGGAGACUUUGGUC--GG---
                *  *                      
          * *  *                          
CCGAUUACACAACUAUCCCGUUCCUCUGC-ACCACAACCAUU
                 ||: :||||||  ||||   ||   
---------AGAAGUUCGGU-GGGAGACUUUGGUC--GG---
                *  *                      

Reference strain:
A/WSN/1933
CCGAUUACACGAUUAACCCGUUCCUCUGC-ACCACAACCAUU
            |:|| ||| :||||||  ||||   ||   
---------AGAAGUUUGGC-GGGAGACUUUGGUC--GG---
Comparison strain:
A/duck/Moscow/4182-C/2017[H5N3]
CCGAUUACACAACUAUCCCGUUCCUCUGC-AUCACAACCAUU
                 ||: :||||||  |:||   ||   
---------AGAAGUUCGGU-GGGAGACUUUGGUC--GG---

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nn0', 10: 'nn1', 11: 'nn0', 12: 'bn1>bn0', 13: 'bn0>bn0', 14: 'bn0>bn0', 15: 'bn1>bn0', 16: 'nn0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb1', 20: 'nn0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'bb0>bb0', 30: 'bb1>bb0', 31: 'bb0>bb0', 32: 'bb0>bb0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'bb0>bb0', 37: 'bb0>bb0', 38: 'nn0', 39: 'nn0', 40: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'bn0>bn1', 4: 'bn0>bn0', 5: 'bn0>bn0', 6: 'bn0>bn1', 7: 'nn1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb1>bb0', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'nn0', 19: 'nn0', 20: 'bb0>bb0', 21: 'bb0>bb1', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nn0', 25: 'bb0>bb0', 26: 'bb0>bb0'}

Covariance layer complex:
          * *  *               *          
CCGAUUACACGAUUAACCCGUUCCUCUGC-ACCACAACCAUU
            |:|| ||| :||||||  ||||   ||   
---------AGAAGUUUGGC-GGGAGACUUUGGUC--GG---
                *  *                      
          * *  *               *          
CCGAUUACACAACUAUCCCGUUCCUCUGC-AUCACAACCAUU
                 ||: :||||||  |:||   ||   
---------AGAAGUUCGGU-GGGAGACUUUGGUC--GG---
                *  *                      

Reference strain:
A/WSN/1933
CCGAUUACACGAUUAACCCGUUCCUCUGC-ACCACAACCAUU
            |:|| ||| :||||||  ||||   ||   
---------AGAAGUUUGGC-GGGAGACUUUGGUC--GG---
Comparison strain:
A/duck/Zhejiang/6DK19-MA/2013[H5N2]
UCGAUUACACGACUAUCCCGUUCCUCUGC-ACCACAACCAUU
                 ||: :||||||  ||||        
---------AGAAGUUCGGU-GGGAGACUUUGGUCAG-----

Strand 1 variance codes:
{0: 'nn1', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nn0', 10: 'nn0', 11: 'nn0', 12: 'bn1>bn0', 13: 'bn0>bn0', 14: 'bn0>bn0', 15: 'bn1>bn0', 16: 'nn0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb1', 20: 'nn0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'bb0>bb0', 30: 'bb0>bb0', 31: 'bb0>bb0', 32: 'bb0>bb0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'bn0>bn1', 37: 'bn0>bn0', 38: 'nn0', 39: 'nn0', 40: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'bn0>bn1', 4: 'bn0>bn0', 5: 'bn0>bn0', 6: 'bn0>bn1', 7: 'nn1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb1>bb0', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'nn0', 19: 'nn0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nn0', 25: 'bn1>bn0', 26: 'bn0>bn0'}

Covariance layer complex:
*           *  *                          
CCGAUUACACGAUUAACCCGUUCCUCUGC-ACCACAACCAUU
            |:|| ||| :||||||  ||||   ||   
---------AGAAGUUUGGC-GGGAGACUUUGGUC--GG---
                *  *                 *    
*           *  *                          
UCGAUUACACGACUAUCCCGUUCCUCUGC-ACCACAACCAUU
                 ||: :||||||  ||||        
---------AGAAGUUCGGU-GGGAGACUUUGGUCAG-----
                *  *               *      

Reference strain:
A/WSN/1933
CCGAUUACACGAUUAACCCGUUCCUCUGC-ACCACAACCAUU
            |:|| ||| :||||||  ||||   ||   
---------AGAAGUUUGGC-GGGAGACUUUGGUC--GG---
Comparison strain:
A/Anas Platyrhynchos/Belgium/10811-6/2019[H5N6]
CCGAUUACACAACUAUCCCGUUCCUCUGC-ACCACAACCAUU
                 ||: :||||||  ||||   ||   
---------AGAAGUUCGGU-GGGAGACUUUGGUC--GG---

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nn0', 10: 'nn1', 11: 'nn0', 12: 'bn1>bn0', 13: 'bn0>bn0', 14: 'bn0>bn0', 15: 'bn1>bn0', 16: 'nn0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb1', 20: 'nn0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'bb0>bb0', 30: 'bb0>bb0', 31: 'bb0>bb0', 32: 'bb0>bb0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'bb0>bb0', 37: 'bb0>bb0', 38: 'nn0', 39: 'nn0', 40: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'bn0>bn1', 4: 'bn0>bn0', 5: 'bn0>bn0', 6: 'bn0>bn1', 7: 'nn1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb1>bb0', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'nn0', 19: 'nn0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nn0', 25: 'bb0>bb0', 26: 'bb0>bb0'}

Covariance layer complex:
          * *  *                          
CCGAUUACACGAUUAACCCGUUCCUCUGC-ACCACAACCAUU
            |:|| ||| :||||||  ||||   ||   
---------AGAAGUUUGGC-GGGAGACUUUGGUC--GG---
                *  *                      
          * *  *                          
CCGAUUACACAACUAUCCCGUUCCUCUGC-ACCACAACCAUU
                 ||: :||||||  ||||   ||   
---------AGAAGUUCGGU-GGGAGACUUUGGUC--GG---
                *  *                      

Reference strain:
A/WSN/1933
CCGAUUACACGAUUAACCCGUUCCUCUGC-ACCACAACCAUU
            |:|| ||| :||||||  ||||   ||   
---------AGAAGUUUGGC-GGGAGACUUUGGUC--GG---
Comparison strain:
A/swine/Banten/UT2071/2005[H5N1]
CCGUUUACAUGACUAUCCCGUUCCUCUAC-ACCACAACCAUU
                 ||: :|||||   ||||   ||   
---------GGAAGUUCGGU-GGGAGACUUUGGUC--GG---

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn1', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nn1', 10: 'nn0', 11: 'nn0', 12: 'bn1>bn0', 13: 'bn0>bn0', 14: 'bn0>bn0', 15: 'bn1>bn0', 16: 'nn0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb1', 20: 'nn0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bn1>bn0', 28: 'nn0', 29: 'bb0>bb0', 30: 'bb0>bb0', 31: 'bb0>bb0', 32: 'bb0>bb0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'bb0>bb0', 37: 'bb0>bb0', 38: 'nn0', 39: 'nn0', 40: 'nn0'}
Strand 2 variance codes:
{0: 'nn1', 1: 'nn0', 2: 'nn0', 3: 'bn0>bn1', 4: 'bn0>bn0', 5: 'bn0>bn0', 6: 'bn0>bn1', 7: 'nn1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb1>bb0', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bn0>bn1', 18: 'nn0', 19: 'nn0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nn0', 25: 'bb0>bb0', 26: 'bb0>bb0'}

Covariance layer complex:
   *     *  *  *           *              
CCGAUUACACGAUUAACCCGUUCCUCUGC-ACCACAACCAUU
            |:|| ||| :||||||  ||||   ||   
---------AGAAGUUUGGC-GGGAGACUUUGGUC--GG---
         *      *  *                      
   *     *  *  *           *              
CCGUUUACAUGACUAUCCCGUUCCUCUAC-ACCACAACCAUU
                 ||: :|||||   ||||   ||   
---------GGAAGUUCGGU-GGGAGACUUUGGUC--GG---
         *      *  *                      

Reference strain:
A/WSN/1933
CCGAUUACACGAUUAACCCGUUCCUCUGC-ACCACAACCAUU
            |:|| ||| :||||||  ||||   ||   
---------AGAAGUUUGGC-GGGAGACUUUGGUC--GG---
Comparison strain:
A/swine/Zhejiang/SW57/2015[H5N1]
CCGAUUACACAACUAUCCCGUUCCUCUGC-ACCACAACCAUU
                 ||: :||||:|  ||||        
---------AGAAGUUCGGU-GGGAGGCUUUGGUCAG-----

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nn0', 10: 'nn1', 11: 'nn0', 12: 'bn1>bn0', 13: 'bn0>bn0', 14: 'bn0>bn0', 15: 'bn1>bn0', 16: 'nn0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb1', 20: 'nn0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb1', 27: 'bb0>bb0', 28: 'nn0', 29: 'bb0>bb0', 30: 'bb0>bb0', 31: 'bb0>bb0', 32: 'bb0>bb0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'bn0>bn1', 37: 'bn0>bn0', 38: 'nn0', 39: 'nn0', 40: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'bn0>bn1', 4: 'bn0>bn0', 5: 'bn0>bn0', 6: 'bn0>bn1', 7: 'nn1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb1>bb0', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb1>bb0', 17: 'bb0>bb0', 18: 'nn0', 19: 'nn0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nn0', 25: 'bn1>bn0', 26: 'bn0>bn0'}

Covariance layer complex:
          * *  *                          
CCGAUUACACGAUUAACCCGUUCCUCUGC-ACCACAACCAUU
            |:|| ||| :||||||  ||||   ||   
---------AGAAGUUUGGC-GGGAGACUUUGGUC--GG---
                *  *      *          *    
          * *  *                          
CCGAUUACACAACUAUCCCGUUCCUCUGC-ACCACAACCAUU
                 ||: :||||:|  ||||        
---------AGAAGUUCGGU-GGGAGGCUUUGGUCAG-----
                *  *      *        *      

Reference strain:
A/WSN/1933
CCGAUUACACGAUUAACCCGUUCCUCUGC-ACCACAACCAUU
            |:|| ||| :||||||  ||||   ||   
---------AGAAGUUUGGC-GGGAGACUUUGGUC--GG---
Comparison strain:
A/Crow/Aghakhan/2017[H5N8]
CCGAUUGCACAACUAUCCUGUUCCUCUGC-AUCACAACCAUU
                ||   :||||||  |:||   ||   
--------AGAAGUUCGGU--GGGAGACUUUGGUC--GG---

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'nn1', 7: 'nn0', 8: 'nn0', 9: 'nn0', 10: 'nn1', 11: 'nn0', 12: 'bn1>bn0', 13: 'bn0>bn0', 14: 'bn0>bn0', 15: 'bn1>bn0', 16: 'nb0>bb0s', 17: 'bb0s>bb0s', 18: 'bn1>bb0s', 19: 'bn0>bn1', 20: 'nn0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'bb0>bb0', 30: 'bb1>bb0', 31: 'bb0>bb0', 32: 'bb0>bb0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'bb0>bb0', 37: 'bb0>bb0', 38: 'nn0', 39: 'nn0', 40: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'bn0>bn1', 4: 'bn0>bn0', 5: 'bn0>bn0', 6: 'bn0>bn1', 7: 'nn1', 8: 'bb0s>nb0', 9: 'bb0s>bb0s', 10: 'bn1>bn0', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'nn0', 19: 'nn0', 20: 'bb0>bb0', 21: 'bb0>bb1', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nn0', 25: 'bb0>bb0', 26: 'bb0>bb0'}

Covariance layer complex:
      *   * *  *1 *            *          
CCGAUUACACGAUUAACCCGUUCCUCUGC-ACCACAACCAUU
            |:|| ||| :||||||  ||||   ||   
---------AGAAGUUUGGC-GGGAGACUUUGGUC--GG---
                *  *                      
      *   * *  *1 *            *          
CCGAUUGCACAACUAUCCUGUUCCUCUGC-AUCACAACCAUU
                ||   :||||||  |:||   ||   
--------AGAAGUUCGGU--GGGAGACUUUGGUC--GG---
               *  *                       

Reference strain:
A/WSN/1933
CCGAUUACACGAUUAACCCGUUCCUCUGC-ACCACAACCAUU
            |:|| ||| :||||||  ||||   ||   
---------AGAAGUUUGGC-GGGAGACUUUGGUC--GG---
Comparison strain:
A/swan/Krasnodar/44/2017[H5N8]
CCGAUUACACAACUAUCCCGUUCCUCUGC-ACCACAACCAUU
                 ||: :||||||  ||||   ||   
---------AGAAGUUCGGU-GGGAGACUUUGGUC--GG---

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nn0', 10: 'nn1', 11: 'nn0', 12: 'bn1>bn0', 13: 'bn0>bn0', 14: 'bn0>bn0', 15: 'bn1>bn0', 16: 'nn0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb1', 20: 'nn0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'bb0>bb0', 30: 'bb0>bb0', 31: 'bb0>bb0', 32: 'bb0>bb0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'bb0>bb0', 37: 'bb0>bb0', 38: 'nn0', 39: 'nn0', 40: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'bn0>bn1', 4: 'bn0>bn0', 5: 'bn0>bn0', 6: 'bn0>bn1', 7: 'nn1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb1>bb0', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'nn0', 19: 'nn0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nn0', 25: 'bb0>bb0', 26: 'bb0>bb0'}

Covariance layer complex:
          * *  *                          
CCGAUUACACGAUUAACCCGUUCCUCUGC-ACCACAACCAUU
            |:|| ||| :||||||  ||||   ||   
---------AGAAGUUUGGC-GGGAGACUUUGGUC--GG---
                *  *                      
          * *  *                          
CCGAUUACACAACUAUCCCGUUCCUCUGC-ACCACAACCAUU
                 ||: :||||||  ||||   ||   
---------AGAAGUUCGGU-GGGAGACUUUGGUC--GG---
                *  *                      
