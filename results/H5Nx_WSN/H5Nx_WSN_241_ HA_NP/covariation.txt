Covariation report for: 241 HA NP

Reference strain:
A/WSN/1933
CCCUAGUCCGAUACGUCGCCUAG---------UUUUUUCGUGUGUCUUACGG
              |:|| ||||         |:: |:||| :|||:|    
-------------ACGGCAGAUCCAUACACACAGGCAGGCAGGCAGGA----
Comparison strain:
A/Anhui/1/2005[H5N1]
----CCCCUCACCCAUGCGA-CGUCUGUUUCUUAGGUGAGUUUUCCGU
               ||||  ||||:|||:                  
ACUGCAAGUCCGUACACGCAAGCAGGCAAGCAAGA-------------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn1', 4: 'nn1', 5: 'nn1', 6: 'nn1', 7: 'nn0', 8: 'nn0', 9: 'nn1', 10: 'nn0', 11: 'nb0>nb0', 12: 'nb1>nb0', 13: 'nb0>nb1', 14: 'bb0s>nb0', 15: 'bn1>bn1', 16: 'bb0s>bb0s', 17: 'bb0s>nb0', 18: 'nb1>bb0s', 19: 'bb0s>bb0s', 20: 'bb0s>bb0s', 21: 'bb1s>bb0s', 22: 'bb1s>bb0s', 23: 'bb0s>nb1', 24: 'bb0s>bb0s', 25: 'bn1>bb0s', 26: 'nn0', 27: 'bn0>bb0s', 28: 'bn1>bb0s', 29: 'bn1>bb0s', 30: 'bn0>bb0s', 31: 'bn0>bb0s', 32: 'nn0', 33: 'bn1>bb0s', 34: 'bn0>bn0', 35: 'bn0>bn0', 36: 'bn1>bn1', 37: 'bn0>bn0', 38: 'bn0>bn0', 39: 'nn1', 40: 'nn0', 41: 'nn0', 42: 'nn1'}
Strand 2 variance codes:
{0: 'nn0', 1: 'bn0>bb0s', 2: 'bn1>bn1', 3: 'bn0>bb0s', 4: 'bn0>bb0s', 5: 'nn0', 6: 'bn1>bb0s', 7: 'bn1>bb0s', 8: 'bn0>bb1s', 9: 'bn0>bb1s', 10: 'nn0', 11: 'nn1', 12: 'nn0', 13: 'nn0', 14: 'nn0', 15: 'nb0>nb0', 16: 'nb0>nb1', 17: 'nb1>nb0', 18: 'nb0>bb0s', 19: 'bn0>bb0s', 20: 'bn1>bb0s', 21: 'bb0s>bb0s', 22: 'nb0>bb0s', 23: 'bb0s>nb1', 24: 'bb0s>bb0s', 25: 'bb0s>bb0s', 26: 'bb0s>bb1s', 27: 'bb0s>bb1s', 28: 'nb1>bb0s', 29: 'bb0s>bb0s', 30: 'bn0>bn0', 31: 'bn0>bn0', 32: 'bn1>bn1', 33: 'bn0>bn0', 34: 'bn0>bn0'}

Covariance layer complex:
   ****  * 111 *  1  **           *  **   *  *  *  *
CCCUAGUCCGAUACGUCGCCUAG---------UUUUUUCGUGUGUCUUACGG
              |:|| ||||         |:: |:||| :|||:|    
-------------ACGGCAGAUCCAUACACACAGGCAGGCAGGCAGGA----
               *   **   *   1111 * 1     1   *      
       ****  * 111 *   1  **  *  **   *  *  *  *
----CCCCUCACCCAUGCGA-CGUCUGUUUCUUAGGUGAGUUUUCCGU
               ||||  ||||:|||:                  
ACUGCAAGUCCGUACACGCAAGCAGGCAAGCAAGA-------------
  *   **   *   1111 * 1     1   *               

Reference strain:
A/WSN/1933
CCCUAGUCCGAUACGUCGCCUAG---------UUUUUUCGUGUGUCUUACGG
              |:|| ||||         |:: |:||| :|||:|    
-------------ACGGCAGAUCCAUACACACAGGCAGGCAGGCAGGA----
Comparison strain:
A/Vietnam/UT36282/2010[H5N1]
----CCCCUCACCCAUGCGA-CGUCUGUUUCUUAGGUGGGUUUUCCGU
               ||||  ||||:||                    
ACUGCAAGUCCAUACACGCAAGCAGGCAAGCAGGA-------------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn1', 4: 'nn1', 5: 'nn1', 6: 'nn1', 7: 'nn0', 8: 'nn0', 9: 'nn1', 10: 'nn0', 11: 'nb0>nb0', 12: 'nb1>nb0', 13: 'nb0>nb1', 14: 'bb0s>nb0', 15: 'bn1>bn1', 16: 'bb0s>bb0s', 17: 'bb0s>nb0', 18: 'nb1>bb0s', 19: 'bb0s>bb0s', 20: 'bb0s>bb0s', 21: 'bb1s>bb0s', 22: 'bb1s>bb0s', 23: 'bn0>bn0', 24: 'bn0>bn1', 25: 'bn1>bb0s', 26: 'nn0', 27: 'bn0>bb0s', 28: 'bn1>bb0s', 29: 'bn1>bb0s', 30: 'bn0>bb0s', 31: 'bn0>bb0s', 32: 'nn0', 33: 'bn1>bn0', 34: 'bn0>bn0', 35: 'bn0>bn0', 36: 'bn1>bn0', 37: 'bn0>bn0', 38: 'bn0>bn0', 39: 'nn1', 40: 'nn0', 41: 'nn0', 42: 'nn1'}
Strand 2 variance codes:
{0: 'nn0', 1: 'bn0>bb0s', 2: 'bn1>bn1', 3: 'bn0>bb0s', 4: 'bn0>bb0s', 5: 'nn0', 6: 'bn1>bb0s', 7: 'bn1>bb0s', 8: 'bn0>bb1s', 9: 'bn0>bb1s', 10: 'nn0', 11: 'nn0', 12: 'nn0', 13: 'nn0', 14: 'nn0', 15: 'nb0>nb0', 16: 'nb0>nb1', 17: 'nb1>nb0', 18: 'nb0>bb0s', 19: 'bn0>bn0', 20: 'bn1>bn0', 21: 'bb0s>bb0s', 22: 'nb0>bb0s', 23: 'bb0s>nb1', 24: 'bb0s>bb0s', 25: 'bb0s>bb0s', 26: 'bb0s>bb1s', 27: 'bb0s>bb1s', 28: 'nn1', 29: 'bn0>bn1', 30: 'bn0>bn0', 31: 'bn0>bn0', 32: 'bn0>bn1', 33: 'bn0>bn0', 34: 'bn0>bn0'}

Covariance layer complex:
   ****  * 111 *  1  **           *  **   *  *  *  *
CCCUAGUCCGAUACGUCGCCUAG---------UUUUUUCGUGUGUCUUACGG
              |:|| ||||         |:: |:||| :|||:|    
-------------ACGGCAGAUCCAUACACACAGGCAGGCAGGCAGGA----
               *   **       1111 * 1     *          
       ****  * 111 *   1  **  *  **   *  *  *  *
----CCCCUCACCCAUGCGA-CGUCUGUUUCUUAGGUGGGUUUUCCGU
               ||||  ||||:||                    
ACUGCAAGUCCAUACACGCAAGCAGGCAAGCAGGA-------------
  *   **       1111 * 1     *                   

Reference strain:
A/WSN/1933
CCCUAGUCCGAUACGUCGCCUAG---------UUUUUUCGUGUGUCUUACGG
              |:|| ||||         |:: |:||| :|||:|    
-------------ACGGCAGAUCCAUACACACAGGCAGGCAGGCAGGA----
Comparison strain:
A/Egypt/MOH-NRC-8434/2014[H5N1]
--CCCCUCACCCAUGCGA---CGUCUGUUUCUUAGGUGAGUUUCCCGA
           ||||      ||||:|||:                  
ACAGCAAGUCCGUACACACAGGCAGGCAAGCAGGA-------------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn1', 4: 'nn1', 5: 'nn1', 6: 'nn1', 7: 'nn0', 8: 'nn0', 9: 'nb1>nb1', 10: 'nb0>nb0', 11: 'nb0>nb0', 12: 'nb1>nb0', 13: 'nn0', 14: 'bn0>bn0', 15: 'bn1>bn1', 16: 'bb0s>bb0s', 17: 'bb0s>nb0', 18: 'nb1>bb0s', 19: 'bb0s>bb0s', 20: 'bb0s>bb0s', 21: 'bb1s>bb0s', 22: 'bb1s>bb0s', 23: 'bb0s>nb1', 24: 'bb0s>bb0s', 25: 'bn1>bb0s', 26: 'nn0', 27: 'bn0>bb0s', 28: 'bn1>bb0s', 29: 'bn1>bb0s', 30: 'bn0>bb0s', 31: 'bn0>bb0s', 32: 'nn0', 33: 'bn1>bb0s', 34: 'bn0>bn0', 35: 'bn0>bn0', 36: 'bn1>bn0', 37: 'bn0>bn0', 38: 'bn1>bn0', 39: 'nn1', 40: 'nn0', 41: 'nn0', 42: 'nn1'}
Strand 2 variance codes:
{0: 'nn0', 1: 'bn0>bn0', 2: 'bn1>bn1', 3: 'bn0>bb0s', 4: 'bn0>bb0s', 5: 'nn0', 6: 'bn1>bb0s', 7: 'bn1>bb0s', 8: 'bn0>bb1s', 9: 'bn0>bb1s', 10: 'nn0', 11: 'nb1>nb1', 12: 'nb0>nb0', 13: 'nb0>nb0', 14: 'nb0>nb1', 15: 'nn0', 16: 'nn0', 17: 'nn0', 18: 'nn0', 19: 'bn0>bb0s', 20: 'bn0>bb0s', 21: 'bb0s>bb0s', 22: 'nb0>bb0s', 23: 'bb0s>nb1', 24: 'bb0s>bb0s', 25: 'bb0s>bb0s', 26: 'bb0s>bb1s', 27: 'bb0s>bb1s', 28: 'nb1>bb0s', 29: 'bb0s>bb0s', 30: 'bn0>bn0', 31: 'bn0>bn0', 32: 'bn0>bn1', 33: 'bn0>bn0', 34: 'bn0>bn1'}

Covariance layer complex:
   ****  1111  *  1  **           *  **   *  * **  *
CCCUAGUCCGAUACGUCGCCUAG---------UUUUUUCGUGUGUCUUACGG
              |:|| ||||         |:: |:||| :|||:|    
-------------ACGGCAGAUCCAUACACACAGGCAGGCAGGCAGGA----
               *   **   1111       1     1          
     ****  1111  *     1  **  *  **   *  * **  *
--CCCCUCACCCAUGCGA---CGUCUGUUUCUUAGGUGAGUUUCCCGA
           ||||      ||||:|||:                  
ACAGCAAGUCCGUACACACAGGCAGGCAAGCAGGA-------------
  *   **   1111       1     1                   

Reference strain:
A/WSN/1933
CCCUAGUCCGAUACGUCGCCUAG---------UUUUUUCGUGUGUCUUACGG
              |:|| ||||         |:: |:||| :|||:|    
-------------ACGGCAGAUCCAUACACACAGGCAGGCAGGCAGGA----
Comparison strain:
A/Changsha/1/2014[H5N6]
--CCCCUCACCCAUGCGA---CGUCUGUCUCUUAGGUGGGUUUUCCGU
           |||| |    ||||:||                    
ACUGCAAGUCCGUACACACAAGCAGGCAAGCAGGA-------------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn1', 4: 'nn1', 5: 'nn1', 6: 'nn1', 7: 'nn0', 8: 'nn0', 9: 'nb1>nb1', 10: 'nb0>nb0', 11: 'nb0>nb0', 12: 'nb1>nb0', 13: 'nn0', 14: 'bb0s>nb0', 15: 'bn1>bn1', 16: 'bb0s>bb0s', 17: 'bb0s>nb0', 18: 'nb1>bb0s', 19: 'bb0s>bb0s', 20: 'bb0s>bb0s', 21: 'bb1s>bb0s', 22: 'bb1s>bb0s', 23: 'bn1>bn0', 24: 'bn0>bn1', 25: 'bn1>bb0s', 26: 'nn0', 27: 'bn0>bb0s', 28: 'bn1>bb0s', 29: 'bn1>bb0s', 30: 'bn0>bb0s', 31: 'bn0>bb0s', 32: 'nn0', 33: 'bn1>bn0', 34: 'bn0>bn0', 35: 'bn0>bn0', 36: 'bn1>bn0', 37: 'bn0>bn0', 38: 'bn0>bn0', 39: 'nn1', 40: 'nn0', 41: 'nn0', 42: 'nn1'}
Strand 2 variance codes:
{0: 'nn0', 1: 'bn0>bb0s', 2: 'bn1>bn1', 3: 'bn0>bb0s', 4: 'bn0>bb0s', 5: 'nn0', 6: 'bn1>bb0s', 7: 'bn1>bb0s', 8: 'bn0>bb1s', 9: 'bn0>bb1s', 10: 'nn0', 11: 'nb1>nb1', 12: 'nb0>nb0', 13: 'nb0>nb0', 14: 'nb0>nb1', 15: 'nn0', 16: 'nb0>bb0s', 17: 'nn0', 18: 'nn0', 19: 'bn0>bn1', 20: 'bn1>bn0', 21: 'bb0s>bb0s', 22: 'nb0>bb0s', 23: 'bb0s>nb1', 24: 'bb0s>bb0s', 25: 'bb0s>bb0s', 26: 'bb0s>bb1s', 27: 'bb0s>bb1s', 28: 'nn1', 29: 'bn0>bn1', 30: 'bn0>bn0', 31: 'bn0>bn0', 32: 'bn0>bn1', 33: 'bn0>bn0', 34: 'bn0>bn0'}

Covariance layer complex:
   ****  1111  *  1  **         * *  **   *  *  *  *
CCCUAGUCCGAUACGUCGCCUAG---------UUUUUUCGUGUGUCUUACGG
              |:|| ||||         |:: |:||| :|||:|    
-------------ACGGCAGAUCCAUACACACAGGCAGGCAGGCAGGA----
               *   **   1111 1   * 1     *          
     ****  1111  *     1  *** *  **   *  *  *  *
--CCCCUCACCCAUGCGA---CGUCUGUCUCUUAGGUGGGUUUUCCGU
           |||| |    ||||:||                    
ACUGCAAGUCCGUACACACAAGCAGGCAAGCAGGA-------------
  *   **   1111 1   * 1     *                   

Reference strain:
A/WSN/1933
CCCUAGUCCGAUACGUCGCCUAG---------UUUUUUCGUGUGUCUUACGG
              |:|| ||||         |:: |:||| :|||:|    
-------------ACGGCAGAUCCAUACACACAGGCAGGCAGGCAGGA----
Comparison strain:
A/Yunnan/0127/2015[H5N6]
-----CCCCUCACCCAUGCGACGUCUG-UC-UCUUAGGUGGGUUUUCCGU
                     ||||:| || ||:|               
ACUGUAAGUCCGUACACACAAGCAGGCAAGCAGGA---------------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn1', 4: 'nn1', 5: 'nn1', 6: 'nn1', 7: 'nn0', 8: 'nn0', 9: 'nn1', 10: 'nn0', 11: 'nn0', 12: 'nn1', 13: 'nn0', 14: 'bn0>bn0', 15: 'bn1>bn1', 16: 'bb0s>bb0s', 17: 'bb0s>nb0', 18: 'nb1>bb0s', 19: 'bb0s>bb0s', 20: 'bb0s>bb0s', 21: 'bb1s>bb0s', 22: 'bb1s>nb1', 23: 'bb1s>bb0s', 24: 'bb0s>bb0s', 25: 'bb1s>bb0s', 26: 'nb0>bb0s', 27: 'bb0s>bb0s', 28: 'bn1>bb0s', 29: 'bn1>bb0s', 30: 'bn0>bb0s', 31: 'bn0>bn0', 32: 'nn0', 33: 'bn1>bb0s', 34: 'bn0>bn0', 35: 'bn0>bb0s', 36: 'bn1>bb0s', 37: 'bn0>bb0s', 38: 'bn0>bb0s', 39: 'nn1', 40: 'nn0', 41: 'nn0', 42: 'nn1'}
Strand 2 variance codes:
{0: 'nn0', 1: 'bn0>bn0', 2: 'bn1>bn1', 3: 'bn0>bb0s', 4: 'bn1>bb0s', 5: 'nn0', 6: 'bn1>bb0s', 7: 'bn1>bb0s', 8: 'bn0>bb1s', 9: 'bn0>bb1s', 10: 'nn0', 11: 'nn1', 12: 'nn0', 13: 'nn0', 14: 'nn0', 15: 'nn0', 16: 'nn0', 17: 'nn0', 18: 'nn0', 19: 'bn0>bb1s', 20: 'bn1>bb0s', 21: 'bb0s>bb0s', 22: 'nb0>bb0s', 23: 'bb0s>nb1', 24: 'bb0s>bb0s', 25: 'bb0s>bb0s', 26: 'bb0s>bb1s', 27: 'bn0>bn0', 28: 'nb1>bb1s', 29: 'bb0s>bb1s', 30: 'bn0>bn0', 31: 'bb0s>bb0s', 32: 'bb0s>bb1s', 33: 'bb0s>nb0', 34: 'bb0s>bb0s'}

Covariance layer complex:
   ****  *  *  *  1  **         * *1 **   *  *  *  *
CCCUAGUCCGAUACGUCGCCUAG---------UUUUUUCGUGUGUCUUACGG
              |:|| ||||         |:: |:||| :|||:|    
-------------ACGGCAGAUCCAUACACACAGGCAGGCAGGCAGGA----
               * * **   *        * 1     1          
        ****  *  *  *  1  * **  *1 **   *  *  *  *
-----CCCCUCACCCAUGCGACGUCUG-UC-UCUUAGGUGGGUUUUCCGU
                     ||||:| || ||:|               
ACUGUAAGUCCGUACACACAAGCAGGCAAGCAGGA---------------
  * * **   *        * 1     1                     

Reference strain:
A/WSN/1933
CCCUAGUCCGAUACGUCGCCUAG---------UUUUUUCGUGUGUCUUACGG
              |:|| ||||         |:: |:||| :|||:|    
-------------ACGGCAGAUCCAUACACACAGGCAGGCAGGCAGGA----
Comparison strain:
A/duck/Mongolia/54+47/01[H5N1]
-----UCCCUCACCUAUGCGACGUCUGUUUCUCAGGUGAGUUUUCCGU
                     ||||:|||:                  
ACAGCAAGUCCGUACACACACGCAGGCAAGCAGGA-------------

Strand 1 variance codes:
{0: 'nn1', 1: 'nn0', 2: 'nn0', 3: 'nn1', 4: 'nn1', 5: 'nn1', 6: 'nn1', 7: 'nn0', 8: 'nn0', 9: 'nn1', 10: 'nn0', 11: 'nn0', 12: 'nn1', 13: 'nn0', 14: 'bn0>bn0', 15: 'bn1>bn1', 16: 'bb0s>bb0s', 17: 'bb0s>nb0', 18: 'nb1>bb0s', 19: 'bb0s>bb0s', 20: 'bb0s>bb0s', 21: 'bb1s>bb0s', 22: 'bb1s>bb0s', 23: 'bb0s>nb1', 24: 'bb0s>bb0s', 25: 'bn1>bb0s', 26: 'nn0', 27: 'bn1>bb0s', 28: 'bn1>bb0s', 29: 'bn1>bb0s', 30: 'bn0>bb0s', 31: 'bn0>bb0s', 32: 'nn0', 33: 'bn1>bb0s', 34: 'bn0>bn0', 35: 'bn0>bn0', 36: 'bn1>bn0', 37: 'bn0>bn0', 38: 'bn0>bn0', 39: 'nn1', 40: 'nn0', 41: 'nn0', 42: 'nn1'}
Strand 2 variance codes:
{0: 'nn0', 1: 'bn0>bn0', 2: 'bn1>bn1', 3: 'bn0>bb0s', 4: 'bn0>bb0s', 5: 'nn0', 6: 'bn1>bb0s', 7: 'bn1>bb0s', 8: 'bn0>bb1s', 9: 'bn0>bb1s', 10: 'nn0', 11: 'nn1', 12: 'nn0', 13: 'nn0', 14: 'nn0', 15: 'nn0', 16: 'nn0', 17: 'nn0', 18: 'nn0', 19: 'bn0>bb0s', 20: 'bn1>bb0s', 21: 'bb0s>bb0s', 22: 'nb0>bb0s', 23: 'bb0s>nb1', 24: 'bb0s>bb0s', 25: 'bb0s>bb0s', 26: 'bb0s>bb1s', 27: 'bb0s>bb1s', 28: 'nb1>bb0s', 29: 'bb0s>bb0s', 30: 'bn0>bn0', 31: 'bn0>bn0', 32: 'bn0>bn1', 33: 'bn0>bn0', 34: 'bn0>bn0'}

Covariance layer complex:
*  ****  *  *  *  1  **           * ***   *  *  *  *
CCCUAGUCCGAUACGUCGCCUAG---------UUUUUUCGUGUGUCUUACGG
              |:|| ||||         |:: |:||| :|||:|    
-------------ACGGCAGAUCCAUACACACAGGCAGGCAGGCAGGA----
               *   **   *        * 1     1          
     *  ****  *  *  *  1  **  * ***   *  *  *  *
-----UCCCUCACCUAUGCGACGUCUGUUUCUCAGGUGAGUUUUCCGU
                     ||||:|||:                  
ACAGCAAGUCCGUACACACACGCAGGCAAGCAGGA-------------
  *   **   *        * 1     1                   

Reference strain:
A/WSN/1933
CCCUAGUCCGAUACGUCGCCUAG---------UUUUUUCGUGUGUCUUACGG
              |:|| ||||         |:: |:||| :|||:|    
-------------ACGGCAGAUCCAUACACACAGGCAGGCAGGCAGGA----
Comparison strain:
A/duck/Hokkaido/Vac-3/2007[H5N1]
-----UCCUUCACCUAUGCGACGUCUGUUUCUUAGGUGGGUUUUCCGU
                     ||||:|||:                  
ACAGCAAGUCCAUAUACACAAGCAGGCAAGCAGGA-------------

Strand 1 variance codes:
{0: 'nn1', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn1', 5: 'nn1', 6: 'nn1', 7: 'nn0', 8: 'nn0', 9: 'nn1', 10: 'nn0', 11: 'nn0', 12: 'nn1', 13: 'nn0', 14: 'bn0>bn0', 15: 'bn1>bn1', 16: 'bb0s>bb0s', 17: 'bb0s>nb0', 18: 'nb1>bb0s', 19: 'bb0s>bb0s', 20: 'bb0s>bb0s', 21: 'bb1s>bb0s', 22: 'bb1s>bb0s', 23: 'bb0s>nb1', 24: 'bb0s>bb0s', 25: 'bn1>bb0s', 26: 'nn0', 27: 'bn0>bb0s', 28: 'bn1>bb0s', 29: 'bn1>bb0s', 30: 'bn0>bb0s', 31: 'bn0>bb0s', 32: 'nn0', 33: 'bn1>bb0s', 34: 'bn0>bn0', 35: 'bn0>bn0', 36: 'bn1>bn0', 37: 'bn0>bn0', 38: 'bn0>bn0', 39: 'nn1', 40: 'nn0', 41: 'nn0', 42: 'nn1'}
Strand 2 variance codes:
{0: 'nn0', 1: 'bn0>bn0', 2: 'bn1>bn1', 3: 'bn0>bb0s', 4: 'bn0>bb0s', 5: 'nn0', 6: 'bn1>bb0s', 7: 'bn1>bb0s', 8: 'bn0>bb1s', 9: 'bn0>bb1s', 10: 'nn0', 11: 'nn0', 12: 'nn0', 13: 'nn0', 14: 'nn1', 15: 'nn0', 16: 'nn0', 17: 'nn0', 18: 'nn0', 19: 'bn0>bb0s', 20: 'bn1>bb0s', 21: 'bb0s>bb0s', 22: 'nb0>bb0s', 23: 'bb0s>nb1', 24: 'bb0s>bb0s', 25: 'bb0s>bb0s', 26: 'bb0s>bb1s', 27: 'bb0s>bb1s', 28: 'nb1>bb0s', 29: 'bb0s>bb0s', 30: 'bn0>bn0', 31: 'bn0>bn0', 32: 'bn0>bn1', 33: 'bn0>bn0', 34: 'bn0>bn0'}

Covariance layer complex:
*   ***  *  *  *  1  **           *  **   *  *  *  *
CCCUAGUCCGAUACGUCGCCUAG---------UUUUUUCGUGUGUCUUACGG
              |:|| ||||         |:: |:||| :|||:|    
-------------ACGGCAGAUCCAUACACACAGGCAGGCAGGCAGGA----
               *   **      *     * 1     1          
     *   ***  *  *  *  1  **  *  **   *  *  *  *
-----UCCUUCACCUAUGCGACGUCUGUUUCUUAGGUGGGUUUUCCGU
                     ||||:|||:                  
ACAGCAAGUCCAUAUACACAAGCAGGCAAGCAGGA-------------
  *   **      *     * 1     1                   

Reference strain:
A/WSN/1933
CCCUAGUCCGAUACGUCGCCUAG---------UUUUUUCGUGUGUCUUACGG
              |:|| ||||         |:: |:||| :|||:|    
-------------ACGGCAGAUCCAUACACACAGGCAGGCAGGCAGGA----
Comparison strain:
A/duck/Moscow/4182-C/2017[H5N3]
-----CCCCUCACCUAUGCGACGUCUGUUCC-UCAGGUGAGUUUUUCGU
                     ||||: |||| ||               
ACAGCAAGCCCGUACACACAAGCAGG-AAGGCAGGA-------------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn1', 4: 'nn1', 5: 'nn1', 6: 'nn1', 7: 'nn0', 8: 'nn0', 9: 'nn1', 10: 'nn0', 11: 'nn0', 12: 'nn1', 13: 'nn0', 14: 'bn0>bn0', 15: 'bn1>bn1', 16: 'bb0s>bb0s', 17: 'bb0s>nb0', 18: 'nb1>bb0s', 19: 'bb0s>bb0s', 20: 'bb0s>bb0s', 21: 'bn1>bn1', 22: 'bb1s>bb1s', 23: 'bb0s>bb0s', 24: 'bb1s>nb0', 25: 'bb1s>bb0s', 26: 'nb0>bb0s', 27: 'bb1s>bb0s', 28: 'bn1>bb0s', 29: 'bn1>bb0s', 30: 'bn0>bb1s', 31: 'bn0>bb0s', 32: 'nn0', 33: 'bn1>bb0s', 34: 'bn0>bn0', 35: 'bn0>bb0s', 36: 'bn1>bb0s', 37: 'bn0>bn0', 38: 'bn0>bn0', 39: 'nn1', 40: 'nn0', 41: 'nn0', 42: 'nn1'}
Strand 2 variance codes:
{0: 'nn0', 1: 'bn0>bn0', 2: 'bn1>bn1', 3: 'bn0>bb0s', 4: 'bn0>bb0s', 5: 'nn0', 6: 'bn1>bb0s', 7: 'bn1>bb0s', 8: 'bn1>bn1', 9: 'bn0>bb1s', 10: 'nn0', 11: 'nn1', 12: 'nn0', 13: 'nn0', 14: 'nn0', 15: 'nn0', 16: 'nn0', 17: 'nn0', 18: 'nn0', 19: 'bn0>bb0s', 20: 'bn1>bb1s', 21: 'bb0s>bb0s', 22: 'nb0>bb0s', 23: 'bb0s>nb1', 24: 'bb0s>bb0s', 25: 'bb0s>bb0s', 26: 'bb1s>bb1s', 27: 'bb0s>bb0s', 28: 'nb0>bb1s', 29: 'bb0s>bb1s', 30: 'bn0>bn0', 31: 'bb0s>nb0', 32: 'bb0s>bb1s', 33: 'bn0>bn0', 34: 'bn0>bn0'}

Covariance layer complex:
   ****  *  *  *  1  **          **1***   *  *  *  *
CCCUAGUCCGAUACGUCGCCUAG---------UUUUUUCGUGUGUCUUACGG
              |:|| ||||         |:: |:||| :|||:|    
-------------ACGGCAGAUCCAUACACACAGGCAGGCAGGCAGGA----
               *   ***  *        * 1   * 1          
        ****  *  *  *  1  ** ** 1***   *  *  *  *
-----CCCCUCACCUAUGCGACGUCUGUUCC-UCAGGUGAGUUUUUCGU
                     ||||: |||| ||               
ACAGCAAGCCCGUACACACAAGCAGG-AAGGCAGGA-------------
  *   ***  *        * 1    * 1                   

Reference strain:
A/WSN/1933
CCCUAGUCCGAUACGUCGCCUAG---------UUUUUUCGUGUGUCUUACGG
              |:|| ||||         |:: |:||| :|||:|    
-------------ACGGCAGAUCCAUACACACAGGCAGGCAGGCAGGA----
Comparison strain:
A/duck/Zhejiang/6DK19-MA/2013[H5N2]
--CCCCUCACCCAUGCGA---CGUCUGUUUCUUAGGUGGGUUUUCCGA
           |||| |    ||||:||                    
ACUGCAAGUCCGUACACACAAGCAGGCAAGCAGGA-------------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn1', 4: 'nn1', 5: 'nn1', 6: 'nn1', 7: 'nn0', 8: 'nn0', 9: 'nb1>nb1', 10: 'nb0>nb0', 11: 'nb0>nb0', 12: 'nb1>nb0', 13: 'nn0', 14: 'bb0s>nb0', 15: 'bn1>bn1', 16: 'bb0s>bb0s', 17: 'bb0s>nb0', 18: 'nb1>bb0s', 19: 'bb0s>bb0s', 20: 'bb0s>bb0s', 21: 'bb1s>bb0s', 22: 'bb1s>bb0s', 23: 'bn0>bn0', 24: 'bn0>bn1', 25: 'bn1>bb0s', 26: 'nn0', 27: 'bn0>bb0s', 28: 'bn1>bb0s', 29: 'bn1>bb0s', 30: 'bn0>bb0s', 31: 'bn0>bb0s', 32: 'nn0', 33: 'bn1>bn0', 34: 'bn0>bn0', 35: 'bn0>bn0', 36: 'bn1>bn0', 37: 'bn0>bn0', 38: 'bn0>bn0', 39: 'nn1', 40: 'nn0', 41: 'nn0', 42: 'nn1'}
Strand 2 variance codes:
{0: 'nn0', 1: 'bn0>bb0s', 2: 'bn1>bn1', 3: 'bn0>bb0s', 4: 'bn0>bb0s', 5: 'nn0', 6: 'bn1>bb0s', 7: 'bn1>bb0s', 8: 'bn0>bb1s', 9: 'bn0>bb1s', 10: 'nn0', 11: 'nb1>nb1', 12: 'nb0>nb0', 13: 'nb0>nb0', 14: 'nb0>nb1', 15: 'nn0', 16: 'nb0>bb0s', 17: 'nn0', 18: 'nn0', 19: 'bn0>bn0', 20: 'bn1>bn0', 21: 'bb0s>bb0s', 22: 'nb0>bb0s', 23: 'bb0s>nb1', 24: 'bb0s>bb0s', 25: 'bb0s>bb0s', 26: 'bb0s>bb1s', 27: 'bb0s>bb1s', 28: 'nn1', 29: 'bn0>bn1', 30: 'bn0>bn0', 31: 'bn0>bn0', 32: 'bn0>bn1', 33: 'bn0>bn0', 34: 'bn0>bn0'}

Covariance layer complex:
   ****  1111  *  1  **           *  **   *  *  *  *
CCCUAGUCCGAUACGUCGCCUAG---------UUUUUUCGUGUGUCUUACGG
              |:|| ||||         |:: |:||| :|||:|    
-------------ACGGCAGAUCCAUACACACAGGCAGGCAGGCAGGA----
               *   **   1111 1   * 1     *          
     ****  1111  *     1  **  *  **   *  *  *  *
--CCCCUCACCCAUGCGA---CGUCUGUUUCUUAGGUGGGUUUUCCGA
           |||| |    ||||:||                    
ACUGCAAGUCCGUACACACAAGCAGGCAAGCAGGA-------------
  *   **   1111 1   * 1     *                   

Reference strain:
A/WSN/1933
CCCUAGUCCGAUACGUCGCCUAG---------UUUUUUCGUGUGUCUUACGG
              |:|| ||||         |:: |:||| :|||:|    
-------------ACGGCAGAUCCAUACACACAGGCAGGCAGGCAGGA----
Comparison strain:
A/Anas Platyrhynchos/Belgium/10811-6/2019[H5N6]
-----CCCUUCACCUAUACGACGUCUGUUCCUCAGGUGAGUCUUUCGU
                     ||||:|||| ||               
ACAGCGAGUCCGUACACACAAGCAGGCAAGCAGGA-------------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn1', 5: 'nn1', 6: 'nn1', 7: 'nn0', 8: 'nn0', 9: 'nn1', 10: 'nn0', 11: 'nn0', 12: 'nn0', 13: 'nn0', 14: 'bn0>bn0', 15: 'bn1>bn1', 16: 'bb0s>bb0s', 17: 'bb0s>nb0', 18: 'nb1>bb0s', 19: 'bb0s>bb0s', 20: 'bb0s>bb0s', 21: 'bb1s>bb0s', 22: 'bb1s>bb0s', 23: 'bb0s>nb1', 24: 'bb1s>bb0s', 25: 'bn1>bb0s', 26: 'nb0>bb0s', 27: 'bb1s>bb0s', 28: 'bn1>bb0s', 29: 'bn1>bb0s', 30: 'bn0>bb0s', 31: 'bn0>bb0s', 32: 'nn0', 33: 'bn1>bb0s', 34: 'bn0>bn0', 35: 'bn0>bb0s', 36: 'bn0>bb0s', 37: 'bn0>bn0', 38: 'bn0>bn0', 39: 'nn1', 40: 'nn0', 41: 'nn0', 42: 'nn1'}
Strand 2 variance codes:
{0: 'nn0', 1: 'bn0>bn0', 2: 'bn1>bn1', 3: 'bn0>bb0s', 4: 'bn0>bb0s', 5: 'nn1', 6: 'bn1>bb0s', 7: 'bn1>bb0s', 8: 'bn0>bb1s', 9: 'bn0>bb1s', 10: 'nn0', 11: 'nn1', 12: 'nn0', 13: 'nn0', 14: 'nn0', 15: 'nn0', 16: 'nn0', 17: 'nn0', 18: 'nn0', 19: 'bn0>bb0s', 20: 'bn1>bb1s', 21: 'bb0s>bb0s', 22: 'nb0>bb0s', 23: 'bb0s>nb1', 24: 'bb0s>bb0s', 25: 'bb0s>bb0s', 26: 'bb0s>bb1s', 27: 'bb0s>bb1s', 28: 'nb1>bb0s', 29: 'bb0s>bb1s', 30: 'bn0>bn0', 31: 'bb0s>nb0', 32: 'bb0s>bb1s', 33: 'bn0>bn0', 34: 'bn0>bn0'}

Covariance layer complex:
    ***  *     *  1  **          **1***   *     *  *
CCCUAGUCCGAUACGUCGCCUAG---------UUUUUUCGUGUGUCUUACGG
              |:|| ||||         |:: |:||| :|||:|    
-------------ACGGCAGAUCCAUACACACAGGCAGGCAGGCAGGA----
               *  ***   *        * 1     1          
         ***  *     *  1  ** **1***   *     *  *
-----CCCUUCACCUAUACGACGUCUGUUCCUCAGGUGAGUCUUUCGU
                     ||||:|||| ||               
ACAGCGAGUCCGUACACACAAGCAGGCAAGCAGGA-------------
  *  ***   *        * 1     1                   

Reference strain:
A/WSN/1933
CCCUAGUCCGAUACGUCGCCUAG---------UUUUUUCGUGUGUCUUACGG
              |:|| ||||         |:: |:||| :|||:|    
-------------ACGGCAGAUCCAUACACACAGGCAGGCAGGCAGGA----
Comparison strain:
A/swine/Banten/UT2071/2005[H5N1]
--CCCCUCACCCAUGCGA---CGUCUGUUUCUUAGGUGAGUUUUCCGU
           |||| |    ||||:||                    
ACUGCAAGUCCGUACACACAAGCAGGCAAGCAGGA-------------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn1', 4: 'nn1', 5: 'nn1', 6: 'nn1', 7: 'nn0', 8: 'nn0', 9: 'nb1>nb1', 10: 'nb0>nb0', 11: 'nb0>nb0', 12: 'nb1>nb0', 13: 'nn0', 14: 'bb0s>nb0', 15: 'bn1>bn1', 16: 'bb0s>bb0s', 17: 'bb0s>nb0', 18: 'nb1>bb0s', 19: 'bb0s>bb0s', 20: 'bb0s>bb0s', 21: 'bb1s>bb0s', 22: 'bb1s>bb0s', 23: 'bn0>bn0', 24: 'bn0>bn1', 25: 'bn1>bb0s', 26: 'nn0', 27: 'bn0>bb0s', 28: 'bn1>bb0s', 29: 'bn1>bb0s', 30: 'bn0>bb0s', 31: 'bn0>bb0s', 32: 'nn0', 33: 'bn1>bn0', 34: 'bn0>bn0', 35: 'bn0>bn0', 36: 'bn1>bn0', 37: 'bn0>bn0', 38: 'bn0>bn0', 39: 'nn1', 40: 'nn0', 41: 'nn0', 42: 'nn1'}
Strand 2 variance codes:
{0: 'nn0', 1: 'bn0>bb0s', 2: 'bn1>bn1', 3: 'bn0>bb0s', 4: 'bn0>bb0s', 5: 'nn0', 6: 'bn1>bb0s', 7: 'bn1>bb0s', 8: 'bn0>bb1s', 9: 'bn0>bb1s', 10: 'nn0', 11: 'nb1>nb1', 12: 'nb0>nb0', 13: 'nb0>nb0', 14: 'nb0>nb1', 15: 'nn0', 16: 'nb0>bb0s', 17: 'nn0', 18: 'nn0', 19: 'bn0>bn0', 20: 'bn1>bn0', 21: 'bb0s>bb0s', 22: 'nb0>bb0s', 23: 'bb0s>nb1', 24: 'bb0s>bb0s', 25: 'bb0s>bb0s', 26: 'bb0s>bb1s', 27: 'bb0s>bb1s', 28: 'nn1', 29: 'bn0>bn1', 30: 'bn0>bn0', 31: 'bn0>bn0', 32: 'bn0>bn1', 33: 'bn0>bn0', 34: 'bn0>bn0'}

Covariance layer complex:
   ****  1111  *  1  **           *  **   *  *  *  *
CCCUAGUCCGAUACGUCGCCUAG---------UUUUUUCGUGUGUCUUACGG
              |:|| ||||         |:: |:||| :|||:|    
-------------ACGGCAGAUCCAUACACACAGGCAGGCAGGCAGGA----
               *   **   1111 1   * 1     *          
     ****  1111  *     1  **  *  **   *  *  *  *
--CCCCUCACCCAUGCGA---CGUCUGUUUCUUAGGUGAGUUUUCCGU
           |||| |    ||||:||                    
ACUGCAAGUCCGUACACACAAGCAGGCAAGCAGGA-------------
  *   **   1111 1   * 1     *                   

Reference strain:
A/WSN/1933
CCCUAGUCCGAUACGUCGCCUAG---------UUUUUUCGUGUGUCUUACGG
              |:|| ||||         |:: |:||| :|||:|    
-------------ACGGCAGAUCCAUACACACAGGCAGGCAGGCAGGA----
Comparison strain:
A/swine/Zhejiang/SW57/2015[H5N1]
CCCCUCACCAAUGCGACGUCUGUUUCUUAGAUGAGUUU-UCCGC-------
                                  |||: ||||        
----------------ACUGCAAGUCCGUACACACAAGCAGGCAAGCAGGA

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn1', 4: 'nn1', 5: 'nn1', 6: 'nn1', 7: 'nn0', 8: 'nn0', 9: 'nn1', 10: 'nn0', 11: 'nn0', 12: 'nn1', 13: 'nn0', 14: 'bn0>bn0', 15: 'bn1>bn1', 16: 'bn0>bn0', 17: 'bn0>bn0', 18: 'nn1', 19: 'bn0>bn1', 20: 'bn0>bn1', 21: 'bn1>bn0', 22: 'bn1>bn0', 23: 'bn0>bb0s', 24: 'bn0>bb1s', 25: 'bn1>bb0s', 26: 'nn0', 27: 'bn0>bb0s', 28: 'bn1>bb0s', 29: 'bn1>bb0s', 30: 'bn1>bb0s', 31: 'bn0>bn0', 32: 'nn0', 33: 'bn1>bn0', 34: 'bb0s>nb0', 35: 'bb0s>bb0s', 36: 'bb1s>bb1s', 37: 'bb0s>bb0s', 38: 'bb0s>bb0s', 39: 'nb1>bb0s', 40: 'nb0>bb0s', 41: 'nb0>bb0s', 42: 'nn1'}
Strand 2 variance codes:
{0: 'nn0', 1: 'bn0>bn0', 2: 'bn1>bn1', 3: 'bn0>bn0', 4: 'bn0>bn0', 5: 'nn0', 6: 'bn1>bn0', 7: 'bn1>bn0', 8: 'bn0>bn1', 9: 'bn0>bn1', 10: 'nn0', 11: 'nn1', 12: 'nn0', 13: 'nn0', 14: 'nn0', 15: 'nn0', 16: 'nn0', 17: 'nn0', 18: 'nb0>bb0s', 19: 'bb0s>bb0s', 20: 'bb1s>bb1s', 21: 'bb0s>bb0s', 22: 'nn0', 23: 'bb0s>bb0s', 24: 'bb0s>nb1', 25: 'bb0s>nb0', 26: 'bb0s>nb0', 27: 'bn0>bn0', 28: 'nn1', 29: 'bn0>bn1', 30: 'bn0>bb0s', 31: 'bn0>bb0s', 32: 'bn0>bb1s', 33: 'bn0>bb0s', 34: 'bn0>bb0s'}

Covariance layer complex:
   ****  *  *  *  *  **           *  ***  *  *  111*
CCCUAGUCCGAUACGUCGCCUAG---------UUUUUUCGUGUGUCUUACGG
              |:|| ||||         |:: |:||| :|||:|    
-------------ACGGCAGAUCCAUACACACAGGCAGGCAGGCAGGA----
               *   **   *      1 *       *          
   ****  *  *  *  *  **  *  ***  *  *   111*       
CCCCUCACCAAUGCGACGUCUGUUUCUUAGAUGAGUUU-UCCGC-------
                                  |||: ||||        
----------------ACUGCAAGUCCGUACACACAAGCAGGCAAGCAGGA
                  *   **   *      1 *       *      

Reference strain:
A/WSN/1933
CCCUAGUCCGAUACGUCGCCUAG---------UUUUUUCGUGUGUCUUACGG
              |:|| ||||         |:: |:||| :|||:|    
-------------ACGGCAGAUCCAUACACACAGGCAGGCAGGCAGGA----
Comparison strain:
A/Crow/Aghakhan/2017[H5N8]
-----CCCUUCACCCAUGCGACGUCUGUUUCUUAGGUGGGUUUUCCGU
                     ||||:|||:                  
ACAGCGAGUCCGUACACACAAGCAGGCAAGCAGGA-------------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn1', 5: 'nn1', 6: 'nn1', 7: 'nn0', 8: 'nn0', 9: 'nn1', 10: 'nn0', 11: 'nn0', 12: 'nn1', 13: 'nn0', 14: 'bn0>bn0', 15: 'bn1>bn1', 16: 'bb0s>bb0s', 17: 'bb0s>nb0', 18: 'nb1>bb0s', 19: 'bb0s>bb0s', 20: 'bb0s>bb0s', 21: 'bb1s>bb0s', 22: 'bb1s>bb0s', 23: 'bb0s>nb1', 24: 'bb0s>bb0s', 25: 'bn1>bb0s', 26: 'nn0', 27: 'bn0>bb0s', 28: 'bn1>bb0s', 29: 'bn1>bb0s', 30: 'bn0>bb0s', 31: 'bn0>bb0s', 32: 'nn0', 33: 'bn1>bb0s', 34: 'bn0>bn0', 35: 'bn0>bn0', 36: 'bn1>bn0', 37: 'bn0>bn0', 38: 'bn0>bn0', 39: 'nn1', 40: 'nn0', 41: 'nn0', 42: 'nn1'}
Strand 2 variance codes:
{0: 'nn0', 1: 'bn0>bn0', 2: 'bn1>bn1', 3: 'bn0>bb0s', 4: 'bn0>bb0s', 5: 'nn1', 6: 'bn1>bb0s', 7: 'bn1>bb0s', 8: 'bn0>bb1s', 9: 'bn0>bb1s', 10: 'nn0', 11: 'nn1', 12: 'nn0', 13: 'nn0', 14: 'nn0', 15: 'nn0', 16: 'nn0', 17: 'nn0', 18: 'nn0', 19: 'bn0>bb0s', 20: 'bn1>bb0s', 21: 'bb0s>bb0s', 22: 'nb0>bb0s', 23: 'bb0s>nb1', 24: 'bb0s>bb0s', 25: 'bb0s>bb0s', 26: 'bb0s>bb1s', 27: 'bb0s>bb1s', 28: 'nb1>bb0s', 29: 'bb0s>bb0s', 30: 'bn0>bn0', 31: 'bn0>bn0', 32: 'bn0>bn1', 33: 'bn0>bn0', 34: 'bn0>bn0'}

Covariance layer complex:
    ***  *  *  *  1  **           *  **   *  *  *  *
CCCUAGUCCGAUACGUCGCCUAG---------UUUUUUCGUGUGUCUUACGG
              |:|| ||||         |:: |:||| :|||:|    
-------------ACGGCAGAUCCAUACACACAGGCAGGCAGGCAGGA----
               *  ***   *        * 1     1          
         ***  *  *  *  1  **  *  **   *  *  *  *
-----CCCUUCACCCAUGCGACGUCUGUUUCUUAGGUGGGUUUUCCGU
                     ||||:|||:                  
ACAGCGAGUCCGUACACACAAGCAGGCAAGCAGGA-------------
  *  ***   *        * 1     1                   

Reference strain:
A/WSN/1933
CCCUAGUCCGAUACGUCGCCUAG---------UUUUUUCGUGUGUCUUACGG
              |:|| ||||         |:: |:||| :|||:|    
-------------ACGGCAGAUCCAUACACACAGGCAGGCAGGCAGGA----
Comparison strain:
A/swan/Krasnodar/44/2017[H5N8]
--CCCCUCACCCAUGCGA---CGUCUGUU--UCUUAGGUGGGUUUUCCGU
     ||||  |||| |    ||||:|||  ||:|               
ACAGCGAGUCCGUACACACAAGCAGGCAAGCAGGA---------------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nb1>nb1', 4: 'nb1>bb1s', 5: 'nb1>bb1s', 6: 'nb1>bb0s', 7: 'nn0', 8: 'nn0', 9: 'nb1>nb1', 10: 'nb0>nb0', 11: 'nb0>nb0', 12: 'nb1>nb0', 13: 'nn0', 14: 'bb0s>nb0', 15: 'bn1>bn1', 16: 'bb0s>bb0s', 17: 'bb0s>nb0', 18: 'nb1>bb0s', 19: 'bb0s>bb0s', 20: 'bb0s>bb0s', 21: 'bb1s>bb0s', 22: 'bb1s>bb0s', 23: 'bb0s>nb1', 24: 'bb0s>bb0s', 25: 'bb1s>bb0s', 26: 'nb0>bb0s', 27: 'bb0s>bb0s', 28: 'bn1>bb0s', 29: 'bn1>bb0s', 30: 'bn0>bb0s', 31: 'bn0>bb0s', 32: 'nn0', 33: 'bn1>bn0', 34: 'bn0>bn0', 35: 'bn0>bb0s', 36: 'bn1>bb0s', 37: 'bn0>bb0s', 38: 'bn0>bb0s', 39: 'nn1', 40: 'nn0', 41: 'nn0', 42: 'nn1'}
Strand 2 variance codes:
{0: 'nn0', 1: 'bn0>bb0s', 2: 'bn1>bn1', 3: 'bn0>bb0s', 4: 'bn0>bb0s', 5: 'nb1>nb1', 6: 'bb1s>nb1', 7: 'bb1s>nb1', 8: 'bb0s>nb1', 9: 'bn0>bb1s', 10: 'nn0', 11: 'nb1>nb1', 12: 'nb0>nb0', 13: 'nb0>nb0', 14: 'nb0>nb1', 15: 'nn0', 16: 'nb0>bb0s', 17: 'nn0', 18: 'nn0', 19: 'bn0>bb0s', 20: 'bn1>bb0s', 21: 'bb0s>bb0s', 22: 'nb0>bb0s', 23: 'bb0s>nb1', 24: 'bb0s>bb0s', 25: 'bb0s>bb0s', 26: 'bb0s>bb1s', 27: 'bb0s>bb1s', 28: 'nb1>bb0s', 29: 'bn0>bn1', 30: 'bn0>bn0', 31: 'bb0s>bb0s', 32: 'bb0s>bb1s', 33: 'bb0s>nb0', 34: 'bb0s>bb0s'}

Covariance layer complex:
   1111  1111  *  1  **           *1 **   *  *  *  *
CCCUAGUCCGAUACGUCGCCUAG---------UUUUUUCGUGUGUCUUACGG
              |:|| ||||         |:: |:||| :|||:|    
-------------ACGGCAGAUCCAUACACACAGGCAGGCAGGCAGGA----
               *  1**   1111 1   * 1     1          
     1111  1111  *     1  **    *1 **   *  *  *  *
--CCCCUCACCCAUGCGA---CGUCUGUU--UCUUAGGUGGGUUUUCCGU
     ||||  |||| |    ||||:|||  ||:|               
ACAGCGAGUCCGUACACACAAGCAGGCAAGCAGGA---------------
  *  1**   1111 1   * 1     1                     
