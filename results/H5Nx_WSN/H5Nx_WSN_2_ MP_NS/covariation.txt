Covariation report for: 2 MP NS

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/Anhui/1/2005[H5N1]
AGUUCGCUCGUCCGUCGCCUUCGGUACCUUCAGCGAUUA---------------
                           |||||||                    
---------------------------GAAGUCGdddddddddddddddGCAUU

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn1', 3: 'nn0', 4: 'nn0', 5: 'nn1', 6: 'bn0>bb0s', 7: 'bn0>bb0s', 8: 'bn0>bb0s', 9: 'bn0>bb1s', 10: 'bn0>bb1s', 11: 'bn1>bb0s', 12: 'bn0>bnd', 13: 'bn0>bnd', 14: 'bn0>bnd', 15: 'nn0', 16: 'bn0>bnd', 17: 'bn1>bnd', 18: 'bn0>bnd', 19: 'bn0>bnd', 20: 'bn1>bnd', 21: 'bn0>bnd', 22: 'bn0>bnd', 23: 'bn0>bnd', 24: 'bn0>bnd', 25: 'bn0>bnd', 26: 'bn0>bnd', 27: 'bb0s>bb0s', 28: 'nb0>bb0s', 29: 'nb1>nb1', 30: 'bb1s>bb0s', 31: 'bb0s>bb1s', 32: 'bb1s>bb1s', 33: 'nb0>bb0s', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn1', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bb0s>bb0s', 1: 'bb0s>nb0', 2: 'nb1>nb1', 3: 'bb0s>bb1s', 4: 'bb1s>bb0s', 5: 'bb1s>bb1s', 6: 'bb0s>nb0', 7: 'bnd>bn0', 8: 'bnd>bn0', 9: 'bnd>bn0', 10: 'bnd>bn0', 11: 'bnd>bn1', 12: 'bnd>bn0', 13: 'bnd>bn0', 14: 'bnd>bn1', 15: 'bnd>bn0', 16: 'bnd>bn0', 17: 'bnd>bn0', 18: 'bnd>bn0', 19: 'bnd>bn0', 20: 'bnd>bn0', 21: 'bnd>bb0s', 22: 'nn1', 23: 'nn0', 24: 'bn0>bb1s', 25: 'bn0>bb0s', 26: 'bn0>bb1s'}

Covariance layer complex:
  *  *      *     *  *       11* *1   * 
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
        1 **                 *          
  *  *     *     *  *       11* *1   *                
AGUUCGCUCGUCCGUCGCCUUCGGUACCUUCAGCGAUUA---------------
                           |||||||                    
---------------------------GAAGUCGdddddddddddddddGCAUU
                             1 **                *    

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/Vietnam/UT36282/2010[H5N1]
AGUUCGCUCGUUCGUCGCCUUCGGUACCUCCAGCGAUUA------
                  ||||||                     
------------------GAAGCCGdddddddddddddddACAUU

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn1', 3: 'nn0', 4: 'nn0', 5: 'nn1', 6: 'bn0>bb0s', 7: 'bn0>bb0s', 8: 'bn0>bb0s', 9: 'bn0>bb0s', 10: 'bn0>bb1s', 11: 'bn0>bn0', 12: 'bn0>bnd', 13: 'bn0>bnd', 14: 'bn0>bnd', 15: 'nn0', 16: 'bn0>bnd', 17: 'bn1>bnd', 18: 'bb0s>bb0s', 19: 'bb0s>bb0s', 20: 'bb1s>nb1', 21: 'bb0s>bb0s', 22: 'bb0s>bb0s', 23: 'bb0s>bb1s', 24: 'bn0>bnd', 25: 'bn0>bnd', 26: 'bn0>bnd', 27: 'bn0>bnd', 28: 'nn0', 29: 'nn1', 30: 'bn1>bn0', 31: 'bn0>bn0', 32: 'bn1>bn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn1', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bb0s>bb0s', 1: 'bb0s>bb0s', 2: 'nb1>bb1s', 3: 'bb0s>bb0s', 4: 'bb0s>bb0s', 5: 'bb1s>bb0s', 6: 'bn0>bn0', 7: 'bnd>bn0', 8: 'bnd>bn0', 9: 'bnd>bn0', 10: 'bnd>bn0', 11: 'bnd>bn1', 12: 'bnd>bb0s', 13: 'bnd>bb0s', 14: 'bnd>bb1s', 15: 'bnd>bb0s', 16: 'bnd>bb0s', 17: 'bnd>bb0s', 18: 'bnd>bn0', 19: 'bnd>bn0', 20: 'bnd>bn0', 21: 'bnd>bn0', 22: 'nn1', 23: 'nn0', 24: 'bn0>bn1', 25: 'bn0>bn0', 26: 'bn0>bn1'}

Covariance layer complex:
  *  *            *  *        ** *    * 
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
        1  *                 *          
  *  *           *  *        ** *    *       
AGUUCGCUCGUUCGUCGCCUUCGGUACCUCCAGCGAUUA------
                  ||||||                     
------------------GAAGCCGdddddddddddddddACAUU
                    1  *                *    

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/Egypt/MOH-NRC-8434/2014[H5N1]
AGUUCAC-UCGUCCGUCGCCUUCGGUACCUUCAGCGAUCA
      | ||| ||||                        
------GAAGCUGGCAddddddddddddGCAUU-------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn1', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'bb0>bb0', 7: 'bb0s>nb1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bn0>bn1', 11: 'bb1>bb0', 12: 'bb0>bb0', 13: 'bb0>bb1', 14: 'bb0>bb0', 15: 'nn0', 16: 'bn0>bnd', 17: 'bn1>bnd', 18: 'bn0>bnd', 19: 'bn0>bnd', 20: 'bn1>bnd', 21: 'bn0>bnd', 22: 'bn0>bnd', 23: 'bn0>bnd', 24: 'bn0>bnd', 25: 'bn0>bnd', 26: 'bn0>bnd', 27: 'bn0>bnd', 28: 'nn0', 29: 'nn1', 30: 'bn1>bn0', 31: 'bn0>bn0', 32: 'bn1>bn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bb0>bb0', 1: 'bn0>bb0s', 2: 'nb1>bb0s', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bn1>bn0', 6: 'bb0>bb1', 7: 'bb0>bb0', 8: 'bb1>bb0', 9: 'bb0>bb0', 10: 'bnd>bn0', 11: 'bnd>bn1', 12: 'bnd>bn0', 13: 'bnd>bn0', 14: 'bnd>bn1', 15: 'bnd>bn0', 16: 'bnd>bn0', 17: 'bnd>bn0', 18: 'bnd>bn0', 19: 'bnd>bn0', 20: 'bnd>bn0', 21: 'bnd>bn0', 22: 'nn1', 23: 'nn0', 24: 'bn0>bn1', 25: 'bn0>bn0', 26: 'bn0>bn1'}

Covariance layer complex:
  *         *     *  *        ** *      
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
        1  *  *              *          
  *         *     *  *        ** *      
AGUUCAC-UCGUCCGUCGCCUUCGGUACCUUCAGCGAUCA
      | ||| ||||                        
------GAAGCUGGCAddddddddddddGCAUU-------
        1  *  *             *           

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/Changsha/1/2014[H5N6]
AGUUCACUCGUCCGUCGUCUUCGGUACCUCCAGCGGUUA---------------
                           ||  ||| ||                 
---------------------------GAAAUCGGCAddddddddddddGCAUU

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn1', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'bn0>bb0s', 7: 'bn0>bb0s', 8: 'bn0>bn1', 9: 'bn0>bb1s', 10: 'bn0>bb1s', 11: 'bn1>bb0s', 12: 'bn0>bn0', 13: 'bn0>bb1s', 14: 'bn0>bb0s', 15: 'nn0', 16: 'bn0>bnd', 17: 'bn0>bnd', 18: 'bn0>bnd', 19: 'bn0>bnd', 20: 'bn1>bnd', 21: 'bn0>bnd', 22: 'bn0>bnd', 23: 'bn0>bnd', 24: 'bn0>bnd', 25: 'bn0>bnd', 26: 'bn0>bnd', 27: 'bb0s>bb0s', 28: 'nb0>bb0s', 29: 'nn1', 30: 'bn1>bn0', 31: 'bb0s>bb1s', 32: 'bb1s>bb1s', 33: 'nb0>bb0s', 34: 'nn0', 35: 'nb1>bb1s', 36: 'nb0>bb0s', 37: 'nn1', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bb0s>bb0s', 1: 'bb0s>nb0', 2: 'nn1', 3: 'bn1>bn0', 4: 'bb1s>bb0s', 5: 'bb1s>bb1s', 6: 'bb0s>nb0', 7: 'bn0>bn0', 8: 'bb1s>nb1', 9: 'bb0s>nb0', 10: 'bnd>bn0', 11: 'bnd>bn0', 12: 'bnd>bn0', 13: 'bnd>bn0', 14: 'bnd>bn1', 15: 'bnd>bn0', 16: 'bnd>bn0', 17: 'bnd>bn0', 18: 'bnd>bn0', 19: 'bnd>bn0', 20: 'bnd>bn0', 21: 'bnd>bb0s', 22: 'nn1', 23: 'nn0', 24: 'bn0>bn1', 25: 'bn0>bb0s', 26: 'bn0>bb1s'}

Covariance layer complex:
  *         *        *       1** *1 11* 
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
        ****  *              *          
  *        *        *       1** *1 11*                
AGUUCACUCGUCCGUCGUCUUCGGUACCUCCAGCGGUUA---------------
                           ||  ||| ||                 
---------------------------GAAAUCGGCAddddddddddddGCAUU
                             ****  *             *    

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/Yunnan/0127/2015[H5N6]
AGUUCAC-UCGUCCGUCGCCUUCGGUACCUUCAGCGUUCA
      | ||| |||| | |||||      ||||      
------GAAGCUGGCA-CUGAAGCAAU---AGUCAUU---

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn1', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'bb0>bb0', 7: 'bb0s>nb1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bn0>bn1', 11: 'bb1>bb0', 12: 'bb0>bb0', 13: 'bb0>bb1', 14: 'bb0>bb0', 15: 'nn0', 16: 'bb0>bb0', 17: 'bn1>bn1', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb1>bb1', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bn0>bn1', 24: 'bn0>bn0', 25: 'bn0>bn0', 26: 'bn0>bb1s', 27: 'bn0>bb0s', 28: 'nn0', 29: 'nb1>bb1s', 30: 'bb1s>bb0s', 31: 'bb0s>nb0', 32: 'bb1s>nb0', 33: 'nn0', 34: 'nn0', 35: 'nn1', 36: 'nn0', 37: 'nn0', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bb0>bb0', 1: 'bn0>bb0s', 2: 'nb1>bb0s', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bn1>bn0', 6: 'bb0>bb1', 7: 'bb0>bb0', 8: 'bb1>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bn1>bn1', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb1>bb1', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bn1>bn0', 18: 'bn0>bn0', 19: 'bn0>bn0', 20: 'bb1s>nb1', 21: 'bb0s>bb1s', 22: 'nb0>bb0s', 23: 'nb0>bb1s', 24: 'bn0>bb1s', 25: 'bn0>bb0s', 26: 'bn0>bb1s'}

Covariance layer complex:
  *         *     *  2        1* *  *   
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
        1  *  *   *  2  *  * 11         
  *         *     *  2        1* *  *   
AGUUCAC-UCGUCCGUCGCCUUCGGUACCUUCAGCGUUCA
      | ||| |||| | |||||      ||||      
------GAAGCUGGCA-CUGAAGCAAU---AGUCAUU---
        1  *  *   *  2  *     * 11      

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/duck/Mongolia/54+47/01[H5N1]
AGCUCAC-UCGUCCGUCGCCUUCGGUACCUCCAACGAUCA
      | ||| |||| | |||||                
------GAAGCCGGCA-CUGAAGCAAUAGUCAUU------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'bb0>bb0', 7: 'bb0s>nb1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bn0>bn1', 11: 'bb1>bb0', 12: 'bb0>bb0', 13: 'bb0>bb1', 14: 'bb0>bb0', 15: 'nn0', 16: 'bb0>bb0', 17: 'bn1>bn1', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb1>bb1', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bn0>bn1', 24: 'bn0>bn0', 25: 'bn0>bn0', 26: 'bn0>bn1', 27: 'bn0>bn0', 28: 'nn0', 29: 'nn1', 30: 'bn1>bn0', 31: 'bn0>bn0', 32: 'bn0>bn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bb0>bb0', 1: 'bn0>bb0s', 2: 'nb1>bb0s', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bn1>bn0', 6: 'bb0>bb1', 7: 'bb0>bb0', 8: 'bb1>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bn1>bn1', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb1>bb1', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bn1>bn0', 18: 'bn0>bn0', 19: 'bn0>bn0', 20: 'bn1>bn0', 21: 'bn0>bn0', 22: 'nn0', 23: 'nn0', 24: 'bn0>bn1', 25: 'bn0>bn0', 26: 'bn0>bn0'}

Covariance layer complex:
            *     *  2        **        
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
        1  *  *   *  2  *  *            
            *     *  2        **        
AGCUCAC-UCGUCCGUCGCCUUCGGUACCUCCAACGAUCA
      | ||| |||| | |||||                
------GAAGCCGGCA-CUGAAGCAAUAGUCAUU------
        1  *  *   *  2  *  *            

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/duck/Hokkaido/Vac-3/2007[H5N1]
AGCUCAC-UUGUCCG-----UCGCCUCCGGUACCUCCAACGAUCA
      | |:| |||     |||    |:|||             
------GAAGCCGGCACUGAAGCAAUAGUCAUU------------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'bb0>bb0', 7: 'bb0s>nb1', 8: 'bb1>bb0', 9: 'bb0>bb0', 10: 'bn0>bn1', 11: 'bb1>bb0', 12: 'bb0>bb0', 13: 'bb0>bb1', 14: 'bb0s>bb1s', 15: 'nb0>bb0s', 16: 'bb0s>bb0s', 17: 'bn1>bn1', 18: 'bn0>bn0', 19: 'bn0>bn0', 20: 'bn0>bb1s', 21: 'bb0s>bb0s', 22: 'bb0s>nb0', 23: 'bb0s>nb0', 24: 'bb0s>bb0s', 25: 'bb0s>bb0s', 26: 'bn0>bn1', 27: 'bn0>bb0s', 28: 'nn0', 29: 'nn1', 30: 'bn1>bb0s', 31: 'bn0>bb0s', 32: 'bn0>bn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bb0>bb0', 1: 'bn0>bb0s', 2: 'nb1>bb0s', 3: 'bb0>bb1', 4: 'bb0>bb0', 5: 'bn1>bn0', 6: 'bb0>bb1', 7: 'bb0>bb0', 8: 'bb1>bb0', 9: 'bn0>bb0s', 10: 'bn0>bb0s', 11: 'bn1>bn1', 12: 'bn0>bn0', 13: 'bn0>bn0', 14: 'bb1s>bb0s', 15: 'bb0s>nb0', 16: 'bb0s>bb0s', 17: 'bn1>bb0s', 18: 'bn0>bb0s', 19: 'bn0>bb0s', 20: 'bn1>bn0', 21: 'bb0s>bb0s', 22: 'nb0>bb0s', 23: 'nb0>bb0s', 24: 'bb0s>bb0s', 25: 'bb0s>bb0s', 26: 'bn0>bn0'}

Covariance layer complex:
         *  *   1 *           **        
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
        1  *  *   *  *  *  * 11         
         *  *        1 *           **        
AGCUCAC-UUGUCCG-----UCGCCUCCGGUACCUCCAACGAUCA
      | |:| |||     |||    |:|||             
------GAAGCCGGCACUGAAGCAAUAGUCAUU------------
        1  *  *  *  *  *  * 11               

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/duck/Moscow/4182-C/2017[H5N3]
AGCUCAC-UCGUUCGUCGCCUUCGGUACCUCCAACGAUCA
      | ||||:||| | |||||                
------GAAGCAGGCA-CUGAAGCAAUAGUCAUU------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'bb0>bb0', 7: 'bb0s>nb1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb0>bb1', 14: 'bb0>bb0', 15: 'nn0', 16: 'bb0>bb0', 17: 'bn1>bn1', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb1>bb1', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bn0>bn1', 24: 'bn0>bn0', 25: 'bn0>bn0', 26: 'bn0>bn1', 27: 'bn0>bn0', 28: 'nn0', 29: 'nn1', 30: 'bn1>bn0', 31: 'bn0>bn0', 32: 'bn0>bn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bb0>bb0', 1: 'bn0>bb0s', 2: 'nb1>bb0s', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'bb0>bb0', 7: 'bb0>bb0', 8: 'bb1>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bn1>bn1', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb1>bb1', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bn1>bn0', 18: 'bn0>bn0', 19: 'bn0>bn0', 20: 'bn1>bn0', 21: 'bn0>bn0', 22: 'nn0', 23: 'nn0', 24: 'bn0>bn1', 25: 'bn0>bn0', 26: 'bn0>bn0'}

Covariance layer complex:
                  *  2        **        
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
        1     *   *  2  *  *            
                  *  2        **        
AGCUCAC-UCGUUCGUCGCCUUCGGUACCUCCAACGAUCA
      | ||||:||| | |||||                
------GAAGCAGGCA-CUGAAGCAAUAGUCAUU------
        1     *   *  2  *  *            

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/duck/Zhejiang/6DK19-MA/2013[H5N2]
AGUUCAC-UCGUCCGUCGUCUUCGUUACCUCCAGCGGUUA
      | ||: ||||                        
------GAAGUCGGCAddddddddddddGCAUU-------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn1', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'bb0>bb0', 7: 'bb0s>nb1', 8: 'bb0>bb0', 9: 'bb0>bb1', 10: 'bn0>bn1', 11: 'bb1>bb0', 12: 'bb0>bb0', 13: 'bb0>bb1', 14: 'bb0>bb0', 15: 'nn0', 16: 'bn0>bnd', 17: 'bn0>bnd', 18: 'bn0>bnd', 19: 'bn0>bnd', 20: 'bn1>bnd', 21: 'bn0>bnd', 22: 'bn0>bnd', 23: 'bn1>bnd', 24: 'bn0>bnd', 25: 'bn0>bnd', 26: 'bn0>bnd', 27: 'bn0>bnd', 28: 'nn0', 29: 'nn1', 30: 'bn1>bn0', 31: 'bn0>bn0', 32: 'bn1>bn0', 33: 'nn0', 34: 'nn0', 35: 'nn1', 36: 'nn0', 37: 'nn1', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bb0>bb0', 1: 'bn0>bb0s', 2: 'nb1>bb0s', 3: 'bb0>bb0', 4: 'bb1>bb0', 5: 'bn1>bn0', 6: 'bb0>bb1', 7: 'bb0>bb0', 8: 'bb1>bb0', 9: 'bb0>bb0', 10: 'bnd>bn0', 11: 'bnd>bn0', 12: 'bnd>bn0', 13: 'bnd>bn0', 14: 'bnd>bn1', 15: 'bnd>bn0', 16: 'bnd>bn0', 17: 'bnd>bn1', 18: 'bnd>bn0', 19: 'bnd>bn0', 20: 'bnd>bn0', 21: 'bnd>bn0', 22: 'nn1', 23: 'nn0', 24: 'bn0>bn1', 25: 'bn0>bn0', 26: 'bn0>bn1'}

Covariance layer complex:
  *         *        *  *     ** *  * * 
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
        1 **  *              *          
  *         *        *  *     ** *  * * 
AGUUCAC-UCGUCCGUCGUCUUCGUUACCUCCAGCGGUUA
      | ||: ||||                        
------GAAGUCGGCAddddddddddddGCAUU-------
        1 **  *             *           

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/Anas Platyrhynchos/Belgium/10811-6/2019[H5N6]
AGCUCAC-UCGUUCGUCGCCUUCGGUACCUCCAACGAUCA
      | ||| :||| | ||:||                
------GAAGCUGGCA-CUGAGGCAAUAGUCAUU------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'bb0>bb0', 7: 'bb0s>nb1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bn0>bn1', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb0>bb1', 14: 'bb0>bb0', 15: 'nn0', 16: 'bb0>bb0', 17: 'bn1>bn1', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb1>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bn0>bn1', 24: 'bn0>bn0', 25: 'bn0>bn0', 26: 'bn0>bn1', 27: 'bn0>bn0', 28: 'nn0', 29: 'nn1', 30: 'bn1>bn0', 31: 'bn0>bn0', 32: 'bn0>bn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bb0>bb0', 1: 'bn0>bb0s', 2: 'nb1>bb0s', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bn1>bn0', 6: 'bb0>bb0', 7: 'bb0>bb0', 8: 'bb1>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bn1>bn1', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb1', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bn1>bn0', 18: 'bn0>bn0', 19: 'bn0>bn0', 20: 'bn1>bn0', 21: 'bn0>bn0', 22: 'nn0', 23: 'nn0', 24: 'bn0>bn1', 25: 'bn0>bn0', 26: 'bn0>bn0'}

Covariance layer complex:
                  *  *        **        
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
        1  *  *   *     *  *            
                  *  *        **        
AGCUCAC-UCGUUCGUCGCCUUCGGUACCUCCAACGAUCA
      | ||| :||| | ||:||                
------GAAGCUGGCA-CUGAGGCAAUAGUCAUU------
        1  *  *   *     *  *            

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/swine/Banten/UT2071/2005[H5N1]
AGUUCACUCGUCCGUCGCCUUCGGUACCUCCAGCGAUUA------
                  ||||||                     
------------------GAAGCCGdddddddddddddddGCAUU

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn1', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'bn0>bb0s', 7: 'bn0>bb0s', 8: 'bn0>bb0s', 9: 'bn0>bb0s', 10: 'bn0>bb1s', 11: 'bn1>bn0', 12: 'bn0>bnd', 13: 'bn0>bnd', 14: 'bn0>bnd', 15: 'nn0', 16: 'bn0>bnd', 17: 'bn1>bnd', 18: 'bb0s>bb0s', 19: 'bb0s>bb0s', 20: 'bb1s>nb1', 21: 'bb0s>bb0s', 22: 'bb0s>bb0s', 23: 'bb0s>bb1s', 24: 'bn0>bnd', 25: 'bn0>bnd', 26: 'bn0>bnd', 27: 'bn0>bnd', 28: 'nn0', 29: 'nn1', 30: 'bn1>bn0', 31: 'bn0>bn0', 32: 'bn1>bn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn1', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bb0s>bb0s', 1: 'bb0s>bb0s', 2: 'nb1>bb1s', 3: 'bb0s>bb0s', 4: 'bb0s>bb0s', 5: 'bb1s>bb0s', 6: 'bn0>bn1', 7: 'bnd>bn0', 8: 'bnd>bn0', 9: 'bnd>bn0', 10: 'bnd>bn0', 11: 'bnd>bn1', 12: 'bnd>bb0s', 13: 'bnd>bb0s', 14: 'bnd>bb1s', 15: 'bnd>bb0s', 16: 'bnd>bb0s', 17: 'bnd>bb0s', 18: 'bnd>bn0', 19: 'bnd>bn0', 20: 'bnd>bn0', 21: 'bnd>bn0', 22: 'nn1', 23: 'nn0', 24: 'bn0>bn1', 25: 'bn0>bn0', 26: 'bn0>bn1'}

Covariance layer complex:
  *         *     *  *        ** *    * 
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
        1  *                 *          
  *        *     *  *        ** *    *       
AGUUCACUCGUCCGUCGCCUUCGGUACCUCCAGCGAUUA------
                  ||||||                     
------------------GAAGCCGdddddddddddddddGCAUU
                    1  *                *    

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/swine/Zhejiang/SW57/2015[H5N1]
AGUUCGCUCGUCCGUCGUCUUCGGUACCUCCAGCGGUUA---------------
                           || |||| |                  
---------------------------GAAGUCGGCdddddddddddddGCGUU

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn1', 3: 'nn0', 4: 'nn0', 5: 'nn1', 6: 'bn0>bb0s', 7: 'bn0>bb0s', 8: 'bn0>bb0s', 9: 'bn0>bb1s', 10: 'bn0>bb1s', 11: 'bn1>bb0s', 12: 'bn0>bn0', 13: 'bn0>bb1s', 14: 'bn0>bnd', 15: 'nn0', 16: 'bn0>bnd', 17: 'bn0>bnd', 18: 'bn0>bnd', 19: 'bn0>bnd', 20: 'bn1>bnd', 21: 'bn0>bnd', 22: 'bn0>bnd', 23: 'bn0>bnd', 24: 'bn0>bnd', 25: 'bn0>bnd', 26: 'bn0>bnd', 27: 'bb0s>bb0s', 28: 'nb0>bb0s', 29: 'nn1', 30: 'bb1s>bb0s', 31: 'bb0s>bb1s', 32: 'bb1s>bb1s', 33: 'nb0>bb0s', 34: 'nn0', 35: 'nb1>bb1s', 36: 'nn0', 37: 'nn1', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bb0s>bb0s', 1: 'bb0s>nb0', 2: 'nn1', 3: 'bb0s>bb1s', 4: 'bb1s>bb0s', 5: 'bb1s>bb1s', 6: 'bb0s>nb0', 7: 'bn0>bn0', 8: 'bb1s>nb1', 9: 'bnd>bn0', 10: 'bnd>bn0', 11: 'bnd>bn0', 12: 'bnd>bn0', 13: 'bnd>bn0', 14: 'bnd>bn1', 15: 'bnd>bn0', 16: 'bnd>bn0', 17: 'bnd>bn0', 18: 'bnd>bn0', 19: 'bnd>bn0', 20: 'bnd>bn0', 21: 'bnd>bb0s', 22: 'nn1', 23: 'nn0', 24: 'bn1>bb1s', 25: 'bn0>bb0s', 26: 'bn0>bb1s'}

Covariance layer complex:
  *  *      *        *       1** *1 1 * 
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
        * **  *              * *        
  *  *     *        *       1** *1 1 *                
AGUUCGCUCGUCCGUCGUCUUCGGUACCUCCAGCGGUUA---------------
                           || |||| |                  
---------------------------GAAGUCGGCdddddddddddddGCGUU
                             * **  *             * *  

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/Crow/Aghakhan/2017[H5N8]
AGCUCACUCGUCCGUC-GCCUUCGGUACCUCCAACGAUCA
              || ||||                   
------------GAAGACGGACUUGAAGCAACAGUCAUU-

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'bn0>bn0', 7: 'bn0>bn0', 8: 'bn0>bb0s', 9: 'bn0>bn1', 10: 'bn0>bb1s', 11: 'bn1>bb0s', 12: 'bn0>bb0s', 13: 'bn0>bb1s', 14: 'bb0s>nb1', 15: 'nb0>bb0s', 16: 'bb0s>bb1s', 17: 'bb1s>bb0s', 18: 'bb0s>bb0s', 19: 'bb0s>bb1s', 20: 'bn1>bn1', 21: 'bn0>bn0', 22: 'bn0>bn0', 23: 'bn0>bn1', 24: 'bn0>bn0', 25: 'bn0>bn1', 26: 'bn0>bn1', 27: 'bn0>bn0', 28: 'nn0', 29: 'nn1', 30: 'bn1>bn0', 31: 'bn0>bn0', 32: 'bn0>bn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bn0>bn0', 1: 'bn0>bn0', 2: 'nb1>bb0s', 3: 'bb0s>nb0', 4: 'bn1>bn0', 5: 'bb1s>bb0s', 6: 'bb0s>bb1s', 7: 'bb0s>bb0s', 8: 'bb1s>bb0s', 9: 'bn1>bb0s', 10: 'bn1>bb0s', 11: 'bn1>bb1s', 12: 'bn0>bb0s', 13: 'bn0>bb0s', 14: 'bn1>bn1', 15: 'bn0>bn0', 16: 'bn0>bn0', 17: 'bn1>bn0', 18: 'bn0>bn0', 19: 'bn1>bn0', 20: 'bn1>bn0', 21: 'bn0>bn0', 22: 'nn0', 23: 'nn0', 24: 'bn0>bn1', 25: 'bn0>bn0', 26: 'bn0>bn0'}

Covariance layer complex:
            *   1 *  *        **        
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
        1 **  ** **  *  * **            
           *   1  *  *        **        
AGCUCACUCGUCCGUC-GCCUUCGGUACCUCCAACGAUCA
              || ||||                   
------------GAAGACGGACUUGAAGCAACAGUCAUU-
              1 **  ****  *  * **       

Reference strain:
A/WSN/1933
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
Comparison strain:
A/swan/Krasnodar/44/2017[H5N8]
AGCUCACUCGUCCGUC-GCCUUCGGUACCUCCAACGAUCA
              || ||||                   
------------GAAGACGGACUUGAAGCAACAGUCAUU-

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'bn0>bn0', 7: 'bn0>bn0', 8: 'bn0>bb0s', 9: 'bn0>bn1', 10: 'bn0>bb1s', 11: 'bn1>bb0s', 12: 'bn0>bb0s', 13: 'bn0>bb1s', 14: 'bb0s>nb1', 15: 'nb0>bb0s', 16: 'bb0s>bb1s', 17: 'bb1s>bb0s', 18: 'bb0s>bb0s', 19: 'bb0s>bb1s', 20: 'bn1>bn1', 21: 'bn0>bn0', 22: 'bn0>bn0', 23: 'bn0>bn1', 24: 'bn0>bn0', 25: 'bn0>bn1', 26: 'bn0>bn1', 27: 'bn0>bn0', 28: 'nn0', 29: 'nn1', 30: 'bn1>bn0', 31: 'bn0>bn0', 32: 'bn0>bn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0'}
Strand 2 variance codes:
{0: 'bn0>bn0', 1: 'bn0>bn0', 2: 'nb1>bb0s', 3: 'bb0s>nb0', 4: 'bn1>bn0', 5: 'bb1s>bb0s', 6: 'bb0s>bb1s', 7: 'bb0s>bb0s', 8: 'bb1s>bb0s', 9: 'bn1>bb0s', 10: 'bn1>bb0s', 11: 'bn1>bb1s', 12: 'bn0>bb0s', 13: 'bn0>bb0s', 14: 'bn1>bn1', 15: 'bn0>bn0', 16: 'bn0>bn0', 17: 'bn1>bn0', 18: 'bn0>bn0', 19: 'bn1>bn0', 20: 'bn1>bn0', 21: 'bn0>bn0', 22: 'nn0', 23: 'nn0', 24: 'bn0>bn1', 25: 'bn0>bn0', 26: 'bn0>bn0'}

Covariance layer complex:
            *   1 *  *        **        
AGCUCACU-CGUUCGUCGUCUCCGGUACCUAUAACGAUCA
      || |||:|:| ||||||||||||  |||      
------GAUGCAGGUA-CAGAGGCCAUGGUCAUU------
        1 **  ** **  *  * **            
           *   1  *  *        **        
AGCUCACUCGUCCGUC-GCCUUCGGUACCUCCAACGAUCA
              || ||||                   
------------GAAGACGGACUUGAAGCAACAGUCAUU-
              1 **  ****  *  * **       
