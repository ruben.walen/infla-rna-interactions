36 NP MP

Strains found:
A/WSN/1933
A/Puerto Rico/8/1934-Korea/426/1968[H2N2]
A/Anhui/1-BALF_RG1/2013[H7N9]
A/Belgium/145-MA/2009[H1N1]
A/Brisbane/59/2007[H1N1]
A/Hong Kong/01/1968[H3N2]
A/duck/Hokkaido/Vac-3/2007[H5N1]
A/duck/Zhejiang/6DK19-MA/2013[H5N2]
A/mallard/Alberta/70/2017[H7N3]

forna copy-able:
>A/WSN/1933 38-13 10-35
AACGAGUGUUCAGGACGGACGGACGGUCGUUUACCGACCUAGCUCACUCGUU
((((((((..((((.(((....(((..)))...))).))).)..))))))))
>A/Puerto Rico/8/1934-Korea/426/1968[H2N2] 38-13 10-35
AACGAGUGUUUAGAACAGACGGGCGGUCGUUUACCGACCUAGCUCACUCGUU
((((((((..(((....(.(((.((..))....))).))))...))))))))
>A/Anhui/1-BALF_RG1/2013[H7N9] 27-13 2-14
AGGACGAACGGACGAUUGUCCGUCGCCU
(((.((.((((((.().)))))))))))
>A/Belgium/145-MA/2009[H1N1] 27-13 2-14
AGGACGGACGGACGAUUGUCCGUCGCCU
(((.(.(((((((.().)))))))))))
>A/Brisbane/59/2007[H1N1] 42-13 2-35
AGUCAACGAGUGUUCAGAACGGACGGACGGUCGUUUACCGACCUAGCUCGCUUGUUCGUCGACU
(((((((.((.(..(((..(((....(((..)))...)))..)).)..).)).)))....))))
>A/Hong Kong/01/1968[H3N2] 27-13 2-14
AGAACAGACGGGCGGUCGUCCGUCGUCU
((.((.(((((.((..)).)))))))))
>A/duck/Hokkaido/Vac-3/2007[H5N1] 27-13 2-14
AGGACGAACGGACGAUUGUCCGUCGCCU
(((.((.((((((.().)))))))))))
>A/duck/Zhejiang/6DK19-MA/2013[H5N2] 27-13 1-14
AGGACGAACGGACGAUCGUCCGUCGUCUU
(.((((.(((((((()))))))))))).)
>A/mallard/Alberta/70/2017[H7N3] 26-13 2-14
GGACGGACGGACGAUCGUCCGUCGUCU
.(((.((((((((()))))))))))).

Mutation block
A/WSN/1933 36 NP 675-717 MP 400-434
v = genomic start position; $ = genomic end position
               	$                                         v v                                 $
A/WSN/1933	CAGTCAACGAGTGTTCAGGACGGACGGACGGACACACATACCT&CTCTGCTGCTTGCTCACTCGATCCAGCCATTTGCT
A/WSN/1933	                                           &                                   
A/Puerto Rico..	               T  A  A     G               &                                   
A/Anhui/1-BAL..	T     C     A  T      A       A        G   &T  C     C  T     T           C    
A/Belgium/145..	T        T  A  T              A           C&   C     C  T                 C  T 
A/Brisbane/59..	T                 A                        &   A        T  G                   
A/Hong Kong/0..	               T  A  A     G               &         C                         
A/duck/Hokkai..	T     C     A         A       A     T      &   C     C  T        C        C    
A/duck/Zhejia..	T     C  G            A       A        G   &T        C        T     C     C    
A/mallard/Alb..	T     C     A                 A            &         C        T  C  T          

Per-nucleotide structure block
A/WSN/1933	CAGUCAACGAGUGUUCAGGACGGACGGACGGACACACAUACCU&CUCUGCUGCUUGCUCACUCGAUCCAGCCAUUUGCU
A/WSN/1933	.....||||||||..||||.|||..::|||:............&.........||||||||..|.|||.|||.::|||:
A/WSN/1933	                                           &                                   	-13.94 kcal/mol
A/Puerto Rico..	              :   . ..  |||:               &                   :|        ..:   	-8.18 kcal/mol
A/Anhui/1-BAL..	     ........  .      .||||  :|            & ||||||||   : ...  . ... ... ......	-11.41 kcal/mol
A/Belgium/145..	     ........  .     . ||||  :|            & ||||||||   : ...  . ... ... ......	-10.88 kcal/mol
A/Brisbane/59..	 ||||   :  :      .                        & ||||       :  :       .           	-9.34 kcal/mol
A/Hong Kong/0..	     ........  .  .| . ||||:               & |||||||| :  :...  . ... ... ......	-10.15 kcal/mol
A/duck/Hokkai..	     ........  .      .||||  :|            & ||||||||   : ...  . ... ... ......	-10.89 kcal/mol
A/duck/Zhejia..	     ........  . : |  .||||   |            &|:|||||||     ...  . ... ... ......	-15.43 kcal/mol
A/mallard/Alb..	     ........  ..: | . ||||   |            & :|||||||     ...  . ... ... ......	-15.65 kcal/mol