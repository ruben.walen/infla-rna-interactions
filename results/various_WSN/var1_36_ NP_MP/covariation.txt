Covariation report for: 36 NP MP

Reference strain:
A/WSN/1933
----CAGUCAACGAGUGUUC-AGGACGGACGGACGGACACACAUACCU
         ||||||||  | ||| |||  ::|||:            
CUCUGCUGCUUGCUCACUCGAUCCAGCCA-UUUGCU------------
Comparison strain:
A/Puerto Rico/8/1934-Korea/426/1968[H2N2]
----CAGUCAACGAGUGU-UUAGAACAGACGG---GCGGACACACAUACCU
         ||||||||  :|||    | |||   :||:            
CUCUGCUGCUUGCUCACUCGAUC----CAGCCAUUUGCU------------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'bb0>bb0', 6: 'bb0>bb0', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'nn0', 14: 'nb0>bb0s', 15: 'bb1s>nb0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bn1>bb0s', 19: 'nn0', 20: 'bn0>bb0s', 21: 'bn1>bb0s', 22: 'bb0s>bb0s', 23: 'nn0', 24: 'nb0>bb0s', 25: 'bb0s>bb0s', 26: 'bb0s>bb0s', 27: 'bb1>bb0', 28: 'bb0>bb0', 29: 'bb0>bb0', 30: 'bb0>bb0', 31: 'nn0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn0', 42: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'nn0', 18: 'nn0', 19: 'bb0s>nb0', 20: 'nb0>bb1s', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0s>bb0s', 24: 'nn0', 25: 'bb0s>nb0', 26: 'bb0s>bb0s', 27: 'bb0s>bb0s', 28: 'nn0', 29: 'bn0>bb0s', 30: 'bn0>bb0s', 31: 'bb0>bb1', 32: 'bb0>bb0', 33: 'bb0>bb0', 34: 'bb0>bb0'}

Covariance layer complex:
                  1*   *  *  1  *               
----CAGUCAACGAGUGUUC-AGGACGGACGGACGGACACACAUACCU
         ||||||||  | ||| |||  ::|||:            
CUCUGCUGCUUGCUCACUCGAUCCAGCCA-UUUGCU------------
                    1                           
                   1*  *  *  1     *               
----CAGUCAACGAGUGU-UUAGAACAGACGG---GCGGACACACAUACCU
         ||||||||  :|||    | |||   :||:            
CUCUGCUGCUUGCUCACUCGAUC----CAGCCAUUUGCU------------
                    1                              

Reference strain:
A/WSN/1933
----CAGUCAACGAGUGUUC-AGGACGGACGGACGGACACACAUACCU
         ||||||||  | ||| |||  ::|||:            
CUCUGCUGCUUGCUCACUCGAUCCAGCCA-UUUGCU------------
Comparison strain:
A/Anhui/1-BALF_RG1/2013[H7N9]
UAGUCACCGAGUAUUUAGGACGAACGGACGAACACACAUGCCU---------
                ||| || ||||||:|                     
---------------UUCC-GC-UGCCUGUUCACUUGAUCCAGCCAUCUGCU

Strand 1 variance codes:
{0: 'nn1', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'bn0>bb1s', 6: 'bn1>bb0s', 7: 'bn0>bb0s', 8: 'bn0>bb1s', 9: 'bn0>bb0s', 10: 'bn0>bn0', 11: 'bn0>bn0', 12: 'bn1>bn0', 13: 'nn0', 14: 'nn0', 15: 'bn1>bn0', 16: 'bb0s>nb0', 17: 'bb0s>nb0', 18: 'bb0s>nb1', 19: 'nn0', 20: 'bb0s>nb0', 21: 'bb0s>nb0', 22: 'bn1>bn0', 23: 'nb0>nb0', 24: 'nb0>nb0', 25: 'bb0s>nb0', 26: 'bb0s>bb1s', 27: 'bb0s>bb0s', 28: 'bb0s>bb0s', 29: 'bb0s>bb1s', 30: 'bb1s>bb0s', 31: 'nn0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn1', 40: 'nn0', 41: 'nn0', 42: 'nn0'}
Strand 2 variance codes:
{0: 'nn1', 1: 'nb0>bb0s', 2: 'nb0>bb0s', 3: 'nb1>bb0s', 4: 'nb0>bb0s', 5: 'nb0>bb0s', 6: 'nb0>nb0', 7: 'nb0>nb0', 8: 'nb0>bb0s', 9: 'bb1s>bb0s', 10: 'bb0s>bb0s', 11: 'bb0s>bb0s', 12: 'bb1s>bb0s', 13: 'bb0s>bb1s', 14: 'bn0>bn0', 15: 'bn0>bn0', 16: 'bn0>bn1', 17: 'nn0', 18: 'nn1', 19: 'bn0>bn1', 20: 'nn0', 21: 'bn0>bb0s', 22: 'bn0>bb0s', 23: 'bn0>bb0s', 24: 'nn0', 25: 'bn0>bb0s', 26: 'bn0>bb0s', 27: 'bn0>bn1', 28: 'nn0', 29: 'bn0>bb0s', 30: 'bn1>bb0s', 31: 'bn0>bb0s', 32: 'bn0>bb0s', 33: 'bn0>bb0s', 34: 'bn0>bb1s'}

Covariance layer complex:
    *     *     *  *       *11     *        *   
----CAGUCAACGAGUGUUC-AGGACGGACGGACGGACACACAUACCU
         ||||||||  | ||| |||  ::|||:            
CUCUGCUGCUUGCUCACUCGAUCCAGCCA-UUUGCU------------
*11111111*  *     *            *                
*     *     *  *      *11     *        *            
UAGUCACCGAGUAUUUAGGACGAACGGACGAACACACAUGCCU---------
                ||| || ||||||:|                     
---------------UUCC-GC-UGCCUGUUCACUUGAUCCAGCCAUCUGCU
               *111 11 111*  *     *           *    

Reference strain:
A/WSN/1933
----CAGUCAACGAGUGUUC-AGGACGGACGGACGGACACACAUACCU
         ||||||||  | ||| |||  ::|||:            
CUCUGCUGCUUGCUCACUCGAUCCAGCCA-UUUGCU------------
Comparison strain:
A/Belgium/145-MA/2009[H1N1]
UAGUCAACGUGUAUUUAGGACGGACGGACGAACACACAUACCC---------
                ||| | |||||||:|                     
---------------CUCC-G-CUGCCUGUUCACUCGAUCCAGCCAUCUGUU

Strand 1 variance codes:
{0: 'nn1', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'bn0>bb1s', 6: 'bn0>bb0s', 7: 'bn0>bb0s', 8: 'bn0>bb1s', 9: 'bn1>bb0s', 10: 'bn0>bn0', 11: 'bn0>bn0', 12: 'bn1>bn0', 13: 'nn0', 14: 'nn0', 15: 'bn1>bn0', 16: 'bb0s>nb0', 17: 'bb0s>nb0', 18: 'bb0s>nb1', 19: 'nn0', 20: 'bb0s>nb0', 21: 'bn0>bn0', 22: 'bb0s>nb0', 23: 'nb0>nb0', 24: 'nb0>nb0', 25: 'bb0s>nb0', 26: 'bb0s>bb1s', 27: 'bb0s>bb0s', 28: 'bb0s>bb0s', 29: 'bb0s>bb1s', 30: 'bb1s>bb0s', 31: 'nn0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn0', 42: 'nn1'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nb0>bb0s', 2: 'nb0>bb0s', 3: 'nb1>bb0s', 4: 'nb0>bb0s', 5: 'nb0>bb0s', 6: 'nb0>nb0', 7: 'nb0>nb0', 8: 'nb0>bb0s', 9: 'bb1s>bb0s', 10: 'bb0s>bb0s', 11: 'bb0s>bb0s', 12: 'bb1s>bb0s', 13: 'bb0s>bb1s', 14: 'bn0>bn0', 15: 'bn0>bn0', 16: 'bn0>bn1', 17: 'nn0', 18: 'nn0', 19: 'bn0>bn1', 20: 'nn0', 21: 'bn0>bb0s', 22: 'bn0>bb0s', 23: 'bn0>bb0s', 24: 'nn0', 25: 'bn0>bb0s', 26: 'bn0>bn0', 27: 'bn0>bb0s', 28: 'nn0', 29: 'bn0>bb0s', 30: 'bn1>bb0s', 31: 'bn0>bb0s', 32: 'bn0>bb0s', 33: 'bn1>bb0s', 34: 'bn0>bb1s'}

Covariance layer complex:
    *        *  *  *        11     *           *
----CAGUCAACGAGUGUUC-AGGACGGACGGACGGACACACAUACCU
         ||||||||  | ||| |||  ::|||:            
CUCUGCUGCUUGCUCACUCGAUCCAGCCA-UUUGCU------------
 11111111*  *                  *  *             
*        *  *  *       11     *           *         
UAGUCAACGUGUAUUUAGGACGGACGGACGAACACACAUACCC---------
                ||| | |||||||:|                     
---------------CUCC-G-CUGCCUGUUCACUCGAUCCAGCCAUCUGUU
                111 1 1111*  *                 *  * 

Reference strain:
A/WSN/1933
----CAGUCAACGAGUGUUC-AGGACGGACGGACGGACACACAUACCU
         ||||||||  | ||| |||  ::|||:            
CUCUGCUGCUUGCUCACUCGAUCCAGCCA-UUUGCU------------
Comparison strain:
A/Brisbane/59/2007[H1N1]
UAGUC----AACGAGUGUUC-AGAACGGACGGACGGACACACAUACCU
 ||||    |||:||:|  | ||  |||  ::|||:            
CUCAGCUGCUUGUUCGCUCGAUCCAGCCA-UUUGCU------------

Strand 1 variance codes:
{0: 'nn1', 1: 'nb0>nb0', 2: 'nb0>nb0', 3: 'nb0>nb1', 4: 'nb0>nb0', 5: 'bb0>bb0', 6: 'bb0>bb0', 7: 'bb0>bb0', 8: 'bb0>bb1', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bb0>bb1', 12: 'bb0>bb0', 13: 'nn0', 14: 'nn0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bn1>bn0', 19: 'nn0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'nn0', 24: 'nn0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'bb0>bb0', 29: 'bb0>bb0', 30: 'bb0>bb0', 31: 'nn0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn0', 42: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nb0>nb0', 2: 'nb0>nb0', 3: 'nb1>nb0', 4: 'nb0>nb0', 5: 'nn0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bb0>bb0', 12: 'bb1>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb1>bb0', 16: 'bb0>bb0', 17: 'nn0', 18: 'nn0', 19: 'bb0>bb0', 20: 'nn0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bn0>bn1', 24: 'nn0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'bb0>bb0', 30: 'bb0>bb0', 31: 'bb0>bb0', 32: 'bb0>bb0', 33: 'bb0>bb0', 34: 'bb0>bb0'}

Covariance layer complex:
    *1111              *                        
----CAGUCAACGAGUGUUC-AGGACGGACGGACGGACACACAUACCU
         ||||||||  | ||| |||  ::|||:            
CUCUGCUGCUUGCUCACUCGAUCCAGCCA-UUUGCU------------
 1111       *  *                                
*1111                  *                        
UAGUC----AACGAGUGUUC-AGAACGGACGGACGGACACACAUACCU
 ||||    |||:||:|  | ||  |||  ::|||:            
CUCAGCUGCUUGUUCGCUCGAUCCAGCCA-UUUGCU------------
 1111       *  *                                

Reference strain:
A/WSN/1933
----CAGUCAACGAGUGUUC-AGGACGGACGGACGGACACACAUACCU
         ||||||||  | ||| |||  ::|||:            
CUCUGCUGCUUGCUCACUCGAUCCAGCCA-UUUGCU------------
Comparison strain:
A/Hong Kong/01/1968[H3N2]
CAGUCAACGAGUGUUUAGAACAGACGGGCGGACACACAUACCU---------
                || || |||||:||:                     
---------------CUC-UG-CUGCCUGCUCACUCGAUCCAGCCAUUUGCU

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'bn0>bb1s', 6: 'bn0>bb0s', 7: 'bn0>bb0s', 8: 'bn0>bb0s', 9: 'bn0>bb0s', 10: 'bn0>bn0', 11: 'bn0>bn0', 12: 'bn0>bn0', 13: 'nn0', 14: 'nn0', 15: 'bn1>bn0', 16: 'bb0s>nb0', 17: 'bb0s>nb0', 18: 'bn1>bn0', 19: 'nb0>nb0', 20: 'bb0s>nb0', 21: 'bn1>bn0', 22: 'bb0s>nb0', 23: 'nb0>nb0', 24: 'nb0>nb0', 25: 'bb0s>nb0', 26: 'bb0s>bb1s', 27: 'bb1s>bb0s', 28: 'bb0s>bb0s', 29: 'bb0s>bb0s', 30: 'bb0s>bb0s', 31: 'nn0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn0', 42: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nb0>bb0s', 2: 'nb0>bb0s', 3: 'nb0>nb0', 4: 'nb0>bb0s', 5: 'nb0>bb0s', 6: 'nb0>nb0', 7: 'nb0>nb0', 8: 'nb0>bb0s', 9: 'bb1s>bb0s', 10: 'bb0s>bb1s', 11: 'bb0s>bb0s', 12: 'bb0s>bb0s', 13: 'bb0s>bb0s', 14: 'bn0>bn0', 15: 'bn0>bn0', 16: 'bn0>bn0', 17: 'nn0', 18: 'nn0', 19: 'bn0>bn1', 20: 'nn0', 21: 'bn0>bb0s', 22: 'bn0>bb0s', 23: 'bn0>bn1', 24: 'nn0', 25: 'bn0>bb0s', 26: 'bn0>bn1', 27: 'bn0>bb0s', 28: 'nn0', 29: 'bn0>bb0s', 30: 'bn0>bb0s', 31: 'bn0>bb1s', 32: 'bn0>bb0s', 33: 'bn0>bb0s', 34: 'bn0>bb0s'}

Covariance layer complex:
                   *   *1 * 11  *               
----CAGUCAACGAGUGUUC-AGGACGGACGGACGGACACACAUACCU
         ||||||||  | ||| |||  ::|||:            
CUCUGCUGCUUGCUCACUCGAUCCAGCCA-UUUGCU------------
 11111111*                                      
               *  *1 * 11  *                        
CAGUCAACGAGUGUUUAGAACAGACGGGCGGACACACAUACCU---------
                || || |||||:||:                     
---------------CUC-UG-CUGCCUGCUCACUCGAUCCAGCCAUUUGCU
                11 11 1111*                         

Reference strain:
A/WSN/1933
----CAGUCAACGAGUGUUC-AGGACGGACGGACGGACACACAUACCU
         ||||||||  | ||| |||  ::|||:            
CUCUGCUGCUUGCUCACUCGAUCCAGCCA-UUUGCU------------
Comparison strain:
A/duck/Hokkaido/Vac-3/2007[H5N1]
UAGUCACCGAGUAUUCAGGACGAACGGACGAACACAUAUACCU---------
                ||| || ||||||:|                     
---------------CUCC-GC-UGCCUGUUCACUCGACCCAGCCAUCUGCU

Strand 1 variance codes:
{0: 'nn1', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'bn0>bb1s', 6: 'bn1>bb0s', 7: 'bn0>bb0s', 8: 'bn0>bb1s', 9: 'bn0>bb0s', 10: 'bn0>bn0', 11: 'bn0>bn0', 12: 'bn1>bn0', 13: 'nn0', 14: 'nn0', 15: 'bn0>bn0', 16: 'bb0s>nb0', 17: 'bb0s>nb0', 18: 'bb0s>nb1', 19: 'nn0', 20: 'bb0s>nb0', 21: 'bb0s>nb0', 22: 'bn1>bn0', 23: 'nb0>nb0', 24: 'nb0>nb0', 25: 'bb0s>nb0', 26: 'bb0s>bb1s', 27: 'bb0s>bb0s', 28: 'bb0s>bb0s', 29: 'bb0s>bb1s', 30: 'bb1s>bb0s', 31: 'nn0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn1', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn0', 42: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nb0>bb0s', 2: 'nb0>bb0s', 3: 'nb1>bb0s', 4: 'nb0>bb0s', 5: 'nb0>bb0s', 6: 'nb0>nb0', 7: 'nb0>nb0', 8: 'nb0>bb0s', 9: 'bb1s>bb0s', 10: 'bb0s>bb0s', 11: 'bb0s>bb0s', 12: 'bb1s>bb0s', 13: 'bb0s>bb1s', 14: 'bn0>bn0', 15: 'bn0>bn0', 16: 'bn0>bn1', 17: 'nn0', 18: 'nn0', 19: 'bn0>bn0', 20: 'nn0', 21: 'bn1>bb0s', 22: 'bn0>bb0s', 23: 'bn0>bb0s', 24: 'nn0', 25: 'bn0>bb0s', 26: 'bn0>bb0s', 27: 'bn0>bn1', 28: 'nn0', 29: 'bn0>bb0s', 30: 'bn1>bb0s', 31: 'bn0>bb0s', 32: 'bn0>bb0s', 33: 'bn0>bb0s', 34: 'bn0>bb1s'}

Covariance layer complex:
    *     *     *          *11     *     *      
----CAGUCAACGAGUGUUC-AGGACGGACGGACGGACACACAUACCU
         ||||||||  | ||| |||  ::|||:            
CUCUGCUGCUUGCUCACUCGAUCCAGCCA-UUUGCU------------
 11111111*  *        *         *                
*     *     *         *11     *     *               
UAGUCACCGAGUAUUCAGGACGAACGGACGAACACAUAUACCU---------
                ||| || ||||||:|                     
---------------CUCC-GC-UGCCUGUUCACUCGACCCAGCCAUCUGCU
                111 11 111*  *        *        *    

Reference strain:
A/WSN/1933
----CAGUCAACGAGUGUUC-AGGACGGACGGACGGACACACAUACCU
         ||||||||  | ||| |||  ::|||:            
CUCUGCUGCUUGCUCACUCGAUCCAGCCA-UUUGCU------------
Comparison strain:
A/duck/Zhejiang/6DK19-MA/2013[H5N2]
UAGUCACCGGGUGUUCAGGACGAACGGACGAACACACAUGCCU---------
                |:|||| ||||||||                     
----------------UUCUGC-UGCCUGCUCACUUGAUCCCGCCAUCUGCU

Strand 1 variance codes:
{0: 'nn1', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'bn0>bb1s', 6: 'bn1>bb0s', 7: 'bn0>bb0s', 8: 'bn0>bb0s', 9: 'bn1>bb0s', 10: 'bn0>bn0', 11: 'bn0>bn0', 12: 'bn0>bn0', 13: 'nn0', 14: 'nn0', 15: 'bn0>bn0', 16: 'bb0s>nb1', 17: 'bb0s>nb0', 18: 'bb0s>nb0', 19: 'nb0>nb0', 20: 'bb0s>nb0', 21: 'bb0s>nb0', 22: 'bn1>bn0', 23: 'nb0>nb0', 24: 'nb0>nb0', 25: 'bb0s>nb0', 26: 'bb0s>bb1s', 27: 'bb0s>bb0s', 28: 'bb0s>bb0s', 29: 'bb0s>bb0s', 30: 'bb1s>bb0s', 31: 'nn0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn1', 40: 'nn0', 41: 'nn0', 42: 'nn0'}
Strand 2 variance codes:
{0: 'nb1>bb0s', 1: 'nb0>bb0s', 2: 'nb0>bb0s', 3: 'nb0>nb0', 4: 'nb0>bb0s', 5: 'nb0>bb0s', 6: 'nb0>nb0', 7: 'nb0>nb0', 8: 'nb0>bb0s', 9: 'bb1s>bb0s', 10: 'bb0s>bb0s', 11: 'bb0s>bb0s', 12: 'bb0s>bb0s', 13: 'bb0s>bb1s', 14: 'bn0>bn0', 15: 'bn0>bn0', 16: 'bn0>bn0', 17: 'nn0', 18: 'nn1', 19: 'bn0>bn0', 20: 'nn0', 21: 'bn0>bb0s', 22: 'bn0>bb0s', 23: 'bn0>bb0s', 24: 'nn1', 25: 'bn0>bb0s', 26: 'bn0>bb0s', 27: 'bn0>bn1', 28: 'nn0', 29: 'bn0>bb0s', 30: 'bn1>bb0s', 31: 'bn0>bb0s', 32: 'bn0>bb0s', 33: 'bn0>bb0s', 34: 'bn0>bb1s'}

Covariance layer complex:
    *     *  *          1  *11     *        *   
----CAGUCAACGAGUGUUC-AGGACGGACGGACGGACACACAUACCU
         ||||||||  | ||| |||  ::|||:            
CUCUGCUGCUUGCUCACUCGAUCCAGCCA-UUUGCU------------
111111111*        *     *      *                
*     *  *         1  *11     *        *            
UAGUCACCGGGUGUUCAGGACGAACGGACGAACACACAUGCCU---------
                |:|||| ||||||||                     
----------------UUCUGC-UGCCUGCUCACUUGAUCCCGCCAUCUGCU
                111111 111*        *     *     *    

Reference strain:
A/WSN/1933
----CAGUCAACGAGUGUUC-AGGACGGACGGACGGACACACAUACCU
         ||||||||  | ||| |||  ::|||:            
CUCUGCUGCUUGCUCACUCGAUCCAGCCA-UUUGCU------------
Comparison strain:
A/mallard/Alberta/70/2017[H7N3]
UAGUCACCGAGUAUUCAGGACGGACGGACGAACACACAUACCU---------
                 :||| |||||||||                     
----------------CUCUG-CUGCCUGCUCACUUGACCCUGCCAUUUGCU

Strand 1 variance codes:
{0: 'nn1', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'bn0>bb1s', 6: 'bn1>bb0s', 7: 'bn0>bb0s', 8: 'bn0>bb0s', 9: 'bn0>bb0s', 10: 'bn0>bn0', 11: 'bn0>bn0', 12: 'bn1>bn0', 13: 'nn0', 14: 'nn0', 15: 'bn0>bn0', 16: 'bn0>bn1', 17: 'bb0s>nb0', 18: 'bb0s>nb0', 19: 'nb0>nb0', 20: 'bb0s>nb0', 21: 'bn0>bn0', 22: 'bb0s>nb0', 23: 'nb0>nb0', 24: 'nb0>nb0', 25: 'bb0s>nb0', 26: 'bb0s>bb1s', 27: 'bb0s>bb0s', 28: 'bb0s>bb0s', 29: 'bb0s>bb0s', 30: 'bb1s>bb0s', 31: 'nn0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn0', 42: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nb0>bb0s', 2: 'nb0>bb0s', 3: 'nb0>nb0', 4: 'nb0>bb0s', 5: 'nb0>bb0s', 6: 'nb0>nb0', 7: 'nb0>nb0', 8: 'nb0>bb0s', 9: 'bb1s>bb0s', 10: 'bb0s>bb0s', 11: 'bb0s>bb0s', 12: 'bb0s>bb0s', 13: 'bb0s>bb1s', 14: 'bn0>bn0', 15: 'bn0>bn0', 16: 'bn0>bn1', 17: 'nn0', 18: 'nn1', 19: 'bn0>bn0', 20: 'nn0', 21: 'bn1>bn0', 22: 'bn0>bb0s', 23: 'bn0>bb0s', 24: 'nn1', 25: 'bn0>bb0s', 26: 'bn0>bn0', 27: 'bn0>bb0s', 28: 'nn0', 29: 'bn0>bb0s', 30: 'bn0>bb0s', 31: 'bn0>bb0s', 32: 'bn0>bb0s', 33: 'bn0>bb0s', 34: 'bn0>bb1s'}

Covariance layer complex:
    *     *     *       1   11     *            
----CAGUCAACGAGUGUUC-AGGACGGACGGACGGACACACAUACCU
         ||||||||  | ||| |||  ::|||:            
CUCUGCUGCUUGCUCACUCGAUCCAGCCA-UUUGCU------------
 11111111*        *  *  *                       
*     *     *      1   11     *                     
UAGUCACCGAGUAUUCAGGACGGACGGACGAACACACAUACCU---------
                 :||| |||||||||                     
----------------CUCUG-CUGCCUGCUCACUUGACCCUGCCAUUUGCU
                 1111 1111*        *  *  *          
