241 HA NP

Strains found:
A/WSN/1933
A/Puerto Rico/8/1934-Korea/426/1968[H2N2]
A/Anhui/1-BALF_RG1/2013[H7N9]
A/Belgium/145-MA/2009[H1N1]
A/Brisbane/59/2007[H1N1]
A/Hong Kong/01/1968[H3N2]
A/duck/Hokkaido/Vac-3/2007[H5N1]
A/duck/Zhejiang/6DK19-MA/2013[H5N2]
A/mallard/Alberta/70/2017[H7N3]

forna copy-able:
>A/WSN/1933 29-5 2-35
GUCGCCUAGUUUUUUCGUGUGUCUUAGGACGGACGGACGGACACACAUACCUAGACGGC
(.((.(((((...(.(((..(((.().)))..))).)...).........)))).)).)
>A/Puerto Rico/8/1934-Korea/426/1968[H2N2] 38-19 19-34
GUCCCAUACGUCGUCUGUUUGAACAGACGGGCGGAC
((((.......((((((((..))))))))...))))
>A/Anhui/1-BALF_RG1/2013[H7N9] 29-3 2-34
GACGUCUAAUGUUUUCGUGAGUUAGCCGGACGAACGGACGAACACACAUGCCUGAACGUC
((((((..((((.(((((..(((..(())...)))..)))))...))))....)).))))
>A/Brisbane/59/2007[H1N1] 15-5 25-35
UCGUGUGUUUUAGAACGGACGG
.(((..(((.().)))..))).
>A/Hong Kong/01/1968[H3N2] 29-1 2-27
GUCGUCUAGAAUUUUCGUGAGUUCGUCGGCGGGCGGACACACAUACCUGGACGGC
(.((((.((.......(((.((.((.(.().).)).)).)))....)).)))).)
>A/duck/Hokkaido/Vac-3/2007[H5N1] 27-19 22-30
CGUCUGUUUGAACGGACG
((((.(((..))).))))
>A/duck/Zhejiang/6DK19-MA/2013[H5N2] 34-21 12-28
CAUGCGACGUCUGUACGGACGAACACACAUG
((((.(.((((.(()).))))....).))))
>A/mallard/Alberta/70/2017[H7N3] 43-35 24-34
UCCUCUUCCGGACGGACGGA
(((((.((()))..)).)))

Mutation block
A/WSN/1933 241 HA 583-625 NP 667-701
v = genomic start position; $ = genomic end position
               	$                                         v v                                 $
A/WSN/1933	CCCTAGTCCGATACGTCGCCTAGTTTTTTCGTGTGTCTTACGG&ACGGCAGATCCATACACACAGGCAGGCAGGCAGGA
A/WSN/1933	                                           &                                   
A/Puerto Rico..	         C        T  GT  C  AG   A  T  C  T& T     G               G     A  A  
A/Anhui/1-BAL..	    CTC  TTG   A  T   A G        A  TAGC  T&  T   AG   G        A       A      
A/Belgium/145..	   C     T        G  G AC  C               &  T   AGC           A              
A/Brisbane/59..	T     A        A  T                 T      &     T G                        A  
A/Hong Kong/0..	   GT    TG T     T    AA        A  TCGT   &       G               G     A  A  
A/duck/Hokkai..	T   TCA  T  G  A  T  GT  C  AG   G  T  C  T&  A   AG      T     A       A      
A/duck/Zhejia..	   CTCA  C  G  A  T  GT  C  AG   G  T  C  A&  T   AG   G        A       A      
A/mallard/Alb..	T   CT   ATG      A   A G        A  TAG   T&  A   AG            A              

Per-nucleotide structure block
A/WSN/1933	CCCUAGUCCGAUACGUCGCCUAGUUUUUUCGUGUGUCUUACGG&ACGGCAGAUCCAUACACACAGGCAGGCAGGCAGGA
A/WSN/1933	..............|:||.|||||::.|:|||.:|||:|....&.|:||.||||.........|::.|:|||.:|||:|
A/WSN/1933	                                           &                                   	-11.65 kcal/mol
A/Puerto Rico..	     ||||     .   |      . ..... ......    & .... ....        | || .    ||    .	-9.38 kcal/mol
A/Anhui/1-BAL..	               |  | ..  ||  |    .   ..||  &  |     ..  ||||    ||| ..  |... |.	-3.34 kcal/mol
A/Belgium/145..	xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx&xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx	N/A
A/Brisbane/59..	              .... ....... .               & .... ....         ... .           	-6.45 kcal/mol
A/Hong Kong/0..	                  | :  ... ...  |.  :| :| |&     | :      ||| |  ||:|. . ......	-15.83 kcal/mol
A/duck/Hokkai..	              ..  | :    . ..... ......    & .... ....         ..|| |:  | .....	-8.11 kcal/mol
A/duck/Zhejia..	         ||||  .  | :  ... ..... ......    & .... .... |||| |  ..|| |:   ......	-4.56 kcal/mol
A/mallard/Alb..	|||||:|||     .... ....... ..... ......    & .... ....         ...  | . | .  |.	-4.45 kcal/mol