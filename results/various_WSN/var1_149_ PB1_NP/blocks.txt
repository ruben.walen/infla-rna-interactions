149 PB1 NP

Strains found:
A/WSN/1933
A/Puerto Rico/8/1934-Korea/426/1968[H2N2]
A/Anhui/1-BALF_RG1/2013[H7N9]
A/Belgium/145-MA/2009[H1N1]
A/Brisbane/59/2007[H1N1]
A/Hong Kong/01/1968[H3N2]
A/duck/Hokkaido/Vac-3/2007[H5N1]
A/duck/Zhejiang/6DK19-MA/2013[H5N2]
A/mallard/Alberta/70/2017[H7N3]

forna copy-able:
>A/WSN/1933 3-27 32-9
AGUCAUAGGUCUGUCGGCCUUGUGUACACACAGGCAGGCAGGCAGGACU
((((....(.(((.(.((((((((())))).)))).).))).)..))))
>A/Puerto Rico/8/1934-Korea/426/1968[H2N2] 21-24 29-26
CUUGCAAG
(((())))
>A/Anhui/1-BALF_RG1/2013[H7N9] 19-22 22-19
GCCUAGGC
(((())))
>A/Belgium/145-MA/2009[H1N1] 3-27 32-11
AAUCAUAAGUCUGGCGACCUUGAGUACACAAGCAGGCAGGCAGGAUU
((((....(.(((.(...((((.(()).))))..).))).)..))))
>A/Brisbane/59/2007[H1N1] 3-27 32-9
AGUCAUAGGUCUGUCUGCCUUGUGUACACACAGGCAGGCAGGCAAGACU
((((....(.(((.((((((((((())))).)))))).))).)..))))
>A/Hong Kong/01/1968[H3N2] 30-35 18-13
GUCUGUACAGGC
(.(((()))).)
>A/duck/Hokkaido/Vac-3/2007[H5N1] 19-22 22-19
GCCUAGGC
(((())))
>A/duck/Zhejiang/6DK19-MA/2013[H5N2] 26-35 22-13
GUCAGUUUGUACAAGCAGGC
(.(.(.(((()))).).).)

Mutation block
A/WSN/1933 149 PB1 1925-1959 NP 672-706
v = genomic start position; $ = genomic end position
               	v                                 $ $                                 v
A/WSN/1933	CCAGTCATAGGTCTGTCGGCCTTGTGTCAGCTTGT&TGTTCAGGACGGACGGACGGACACACATACCTAGA
A/WSN/1933	                                   &                                   
A/Puerto Rico..	               A  A     G     T    &    T  A  A     G               G  
A/Anhui/1-BAL..	               G        G     T  A & A  T      A       A        G   GA 
A/Belgium/145..	   A     A     G  A     A  T  T  A & A  T              A           CGA 
A/Brisbane/59..	                 T            T    &       A                        G T
A/Hong Kong/0..	   A           A  A     G     TC   &    T  A  A     G               G  
A/duck/Hokkai..	   A           A        G    AT    & A         A       A     T      GA 
A/duck/Zhejia..	               G  A     G     T    &           A       A        G   GA 
A/mallard/Alb..	   A           A        G    AT    & A                 A            GA 

Per-nucleotide structure block
A/WSN/1933	CCAGUCAUAGGUCUGUCGGCCUUGUGUCAGCUUGU&UGUUCAGGACGGACGGACGGACACACAUACCUAGA
A/WSN/1933	..||||....|:|||:|.|||||||||........&...||||..|:|||:|.||||.|||||........
A/WSN/1933	                                   &                                   	-14.78 kcal/mol
A/Puerto Rico..	  ....    ....... ..    ...        &   ... || ...... .... .....        	-2.92 kcal/mol
A/Anhui/1-BAL..	  ....    .......     .....        &   ....  .... | |.... .....        	-4.43 kcal/mol
A/Belgium/145..	               .  ..    .          &              .  .   |.  ..        	-4.85 kcal/mol
A/Brisbane/59..	                 |                 &                |                  	-20.93 kcal/mol
A/Hong Kong/0..	  ....    ....... .........  |:||||&   ....  .......  :  | ....        	-4.04 kcal/mol
A/duck/Hokkai..	  ....    .......     .....        &   ....  .... | |.... .....        	-2.94 kcal/mol
A/duck/Zhejia..	  ....    ....... ....... :| |:||||&   ....  ....     :  | ....        	-5.58 kcal/mol
A/mallard/Alb..	xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx&xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx	N/A