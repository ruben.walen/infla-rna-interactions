>A/WSN/1933
----CAGUCAACGAGUGUUC-AGGACGGACGGACGGACACACAUACCU
         ||||||||  | ||| |||  ::|||:            
CUCUGCUGCUUGCUCACUCGAUCCAGCCA-UUUGCU------------

>A/Bayern/7/1995
No sign. structure

>A/Beijing/262/1995
CAGUC----AACGAGUGUUC-AGAACGGAUGGACGGACACACAUACCU
 ||||    |||:||||  | ||  ||| |::|||:            
CUCAGCUGCUUGUUCACUCGAUCCAGCC-AUUUGCU------------

>A/Brazil/11/1978
----CAGUCAACGAGUGUUC-AGAACGGACGGACGGACACACAUACCU
         ||||||||  | ||  |||  ::|||:            
CUCUGCUGCUUGCUCACUCGAUCCAGCCA-UUUGCU------------

>A/Brisbane/59/2007
UAGUC----AACGAGUGUUC-AGAACGGACGGACGGACACACAUACCU
 ||||    |||:||:|  | ||  |||  ::|||:            
CUCAGCUGCUUGUUCGCUCGAUCCAGCCA-UUUGCU------------

>A/California/07/2009
UAGUCAACGUGUAUUUAGGACGGACGGACGAACACACAUACCC---------
                ||| | |||||||:|                     
---------------CUCC-G-CUGCCUGUUCACUCGAUCCAGCCAUCUGUU

>A/Chile/1/1983
----CAGUCAACGAGUGUUC-AGAACGGACGGACGGACACACAUACCU
         ||||||||  | ||  |||  ::|||:            
CUCUGCUGCUUGCUCACUCGAUCCAGCCA-UUUGCU------------

>A/Michigan/45/2015
-------UAGUCAACGUGUAUUUAGGACGGACGGACGAACACACAUACCC
           ||| |||   :|||| |||  ||||:|            
CUCUGCUGCCUGUU-CACUC-GAUCCAGCCA-CCUGUU------------

>A/New Caledonia/20/1999
CAGUC----AACGAGUGUUC-AGAACGGACGGACGGACACACAUACCU
 ||||    |||:||||  | ||  |||  ::|||:            
CUCAGCUGCUUGUUCACUCGAUCCAGCCA-UUUGCU------------

>A/Singapore/6/1986
----CAGUCAACGAGUGUUC-AGGACGGACGGACGGACACACAUACCU
          |||||||  | ||| |||  ::|||:            
CUCUGCUGCCUGCUCACUCGAUCCAGCCA-UUUGCU------------

>A/Solomon Islands/3/2006
CAGUC----AACGAGUGUUC-AGAACGGACGGACGGACACACAUGCCC
 ||||    |||:||||  | ||  |||  ::|||:            
CUCAGCUGCUUGUUCACUCGAUCCAGCCA-UUUGCU------------

>A/South Dakota/06/2007
UAGUC----AACGAGUGUUC-AGAACGGACGGACGGACACACAUACCU
 ||||    |||:||:|  | ||  |||  ::|||:            
CUCAGCUGCUUGUUCGCUCGAUCCAGCCA-UUUGCU------------

>A/USSR/90/1977
----CAGUCAACGAGUGUUC-AGAACGGACGGACGGACACACAUACCU
         ||||||||  | ||  |||  ::|||:            
CUCUGCUGCUUGCUCACUCGAUCCAGCCA-UUUGCU------------

