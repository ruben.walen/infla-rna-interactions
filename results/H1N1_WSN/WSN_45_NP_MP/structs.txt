>A/WSN/1933
-ACUCCCUGCC-GACUAAGUCUUGUCGAAUUGUUAUCUCUCUUACCACG------
 ||||||  || ||||  |:|:||        :||||||||||            
UUGAGGG--GGCCUGA--CGGGAC--------GAUAGAGAGAACGUACGUUUCGA

>A/Bayern/7/1995
No sign. structure

>A/Beijing/262/1995
-ACUCCCUGCUGACUAGGUCUUGUCGAAUUGUUAUCUCUCUUACCACG------
 ||||||: | |||| | :||         |:||||||||||            
UUGAGGGG-GCCUGA-CGGGA---------CGAUAGAGAGAACGUACGUUUCGA

>A/Brazil/11/1978
-ACUCCCUGCC-GACUAGGUCUUGUCGAAUUGUUAUCUCUCUUACCACG------
 ||||||  || |||| | :||         |:||||||||||            
UUGAGGG--GGCCUGA-CGGGA---------CGAUAGAGAGAACGUACGUUUCGA

>A/Brisbane/59/2007
-ACUCCCUGCC-GACUAGGUCUUGUCGAAUUGUUAUCUCUCUUACCACG------
 ||||||  || ||||  |:|:|:        :||||||||||            
UUGAGGG--GGCCUGA--CGGGAU--------GAUAGAGAGAACGUACGUUUCGA

>A/California/07/2009
ACUACCUGCUGAUUAGGUCUUAUCGUAUUGUUAUCUCUCCUACCACG----------------------------
                                  |||:||                                   
--------------------------------UUGAGGGGGCCUGACGGGAUGAUAGAAAGAACGUACGUUUCGA

>A/Chile/1/1983
-ACUCCCUGCC-GACUAGGUCUUGUCGAAUUGUUAUCUCUCUUACCACG------
 ||||||  || |||| | :||         |:||||||||||            
UUGAGGG--GGCCUGA-CGGGA---------CGAUAGAGAGAACGUACGUUUCGA

>A/Michigan/45/2015
ACUACCUGCUGAUUAGGUCUUAUCGUAUUGUUAUCUCUCCUACCACG----------------------------
                                  |||:||                                   
--------------------------------UUGAGGGGGCCUGACGGGAUGAUAGAAAGAACGUACGUUUCGA

>A/New Caledonia/20/1999
-ACUCCCUGCUGACUAGGUCUUGUCGAACUGUUAUCUCUCUUACCACG------
 ||||||: | ||||  |:|        |||:||||||||||            
UUGAGGGG-GCCUGA--CGG--------GACGAUAGAGAGAACGUACGUUUCGA

>A/Singapore/6/1986
-ACUCCCUGCUGACUAGGUCUUGUCGAAUUGUUAUCUCUCUUACCACG------
 ||||||: | |||| | :||         |:||||||||||            
UUGAGGGG-GCCUGA-CGGGA---------CGAUAGAGAGAACGUACGUUUCGA

>A/Solomon Islands/3/2006
-ACUCCCUGCC-GACUAGGUCUUGUCGAAUUGUUAUCUCUCUUACCACG------
 ||||||  || ||||  |:|:|:        :||||||||||            
UUGAGGG--GGCCUGA--CGGGAU--------GAUAGAGAGAACGUACGUUUCGA

>A/South Dakota/06/2007
-ACUCCCUGCC-GACUAGGUCUUGUCGAAUUGUUAUCUCUCUUACCACG------
 ||||||  || ||||  |:|:|:        :||||||||||            
UUGAGGG--GGCCUGA--CGGGAU--------GAUAGAGAGAACGUACGUUUCGA

>A/USSR/90/1977
-ACUCCCUGCC-GACUAGGUCUUGUCGAAUUGUUAUCUCUCUUACCACG------
 ||||||  || |||| | :||         |:||||||||||            
UUGAGGG--GGCCUGA-CGGGA---------CGAUAGAGAGAACGUACGUUUCGA

