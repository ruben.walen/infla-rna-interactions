92 PB2 112-152 (40) PA 1540-1566 (26)
Reference strain: A/WSN/1933
Strain	FE/interaction found	Seg. 1 span	Seg. 2 span	Full bonds/half bonds/no bonds	Mutations in whole site seg.1 / seg. 2
A/WSN/1933	-16.67	29-4 (25)	4-27 (23)	18/2/7	0/0
A/Bayern/7/1995	Incomplete sequence	N/A	N/A	N/A	N/A/N/A
A/Beijing/262/1995	-6.63	19-3 (16)	9-24 (15)	11/1/6	4/4
A/Brazil/11/1978	-6.17	29-4 (25)	4-27 (23)	16/3/9	4/1
A/Brisbane/59/2007	-7.04	29-3 (26)	4-24 (20)	15/2/10	4/4
A/California/07/2009	-9.61	29-4 (25)	4-27 (23)	16/3/9	2/3
A/Chile/1/1983	-7.59	29-3 (26)	4-24 (20)	16/2/10	4/2
A/Michigan/45/2015	-10.33	29-4 (25)	4-27 (23)	16/3/8	5/1
A/New Caledonia/20/1999	-6.94	29-3 (26)	4-24 (20)	15/2/10	3/4
A/Singapore/6/1986	-7.81	29-3 (26)	4-24 (20)	15/2/10	4/3
A/Solomon Islands/3/2006	Incomplete sequence	N/A	N/A	N/A	N/A/N/A
A/South Dakota/06/2007	-7.04	29-3 (26)	4-24 (20)	15/2/10	4/4
A/USSR/90/1977	-6.17	29-4 (25)	4-27 (23)	16/3/9	4/1