92 PB2 PA

Strains found:
A/WSN/1933
A/Bayern/7/1995
A/Beijing/262/1995
A/Brazil/11/1978
A/Brisbane/59/2007
A/California/07/2009
A/Chile/1/1983
A/Michigan/45/2015
A/New Caledonia/20/1999
A/Singapore/6/1986
A/Solomon Islands/3/2006
A/South Dakota/06/2007
A/USSR/90/1977

forna copy-able:
>A/WSN/1933 29-4 4-27
UUAACCCGUUCCUCUGCACCACAACCGGCUGGUUUCAGAGGGCGGUUUGA
(.((.(((..((((((.((((...(()).))))..)))))).))).)).)
>A/Beijing/262/1995 19-3 9-24
CCCCUGCAUCACAACCAUGGUUUCGGAAGGCGG
(((((....(..((((()))))..)..))).))
>A/Brazil/11/1978 29-4 4-27
UUAACCUGUUCCCCUGCAUCACAACCGGCUGGUUUCAGAGGGCGGCUUGA
(.((.(.(..((((((.(.((...(()).)).)..))).)))).).)).)
>A/Brisbane/59/2007 29-3 4-24
UUAACCCGUUCCCCUACAUCACAACCAUGGUUUCGGAAGGCGGCUUGA
(.((.(((....(((....(..((((()))))..)..)))))).)).)
>A/California/07/2009 29-4 4-27
UUAACCCGUUCCCCUGCAUCACAACCGGCUGGUUUCAGAGGGUGGCUUGA
(.((.((...((((((.(.((...(()).)).)..))).))).)).)).)
>A/Chile/1/1983 29-3 4-24
UUAACCUGUUCCCCUGCAUCACAACCAUGGUUUCAGAGGGCGGCUUGA
(.((.(.(..((((((......((((())))).))).)))).).)).)
>A/Michigan/45/2015 29-4 4-27
UUAACCCGUCCCCCUGUAUCACAACCGGCUGGUUUCAGAGGGUGGUUUGA
(.((.((..(((.(((.(.((...(()).)).)..))).))).)).)).)
>A/New Caledonia/20/1999 29-3 4-24
UUAACCCGUUCCCCUGCAUCACAACCAUGGUUUCGGAAGGCGGCUUGA
(.((.(((....(((....(..((((()))))..)..)))))).)).)
>A/Singapore/6/1986 29-3 4-24
UUAACCUGUUCCCCUGCAACACAACCAUGGUUUCGGAGGGCGGCUUGA
(.((.(.(...((((.(.....((((()))))..).))))).).)).)
>A/South Dakota/06/2007 29-3 4-24
UUAACCCGUUCCCCUACAUCACAACCAUGGUUUCGGAAGGCGGCUUGA
(.((.(((....(((....(..((((()))))..)..)))))).)).)
>A/USSR/90/1977 29-4 4-27
UUAACCUGUUCCCCUGCAUCACAACCGGCUGGUUUCAGAGGGCGGCUUGA
(.((.(.(..((((((.(.((...(()).)).)..))).)))).).)).)

Mutation block
A/WSN/1933 92 PB2 112-152 PA 1540-1566
v = genomic start position; $ = genomic end position
               	$                                       v v                         $
A/WSN/1933	CCGATTACACGATTAACCCGTTCCTCTGCACCACAACCATT&AGAAGTTTGGCGGGAGACTTTGGTCGG
A/WSN/1933	                                         &                           
A/Bayern/7/1995	xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx&xxxxxxxxxxxxxxxxxxxxxxxxxxx
A/Beijing/262..	T                 T     C     T          &       C     A  G        A 
A/Brazil/11/1..	T                 T     C     T          &       C                   
A/Brisbane/59..	T                       C  A  T          &       C     A  G        A 
A/California/..	                        C     T          &G      C  T                
A/Chile/1/1983	T                 T     C     T          &       C                 A 
A/Michigan/45..	T                    C  C   T T          &          T                
A/New Caledon..	T                       C     T          &       C     A  G        A 
A/Singapore/6..	T                 T     C     A          &       C        G        A 
A/Solomon Isl..	xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx&xxxxxxxxxxxxxxxxxxxxxxxxxxx
A/South Dakot..	T                       C  A  T          &       C     A  G        A 
A/USSR/90/1977	T                 T     C     T          &       C                   

Per-nucleotide structure block
A/WSN/1933	CCGAUUACACGAUUAACCCGUUCCUCUGCACCACAACCAUU&AGAAGUUUGGCGGGAGACUUUGGUCGG
A/WSN/1933	............|:||.|||.:||||||.||||...||...&...|:||.|||:||||||..||||.||
A/WSN/1933	                                         &                           	-16.67 kcal/mol
A/Bayern/7/1995	xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx&xxxxxxxxxxxxxxxxxxxxxxxxxxx	N/A
A/Beijing/262..	            .... ... .     . .: . ||  |  &   ....   .|  .: . |     ..	-6.63 kcal/mol
A/Brazil/11/1..	                  :  .        :          &         : |  .      :     	-6.17 kcal/mol
A/Brisbane/59..	                     ...   . .: . ||  |  &           |  .: . |     ..	-7.04 kcal/mol
A/California/..	                   : .        :          &          :|  .      :     	-9.61 kcal/mol
A/Chile/1/1983	                  :  .       .... ||  |  &         : |  .    |     ..	-7.59 kcal/mol
A/Michigan/45..	                   : |  .     :          &          :|  .      :     	-10.33 kcal/mol
A/New Caledon..	                     ...   . .: . ||  |  &           |  .: . |     ..	-6.94 kcal/mol
A/Singapore/6..	                  :  ..    .|.... ||  |  &         : |   . . |     ..	-7.81 kcal/mol
A/Solomon Isl..	xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx&xxxxxxxxxxxxxxxxxxxxxxxxxxx	N/A
A/South Dakot..	                     ...   . .: . ||  |  &           |  .: . |     ..	-7.04 kcal/mol
A/USSR/90/1977	                  :  .        :          &         : |  .      :     	-6.17 kcal/mol