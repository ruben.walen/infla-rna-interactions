323 NP 1087-1123 (36) NS 504-546 (42)
Reference strain: A/WSN/1933
Strain	FE/interaction found	Seg. 1 span	Seg. 2 span	Full bonds/half bonds/no bonds	Mutations in whole site seg.1 / seg. 2
A/WSN/1933	-1.81	19-22 (3)	37-34 (3)	4/0/0	0/0
A/Bayern/7/1995	Incomplete sequence	N/A	N/A	N/A	N/A/N/A
A/Beijing/262/1995	-2.3	18-35 (17)	42-27 (15)	11/2/5	2/1
A/Brazil/11/1978	-1.79	18-35 (17)	42-27 (15)	9/2/7	1/0
A/Brisbane/59/2007	-2.71	10-24 (14)	12-1 (11)	10/2/3	3/3
A/California/07/2009	-1.7	8-19 (11)	9-1 (8)	7/2/3	7/13
A/Chile/1/1983	-1.97	29-32 (3)	16-13 (3)	4/0/0	2/1
A/Michigan/45/2015	-1.7	8-19 (11)	9-1 (8)	7/2/3	7/13
A/New Caledonia/20/1999	-2.21	18-35 (17)	42-27 (15)	11/2/5	3/2
A/Singapore/6/1986	-2.3	18-35 (17)	42-27 (15)	11/2/5	2/1
A/Solomon Islands/3/2006	Incomplete sequence	N/A	N/A	N/A	N/A/N/A
A/South Dakota/06/2007	-2.71	10-24 (14)	12-1 (11)	10/2/3	3/3
A/USSR/90/1977	-1.79	18-35 (17)	42-27 (15)	9/2/7	1/0