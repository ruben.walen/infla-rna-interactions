Covariation report for: 153 PA NP

Reference strain:
A/WSN/1933
CGUACCUCAAAAGGGAGUG-ACUGGGUUCUGAACUU
    ||     ||:||||| |||:|||    |||  
-UCCGGCUC--UCUCUCACUUGAUCCACCA-UUGU-
Comparison strain:
A/Beijing/262/1995
CGUACCUCAAAAGGGAGUG-ACUGGGUUCUGAACUC
    ||   ||||:||||| |||:|||    |||  
-UUCGGC--UUUCUCUCACUUGAUCCAUCA-UUGC-

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nb0>nb0', 10: 'nb0>nb1', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'nn0', 27: 'nn0', 28: 'nn0', 29: 'nn0', 30: 'bb0>bb0', 31: 'bb0>bb0', 32: 'bb0>bb0', 33: 'nn0', 34: 'nn1'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn1', 2: 'nn0', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'nn0', 6: 'nb0>nb0', 7: 'nb1>nb0', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'nn0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nn1', 25: 'nn0', 26: 'nn0', 27: 'bb0>bb0', 28: 'bb0>bb0', 29: 'bb0>bb0', 30: 'nn1'}

Covariance layer complex:
         11                        *
CGUACCUCAAAAGGGAGUG-ACUGGGUUCUGAACUU
    ||     ||:||||| |||:|||    |||  
-UCCGGCUC--UCUCUCACUUGAUCCACCA-UUGU-
  *    11                  *      * 
         11                        *
CGUACCUCAAAAGGGAGUG-ACUGGGUUCUGAACUC
    ||   ||||:||||| |||:|||    |||  
-UUCGGC--UUUCUCUCACUUGAUCCAUCA-UUGC-
  *      11                *      * 

Reference strain:
A/WSN/1933
CGUACCUCAAAAGGGAGUG-ACUGGGUUCUGAACUU
    ||     ||:||||| |||:|||    |||  
-UCCGGCUC--UCUCUCACUUGAUCCACCA-UUGU-
Comparison strain:
A/Brazil/11/1978
CGUACCUCAAAAGAGAGUG-ACUGGGUUCUGAACUC
    ||   |||||||||| |||:|||    |||  
-UCCGGC--UUUCUCUCACUUGAUCCAUCA-UUGC-

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nb0>nb0', 10: 'nb0>nb1', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb1>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'nn0', 27: 'nn0', 28: 'nn0', 29: 'nn0', 30: 'bb0>bb0', 31: 'bb0>bb0', 32: 'bb0>bb0', 33: 'nn0', 34: 'nn1'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'nn0', 6: 'nb0>nb0', 7: 'nb1>nb0', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb1', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'nn0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nn1', 25: 'nn0', 26: 'nn0', 27: 'bb0>bb0', 28: 'bb0>bb0', 29: 'bb0>bb0', 30: 'nn1'}

Covariance layer complex:
         11  *                     *
CGUACCUCAAAAGGGAGUG-ACUGGGUUCUGAACUU
    ||     ||:||||| |||:|||    |||  
-UCCGGCUC--UCUCUCACUUGAUCCACCA-UUGU-
       11                  *      * 
         11  *                     *
CGUACCUCAAAAGAGAGUG-ACUGGGUUCUGAACUC
    ||   |||||||||| |||:|||    |||  
-UCCGGC--UUUCUCUCACUUGAUCCAUCA-UUGC-
         11                *      * 

Reference strain:
A/WSN/1933
CGUACCUCAAAAGGGAGUG-ACUGGGUUCUGAACUU
    ||     ||:||||| |||:|||    |||  
-UCCGGCUC--UCUCUCACUUGAUCCACCA-UUGU-
Comparison strain:
A/Brisbane/59/2007
CGUACCUCAAAAGGGAGUG-ACUGGGUUCUGAACUC
    ||   ||||:||||| ||::|||    |||  
-UCCGGC--UUUCUCUCACUUGGUCCAUCA-UUGC-

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nb0>nb0', 10: 'nb0>nb1', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb1', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'nn0', 27: 'nn0', 28: 'nn0', 29: 'nn0', 30: 'bb0>bb0', 31: 'bb0>bb0', 32: 'bb0>bb0', 33: 'nn0', 34: 'nn1'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'nn0', 6: 'nb0>nb0', 7: 'nb1>nb0', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'nn0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb1>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nn1', 25: 'nn0', 26: 'nn0', 27: 'bb0>bb0', 28: 'bb0>bb0', 29: 'bb0>bb0', 30: 'nn1'}

Covariance layer complex:
         11                        *
CGUACCUCAAAAGGGAGUG-ACUGGGUUCUGAACUU
    ||     ||:||||| |||:|||    |||  
-UCCGGCUC--UCUCUCACUUGAUCCACCA-UUGU-
       11             *    *      * 
         11                        *
CGUACCUCAAAAGGGAGUG-ACUGGGUUCUGAACUC
    ||   ||||:||||| ||::|||    |||  
-UCCGGC--UUUCUCUCACUUGGUCCAUCA-UUGC-
         11           *    *      * 

Reference strain:
A/WSN/1933
CGUACCUCAAAAGGGAGUG-ACUGGGUUCUGAACUU
    ||     ||:||||| |||:|||    |||  
-UCCGGCUC--UCUCUCACUUGAUCCACCA-UUGU-
Comparison strain:
A/California/07/2009
CAUACCUCAAGAGUGAGUG-ACUGGGCUCUGACCUC
         |:|| ||:|| |||:||   |:||   
---UUCGACUUUCUCUUACUUGAUCCAUCAUUGC--

Strand 1 variance codes:
{0: 'nn0', 1: 'nn1', 2: 'nn0', 3: 'nn0', 4: 'bn0>bn0', 5: 'bn0>bn1', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nb0>nb0', 10: 'nb1>nb1', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bn1>bn0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb1', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bn1>bn0', 26: 'nn0', 27: 'nn0', 28: 'nb0>nb0', 29: 'nb0>bb0s', 30: 'bb0s>bb0s', 31: 'bb1s>bb0s', 32: 'bn0>bb0s', 33: 'nn0', 34: 'nn1'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn1', 2: 'nn0', 3: 'bn0>bn0', 4: 'bn1>bn0', 5: 'nn0', 6: 'nb0>nb0', 7: 'nb1>nb1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bn0>bn1', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb1>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'nn0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bn0>bn1', 24: 'nn1', 25: 'nn0', 26: 'nb0>nb0', 27: 'bb0s>nb0', 28: 'bb0s>bb0s', 29: 'bb0s>bb1s', 30: 'nn1'}

Covariance layer complex:
 *       11  *            *  11 *  *
CGUACCUCAAAAGGGAGUG-ACUGGGUUCUGAACUU
    ||     ||:||||| |||:|||    |||  
-UCCGGCUC--UCUCUCACUUGAUCCACCA-UUGU-
  *  * 11       *          * 1    * 
 *       11  *            *  11 *  *
CAUACCUCAAGAGUGAGUG-ACUGGGCUCUGACCUC
         |:|| ||:|| |||:||   |:||   
---UUCGACUUUCUCUUACUUGAUCCAUCAUUGC--
    *  * 11     *          * 1   *  

Reference strain:
A/WSN/1933
CGUACCUCAAAAGGGAGUG-ACUGGGUUCUGAACUU
    ||     ||:||||| |||:|||    |||  
-UCCGGCUC--UCUCUCACUUGAUCCACCA-UUGU-
Comparison strain:
A/Chile/1/1983
CGUACCUCAAAAGAGAAUG-ACUGGGUUCUGAACUC
    ||   ||||||| || |||:|||    |||  
-UCCGGC--UUUCUCUCACUUGAUCCAUCA-UUGC-

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nb0>nb0', 10: 'nb0>nb1', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb1>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bn1>bn0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'nn0', 27: 'nn0', 28: 'nn0', 29: 'nn0', 30: 'bb0>bb0', 31: 'bb0>bb0', 32: 'bb0>bb0', 33: 'nn0', 34: 'nn1'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'nn0', 6: 'nb0>nb0', 7: 'nb1>nb0', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb1', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bn0>bn1', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'nn0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nn1', 25: 'nn0', 26: 'nn0', 27: 'bb0>bb0', 28: 'bb0>bb0', 29: 'bb0>bb0', 30: 'nn1'}

Covariance layer complex:
         11  *  *                  *
CGUACCUCAAAAGGGAGUG-ACUGGGUUCUGAACUU
    ||     ||:||||| |||:|||    |||  
-UCCGGCUC--UCUCUCACUUGAUCCACCA-UUGU-
       11                  *      * 
         11  *  *                  *
CGUACCUCAAAAGAGAAUG-ACUGGGUUCUGAACUC
    ||   ||||||| || |||:|||    |||  
-UCCGGC--UUUCUCUCACUUGAUCCAUCA-UUGC-
         11                *      * 

Reference strain:
A/WSN/1933
CGUACCUCAAAAGGGAGUG-ACUGGGUUCUGAACUU
    ||     ||:||||| |||:|||    |||  
-UCCGGCUC--UCUCUCACUUGAUCCACCA-UUGU-
Comparison strain:
A/Michigan/45/2015
CAUACCUCAAGAGUGAGUG-ACUAGGUUCUGACCUU
         |:|| ||:|| |||||||  |:||   
---UUCGACUUUCUCUUACUUGAUCCAUCAUUGC--

Strand 1 variance codes:
{0: 'nn0', 1: 'nn1', 2: 'nn0', 3: 'nn0', 4: 'bn0>bn0', 5: 'bn0>bn1', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nb0>nb0', 10: 'nb1>nb1', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bn1>bn0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb1', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb1>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'nn0', 27: 'nn0', 28: 'nb0>nb0', 29: 'nb0>bb0s', 30: 'bb0s>bb0s', 31: 'bb1s>bb0s', 32: 'bn0>bb0s', 33: 'nn0', 34: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn1', 2: 'nn0', 3: 'bn0>bn0', 4: 'bn1>bn0', 5: 'nn0', 6: 'nb0>nb0', 7: 'nb1>nb1', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bn0>bn1', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb1>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'nn0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb1', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nn1', 25: 'nn0', 26: 'nb0>nb0', 27: 'bb0s>nb0', 28: 'bb0s>bb0s', 29: 'bb0s>bb1s', 30: 'nn1'}

Covariance layer complex:
 *       11  *         *     11 *   
CGUACCUCAAAAGGGAGUG-ACUGGGUUCUGAACUU
    ||     ||:||||| |||:|||    |||  
-UCCGGCUC--UCUCUCACUUGAUCCACCA-UUGU-
  *  * 11       *          * 1    * 
 *       11  *         *     11 *   
CAUACCUCAAGAGUGAGUG-ACUAGGUUCUGACCUU
         |:|| ||:|| |||||||  |:||   
---UUCGACUUUCUCUUACUUGAUCCAUCAUUGC--
    *  * 11     *          * 1   *  

Reference strain:
A/WSN/1933
CGUACCUCAAAAGGGAGUG-ACUGGGUUCUGAACUU
    ||     ||:||||| |||:|||    |||  
-UCCGGCUC--UCUCUCACUUGAUCCACCA-UUGU-
Comparison strain:
A/New Caledonia/20/1999
CGUACCUCAAAAGGGAGUG-ACUGGGUUCUGAACUC
    ||   ||||:||||| |||:|||    |||  
-UCCGGC--UUUCUCUCACUUGAUCCAUCA-UUGC-

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nb0>nb0', 10: 'nb0>nb1', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'nn0', 27: 'nn0', 28: 'nn0', 29: 'nn0', 30: 'bb0>bb0', 31: 'bb0>bb0', 32: 'bb0>bb0', 33: 'nn0', 34: 'nn1'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'nn0', 6: 'nb0>nb0', 7: 'nb1>nb0', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'nn0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nn1', 25: 'nn0', 26: 'nn0', 27: 'bb0>bb0', 28: 'bb0>bb0', 29: 'bb0>bb0', 30: 'nn1'}

Covariance layer complex:
         11                        *
CGUACCUCAAAAGGGAGUG-ACUGGGUUCUGAACUU
    ||     ||:||||| |||:|||    |||  
-UCCGGCUC--UCUCUCACUUGAUCCACCA-UUGU-
       11                  *      * 
         11                        *
CGUACCUCAAAAGGGAGUG-ACUGGGUUCUGAACUC
    ||   ||||:||||| |||:|||    |||  
-UCCGGC--UUUCUCUCACUUGAUCCAUCA-UUGC-
         11                *      * 

Reference strain:
A/WSN/1933
CGUACCUCAAAAGGGAGUG-ACUGGGUUCUGAACUU
    ||     ||:||||| |||:|||    |||  
-UCCGGCUC--UCUCUCACUUGAUCCACCA-UUGU-
Comparison strain:
A/Singapore/6/1986
CGUACCUCAAAAGAGAGUG-ACUGGGUUCUGAACUC
    ||   |||||||||| |||:|||    |||  
-UUCGGC--UUUCUCUCACUUGAUCCAUCA-UUGC-

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nb0>nb0', 10: 'nb0>nb1', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb1>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'nn0', 27: 'nn0', 28: 'nn0', 29: 'nn0', 30: 'bb0>bb0', 31: 'bb0>bb0', 32: 'bb0>bb0', 33: 'nn0', 34: 'nn1'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn1', 2: 'nn0', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'nn0', 6: 'nb0>nb0', 7: 'nb1>nb0', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb1', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'nn0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nn1', 25: 'nn0', 26: 'nn0', 27: 'bb0>bb0', 28: 'bb0>bb0', 29: 'bb0>bb0', 30: 'nn1'}

Covariance layer complex:
         11  *                     *
CGUACCUCAAAAGGGAGUG-ACUGGGUUCUGAACUU
    ||     ||:||||| |||:|||    |||  
-UCCGGCUC--UCUCUCACUUGAUCCACCA-UUGU-
  *    11                  *      * 
         11  *                     *
CGUACCUCAAAAGAGAGUG-ACUGGGUUCUGAACUC
    ||   |||||||||| |||:|||    |||  
-UUCGGC--UUUCUCUCACUUGAUCCAUCA-UUGC-
  *      11                *      * 

Reference strain:
A/WSN/1933
CGUACCUCAAAAGGGAGUG-ACUGGGUUCUGAACUU
    ||     ||:||||| |||:|||    |||  
-UCCGGCUC--UCUCUCACUUGAUCCACCA-UUGU-
Comparison strain:
A/South Dakota/06/2007
CGUACCUCAAAAGGGAGUG-ACUGGGUUCUGAACUC
    ||   ||||:||||| ||::|||    |||  
-UCCGGC--UUUCUCUCACUUGGUCCAUCA-UUGC-

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nb0>nb0', 10: 'nb0>nb1', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb1', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'nn0', 27: 'nn0', 28: 'nn0', 29: 'nn0', 30: 'bb0>bb0', 31: 'bb0>bb0', 32: 'bb0>bb0', 33: 'nn0', 34: 'nn1'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'nn0', 6: 'nb0>nb0', 7: 'nb1>nb0', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'nn0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb1>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nn1', 25: 'nn0', 26: 'nn0', 27: 'bb0>bb0', 28: 'bb0>bb0', 29: 'bb0>bb0', 30: 'nn1'}

Covariance layer complex:
         11                        *
CGUACCUCAAAAGGGAGUG-ACUGGGUUCUGAACUU
    ||     ||:||||| |||:|||    |||  
-UCCGGCUC--UCUCUCACUUGAUCCACCA-UUGU-
       11             *    *      * 
         11                        *
CGUACCUCAAAAGGGAGUG-ACUGGGUUCUGAACUC
    ||   ||||:||||| ||::|||    |||  
-UCCGGC--UUUCUCUCACUUGGUCCAUCA-UUGC-
         11           *    *      * 

Reference strain:
A/WSN/1933
CGUACCUCAAAAGGGAGUG-ACUGGGUUCUGAACUU
    ||     ||:||||| |||:|||    |||  
-UCCGGCUC--UCUCUCACUUGAUCCACCA-UUGU-
Comparison strain:
A/USSR/90/1977
CGUACCUCAAAAGAGAGUG-ACUGGGCUCUGAACUC
    ||   |||||||||| |||:||          
-UCCGGC--UUUCUCUCACUUGAUCCAUCAUUGC--

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nb0>nb0', 10: 'nb0>nb1', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb1>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bn1>bn0', 26: 'nn0', 27: 'nn0', 28: 'nn0', 29: 'nn0', 30: 'bn0>bn0', 31: 'bn0>bn0', 32: 'bn0>bn0', 33: 'nn0', 34: 'nn1'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'nn0', 6: 'nb0>nb0', 7: 'nb1>nb0', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb1', 11: 'bb0>bb0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'nn0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bn0>bn1', 24: 'nn1', 25: 'nn0', 26: 'nn0', 27: 'bn0>bn0', 28: 'bn0>bn0', 29: 'bn0>bn0', 30: 'nn1'}

Covariance layer complex:
         11  *            *        *
CGUACCUCAAAAGGGAGUG-ACUGGGUUCUGAACUU
    ||     ||:||||| |||:|||    |||  
-UCCGGCUC--UCUCUCACUUGAUCCACCA-UUGU-
       11                  *      * 
         11  *            *        *
CGUACCUCAAAAGAGAGUG-ACUGGGCUCUGAACUC
    ||   |||||||||| |||:||          
-UCCGGC--UUUCUCUCACUUGAUCCAUCAUUGC--
         11                *     *  
