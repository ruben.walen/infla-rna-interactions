Covariation report for: 13 PB2 NP

Reference strain:
A/WSN/1933
UCUUCUCGGUGUCAUA-GUCGUCUAGGUGAUCGUAGAAAUAACCUCUACACG
       |||| |    |:|||||||||:                       
-----AUCCACUGGCUACGGCAGAUCCAUACACACAGGCAGGCAGGCA----
Comparison strain:
A/Beijing/262/1995
UCUUCUCGACGUCAUAGUCGUCUAGGUGAUCGUAGAAAUAAUCUUUACACG
                |:||||:||||:                       
----ACCCACUGGCCACGGCAGGUCCAUACACACAGGCAGGUAGGCA----

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'bn0>bn0', 8: 'bn1>bn0', 9: 'bn1>bn0', 10: 'bn0>bn0', 11: 'nn0', 12: 'bn0>bn0', 13: 'nn0', 14: 'nn0', 15: 'nn0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb1', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'nn0', 30: 'nn0', 31: 'nn0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn1', 42: 'nn0', 43: 'nn0', 44: 'nn1', 45: 'nn0', 46: 'nn0', 47: 'nn0', 48: 'nn0', 49: 'nn0', 50: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn1', 2: 'bn0>bn0', 3: 'bn0>bn1', 4: 'bn0>bn1', 5: 'bn0>bn0', 6: 'nn0', 7: 'bn0>bn0', 8: 'nn0', 9: 'nn0', 10: 'nn1', 11: 'nn0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb1>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nn0', 25: 'nn0', 26: 'nn0', 27: 'nn0', 28: 'nn0', 29: 'nn0', 30: 'nn0', 31: 'nn0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn1', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn0', 42: 'nn0'}

Covariance layer complex:
        **                                *  *      
UCUUCUCGGUGUCAUA-GUCGUCUAGGUGAUCGUAGAAAUAACCUCUACACG
       |||| |    |:|||||||||:                       
-----AUCCACUGGCUACGGCAGAUCCAUACACACAGGCAGGCAGGCA----
      *        *       *                  *         
        **                               *  *      
UCUUCUCGACGUCAUAGUCGUCUAGGUGAUCGUAGAAAUAAUCUUUACACG
                |:||||:||||:                       
----ACCCACUGGCCACGGCAGGUCCAUACACACAGGCAGGUAGGCA----
     *        *       *                  *         

Reference strain:
A/WSN/1933
UCUUCUCGGUGUCAUA-GUCGUCUAGGUGAUCGUAGAAAUAACCUCUACACG
       |||| |    |:|||||||||:                       
-----AUCCACUGGCUACGGCAGAUCCAUACACACAGGCAGGCAGGCA----
Comparison strain:
A/Brazil/11/1978
UCUUCUCGACGUCAUAGUCGUCUAGGUGAUCGUAGAAAUAACCUUUACACG
                |:||||:||||:                       
----ACCCACUGGCUACGGCAGGUCCAUACACACAGGCAGGCAGGCA----

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'bn0>bn0', 8: 'bn1>bn0', 9: 'bn1>bn0', 10: 'bn0>bn0', 11: 'nn0', 12: 'bn0>bn0', 13: 'nn0', 14: 'nn0', 15: 'nn0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb1', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'nn0', 30: 'nn0', 31: 'nn0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn0', 42: 'nn0', 43: 'nn0', 44: 'nn1', 45: 'nn0', 46: 'nn0', 47: 'nn0', 48: 'nn0', 49: 'nn0', 50: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn1', 2: 'bn0>bn0', 3: 'bn0>bn1', 4: 'bn0>bn1', 5: 'bn0>bn0', 6: 'nn0', 7: 'bn0>bn0', 8: 'nn0', 9: 'nn0', 10: 'nn0', 11: 'nn0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb1>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nn0', 25: 'nn0', 26: 'nn0', 27: 'nn0', 28: 'nn0', 29: 'nn0', 30: 'nn0', 31: 'nn0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn0', 42: 'nn0'}

Covariance layer complex:
        **                                   *      
UCUUCUCGGUGUCAUA-GUCGUCUAGGUGAUCGUAGAAAUAACCUCUACACG
       |||| |    |:|||||||||:                       
-----AUCCACUGGCUACGGCAGAUCCAUACACACAGGCAGGCAGGCA----
      *                *                            
        **                                  *      
UCUUCUCGACGUCAUAGUCGUCUAGGUGAUCGUAGAAAUAACCUUUACACG
                |:||||:||||:                       
----ACCCACUGGCUACGGCAGGUCCAUACACACAGGCAGGCAGGCA----
     *                *                            

Reference strain:
A/WSN/1933
UCUUCUCGGUGUCAUA-GUCGUCUAGGUGAUCGUAGAAAUAACCUCUACACG
       |||| |    |:|||||||||:                       
-----AUCCACUGGCUACGGCAGAUCCAUACACACAGGCAGGCAGGCA----
Comparison strain:
A/Brisbane/59/2007
UCUUCUCGACGUCAUAGUCGUCUAGGUGAUCGUAGAAAUAAUCUUUACACG--------------
                        |||||:||                                 
----------------------ACCCACUGGCUACGGCUGGUCCAUACACACAGGCAGGCAGGCA

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'bn0>bb0s', 8: 'bn1>bb0s', 9: 'bn1>bb0s', 10: 'bn0>bb0s', 11: 'nn0', 12: 'bn0>bb0s', 13: 'nn0', 14: 'nn0', 15: 'nn0', 16: 'bn0>bn0', 17: 'bn0>bn0', 18: 'bn0>bn0', 19: 'bn0>bn0', 20: 'bn0>bn1', 21: 'bn0>bn0', 22: 'bn0>bn1', 23: 'bn0>bn0', 24: 'bb0s>bb0s', 25: 'bb0s>bb0s', 26: 'bb0s>bb0s', 27: 'bb0s>bb0s', 28: 'nb0>nb0', 29: 'nb0>bb0s', 30: 'nb0>nb0', 31: 'nb0>nb0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn1', 42: 'nn0', 43: 'nn0', 44: 'nn1', 45: 'nn0', 46: 'nn0', 47: 'nn0', 48: 'nn0', 49: 'nn0', 50: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn1', 2: 'bb0s>bb0s', 3: 'bb0s>bb0s', 4: 'bb0s>bb0s', 5: 'bb0s>bb0s', 6: 'nb0>nb0', 7: 'bb0s>nb0', 8: 'nb0>nb0', 9: 'nb0>nb0', 10: 'nn0', 11: 'nn0', 12: 'bn0>bn0', 13: 'bn0>bn0', 14: 'bn0>bn0', 15: 'bn0>bn0', 16: 'bn1>bn0', 17: 'bn0>bn0', 18: 'bn1>bn0', 19: 'bn0>bn0', 20: 'bn0>bb0s', 21: 'bn0>bb0s', 22: 'bn0>bb0s', 23: 'bn0>bb0s', 24: 'nn0', 25: 'nn0', 26: 'nn0', 27: 'nn0', 28: 'nn0', 29: 'nn0', 30: 'nn0', 31: 'nn0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn0', 42: 'nn0'}

Covariance layer complex:
        **                   1111         *  *      
UCUUCUCGGUGUCAUA-GUCGUCUAGGUGAUCGUAGAAAUAACCUCUACACG
       |||| |    |:|||||||||:                       
-----AUCCACUGGCUACGGCAGAUCCAUACACACAGGCAGGCAGGCA----
      *    1 11      * *                            
        **                  1111         *  *                    
UCUUCUCGACGUCAUAGUCGUCUAGGUGAUCGUAGAAAUAAUCUUUACACG--------------
                        |||||:||                                 
----------------------ACCCACUGGCUACGGCUGGUCCAUACACACAGGCAGGCAGGCA
                       *    1 11      * *                        

Reference strain:
A/WSN/1933
UCUUCUCGGUGUCAUA-GUCGUCUAGGUGAUCGUAGAAAUAACCUCUACACG
       |||| |    |:|||||||||:                       
-----AUCCACUGGCUACGGCAGAUCCAUACACACAGGCAGGCAGGCA----
Comparison strain:
A/California/07/2009
UCUUCUCGUCGUCACAGUCGUCUGGGUAAUCGUAGAGAGAACCUUUACACG
                  ||| :|||||                       
----GCCCACUUGCUACUGCAAGCCCAUACACACAAGCAGGCAGGCA----

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'bn0>bn0', 8: 'bn1>bn0', 9: 'bn1>bn0', 10: 'bn0>bn0', 11: 'nn0', 12: 'bn0>bn1', 13: 'nn0', 14: 'nn1', 15: 'nn0', 16: 'bn0>bn0', 17: 'bn0>bn1', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bn0>bn1', 22: 'bb0>bb1', 23: 'bb1>bb1', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb1>bb0', 28: 'nn0', 29: 'nn0', 30: 'nn0', 31: 'nn0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn1', 37: 'nn0', 38: 'nn1', 39: 'nn0', 40: 'nn0', 41: 'nn0', 42: 'nn0', 43: 'nn0', 44: 'nn1', 45: 'nn0', 46: 'nn0', 47: 'nn0', 48: 'nn0', 49: 'nn0', 50: 'nn0'}
Strand 2 variance codes:
{0: 'nn1', 1: 'nn1', 2: 'bn0>bn0', 3: 'bn0>bn1', 4: 'bn0>bn1', 5: 'bn0>bn0', 6: 'nn0', 7: 'bn1>bn0', 8: 'nn0', 9: 'nn0', 10: 'nn0', 11: 'nn0', 12: 'bn0>bn0', 13: 'bn1>bn0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bn1>bn0', 18: 'bb1>bb0', 19: 'bb1>bb1', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb1', 24: 'nn0', 25: 'nn0', 26: 'nn0', 27: 'nn0', 28: 'nn0', 29: 'nn0', 30: 'nn0', 31: 'nn1', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn0', 42: 'nn0'}

Covariance layer complex:
        **    *         2   *        * *     *      
UCUUCUCGGUGUCAUA-GUCGUCUAGGUGAUCGUAGAAAUAACCUCUACACG
       |||| |    |:|||||||||:                       
-----AUCCACUGGCUACGGCAGAUCCAUACACACAGGCAGGCAGGCA----
     **     *     *   **2           *               
        **    *        2   *        * *     *      
UCUUCUCGUCGUCACAGUCGUCUGGGUAAUCGUAGAGAGAACCUUUACACG
                  ||| :|||||                       
----GCCCACUUGCUACUGCAAGCCCAUACACACAAGCAGGCAGGCA----
    **     *     *   **2           *               

Reference strain:
A/WSN/1933
UCUUCUCGGUGUCAUA-GUCGUCUAGGUGAUCGUAGAAAUAACCUCUACACG
       |||| |    |:|||||||||:                       
-----AUCCACUGGCUACGGCAGAUCCAUACACACAGGCAGGCAGGCA----
Comparison strain:
A/Chile/1/1983
UCUUCUCGACGUCAUAGUCGUCUAGGUGAUCGUAGAAAUAACCUUUACACG
                |:||||:||||:                       
----ACCCACUGGCUACGGCAGGUCCAUACACACAGGCAGGCAGGCA----

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'bn0>bn0', 8: 'bn1>bn0', 9: 'bn1>bn0', 10: 'bn0>bn0', 11: 'nn0', 12: 'bn0>bn0', 13: 'nn0', 14: 'nn0', 15: 'nn0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb1', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'nn0', 30: 'nn0', 31: 'nn0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn0', 42: 'nn0', 43: 'nn0', 44: 'nn1', 45: 'nn0', 46: 'nn0', 47: 'nn0', 48: 'nn0', 49: 'nn0', 50: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn1', 2: 'bn0>bn0', 3: 'bn0>bn1', 4: 'bn0>bn1', 5: 'bn0>bn0', 6: 'nn0', 7: 'bn0>bn0', 8: 'nn0', 9: 'nn0', 10: 'nn0', 11: 'nn0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb1>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nn0', 25: 'nn0', 26: 'nn0', 27: 'nn0', 28: 'nn0', 29: 'nn0', 30: 'nn0', 31: 'nn0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn0', 42: 'nn0'}

Covariance layer complex:
        **                                   *      
UCUUCUCGGUGUCAUA-GUCGUCUAGGUGAUCGUAGAAAUAACCUCUACACG
       |||| |    |:|||||||||:                       
-----AUCCACUGGCUACGGCAGAUCCAUACACACAGGCAGGCAGGCA----
      *                *                            
        **                                  *      
UCUUCUCGACGUCAUAGUCGUCUAGGUGAUCGUAGAAAUAACCUUUACACG
                |:||||:||||:                       
----ACCCACUGGCUACGGCAGGUCCAUACACACAGGCAGGCAGGCA----
     *                *                            

Reference strain:
A/WSN/1933
UCUUCUCGGUGUCAUA-GUCGUCUAGGUGAUCGUAGAAAUAACCUCUACACG
       |||| |    |:|||||||||:                       
-----AUCCACUGGCUACGGCAGAUCCAUACACACAGGCAGGCAGGCA----
Comparison strain:
A/Michigan/45/2015
UCUUCUCGUCGUCACAGUCGUCUGGGUAAUCGUAGAGAGAACCUUUACACG
                  ||| :|||||                       
----GGCCACUUGCUACUGCAAGCCCAUACACACAAGCAGGCAGGCA----

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'bn0>bn0', 8: 'bn1>bn0', 9: 'bn1>bn0', 10: 'bn0>bn0', 11: 'nn0', 12: 'bn0>bn1', 13: 'nn0', 14: 'nn1', 15: 'nn0', 16: 'bn0>bn0', 17: 'bn0>bn1', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bn0>bn1', 22: 'bb0>bb1', 23: 'bb1>bb1', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb1>bb0', 28: 'nn0', 29: 'nn0', 30: 'nn0', 31: 'nn0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn1', 37: 'nn0', 38: 'nn1', 39: 'nn0', 40: 'nn0', 41: 'nn0', 42: 'nn0', 43: 'nn0', 44: 'nn1', 45: 'nn0', 46: 'nn0', 47: 'nn0', 48: 'nn0', 49: 'nn0', 50: 'nn0'}
Strand 2 variance codes:
{0: 'nn1', 1: 'nn1', 2: 'bn0>bn0', 3: 'bn0>bn1', 4: 'bn0>bn1', 5: 'bn0>bn0', 6: 'nn0', 7: 'bn1>bn0', 8: 'nn0', 9: 'nn0', 10: 'nn0', 11: 'nn0', 12: 'bn0>bn0', 13: 'bn1>bn0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bn1>bn0', 18: 'bb1>bb0', 19: 'bb1>bb1', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb1', 24: 'nn0', 25: 'nn0', 26: 'nn0', 27: 'nn0', 28: 'nn0', 29: 'nn0', 30: 'nn0', 31: 'nn1', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn0', 42: 'nn0'}

Covariance layer complex:
        **    *         2   *        * *     *      
UCUUCUCGGUGUCAUA-GUCGUCUAGGUGAUCGUAGAAAUAACCUCUACACG
       |||| |    |:|||||||||:                       
-----AUCCACUGGCUACGGCAGAUCCAUACACACAGGCAGGCAGGCA----
     **     *     *   **2           *               
        **    *        2   *        * *     *      
UCUUCUCGUCGUCACAGUCGUCUGGGUAAUCGUAGAGAGAACCUUUACACG
                  ||| :|||||                       
----GGCCACUUGCUACUGCAAGCCCAUACACACAAGCAGGCAGGCA----
    **     *     *   **2           *               

Reference strain:
A/WSN/1933
UCUUCUCGGUGUCAUA-GUCGUCUAGGUGAUCGUAGAAAUAACCUCUACACG
       |||| |    |:|||||||||:                       
-----AUCCACUGGCUACGGCAGAUCCAUACACACAGGCAGGCAGGCA----
Comparison strain:
A/New Caledonia/20/1999
UCUUCUCGACGUCAUAGUCGUCUAGGUGAUCGUAGAAAUAAUCUUUACACG--------------
                        |||||:||                                 
----------------------ACCCACUGGCUACGGCUGGUCCAUACACACAGGCAGGCAGGCA

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'bn0>bb0s', 8: 'bn1>bb0s', 9: 'bn1>bb0s', 10: 'bn0>bb0s', 11: 'nn0', 12: 'bn0>bb0s', 13: 'nn0', 14: 'nn0', 15: 'nn0', 16: 'bn0>bn0', 17: 'bn0>bn0', 18: 'bn0>bn0', 19: 'bn0>bn0', 20: 'bn0>bn1', 21: 'bn0>bn0', 22: 'bn0>bn1', 23: 'bn0>bn0', 24: 'bb0s>bb0s', 25: 'bb0s>bb0s', 26: 'bb0s>bb0s', 27: 'bb0s>bb0s', 28: 'nb0>nb0', 29: 'nb0>bb0s', 30: 'nb0>nb0', 31: 'nb0>nb0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn1', 42: 'nn0', 43: 'nn0', 44: 'nn1', 45: 'nn0', 46: 'nn0', 47: 'nn0', 48: 'nn0', 49: 'nn0', 50: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn1', 2: 'bb0s>bb0s', 3: 'bb0s>bb0s', 4: 'bb0s>bb0s', 5: 'bb0s>bb0s', 6: 'nb0>nb0', 7: 'bb0s>nb0', 8: 'nb0>nb0', 9: 'nb0>nb0', 10: 'nn0', 11: 'nn0', 12: 'bn0>bn0', 13: 'bn0>bn0', 14: 'bn0>bn0', 15: 'bn0>bn0', 16: 'bn1>bn0', 17: 'bn0>bn0', 18: 'bn1>bn0', 19: 'bn0>bn0', 20: 'bn0>bb0s', 21: 'bn0>bb0s', 22: 'bn0>bb0s', 23: 'bn0>bb0s', 24: 'nn0', 25: 'nn0', 26: 'nn0', 27: 'nn0', 28: 'nn0', 29: 'nn0', 30: 'nn0', 31: 'nn0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn0', 42: 'nn0'}

Covariance layer complex:
        **                   1111         *  *      
UCUUCUCGGUGUCAUA-GUCGUCUAGGUGAUCGUAGAAAUAACCUCUACACG
       |||| |    |:|||||||||:                       
-----AUCCACUGGCUACGGCAGAUCCAUACACACAGGCAGGCAGGCA----
      *    1 11      * *                            
        **                  1111         *  *                    
UCUUCUCGACGUCAUAGUCGUCUAGGUGAUCGUAGAAAUAAUCUUUACACG--------------
                        |||||:||                                 
----------------------ACCCACUGGCUACGGCUGGUCCAUACACACAGGCAGGCAGGCA
                       *    1 11      * *                        

Reference strain:
A/WSN/1933
UCUUCUCGGUGUCAUA-GUCGUCUAGGUGAUCGUAGAAAUAACCUCUACACG
       |||| |    |:|||||||||:                       
-----AUCCACUGGCUACGGCAGAUCCAUACACACAGGCAGGCAGGCA----
Comparison strain:
A/Singapore/6/1986
UCUUCUCGACGUCAUAGUCGUCUGGGUGAUCGUAGAAAUAACCUUUACACG--------------
                      |||||||:||                                 
----------------------ACCCACUGGCUACGGCAGGUCCAUACACACAGGCAGGCAGGCA

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'bn0>bb0s', 8: 'bn1>bb0s', 9: 'bn1>bb0s', 10: 'bn0>bb0s', 11: 'nn0', 12: 'bn0>bb0s', 13: 'nn0', 14: 'nn0', 15: 'nn0', 16: 'bn0>bn0', 17: 'bn0>bn0', 18: 'bn0>bn0', 19: 'bn0>bn0', 20: 'bn0>bn0', 21: 'bn0>bn0', 22: 'bb0s>nb0', 23: 'bb1s>nb1', 24: 'bb0s>bb0s', 25: 'bb0s>bb0s', 26: 'bb0s>bb0s', 27: 'bb0s>bb0s', 28: 'nb0>nb0', 29: 'nb0>bb0s', 30: 'nb0>nb0', 31: 'nb0>nb0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn0', 42: 'nn0', 43: 'nn0', 44: 'nn1', 45: 'nn0', 46: 'nn0', 47: 'nn0', 48: 'nn0', 49: 'nn0', 50: 'nn0'}
Strand 2 variance codes:
{0: 'nb0>bb0s', 1: 'nb1>bb1s', 2: 'bb0s>bb0s', 3: 'bb0s>bb0s', 4: 'bb0s>bb0s', 5: 'bb0s>bb0s', 6: 'nb0>nb0', 7: 'bb0s>nb0', 8: 'nb0>nb0', 9: 'nb0>nb0', 10: 'nn0', 11: 'nn0', 12: 'bn0>bn0', 13: 'bn0>bn0', 14: 'bn0>bn0', 15: 'bn0>bn0', 16: 'bn0>bn0', 17: 'bn0>bn0', 18: 'bn1>bb0s', 19: 'bn0>bb1s', 20: 'bn0>bb0s', 21: 'bn0>bb0s', 22: 'bn0>bb0s', 23: 'bn0>bb0s', 24: 'nn0', 25: 'nn0', 26: 'nn0', 27: 'nn0', 28: 'nn0', 29: 'nn0', 30: 'nn0', 31: 'nn0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn0', 42: 'nn0'}

Covariance layer complex:
        **              *    1111            *      
UCUUCUCGGUGUCAUA-GUCGUCUAGGUGAUCGUAGAAAUAACCUCUACACG
       |||| |    |:|||||||||:                       
-----AUCCACUGGCUACGGCAGAUCCAUACACACAGGCAGGCAGGCA----
     11    1 11        *                            
        **             *    1111            *                    
UCUUCUCGACGUCAUAGUCGUCUGGGUGAUCGUAGAAAUAACCUUUACACG--------------
                      |||||||:||                                 
----------------------ACCCACUGGCUACGGCAGGUCCAUACACACAGGCAGGCAGGCA
                      11    1 11        *                        

Reference strain:
A/WSN/1933
UCUUCUCGGUGUCAUA-GUCGUCUAGGUGAUCGUAGAAAUAACCUCUACACG
       |||| |    |:|||||||||:                       
-----AUCCACUGGCUACGGCAGAUCCAUACACACAGGCAGGCAGGCA----
Comparison strain:
A/South Dakota/06/2007
UCUUCUCGACGUCAUAGUCGUCUAGGUGAUCGUAGAAAUAAUCUUUACACG--------------
                        |||||:||                                 
----------------------ACCCACUGGCUACGGCUGGUCCAUACACACAGGCAGGCAGGCA

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'bn0>bb0s', 8: 'bn1>bb0s', 9: 'bn1>bb0s', 10: 'bn0>bb0s', 11: 'nn0', 12: 'bn0>bb0s', 13: 'nn0', 14: 'nn0', 15: 'nn0', 16: 'bn0>bn0', 17: 'bn0>bn0', 18: 'bn0>bn0', 19: 'bn0>bn0', 20: 'bn0>bn1', 21: 'bn0>bn0', 22: 'bn0>bn1', 23: 'bn0>bn0', 24: 'bb0s>bb0s', 25: 'bb0s>bb0s', 26: 'bb0s>bb0s', 27: 'bb0s>bb0s', 28: 'nb0>nb0', 29: 'nb0>bb0s', 30: 'nb0>nb0', 31: 'nb0>nb0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn1', 42: 'nn0', 43: 'nn0', 44: 'nn1', 45: 'nn0', 46: 'nn0', 47: 'nn0', 48: 'nn0', 49: 'nn0', 50: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn1', 2: 'bb0s>bb0s', 3: 'bb0s>bb0s', 4: 'bb0s>bb0s', 5: 'bb0s>bb0s', 6: 'nb0>nb0', 7: 'bb0s>nb0', 8: 'nb0>nb0', 9: 'nb0>nb0', 10: 'nn0', 11: 'nn0', 12: 'bn0>bn0', 13: 'bn0>bn0', 14: 'bn0>bn0', 15: 'bn0>bn0', 16: 'bn1>bn0', 17: 'bn0>bn0', 18: 'bn1>bn0', 19: 'bn0>bn0', 20: 'bn0>bb0s', 21: 'bn0>bb0s', 22: 'bn0>bb0s', 23: 'bn0>bb0s', 24: 'nn0', 25: 'nn0', 26: 'nn0', 27: 'nn0', 28: 'nn0', 29: 'nn0', 30: 'nn0', 31: 'nn0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn0', 42: 'nn0'}

Covariance layer complex:
        **                   1111         *  *      
UCUUCUCGGUGUCAUA-GUCGUCUAGGUGAUCGUAGAAAUAACCUCUACACG
       |||| |    |:|||||||||:                       
-----AUCCACUGGCUACGGCAGAUCCAUACACACAGGCAGGCAGGCA----
      *    1 11      * *                            
        **                  1111         *  *                    
UCUUCUCGACGUCAUAGUCGUCUAGGUGAUCGUAGAAAUAAUCUUUACACG--------------
                        |||||:||                                 
----------------------ACCCACUGGCUACGGCUGGUCCAUACACACAGGCAGGCAGGCA
                       *    1 11      * *                        

Reference strain:
A/WSN/1933
UCUUCUCGGUGUCAUA-GUCGUCUAGGUGAUCGUAGAAAUAACCUCUACACG
       |||| |    |:|||||||||:                       
-----AUCCACUGGCUACGGCAGAUCCAUACACACAGGCAGGCAGGCA----
Comparison strain:
A/USSR/90/1977
UCUUCUCGACGUCAUAGUCGUCUAGGUGAUCGUAGAAAUAACCUUUACACG
                |:||||:||||:                       
----ACCCACUGGCUACGGCAGGUCCAUACACACAGGCAGGCAGGCA----

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'bn0>bn0', 8: 'bn1>bn0', 9: 'bn1>bn0', 10: 'bn0>bn0', 11: 'nn0', 12: 'bn0>bn0', 13: 'nn0', 14: 'nn0', 15: 'nn0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb1', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bb0>bb0', 27: 'bb0>bb0', 28: 'nn0', 29: 'nn0', 30: 'nn0', 31: 'nn0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn0', 42: 'nn0', 43: 'nn0', 44: 'nn1', 45: 'nn0', 46: 'nn0', 47: 'nn0', 48: 'nn0', 49: 'nn0', 50: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn1', 2: 'bn0>bn0', 3: 'bn0>bn1', 4: 'bn0>bn1', 5: 'bn0>bn0', 6: 'nn0', 7: 'bn0>bn0', 8: 'nn0', 9: 'nn0', 10: 'nn0', 11: 'nn0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb1>bb0', 19: 'bb0>bb0', 20: 'bb0>bb0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'nn0', 25: 'nn0', 26: 'nn0', 27: 'nn0', 28: 'nn0', 29: 'nn0', 30: 'nn0', 31: 'nn0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn0', 42: 'nn0'}

Covariance layer complex:
        **                                   *      
UCUUCUCGGUGUCAUA-GUCGUCUAGGUGAUCGUAGAAAUAACCUCUACACG
       |||| |    |:|||||||||:                       
-----AUCCACUGGCUACGGCAGAUCCAUACACACAGGCAGGCAGGCA----
      *                *                            
        **                                  *      
UCUUCUCGACGUCAUAGUCGUCUAGGUGAUCGUAGAAAUAACCUUUACACG
                |:||||:||||:                       
----ACCCACUGGCUACGGCAGGUCCAUACACACAGGCAGGCAGGCA----
     *                *                            
