Covariation report for: 304 PB2 PA

Reference strain:
A/WSN/1933
UACUCCCUACACGAACCCUGUAAACU-------AUGGCGAGUCUAUUAUUUU
            ||| ||||||||||       |:|| ||||          
---------CUUCUUUGGACAUUUGAGAAAGCUUGCC-CUCAAUGUA-----
Comparison strain:
A/Beijing/262/1995
----------UACUCCCUACACGAACCCUGGAAACUGUGGUGAGUCUAUUAUUUU
                     |||||                             
CUUCUUUGGACAUUUGAGAAAGCUUGCCCUCAAUGUA------------------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nn0', 10: 'nn0', 11: 'nb0>nb0', 12: 'bb0s>nb0', 13: 'bb0s>nb0', 14: 'bb0s>bb0s', 15: 'nb0>bb0s', 16: 'bn0>bn0', 17: 'bn0>bn0', 18: 'bn0>bn0', 19: 'bn0>bn0', 20: 'bn1>bn0', 21: 'bn0>bn0', 22: 'bn0>bn0', 23: 'bn0>bn0', 24: 'bn0>bn0', 25: 'bn0>bn0', 26: 'bn1>bb0s', 27: 'bn0>bb0s', 28: 'bn0>bn0', 29: 'bn0>bn0', 30: 'nn1', 31: 'bn0>bn0', 32: 'bn0>bn0', 33: 'bn0>bn0', 34: 'bn0>bn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn0', 42: 'nn0', 43: 'nn0', 44: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'bn0>bb0s', 4: 'bn0>bb0s', 5: 'bn0>bb0s', 6: 'nn0', 7: 'bn0>bn0', 8: 'bn0>bn0', 9: 'bn0>bn0', 10: 'bn0>bn0', 11: 'bn0>bn1', 12: 'bn0>bn0', 13: 'bn0>bn0', 14: 'bn0>bn0', 15: 'bn0>bn0', 16: 'bn0>bn0', 17: 'nn0', 18: 'nn0', 19: 'nn0', 20: 'nn0', 21: 'nb0>nb0', 22: 'nb0>bb0s', 23: 'nb0>bb0s', 24: 'bb0s>bb0s', 25: 'bb0s>nb0', 26: 'bn0>bn0', 27: 'bn0>bn0', 28: 'bn0>bn0', 29: 'bn0>bn0', 30: 'bn0>bn0', 31: 'bn0>bn0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0'}

Covariance layer complex:
           1   1    *            *   *              
UACUCCCUACACGAACCCUGUAAACU-------AUGGCGAGUCUAUUAUUUU
            ||| ||||||||||       |:|| ||||          
---------CUUCUUUGGACAUUUGAGAAAGCUUGCC-CUCAAUGUA-----
                              111                   
                     1   1    *     *   *              
----------UACUCCCUACACGAACCCUGGAAACUGUGGUGAGUCUAUUAUUUU
                     |||||                             
CUUCUUUGGACAUUUGAGAAAGCUUGCCCUCAAUGUA------------------
                     111                               

Reference strain:
A/WSN/1933
UACUCCCUACACGAACCCUGUAAACU-------AUGGCGAGUCUAUUAUUUU
            ||| ||||||||||       |:|| ||||          
---------CUUCUUUGGACAUUUGAGAAAGCUUGCC-CUCAAUGUA-----
Comparison strain:
A/Brazil/11/1978
UACUCCCUACACGAACCCUGAAAACUGU----GGUGGGUCUAUUAUUUU
            ||| |||| ||||| |    :::|||           
---------CUUCUUUGGACAUUUGAGAAAGCUUGCCCUCAAUGUA---

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nn0', 10: 'nn0', 11: 'nn0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'nn0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bn1>bn0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bn1>bb0s', 27: 'bb0s>nb0', 28: 'bb0s>nb0', 29: 'bb0s>bb0s', 30: 'nb1>bb0s', 31: 'bb0s>bb0s', 32: 'bb1s>bb0s', 33: 'bb0s>bb0s', 34: 'bn0>bn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn0', 42: 'nn0', 43: 'nn0', 44: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'nn0', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bn0>bn1', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'nn0', 18: 'nb0>bb0s', 19: 'nn0', 20: 'nn0', 21: 'nn0', 22: 'nn0', 23: 'nb0>bb0s', 24: 'bb0s>bb0s', 25: 'bb0s>nb1', 26: 'bb0s>bb0s', 27: 'bb0s>bb1s', 28: 'bb0s>bb0s', 29: 'bn0>bb1s', 30: 'bn0>bb0s', 31: 'bn0>bn0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0'}

Covariance layer complex:
                    *            *   1 *            
UACUCCCUACACGAACCCUGUAAACU-------AUGGCGAGUCUAUUAUUUU
            ||| ||||||||||       |:|| ||||          
---------CUUCUUUGGACAUUUGAGAAAGCUUGCC-CUCAAUGUA-----
                           1    1                   
                    *     *       1 *            
UACUCCCUACACGAACCCUGAAAACUGU----GGUGGGUCUAUUAUUUU
            ||| |||| ||||| |    :::|||           
---------CUUCUUUGGACAUUUGAGAAAGCUUGCCCUCAAUGUA---
                           1    1                

Reference strain:
A/WSN/1933
UACUCCCUACACGAACCCUGUAAACU-------AUGGCGAGUCUAUUAUUUU
            ||| ||||||||||       |:|| ||||          
---------CUUCUUUGGACAUUUGAGAAAGCUUGCC-CUCAAUGUA-----
Comparison strain:
A/Brisbane/59/2007
UACUCCCUACACGAACCCUGAAAACUGUGUUGAGUCUAUUAUUUU-
            ||| |||| ||||| | |:||:            
---------CUUCUUUGGACAUUUGAGAAAGCUUGCCCUCAAUGUA

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nn0', 10: 'nn0', 11: 'nn0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'nn0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bn1>bn0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bn1>bb0s', 27: 'bb0s>nb0', 28: 'bn0>bn0', 29: 'bb1s>nb0', 30: 'nb1>nb0', 31: 'bb0s>nb0', 32: 'bb0s>nb0', 33: 'bb0s>bb0s', 34: 'bn0>bn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn0', 42: 'nn0', 43: 'nn0', 44: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'nn0', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bn0>bn1', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'nn0', 18: 'nb0>bb0s', 19: 'nn0', 20: 'nb0>bb1s', 21: 'nb0>nb1', 22: 'nb0>bb0s', 23: 'nb0>bb0s', 24: 'bb0s>bb0s', 25: 'bn0>bb0s', 26: 'bn0>bn0', 27: 'bn0>bb1s', 28: 'bn0>bb0s', 29: 'bn0>bb0s', 30: 'bn0>bb0s', 31: 'bn0>bn0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0'}

Covariance layer complex:
                    *            *  *1              
UACUCCCUACACGAACCCUGUAAACU-------AUGGCGAGUCUAUUAUUUU
            ||| ||||||||||       |:|| ||||          
---------CUUCUUUGGACAUUUGAGAAAGCUUGCC-CUCAAUGUA-----
                           1 1111                   
                    *     *  *1               
UACUCCCUACACGAACCCUGAAAACUGUGUUGAGUCUAUUAUUUU-
            ||| |||| ||||| | |:||:            
---------CUUCUUUGGACAUUUGAGAAAGCUUGCCCUCAAUGUA
                           1 1111             

Reference strain:
A/WSN/1933
UACUCCCUACACGAACCCUGUAAACU-------AUGGCGAGUCUAUUAUUUU
            ||| ||||||||||       |:|| ||||          
---------CUUCUUUGGACAUUUGAGAAAGCUUGCC-CUCAAUGUA-----
Comparison strain:
A/California/07/2009
----------UACGCCCUACACGAACCCUGUAAACUGUGACAGGUUUAUUAUUUU
                     |||||                             
CUUCUUUUGACAUUUGGGAAAGCUUGCCCUCAAUGCA------------------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn1', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nn0', 10: 'nn0', 11: 'nb0>nb0', 12: 'bb0s>nb0', 13: 'bb0s>nb0', 14: 'bb0s>bb0s', 15: 'nb0>bb0s', 16: 'bn0>bn1', 17: 'bn0>bn0', 18: 'bn0>bn0', 19: 'bn0>bn0', 20: 'bn0>bn0', 21: 'bn0>bn0', 22: 'bn0>bn0', 23: 'bn0>bn0', 24: 'bn0>bn0', 25: 'bn0>bn1', 26: 'bn1>bb0s', 27: 'bn0>bb0s', 28: 'bn0>bn0', 29: 'bn1>bn0', 30: 'nn0', 31: 'bn1>bn0', 32: 'bn1>bn0', 33: 'bn0>bn0', 34: 'bn0>bn0', 35: 'nn1', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn0', 42: 'nn0', 43: 'nn0', 44: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'bn0>bb0s', 4: 'bn0>bb0s', 5: 'bn0>bb0s', 6: 'nn0', 7: 'bn1>bn0', 8: 'bn0>bn0', 9: 'bn0>bn0', 10: 'bn0>bn0', 11: 'bn0>bn0', 12: 'bn0>bn0', 13: 'bn0>bn0', 14: 'bn0>bn0', 15: 'bn0>bn0', 16: 'bn1>bn0', 17: 'nn0', 18: 'nn0', 19: 'nn0', 20: 'nn0', 21: 'nb0>nb0', 22: 'nb0>bb0s', 23: 'nb0>bb0s', 24: 'bb0s>bb0s', 25: 'bb0s>nb0', 26: 'bn0>bn0', 27: 'bn0>bn1', 28: 'bn0>bn1', 29: 'bn0>bn1', 30: 'bn0>bn0', 31: 'bn0>bn0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn1', 36: 'nn0'}

Covariance layer complex:
   *       1   1                 *  * **  *         
UACUCCCUACACGAACCCUGUAAACU-------AUGGCGAGUCUAUUAUUUU
            ||| ||||||||||       |:|| ||||          
---------CUUCUUUGGACAUUUGAGAAAGCUUGCC-CUCAAUGUA-----
                *        *    111            *      
             *       1   1          *  * **  *         
----------UACGCCCUACACGAACCCUGUAAACUGUGACAGGUUUAUUAUUUU
                     |||||                             
CUUCUUUUGACAUUUGGGAAAGCUUGCCCUCAAUGCA------------------
       *        *    111           *                   

Reference strain:
A/WSN/1933
UACUCCCUACACGAACCCUGUAAACU-------AUGGCGAGUCUAUUAUUUU
            ||| ||||||||||       |:|| ||||          
---------CUUCUUUGGACAUUUGAGAAAGCUUGCC-CUCAAUGUA-----
Comparison strain:
A/Chile/1/1983
UACUCCCUACACGAACCCUGAAAACUGU----GGUGGGUCUAUUAUUUU
            ||| |||| ||||| |    :::|||           
---------CUUCUUUGGACAUUUGAGAAAGCUUGCCCUCAAUGUA---

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nn0', 10: 'nn0', 11: 'nn0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'nn0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bn1>bn0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bn1>bb0s', 27: 'bb0s>nb0', 28: 'bb0s>nb0', 29: 'bb0s>bb0s', 30: 'nb1>bb0s', 31: 'bb0s>bb0s', 32: 'bb1s>bb0s', 33: 'bb0s>bb0s', 34: 'bn0>bn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn0', 42: 'nn0', 43: 'nn0', 44: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'nn0', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bn0>bn1', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'nn0', 18: 'nb0>bb0s', 19: 'nn0', 20: 'nn0', 21: 'nn0', 22: 'nn0', 23: 'nb0>bb0s', 24: 'bb0s>bb0s', 25: 'bb0s>nb1', 26: 'bb0s>bb0s', 27: 'bb0s>bb1s', 28: 'bb0s>bb0s', 29: 'bn0>bb1s', 30: 'bn0>bb0s', 31: 'bn0>bn0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0'}

Covariance layer complex:
                    *            *   1 *            
UACUCCCUACACGAACCCUGUAAACU-------AUGGCGAGUCUAUUAUUUU
            ||| ||||||||||       |:|| ||||          
---------CUUCUUUGGACAUUUGAGAAAGCUUGCC-CUCAAUGUA-----
                           1    1                   
                    *     *       1 *            
UACUCCCUACACGAACCCUGAAAACUGU----GGUGGGUCUAUUAUUUU
            ||| |||| ||||| |    :::|||           
---------CUUCUUUGGACAUUUGAGAAAGCUUGCCCUCAAUGUA---
                           1    1                

Reference strain:
A/WSN/1933
UACUCCCUACACGAACCCUGUAAACU-------AUGGCGAGUCUAUUAUUUU
            ||| ||||||||||       |:|| ||||          
---------CUUCUUUGGACAUUUGAGAAAGCUUGCC-CUCAAUGUA-----
Comparison strain:
A/Michigan/45/2015
----------UACGCCCUACACGAACCCUGUAAACUGUGACAGGUUUAUUAUUUU
                     |||||                             
CUUCUUUUGACAUUUGGGAAAGCUUGCCCUCAAUGCA------------------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn1', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nn0', 10: 'nn0', 11: 'nb0>nb0', 12: 'bb0s>nb0', 13: 'bb0s>nb0', 14: 'bb0s>bb0s', 15: 'nb0>bb0s', 16: 'bn0>bn1', 17: 'bn0>bn0', 18: 'bn0>bn0', 19: 'bn0>bn0', 20: 'bn0>bn0', 21: 'bn0>bn0', 22: 'bn0>bn0', 23: 'bn0>bn0', 24: 'bn0>bn0', 25: 'bn0>bn1', 26: 'bn1>bb0s', 27: 'bn0>bb0s', 28: 'bn0>bn0', 29: 'bn1>bn0', 30: 'nn0', 31: 'bn1>bn0', 32: 'bn1>bn0', 33: 'bn0>bn0', 34: 'bn0>bn0', 35: 'nn1', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn0', 42: 'nn0', 43: 'nn0', 44: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'bn0>bb0s', 4: 'bn0>bb0s', 5: 'bn0>bb0s', 6: 'nn0', 7: 'bn1>bn0', 8: 'bn0>bn0', 9: 'bn0>bn0', 10: 'bn0>bn0', 11: 'bn0>bn0', 12: 'bn0>bn0', 13: 'bn0>bn0', 14: 'bn0>bn0', 15: 'bn0>bn0', 16: 'bn1>bn0', 17: 'nn0', 18: 'nn0', 19: 'nn0', 20: 'nn0', 21: 'nb0>nb0', 22: 'nb0>bb0s', 23: 'nb0>bb0s', 24: 'bb0s>bb0s', 25: 'bb0s>nb0', 26: 'bn0>bn0', 27: 'bn0>bn1', 28: 'bn0>bn1', 29: 'bn0>bn1', 30: 'bn0>bn0', 31: 'bn0>bn0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn1', 36: 'nn0'}

Covariance layer complex:
   *       1   1                 *  * **  *         
UACUCCCUACACGAACCCUGUAAACU-------AUGGCGAGUCUAUUAUUUU
            ||| ||||||||||       |:|| ||||          
---------CUUCUUUGGACAUUUGAGAAAGCUUGCC-CUCAAUGUA-----
                *        *    111            *      
             *       1   1          *  * **  *         
----------UACGCCCUACACGAACCCUGUAAACUGUGACAGGUUUAUUAUUUU
                     |||||                             
CUUCUUUUGACAUUUGGGAAAGCUUGCCCUCAAUGCA------------------
       *        *    111           *                   

Reference strain:
A/WSN/1933
UACUCCCUACACGAACCCUGUAAACU-------AUGGCGAGUCUAUUAUUUU
            ||| ||||||||||       |:|| ||||          
---------CUUCUUUGGACAUUUGAGAAAGCUUGCC-CUCAAUGUA-----
Comparison strain:
A/New Caledonia/20/1999
UACUCCCUACACGAACCCUGGAAACUGUGUUGAGUCUAUUAUUUU-
            ||| |||| ||||| | |:||:            
---------CUUCUUUGGACAUUUGAGAAAGCUUGCCCUCAAUGUA

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nn0', 10: 'nn0', 11: 'nn0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'nn0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bn1>bn0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bn1>bb0s', 27: 'bb0s>nb0', 28: 'bn0>bn0', 29: 'bb1s>nb0', 30: 'nb1>nb0', 31: 'bb0s>nb0', 32: 'bb0s>nb0', 33: 'bb0s>bb0s', 34: 'bn0>bn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn0', 42: 'nn0', 43: 'nn0', 44: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'nn0', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bn0>bn1', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'nn0', 18: 'nb0>bb0s', 19: 'nn0', 20: 'nb0>bb1s', 21: 'nb0>nb1', 22: 'nb0>bb0s', 23: 'nb0>bb0s', 24: 'bb0s>bb0s', 25: 'bn0>bb0s', 26: 'bn0>bn0', 27: 'bn0>bb1s', 28: 'bn0>bb0s', 29: 'bn0>bb0s', 30: 'bn0>bb0s', 31: 'bn0>bn0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0'}

Covariance layer complex:
                    *            *  *1              
UACUCCCUACACGAACCCUGUAAACU-------AUGGCGAGUCUAUUAUUUU
            ||| ||||||||||       |:|| ||||          
---------CUUCUUUGGACAUUUGAGAAAGCUUGCC-CUCAAUGUA-----
                           1 1111                   
                    *     *  *1               
UACUCCCUACACGAACCCUGGAAACUGUGUUGAGUCUAUUAUUUU-
            ||| |||| ||||| | |:||:            
---------CUUCUUUGGACAUUUGAGAAAGCUUGCCCUCAAUGUA
                           1 1111             

Reference strain:
A/WSN/1933
UACUCCCUACACGAACCCUGUAAACU-------AUGGCGAGUCUAUUAUUUU
            ||| ||||||||||       |:|| ||||          
---------CUUCUUUGGACAUUUGAGAAAGCUUGCC-CUCAAUGUA-----
Comparison strain:
A/Singapore/6/1986
----------UACUCCCUACACGAACCUUGAAAACUGUGGUGGGUCUAUUAUUUU
                     |||||                             
CUUCUUUGGACAUUUGAGAAAGCUUGCCCUCAAUGUA------------------

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nn0', 10: 'nn0', 11: 'nb0>nb0', 12: 'bb0s>nb0', 13: 'bb0s>nb0', 14: 'bb0s>bb0s', 15: 'nb0>bb0s', 16: 'bn0>bn0', 17: 'bn1>bn0', 18: 'bn0>bn0', 19: 'bn0>bn0', 20: 'bn1>bn0', 21: 'bn0>bn0', 22: 'bn0>bn0', 23: 'bn0>bn0', 24: 'bn0>bn0', 25: 'bn0>bn0', 26: 'bn1>bb0s', 27: 'bn0>bb0s', 28: 'bn0>bn0', 29: 'bn0>bn0', 30: 'nn1', 31: 'bn0>bn0', 32: 'bn1>bn0', 33: 'bn0>bn0', 34: 'bn0>bn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn0', 42: 'nn0', 43: 'nn0', 44: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'bn0>bb0s', 4: 'bn0>bb0s', 5: 'bn0>bb0s', 6: 'nn0', 7: 'bn0>bn0', 8: 'bn0>bn1', 9: 'bn0>bn0', 10: 'bn0>bn0', 11: 'bn0>bn1', 12: 'bn0>bn0', 13: 'bn0>bn0', 14: 'bn0>bn0', 15: 'bn0>bn0', 16: 'bn0>bn0', 17: 'nn0', 18: 'nn0', 19: 'nn0', 20: 'nn0', 21: 'nb0>nb0', 22: 'nb0>bb0s', 23: 'nb0>bb0s', 24: 'bb0s>bb0s', 25: 'bb0s>nb0', 26: 'bn0>bn0', 27: 'bn0>bn0', 28: 'bn0>bn0', 29: 'bn0>bn1', 30: 'bn0>bn0', 31: 'bn0>bn0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0'}

Covariance layer complex:
           1   1 *  *            *   * *            
UACUCCCUACACGAACCCUGUAAACU-------AUGGCGAGUCUAUUAUUUU
            ||| ||||||||||       |:|| ||||          
---------CUUCUUUGGACAUUUGAGAAAGCUUGCC-CUCAAUGUA-----
                              111                   
                     1   1 *  *     *   * *            
----------UACUCCCUACACGAACCUUGAAAACUGUGGUGGGUCUAUUAUUUU
                     |||||                             
CUUCUUUGGACAUUUGAGAAAGCUUGCCCUCAAUGUA------------------
                     111                               

Reference strain:
A/WSN/1933
UACUCCCUACACGAACCCUGUAAACU-------AUGGCGAGUCUAUUAUUUU
            ||| ||||||||||       |:|| ||||          
---------CUUCUUUGGACAUUUGAGAAAGCUUGCC-CUCAAUGUA-----
Comparison strain:
A/South Dakota/06/2007
UACUCCCUACACGAACCCUGAAAACUGUGUUGAGUCUAUUAUUUU-
            ||| |||| ||||| | |:||:            
---------CUUCUUUGGACAUUUGAGAAAGCUUGCCCUCAAUGUA

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nn0', 10: 'nn0', 11: 'nn0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'nn0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bn1>bn0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bn1>bb0s', 27: 'bb0s>nb0', 28: 'bn0>bn0', 29: 'bb1s>nb0', 30: 'nb1>nb0', 31: 'bb0s>nb0', 32: 'bb0s>nb0', 33: 'bb0s>bb0s', 34: 'bn0>bn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn0', 42: 'nn0', 43: 'nn0', 44: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'nn0', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bn0>bn1', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'nn0', 18: 'nb0>bb0s', 19: 'nn0', 20: 'nb0>bb1s', 21: 'nb0>nb1', 22: 'nb0>bb0s', 23: 'nb0>bb0s', 24: 'bb0s>bb0s', 25: 'bn0>bb0s', 26: 'bn0>bn0', 27: 'bn0>bb1s', 28: 'bn0>bb0s', 29: 'bn0>bb0s', 30: 'bn0>bb0s', 31: 'bn0>bn0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0'}

Covariance layer complex:
                    *            *  *1              
UACUCCCUACACGAACCCUGUAAACU-------AUGGCGAGUCUAUUAUUUU
            ||| ||||||||||       |:|| ||||          
---------CUUCUUUGGACAUUUGAGAAAGCUUGCC-CUCAAUGUA-----
                           1 1111                   
                    *     *  *1               
UACUCCCUACACGAACCCUGAAAACUGUGUUGAGUCUAUUAUUUU-
            ||| |||| ||||| | |:||:            
---------CUUCUUUGGACAUUUGAGAAAGCUUGCCCUCAAUGUA
                           1 1111             

Reference strain:
A/WSN/1933
UACUCCCUACACGAACCCUGUAAACU-------AUGGCGAGUCUAUUAUUUU
            ||| ||||||||||       |:|| ||||          
---------CUUCUUUGGACAUUUGAGAAAGCUUGCC-CUCAAUGUA-----
Comparison strain:
A/USSR/90/1977
UACUCCCUACACGAACCCUGAAAACUGU----GGUGGGUCUAUUAUUUU
            ||| |||| ||||| |    :::|||           
---------CUUCUUUGGACAUUUGAGAAAGCUUGCCCUCAAUGUA---

Strand 1 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'nn0', 4: 'nn0', 5: 'nn0', 6: 'nn0', 7: 'nn0', 8: 'nn0', 9: 'nn0', 10: 'nn0', 11: 'nn0', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'nn0', 16: 'bb0>bb0', 17: 'bb0>bb0', 18: 'bb0>bb0', 19: 'bb0>bb0', 20: 'bn1>bn0', 21: 'bb0>bb0', 22: 'bb0>bb0', 23: 'bb0>bb0', 24: 'bb0>bb0', 25: 'bb0>bb0', 26: 'bn1>bb0s', 27: 'bb0s>nb0', 28: 'bb0s>nb0', 29: 'bb0s>bb0s', 30: 'nb1>bb0s', 31: 'bb0s>bb0s', 32: 'bb1s>bb0s', 33: 'bb0s>bb0s', 34: 'bn0>bn0', 35: 'nn0', 36: 'nn0', 37: 'nn0', 38: 'nn0', 39: 'nn0', 40: 'nn0', 41: 'nn0', 42: 'nn0', 43: 'nn0', 44: 'nn0'}
Strand 2 variance codes:
{0: 'nn0', 1: 'nn0', 2: 'nn0', 3: 'bb0>bb0', 4: 'bb0>bb0', 5: 'bb0>bb0', 6: 'nn0', 7: 'bb0>bb0', 8: 'bb0>bb0', 9: 'bb0>bb0', 10: 'bb0>bb0', 11: 'bn0>bn1', 12: 'bb0>bb0', 13: 'bb0>bb0', 14: 'bb0>bb0', 15: 'bb0>bb0', 16: 'bb0>bb0', 17: 'nn0', 18: 'nb0>bb0s', 19: 'nn0', 20: 'nn0', 21: 'nn0', 22: 'nn0', 23: 'nb0>bb0s', 24: 'bb0s>bb0s', 25: 'bb0s>nb1', 26: 'bb0s>bb0s', 27: 'bb0s>bb1s', 28: 'bb0s>bb0s', 29: 'bn0>bb1s', 30: 'bn0>bb0s', 31: 'bn0>bn0', 32: 'nn0', 33: 'nn0', 34: 'nn0', 35: 'nn0', 36: 'nn0'}

Covariance layer complex:
                    *            *   1 *            
UACUCCCUACACGAACCCUGUAAACU-------AUGGCGAGUCUAUUAUUUU
            ||| ||||||||||       |:|| ||||          
---------CUUCUUUGGACAUUUGAGAAAGCUUGCC-CUCAAUGUA-----
                           1    1                   
                    *     *       1 *            
UACUCCCUACACGAACCCUGAAAACUGU----GGUGGGUCUAUUAUUUU
            ||| |||| ||||| |    :::|||           
---------CUUCUUUGGACAUUUGAGAAAGCUUGCCCUCAAUGUA---
                           1    1                
