"""Load interaction sites, do intaRNA calls, and output resulting sequences/structures for given strains."""

from Bio import AlignIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
import subprocess
import os
import re
import copy
import random
import math

from intarna_functions import *
import sequence_eda
import get_interaction_sites
import tools.config as config
from tools.batch import *


def getUngappedSlice(alignment, row, start, end, offset=0):
    """Get ungapped slice from alignment: containing end - start non-gapped characters in selected row.
        Includes end character.
        offset (int): offsets start, end by -offset to compensate for array start dilemma"""
    ungapped_i = -1
    start_i = None
    end_i = None
    in_slice = False
    start, end = start - offset, end - offset
    for i in range(alignment.get_alignment_length()):
        if alignment[row, i] != "-":
            ungapped_i += 1
        if not in_slice:
            if ungapped_i == start:
                start_i = i
                in_slice = True
        else:
            if ungapped_i == end:
                end_i = i
                return alignment[:, start_i:end_i+1] # end + 1: include last
    print(alignment, "\n", alignment.get_alignment_length(), row, start, end, ungapped_i, start_i)
    raise ValueError("@getUngappedSlice: reached end of alignment without covering start to end.""")


def getSubAlignments(interaction, alignments_dict, ungapped_slice=False, ungapping_row=0, ungapping_offset=0):
    """Get alignment column for interaction.
        ungapped_slice (bool): whether to use ungapped slicing w.r.t. row ungapping_row (see below)"""
    if not ungapped_slice:
        a1 = alignments_dict[interaction.segment1][:, interaction.start1:interaction.end1]
        a2 = alignments_dict[interaction.segment2][:, interaction.start2:interaction.end2]
    else:
        a1 = getUngappedSlice(alignments_dict[interaction.segment1], ungapping_row,
                              interaction.start1, interaction.end1, offset=ungapping_offset)
        a2 = getUngappedSlice(alignments_dict[interaction.segment2], ungapping_row,
                              interaction.start2, interaction.end2, offset=ungapping_offset)
    return a1, a2

def stripGaps(string):
    """Strip "-" from sequence string."""
    if type(string) == str:
        return string.replace("-", "")
    elif type(string) == Seq:
        return Seq(str(string).replace("-", ""))
    elif type(string) == SeqRecord:
        nseq = Seq(str(string.seq).replace("-", ""))
        nsqr = copy.copy(string) # is this good practice?
        nsqr.seq = nseq
        return nsqr
    else:
        raise ValueError("@stripGaps: inappropriate type + " + str(type(string))
                         + " for string: must be str, Seq, or SeqRecord")

def writeFASTA(fasta_list, filename, overwrite=True):
    """Write list of (header, sequence) to FASTA file."""
    mode = ['a', 'w'][overwrite] # file mode
    with open(filename, mode) as f:
        for tup in fasta_list:
            f.write(">" + tup[0] + "\n")
            f.write(tup[1] + "\n")
    return None


# FIX: None output in "sign. interaction found?" entry in .csv --> useful behaviour
# DISCUSSED: it seems that intaRNA only considers interactions in one direction: reverse <--> obverse, is this okay?
if __name__ == "__main__":
    # Config extraction
    CONFIG_FILE = "./input/config_H1N1.txt"
    #CONFIG_FILE = "./input/config_H3N2_Udorn_ref.txt"
    #CONFIG_FILE = "./input/config_H3N2.txt"
    #CONFIG_FILE = "./input/config_various_WSNref.txt"
    #CONFIG_FILE = "./input/config_H1N1_all.txt"
    #CONFIG_FILE = "./input/config_H5N1_WSN_ref.txt"
    #CONFIG_FILE = "./input/config_H5N1_WSN_ref_all.txt"
    #CONFIG_FILE = "./input/config_various_WSNref_all.txt"
    #CONFIG_FILE = "./input/config_H1N1_irdef.txt"
    PARAMS, PARAM_TYPES = config.loadConfig(CONFIG_FILE)
    
    ALIGNMENTS_LOCATION = PARAMS["ALIGNMENTS_LOCATION"]
    ALIGNMENTS_PREFIX = PARAMS["ALIGNMENTS_PREFIX"]
    SEGMENTS = PARAMS["SEGMENTS_PAS"]
    INTERACTIONS_FILE = PARAMS["INTERACTIONS_FILE"]
    INTERACTIONS_FILE_FORMAT = PARAMS["INTERACTIONS_FILE_FORMAT"]
    INTA_RNA_FOLDER = PARAMS["INTA_RNA_FOLDER"]
    INTARNA_PARAM_FILE = PARAMS["INTARNA_PARAM_FILE"]
    
    INDICES_FILE_INTERACTIONS = PARAMS["INDICES_FILE_INTERACTIONS"] # for taking subset of interactions
    USE_INDICES_FILE_FOR_SAMPLING = PARAMS["USE_INDICES_FILE_FOR_SAMPLING"] # whether to use file for taking a subset of interactions
    INDICES_FILE_ARRAY_OFFSET = PARAMS["INDICES_FILE_ARRAY_OFFSET"] # start offset (currently -1 due to use of inter.index "native index" (offset by 1) later)

    QUERY_FASTA_FILE = PARAMS["QUERY_FASTA_FILE"] # batch processing (DEFUNCT)
    TARGET_FASTA_FILE = PARAMS["TARGET_FASTA_FILE"] # DEFUNCT
    RANDOM_SEED = PARAMS["RANDOM_SEED"] # random seed for sampling. Set to "random" for random
    SIZE_BATCH = PARAMS["SIZE_BATCH"] # number of samples per batch (keep unchanged)
    NUM_BATCH_SAMPLES = PARAMS["NUM_BATCH_SAMPLES"] # number of batches. Use "all" for all interactions
    UNGAPPED_SEQ_REJECTION_THRESHOLD = PARAMS["UNGAPPED_SEQ_REJECTION_THRESHOLD"] # threshold ungapped length to not reject interaction sequences (for new strains)

    REF_STRAIN = PARAMS["REF_STRAIN"] # index of reference strain in strains
    INTERACTIONS_RESULTS_FILE = PARAMS["INTERACTIONS_RESULTS_FILE"]
    INTERACTIONS_AVERAGES_FILE = PARAMS["INTERACTIONS_AVERAGES_FILE"]

    # additional config (file-specific)
    CONVERT_SEED_TO_NORMAL_BP = True # scrub '+' symbol of seed bonds to '|' or ':' normal binding symbols

    # Inputs
    print("Reading interactions file")
    if USE_INDICES_FILE_FOR_SAMPLING:
        interactions_sampled_indices = get_interaction_sites.loadIndicesFile(INDICES_FILE_INTERACTIONS,
                                                                             array_start_factor=INDICES_FILE_ARRAY_OFFSET)
    interactions_list, interactions_dict = get_interaction_sites.getInteractionSites(INTERACTIONS_FILE,
                                                                                     INTERACTIONS_FILE_FORMAT)
    alignments_dict = {}
    strain_coverage_per_segment = {}
    for segment in SEGMENTS:
        print("-Reading", segment)
        fname = ALIGNMENTS_LOCATION + ALIGNMENTS_PREFIX + "_" + segment + ".fasta"
        alignment = AlignIO.read(fname, "fasta")
        alignments_dict[segment] = alignment

        # Parse segment/strain names
        sequences = sequence_eda.parseFASTASequences(fname, complement=False, reverse=False) # (header, seq)
        _, seq_parsed = sequence_eda.parseFASTAHeadersFromSeqlist(sequences) # index 0 = strain name
        strain_coverage_per_segment[segment] = [seq[0] for seq in seq_parsed] # in order of occurence

    strains = [] # all strains found
    for segment in strain_coverage_per_segment.keys():
        for strain in strain_coverage_per_segment[segment]:
            if strain not in strains:
                strains.append(strain)
    row_index_per_strain_per_segment = {} # {segment : {strain : index}}
    for segment in strain_coverage_per_segment.keys():
        row_index_per_strain_per_segment[segment] = {strain : None for strain in strains}
        for i, strain in enumerate(strain_coverage_per_segment[segment]):
            row_index_per_strain_per_segment[segment][strain] = i

    #for i in range(len(interactions_list)):
    #    inter = interactions_list[i]
    #    a1, a2 = getSubAlignments(inter, alignments_dict, ungapped_slice=True, ungapping_offset=1)
    #    seq0 = a1[0, :].seq
    #    seq1 = a2[0, :].seq
    #    #string, res, info = formatIntaRNAOutput(callIntaRNA(INTA_RNA_FOLDER, seq1, seq0))
    #    #print(info)

    # Get free energy for interactions per strain (cis-strain interactions)
    print("Gathering interactions")
    if RANDOM_SEED != "random":
        random.seed(RANDOM_SEED)

    # Preparing some strain stuff
    #strain_names_dict = {segment : [alignments_dict[segment][row, :].id
    #                                for row in range(len(alignments_dict[segment]))]
    #                     for segment in SEGMENTS}
    #strain_segment_dict = {}
    #for segm in SEGMENTS:
    #    for strain in strain_names_dict[segm]:
    #        if strain not in strain_segment_dict.keys():
    #            strain_segment_dict[strain] = {segm2 : False for segm2 in SEGMENTS}
    #        strain_segment_dict[strain][segm] = True

    qlist_dict = {strain : [] for strain in strains}
    tlist_dict = {strain : [] for strain in strains} # queries and targets for each strain
    all_interactions_indexed = [] # for later use
    reverse_all_interactions_indexed = {} # {interaction index : qlist/tlist index}
    for segment in interactions_dict.keys():
        for segm2 in interactions_dict[segment].keys():
            overlap_strains = list(set(strain_coverage_per_segment[segment]) &
                                   set(strain_coverage_per_segment[segm2])) # only these strains are candidates for the interaction
            for i, inter in enumerate(interactions_dict[segment][segm2]):
                header = segment + "_" + segm2 + "_int=" + str(i)
                a1, a2 = getSubAlignments(inter, alignments_dict, ungapped_slice=True, ungapping_offset=1)
                all_interactions_indexed.append((inter, segment, segm2)) # index all interactions
                reverse_all_interactions_indexed[inter.index] = len(all_interactions_indexed) - 1

                for strain in strains:
                    qlist_dict[strain].append(None) # this element represents unavailable interaction
                    tlist_dict[strain].append(None) # it should be at index i for later potential replacement
                    
                for strain in overlap_strains: # all of these should have not "None" for index1, index2
                    index1 = row_index_per_strain_per_segment[segment][strain]
                    index2 = row_index_per_strain_per_segment[segm2][strain]
                    unstripped_seq1 = str(a1[index1, :].seq)
                    unstripped_seq2 = str(a2[index2, :].seq)
                    seq1 = stripGaps(unstripped_seq1)
                    seq2 = stripGaps(unstripped_seq2)
                    len_ratio1 = len(seq1) / len(unstripped_seq1) # ratio of length between unstripped and stripped
                    len_ratio2 = len(seq2) / len(unstripped_seq2)
                    
                    if len(seq1) >= UNGAPPED_SEQ_REJECTION_THRESHOLD \
                       and len(seq2) >= UNGAPPED_SEQ_REJECTION_THRESHOLD:
                        # now we can add the interaction
                        list_end = len(qlist_dict[strain]) # should be same for tlist_dict[strain]
                        if len(seq1) <= len(seq2): # query supposed to be shorter
                            # replace "None"
                            qlist_dict[strain][list_end-1] = (segment, segm2, header, seq1, len_ratio1, unstripped_seq1)
                            tlist_dict[strain][list_end-1] = (segm2, segment, header, seq2, len_ratio2, unstripped_seq2)
                        else:
                            # first segm. = current ; second segm. = binding
                            qlist_dict[strain][list_end-1] = (segm2, segment, header, seq2, len_ratio2, unstripped_seq2)
                            tlist_dict[strain][list_end-1] = (segment, segm2, header, seq1, len_ratio1, unstripped_seq1)

    if not USE_INDICES_FILE_FOR_SAMPLING:
        if NUM_BATCH_SAMPLES != "all":
            # pick random indices
            indices = random.sample([i for i in range(len(qlist_dict[strains[0]]))], NUM_BATCH_SAMPLES * SIZE_BATCH)
        else:
            indices = list(range(len(qlist_dict[strains[0]])))
    else:
            indices = [reverse_all_interactions_indexed[x] for x in interactions_sampled_indices] # load in order
    sampled_interactions_indexed = [(i, all_interactions_indexed[i]) for i in indices] # contains sampled index, native index
    
    print("IntaRNA calls")
    free_energy_per_inter_per_strain = {strain : [] for strain in strains}
    significant_interaction_found_per_inter_per_strain = {strain : [] for strain in strains}
    intarna_alignment_per_inter_per_strain = {strain : [] for strain in strains}
    # {strain : list(free_energy)}, can have "None" if no inter (segment absent or misaligned) or no good structure
    for si, strain in enumerate(strains): # for each strain
        print("IntaRNA computations for strain:", strain)
        qlist_sub, tlist_sub = [], []
        coverage = [] # coverage of interaction per strain
        free_energy_per_inter = [None for _ in range(len(indices))] # free energy or "None" covering selected indices
        significant_interaction_found_per_inter = [None for _ in range(len(indices))] # whether a sign. inter. is found
        intarna_alignment_per_inter = [None for _ in range(len(indices))]# intaRNA sequences, alignment pos., etc.
        for i in indices:
            if qlist_dict[strain][i] is not None: # checking just this for "None" should be enough
                qlist_sub.append(qlist_dict[strain][i]) # prepare list of interactions for strain
                tlist_sub.append(tlist_dict[strain][i])
                coverage.append(True)
            else:
                coverage.append(False)
        coverage_true_indices = [i for i in range(len(coverage)) if coverage[i] == True] # all indices where cov = True
                
        num_batches = numBatches(qlist_sub, SIZE_BATCH) # interactions batches
        current_coverage_index = 0 # current index in "coverage" list
        for i in range(num_batches):
            print("-Batch %d / %d" % (i + 1, num_batches))
            qbatch = takeBatch(qlist_sub, i, SIZE_BATCH)
            tbatch = takeBatch(tlist_sub, i, SIZE_BATCH)
            res, err, lines, info_dict = formatIntaRNABatchOutput(*callIntaRNABatch(INTA_RNA_FOLDER,
                              [q[3] for q in qbatch],
                              [t[3] for t in tbatch],
                              flag_dict={"parameterFile" : INTARNA_PARAM_FILE}))

            for block_index, fe in enumerate(info_dict["free_energy"]):
                fe_index = coverage_true_indices[current_coverage_index] # map to actual index in ordered list of FEs
                free_energy_per_inter[fe_index] = fe # fe can still be "None"
                significant_interaction_found_per_inter[fe_index] = info_dict["interaction_found"][block_index]

                block_start, block_end = blockStartEnd(block_index, info_dict["block_starts"])
                if info_dict["interaction_found"][block_index]: # if a significant interaction was found for block
                    block = lines[block_start:block_end]
                    seq1_gapped, seq2_gapped, binding_pattern, alignment_positions \
                        = IntaRNAOutput_getAlignment(block) # read the actual intaRNA interaction

                    if CONVERT_SEED_TO_NORMAL_BP: # this converts '+' symbols in the binding seed to '|' or ':'
                        nbp = ""
                        for i, (c1, c2, cbp) in enumerate(zip(seq1_gapped, seq2_gapped, binding_pattern)):
                            if cbp == "+": # seed character
                                if c1 == "G" and c2 == "U" or c1 == "U" and c2 == "G": # half-bond TODO: other half bonds?
                                    nbp += ":"
                                else:
                                    nbp += "|"
                            else:
                                nbp += cbp
                        binding_pattern = nbp

                    intarna_alignment_per_inter[fe_index] = (seq1_gapped, seq2_gapped,
                                                             binding_pattern, alignment_positions)
                else:
                    intarna_alignment_per_inter[fe_index] = None # just to be sure

                current_coverage_index += 1 # increase in coverage list index
        free_energy_per_inter_per_strain[strain] = free_energy_per_inter
        significant_interaction_found_per_inter_per_strain[strain] = significant_interaction_found_per_inter
        intarna_alignment_per_inter_per_strain[strain] = intarna_alignment_per_inter
    print("Done with IntaRNA calls")

    # Processing free_energy_per_inter_per_strain
    # DONE: move plotting/analysis to another script
    import matplotlib.pyplot as plt
    import matplotlib.cm
    
    print("Processing data")
    # - averages
    # TODO: it's better to use per-interaction diffs here for determining average diff
    fe_averages_per_strain = {} # per strain
    fe_stdev_per_strain = {} # standard deviation
    count_per_strain = {} # coverage
    for strain in strains:
        total = 0
        count = 0
        for fe in free_energy_per_inter_per_strain[strain]:
            if fe != None:
                total += fe
                count += 1
        count_per_strain[strain] = count
        if count > 0:
            fe_averages_per_strain[strain] = total / count
        else:
            fe_averages_per_strain[strain] = float('nan')

        devsum = 0
        avg = fe_averages_per_strain[strain]
        if count > 0: # float nan checks don't work
            for fe in free_energy_per_inter_per_strain[strain]:
                if fe != None:
                    devsum += (fe - avg)**2
            fe_stdev_per_strain[strain] = devsum / count # count > 0 determined above
        else:
            fe_stdev_per_strain[strain] = float('nan')
            
    for strain in strains:
        print("Count-for-strain\t" + strain + "\t" + str(count_per_strain[strain]))
        print("Average-for-strain\t" + strain + "\t" + str(fe_averages_per_strain[strain]))
        print("Stdev-for-strain\t" + strain + "\t" + str(fe_stdev_per_strain[strain]))

    with open(INTERACTIONS_AVERAGES_FILE, 'w') as iaf: # write that to a file as well
        for quant in range(4):
            if quant == 0:
                iaf.write("Strain")
                for strain in strains:
                    iaf.write("," + strain)
                iaf.write("\n")
            if quant == 1:
                iaf.write("Count")
                for strain in strains:
                    iaf.write("," + str(count_per_strain[strain]))
                iaf.write("\n")
            if quant == 2:
                iaf.write("Average")
                for strain in strains:
                    iaf.write("," + str(fe_averages_per_strain[strain]))
                iaf.write("\n")
            if quant == 3:
                iaf.write("Stdev")
                for strain in strains:
                    iaf.write("," + str(fe_stdev_per_strain[strain]))

    # - write data
    with open(INTERACTIONS_RESULTS_FILE, 'w') as irf:
        # write: interaction index, seg1, start1, end1, seg2, start2, end2;
        # for every strain: strain, search_seq1, ungapped_length_ratio1, search_seq2, ungapped_length_ratio2,
        # alignment_sequence1, alignment_start1, alignment_end1, alignment_sequence2, alignment_start2, alignment_end2,
        # binding_pattern, free_energy;
        # TODO: binding pattern (forgot)
        for i, tup in enumerate(sampled_interactions_indexed):
            inter = tup[1][0]
            full_index = indices[i] # index in qlist_dict[strain]
            irf.write(str(inter.index) + ",") # interaction index (native to input file)
            irf.write(tup[1][1] + "," + str(inter.start1) + "," + str(inter.end1) + ",") # segment1, start1, end1
            irf.write(tup[1][2] + "," + str(inter.start2) + "," + str(inter.end2)) # segment2, start2, end2
            for strain in strains:
                # block separator = ;
                # significant_interaction_found_per_inter_per_strain can contain True, False or None currently
                irf.write(";" + strain + "," + str(significant_interaction_found_per_inter_per_strain[strain][i]) + ",")
                if qlist_dict[strain][full_index] is not None: # should be enough
                    qtup = qlist_dict[strain][full_index]
                    ttup = tlist_dict[strain][full_index]
                    if qtup[0] == tup[1][1]: # if query sequence == first sequence in stored interaction
                        seq1, len1 = qtup[5], qtup[4] # seq from segment 1; ATTN: now potentially gapped! ([3] --> [5])
                        seq2, len2 = ttup[5], ttup[4] # from segment 2
                    else:
                        seq1, len1 = ttup[5], ttup[4]
                        seq2, len2 = qtup[5], qtup[4]
                    irf.write(seq1 + "," + str(round(len1, 2)) + ","
                              + seq2 + "," + str(round(len2, 2)) + ",") # length ratios & sequences
                    # intarna_alignment_per_inter : (seq1_gapped, seq2_gapped, binding_pattern, alignment_positions)
                    intarna_result = intarna_alignment_per_inter_per_strain[strain][i]
                    if intarna_result is not None:
                        if qtup[0] == tup[1][1]:
                            irf.write(",".join([intarna_result[0], str(intarna_result[3][0]), str(intarna_result[3][1]),
                                                intarna_result[1], str(intarna_result[3][2]), str(intarna_result[3][3]),
                                                intarna_result[2]]))
                        else:
                            irf.write(",".join([intarna_result[1], str(intarna_result[3][2]), str(intarna_result[3][3]),
                                                intarna_result[0], str(intarna_result[3][0]), str(intarna_result[3][1]),
                                                intarna_result[2]]))
                    else:
                        irf.write(",".join([str(None) for _ in range(7)]))
                    if significant_interaction_found_per_inter_per_strain[strain][i]:
                        irf.write("," + str(round(free_energy_per_inter_per_strain[strain][i], 4))) # free energy
                    else:
                        irf.write("," + str(None))
                else:
                    irf.write(",".join([str(None) for _ in range(12)]))
            irf.write("\n")

    # > - plotting moved to interactions_results_analysis.py

