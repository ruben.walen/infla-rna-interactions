"""Mutation analysis on interaction sites vs. full segments."""

from Bio import AlignIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
import os
import random
import math
import matplotlib.pyplot as plt
import copy

import sequence_eda
import get_interaction_sites
import process_alignments_sites as pas
import tools.config as config


def getNumPolymorphisms(alignment, reference_row=0, rows="all", normalize=False):
    """Get num. insertions, deletions, and substitutions of alignment, with reference_row (index) as reference.
        Normalize (bool): whether to divide by alignment square size"""
    ins, dels, subs = 0, 0, 0
    if rows == "all":
        numrows = list(range(len(alignment))) # exclude reference_row
        numrows.pop(reference_row) # should be at this index
    else:
        numrows = rows
    rows_indices_dict = {numrows[i] : i for i in range(len(numrows))} # index in numrows vs. index in alignment
    ins_perrow = [0 for _ in numrows]
    dels_perrow = [0 for _ in numrows]
    subs_perrow = [0 for _ in numrows]

    for i in range(alignment.get_alignment_length()):
        c = alignment[reference_row, i]
        for nri, j in enumerate(numrows):
            ca = alignment[j, i]
            if c != ca:
                if c == "-":
                    ins += 1
                    ins_perrow[nri] += 1
                elif ca == "-":
                    dels += 1
                    dels_perrow[nri] += 1
                else:
                    subs += 1
                    subs_perrow[nri] += 1

    if not normalize: # insertions, deletions, substitutions
        return (ins, dels, subs), [(ins_perrow[i], dels_perrow[i], subs_perrow[i]) for i in range(len(numrows))], rows_indices_dict
    else: # normalize for square alignment size, and for length segments individually
        sqsize = getNumNucleotides(alignment)
        lengthsize = alignment.get_alignment_length()
        tup2 = [(ins_perrow[i] / lengthsize, dels_perrow[i] / lengthsize, subs_perrow[i] / lengthsize) for i in range(len(numrows))]
        #tup2 = ([x / lengthsize for x in ins_perrow], [x / lengthsize for x in dels_perrow], [x / lengthsize for x in subs_perrow])
        return (ins / sqsize, dels / sqsize, subs / sqsize), tup2, rows_indices_dict


def getNumNucleotides(alignment):
    return alignment.get_alignment_length() * len(alignment)


def mutationString(seq1, seq2):
    """Return a string containing . or * or i or d or -: . at site of conservation, * at site of mutation.
        Can deal with gaps: inserts i on insertion or d on deletion or - on gap in both"""
    mutation_string = ""
    for c1, c2 in zip(seq1, seq2):
        if c1 != c2:
            if c1 == "-":
                mutation_string += "i"
            elif c2 == "-":
                mutation_string += "d"
            else:
                mutation_string += "*"
        else: # c1 == c2
            if c1 == "-": # c2 == "-" too
                mutation_string += "-"
            else: # no mutation, no gap
                mutation_string += "."
    return mutation_string


def mutationStringCount(mutation_string):
    """Count mutations in mutationString result.
    Returns: mut_dict (dict): sub, ins, del, gap = gap in seq. (unchanged), con = conserved, len"""
    mut_dict = {"sub" : 0, "ins" : 0, "del" : 0, "gap" : 0, "con" : 0, "len" : len(mutation_string)}
    symb = {"i" : "ins", "d" : "del", "*" : "sub", "-" : "gap", "." : "con"}
    for m in mutation_string:
        mut_dict[symb[m]] += 1
    return mut_dict


def mutationPerAlignmentPosition(alignment, reference_row=0, rows="all", normalize=False, gap_char="-"):
    """Mutation/conservation per position in alignment, given reference row."""
    index_dict = {i : False for i in range(len(alignment))}
    if rows != "all":
        for i in range(rows):
            index_dict[i] = True
        nrows = len(rows)
    else:
        for i in range(len(alignment)):
            index_dict[i] = True
        nrows = len(alignment) # include reference_row in normalization

    if nrows == 0:
        raise ValueError("@mutationPerAlignmentPosition: nrows must be above 0.")

    per_position = []
    for j in range(alignment.get_alignment_length()):
        cref = alignment[reference_row, j]
        mdict = {"sub" : 0, "ins" : 0, "del" : 0, "gap" : 0, "con" : 0}
        for i, c in alignment[:, j]:
            if index_dict[i] and i != reference_row:
                if c == cref: # equal
                    if c != gap_char: # conservation
                        mdict["con"] += 1
                    else: # total gap
                        mdict["gap"] += 1
                else: # not equal
                    if cref == gap_char:  # insertion
                        mdict["ins"] += 1
                    else:
                        if c == gap_char:  # deletion
                            mdict["del"] += 1
                        else:  # substitution
                            mdict["sub"] += 1
        if normalize:
            for k in mdict.keys():
                mdict[k] /= nrows
        per_position.append(mdict)
    return per_position


def randomSequenceInSegment(alignments_dict, segment, length):
    """Get random sequence (alignment form) for segment in alignments_dict of certain alignment-level length."""
    u_bound = alignments_dict[segment].get_alignment_length() - length
    start = random.randint(0, u_bound)
    end = start + length
    return alignments_dict[segment][:, start:end] # all rows


def listOp(func, *lists):
    """Perform func on elements of *lists, elementwise."""
    ln = len(lists[0]) # throw error if no such list
    for ls in lists:
        if len(ls) != ln:
            raise ValueError("@listOp: lists must be of same length.")

    res = [None for _ in range(ln)]
    for i in range(ln):
        res[i] = func([ls[i] for ls in lists])
    return res


if __name__ == "__main__":
    # Config extraction
    CONFIG_FILE = "./input/config_H1N1.txt"
    #CONFIG_FILE = "./input/config_H3N2_Udorn_ref.txt"
    #CONFIG_FILE = "./input/config_various_WSNref_all.txt"
    #CONFIG_FILE = "./input/config_H5N1_WSN_ref.txt"
    PARAMS, PARAM_TYPES = config.loadConfig(CONFIG_FILE)
    
    ALIGNMENTS_LOCATION = PARAMS["ALIGNMENTS_LOCATION"]
    ALIGNMENTS_PREFIX = PARAMS["ALIGNMENTS_PREFIX"]
    SEGMENTS = PARAMS["SEGMENTS_MUTATION_ANALYSIS"]
    INTERACTIONS_FILE = PARAMS["INTERACTIONS_FILE"]
    INTERACTIONS_FILE_FORMAT = PARAMS["INTERACTIONS_FILE_FORMAT"]
    MUTATIONS_RESULTS_FILE = PARAMS["MUTATIONS_RESULTS_FILE"]
    RANDOM_SEED = PARAMS["RANDOM_SEED"] # random seed for sampling. Set to "random" for random NOTE: DEFUNCT
    REF_STRAIN = PARAMS["REF_STRAIN"] # index of reference strain in strains

    ##
    HEATMAP_CMAP = "Blues" # heatmaps color map
    HEATMAP_VMIN, HEATMAP_VMAX = 0.00, 1.10 # heatmap color skewing
    HEATMAP_TXTCOL = "k" # heatmap text color
    HEATMAP_TXTCOL_NA = "k" # txt color for N/A value

    print("Reading interactions file")
    interactions_list, interactions_dict = get_interaction_sites.getInteractionSites(INTERACTIONS_FILE, INTERACTIONS_FILE_FORMAT)
    alignments_dict = {}
    strain_coverage_per_segment = {}
    for segment in SEGMENTS:
        print("Reading", segment)
        fname = ALIGNMENTS_LOCATION + ALIGNMENTS_PREFIX + "_" + segment + ".fasta"
        alignment = AlignIO.read(fname, "fasta")
        alignments_dict[segment] = alignment

        # Parse segment/strain names
        sequences = sequence_eda.parseFASTASequences(fname, complement=False, reverse=False) # (header, seq)
        _, seq_parsed = sequence_eda.parseFASTAHeadersFromSeqlist(sequences) # index 0 = strain name
        strain_coverage_per_segment[segment] = [seq[0] for seq in seq_parsed] # in order of occurence

    strains = [] # all strains found
    for segment in strain_coverage_per_segment.keys():
        for strain in strain_coverage_per_segment[segment]:
            if strain not in strains:
                strains.append(strain)
    row_index_per_strain_per_segment = {} # {segment : {strain : index}}
    for segment in strain_coverage_per_segment.keys():
        row_index_per_strain_per_segment[segment] = {strain : None for strain in strains}
        for i, strain in enumerate(strain_coverage_per_segment[segment]):
            row_index_per_strain_per_segment[segment][strain] = i

    # Get free energy for interactions per strain (cis-strain interactions)
    print("Gathering interactions and computing mutations")
    if RANDOM_SEED != "random":
        random.seed(RANDOM_SEED)

    mutations_per_strain_per_sequence = {strain : {} for strain in strains} # {strain : {native index : [seq1, muts1, seq2, muts2]}}
    mutations_per_interaction = {} # {interaction native index : (length, overall mutations)
    interaction_site_lengths_per_segment = {segment : []
                                            for segment in SEGMENTS} # {segment : list(lengths of interaction sites)}, can be non-unique
    for segment in interactions_dict.keys():
        for segm2 in interactions_dict[segment].keys():
            for i, inter in enumerate(interactions_dict[segment][segm2]):
                header = segment + "_" + segm2 + "_int=" + str(i)
                a1, a2 = pas.getSubAlignments(inter, alignments_dict, ungapped_slice=False, ungapping_offset=1)
                a1_poly_all, a1_poly_rows, rows_dict_1 = getNumPolymorphisms(a1, reference_row=REF_STRAIN, normalize=True)
                a2_poly_all, a2_poly_rows, rows_dict_2 = getNumPolymorphisms(a2, reference_row=REF_STRAIN, normalize=True)
                # (ins, dels, subs), (ins per row, dels per row, subs per row)
                length_a1, length_a2 = a1.get_alignment_length(), a2.get_alignment_length()
                mutations_per_interaction[inter.index] = (length_a1, a1_poly_all, length_a2, a2_poly_all)

                interaction_site_lengths_per_segment[inter.segment1].append(inter.end1 - inter.start1 + 1) # inclusive
                interaction_site_lengths_per_segment[inter.segment2].append(inter.end2 - inter.start2 + 1)

                for strain in strains: # seq1 muts1 seq2 muts2
                    mutations_per_strain_per_sequence[strain][inter.index] = [None, None, None, None]
                for strain in strain_coverage_per_segment[segment]: # strains for a1
                    if strain != strains[REF_STRAIN]:
                        index_r = row_index_per_strain_per_segment[segment][strain] # in alignment
                        index_m = rows_dict_1[index_r] # in mutations list
                        mutations_per_strain_per_sequence[strain][inter.index][0] = a1[index_r, :].seq # seq
                        mutations_per_strain_per_sequence[strain][inter.index][1] = a1_poly_rows[index_m] # muts
                for strain in strain_coverage_per_segment[segm2]: # strains for a2
                    if strain != strains[REF_STRAIN]:
                        index_r = row_index_per_strain_per_segment[segm2][strain] # in alignment
                        index_m = rows_dict_2[index_r] # in mutations list
                        mutations_per_strain_per_sequence[strain][inter.index][2] = a2[index_r, :].seq # seq
                        mutations_per_strain_per_sequence[strain][inter.index][3] = a2_poly_rows[index_m] # muts
    #print(mutations_per_strain_per_sequence[strains[REF_STRAIN+2]], mutations_per_interaction)
    sampled_interations = interactions_list

    # Results
    print("Writing results")
    # - write data
    
    with open(MUTATIONS_RESULTS_FILE, 'w') as mrf:
        # write: interaction; segment1; segment2; [strain; seq1; seq2; FE]
        # header
        mrf.write("Index,Segment 1,Length 1,S1 Ins,S1 Dels,S1 Subs,Segment 2,Length 2,S2 Ins,S2 Dels,S2 Subs")
        for _ in range(len(strains) - 1): # excl. refstrain
            mrf.write(",Strain,S1 Seq,S1 Ins,S1 Dels,S1 Subs,S2 Seq,S2 Ins,S2 Dels,S2 Subs")
        mrf.write("\n")
        
        for i, inter in enumerate(sampled_interations):
            mut = mutations_per_interaction[inter.index]
            muts1 = ",".join([str(round(x, 3)) for x in mut[1]])
            muts2 = ",".join([str(round(x, 3)) for x in mut[3]])
            mrf.write(str(inter.index) + "," + inter.segment1 + "," + str(mut[0]) + "," + muts1
                      + "," + inter.segment2 + "," + str(mut[2]) + "," + muts2)
            
            for strain in strains: # mutation rates per strain
                if strain != strains[REF_STRAIN]: # unnecessary
                    mut_ps = mutations_per_strain_per_sequence[strain][inter.index]
                    mrf.write("," + strain)
                    if mut_ps[1] != None: # must be mutation rate tuple
                        muts_ps1 = ",".join([str(round(x, 3)) for x in mut_ps[1]])
                    else:
                        muts_ps1 = ",".join([str(None) for _ in range(3)])
                    if mut_ps[3] != None:
                        muts_ps2 = ",".join([str(round(x, 3)) for x in mut_ps[3]])
                    else:
                        muts_ps2 = ",".join([str(None) for _ in range(3)])
                    mrf.write("," + str(mut_ps[0]) + "," + muts_ps1 + "," + str(mut_ps[2]) + "," + muts_ps2)
                
            mrf.write("\n")
            
    # - sequence lengths
    interaction_site_lengths = []
    for int_id in mutations_per_interaction.keys():
        mut = mutations_per_interaction[int_id]
        interaction_site_lengths.append(mut[0])
        interaction_site_lengths.append(mut[2])

    # - average mutation rates per strain (all interactions)
    print("-Average mutation rates")
    mutation_rates_per_strain = {} # {strain : [avg_ins, avg_dels, avg_subs]}
    counts_per_strain = {}
    for strain in strains:
        mutation_rates_per_strain[strain] = [0, 0, 0] # for ref_strain this remains
        counts_per_strain[strain] = 0
        if strain != strains[REF_STRAIN]: # unnecessary
            for int_id in mutations_per_strain_per_sequence[strain]:
                mut = mutations_per_strain_per_sequence[strain][int_id]
                if mut[0] != None:
                    counts_per_strain[strain] += 1
                    mutation_rates_per_strain[strain] = listOp(lambda ls: sum(ls),
                                                               mutation_rates_per_strain[strain], mut[1])
                if mut[2] != None:
                    counts_per_strain[strain] += 1
                    mutation_rates_per_strain[strain] = listOp(lambda ls: sum(ls),
                                                               mutation_rates_per_strain[strain], mut[3])
            mutation_rates_per_strain[strain] = listOp(lambda ls : ls[0] / ls[1],
                                                       mutation_rates_per_strain[strain],
                                                       [counts_per_strain[strain] for _ in range(3)])
    conservation_rates_per_strain = {strain : 1 - mutation_rates_per_strain[strain][0]
                                             - mutation_rates_per_strain[strain][1]
                                             - mutation_rates_per_strain[strain][2]
                                             for strain in strains}

    # - barplot per strain
    print("-Mutation rates barplot")
    fig, ax = plt.subplots()
    strains_redux = copy.copy(strains)
    strains_redux.remove(strains[REF_STRAIN])
    ind = [i for i in range(len(strains_redux))]
    width = 0.35 # the width of the bars: can also be len(x) sequence

    pcons = ax.bar(ind, [conservation_rates_per_strain[strain] for strain in strains_redux], width) # TODO: stdev?
    bottoms = [conservation_rates_per_strain[strain] for strain in strains_redux]
    pins = ax.bar(ind, [mutation_rates_per_strain[strain][0] for strain in strains_redux], width, bottom=bottoms)
    bottoms = listOp(lambda ls: sum(ls), bottoms, [mutation_rates_per_strain[strain][0] for strain in strains_redux])
    pdels = ax.bar(ind, [mutation_rates_per_strain[strain][1] for strain in strains_redux], width, bottom=bottoms)
    bottoms = listOp(lambda ls: sum(ls), bottoms, [mutation_rates_per_strain[strain][1] for strain in strains_redux])
    psubs = ax.bar(ind, [mutation_rates_per_strain[strain][2] for strain in strains_redux], width, bottom=bottoms)

    ax.set_ylabel('Normalized rates')
    ax.set_title('Mutation/conservation rates per strain for all interactions, reference strain: ' + strains[REF_STRAIN])
    ax.set_xticks(ind)
    plt.xticks(rotation=45)
    ax.set_xticklabels(strains_redux)
    #ax.set_yticks(np.arange(0, 81, 10))
    ax.legend((pcons[0], pins[0], pdels[0], psubs[0]),
               ("Conservation", "Insertions", "Deletions", "Substitutions"))
    plt.show()

    # - mutation rate per segment per strain
    print("-Segment mutation rates")
    segment_mutation_rate_per_strain = {} # {strain : {segment : (ins, dels, subs) (rates)}}
    segment_mutation_rate_overall = {} # {segment : (ins, dels, subs) (rates)}
    for segment in SEGMENTS:
        segm_sequence = alignments_dict[segment]
        poly_all, poly_rows, rows_dict = getNumPolymorphisms(segm_sequence,
                                                             reference_row=REF_STRAIN, rows="all", normalize=True)
        segment_mutation_rate_per_strain[segment] = {}
        segment_mutation_rate_overall[segment] = list(poly_all)
        for strain in strains_redux: # excl. refstrain
            segment_mutation_rate_per_strain[segment][strain] = [None, None, None]
            if strain in strain_coverage_per_segment[segment]: # strain has segment
                index_r = row_index_per_strain_per_segment[segment][strain] # in alignment
                index_m = rows_dict[index_r] # in mutations list
                segment_mutation_rate_per_strain[segment][strain] = list(poly_rows[index_m])

    segment_conservation_rate_overall = {}
    for segment in SEGMENTS:
        segment_conservation_rate_overall[segment] = 1 - segment_mutation_rate_overall[segment][0]
        segment_conservation_rate_overall[segment] -= segment_mutation_rate_overall[segment][1]
        segment_conservation_rate_overall[segment] -= segment_mutation_rate_overall[segment][2]
        
    fig, ax = plt.subplots()
    ind = [i for i in range(len(SEGMENTS))]
    width = 0.35 # the width of the bars: can also be len(x) sequence

    pcons = ax.bar(ind, [segment_conservation_rate_overall[segment] for segment in SEGMENTS], width) # TODO: stdev?
    bottoms = [segment_conservation_rate_overall[segment] for segment in SEGMENTS]
    pins = ax.bar(ind, [segment_mutation_rate_overall[segment][0] for segment in SEGMENTS], width, bottom=bottoms)
    bottoms = listOp(lambda ls: sum(ls), bottoms, [segment_mutation_rate_overall[segment][0] for segment in SEGMENTS])
    pdels = ax.bar(ind, [segment_mutation_rate_overall[segment][1] for segment in SEGMENTS], width, bottom=bottoms)
    bottoms = listOp(lambda ls: sum(ls), bottoms, [segment_mutation_rate_overall[segment][1] for segment in SEGMENTS])
    psubs = ax.bar(ind, [segment_mutation_rate_overall[segment][2] for segment in SEGMENTS], width, bottom=bottoms)

    ax.set_ylabel('Normalized rates')
    ax.set_title('Mutation/conservation rates per segment, reference strain: ' + strains[REF_STRAIN])
    ax.set_xticks(ind)
    plt.xticks(rotation=45)
    ax.set_xticklabels(SEGMENTS)
    #ax.set_yticks(np.arange(0, 81, 10))
    ax.legend((pcons[0], pins[0], pdels[0], psubs[0]),
               ("Conservation", "Insertions", "Deletions", "Substitutions"))
    plt.show()

    # - interaction sites per segment + length distributions
    import numpy as np

    segment_lengths_histograms = {segment : None for segment in SEGMENTS}
    for segment in SEGMENTS:
        ls = interaction_site_lengths_per_segment[segment]
        segment_lengths_histograms[segment] = np.histogram(ls, bins=np.arange(np.amin(ls), np.amax(ls) + 1, 1))
        
    fig, ax = plt.subplots()
    for segment in SEGMENTS:
        ax.plot(segment_lengths_histograms[segment][1][0:-1], segment_lengths_histograms[segment][0], label=segment)
    ax.legend()
    ax.set_xlabel("Interaction site length (bp)")
    ax.set_ylabel("Absolute frequency")
    ax.set_title("Interaction site lengths in reference genome across segments")
    plt.show()

    # - random sampling (TODO)

    # - heatmaps: segment conservation rate, interaction site conservation rate
    print("-Heatmaps of mutation rates")
    
    segment_conservation_rate_per_strain = {} # {segment : {strain : conservation rate over whole segment}}
    for segment in SEGMENTS:
        segment_conservation_rate_per_strain[segment] = {}
        for strain in strains_redux:
            if segment_mutation_rate_per_strain[segment][strain][0] != None: # segment exists
                segment_conservation_rate_per_strain[segment][strain] = 1
                segment_conservation_rate_per_strain[segment][strain] -= segment_mutation_rate_per_strain[segment][strain][0]
                segment_conservation_rate_per_strain[segment][strain] -= segment_mutation_rate_per_strain[segment][strain][1]
                segment_conservation_rate_per_strain[segment][strain] -= segment_mutation_rate_per_strain[segment][strain][2]
            else:
                segment_conservation_rate_per_strain[segment][strain] = float('nan')

    # adapted: https://matplotlib.org/3.1.0/gallery/images_contours_and_fields/image_annotated_heatmap.html
    data_matrix = np.array([[segment_conservation_rate_per_strain[segment][strain] for strain in strains_redux]
                            for segment in SEGMENTS])

    fig1, ax1 = plt.subplots()
    hmp1 = ax1.imshow(data_matrix, cmap=HEATMAP_CMAP, vmin=HEATMAP_VMIN, vmax=HEATMAP_VMAX)

    ax1.set_xticks(np.arange(len(strains_redux)))
    ax1.set_yticks(np.arange(len(SEGMENTS)))
    ax1.set_xticklabels(strains_redux)
    ax1.set_yticklabels(SEGMENTS)
    plt.setp(ax1.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    for i in range(len(SEGMENTS)):
        for j in range(len(strains_redux)):
            if data_matrix[i, j] != float('nan'):
                txt = round(data_matrix[i, j], 2)
                txtcolor = HEATMAP_TXTCOL
            else:
                txt = "N/A"
                txtcolor = HEATMAP_TXTCOL_NA
            text = ax1.text(j, i, txt,
                           ha="center", va="center", color=txtcolor)

    ax1.set_title("Conservation rate across whole segment per strain per segment")
    fig1.tight_layout()

    # -- interactions per segment per strain
    in_site_conservation_rate_per_sequence_per_strain = {segment : {strain: 0 for strain in strains_redux}
                                                         for segment in SEGMENTS}
    in_site_conservation_rate_stdev = {segment : {strain: [] for strain in strains_redux}
                                                         for segment in SEGMENTS}
    counts_per_sequence_per_strain = {segment : {strain: 0 for strain in strains_redux} # single number later
                                      for segment in SEGMENTS}
    for i, inter in enumerate(interactions_list):
        int_id = inter.index
        segm1, segm2 = inter.segment1, inter.segment2
        for strain in strains_redux:
            mut = mutations_per_strain_per_sequence[strain][int_id] # (seq1, pol1, seq2, poly2)
            if mut[1] != None: # not unrepresented
                cons_rate = 1 - mut[1][0] - mut[1][1] - mut[1][2]
                in_site_conservation_rate_per_sequence_per_strain[segm1][strain] += cons_rate
                in_site_conservation_rate_stdev[segm1][strain].append(cons_rate)
                counts_per_sequence_per_strain[segm1][strain] += 1
            if mut[3] != None:
                cons_rate = 1 - mut[3][0] - mut[3][1] - mut[3][2]
                in_site_conservation_rate_per_sequence_per_strain[segm2][strain] += cons_rate
                in_site_conservation_rate_stdev[segm2][strain].append(cons_rate)
                counts_per_sequence_per_strain[segm2][strain] += 1

    for segment in SEGMENTS:
        for strain in strains_redux:
            if counts_per_sequence_per_strain[segment][strain] > 0:
                in_site_conservation_rate_per_sequence_per_strain[segment][strain] /= counts_per_sequence_per_strain[segment][strain]
                in_site_conservation_rate_stdev[segment][strain] = sum([(x - in_site_conservation_rate_per_sequence_per_strain[segment][strain])**2
                                                                        for x in in_site_conservation_rate_stdev[segment][strain]])
                in_site_conservation_rate_stdev[segment][strain] /= counts_per_sequence_per_strain[segment][strain]
            else: # == 0
                in_site_conservation_rate_per_sequence_per_strain[segment][strain] = float('nan') # unrepresented
                in_site_conservation_rate_stdev[segment][strain] = float('nan')

    # adapted: https://matplotlib.org/3.1.0/gallery/images_contours_and_fields/image_annotated_heatmap.html
    data_matrix2 = np.array([[in_site_conservation_rate_per_sequence_per_strain[segment][strain] for strain in strains_redux]
                            for segment in SEGMENTS])
    fig2, ax2 = plt.subplots()
    hmp2 = ax2.imshow(data_matrix2, cmap=HEATMAP_CMAP, vmin=HEATMAP_VMIN, vmax=HEATMAP_VMAX)

    ax2.set_xticks(np.arange(len(strains_redux)))
    ax2.set_yticks(np.arange(len(SEGMENTS)))
    ax2.set_xticklabels(strains_redux)
    ax2.set_yticklabels(SEGMENTS)
    plt.setp(ax2.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    for i in range(len(SEGMENTS)):
        for j in range(len(strains_redux)):
            if not math.isnan(in_site_conservation_rate_per_sequence_per_strain[SEGMENTS[i]][strains_redux[j]]):
                txt = "{:.1e}\n±{:.1e}".format(data_matrix2[i, j],
                                               in_site_conservation_rate_stdev[SEGMENTS[i]][strains_redux[j]])
                txtcolor = HEATMAP_TXTCOL
            else:
                txt = "N/A"
                txtcolor = HEATMAP_TXTCOL_NA
            text = ax2.text(j, i, txt, ha="center", va="center", color=txtcolor)

    ax2.set_title("Conservation rate across interaction sites per strain per segment")
    fig2.tight_layout()
    plt.show()
