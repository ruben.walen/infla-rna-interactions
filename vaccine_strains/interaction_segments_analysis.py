"""Analysis of interaction density along segments (genome)."""

import re
import matplotlib.pyplot as plt
import numpy as np
import math

import sequence_eda
import get_interaction_sites
import interaction_results_analysis as ira


def getIndexInPlot(index, subplots_layout):
    """Retrieve subplots coordinates of index."""
    col = index // subplots_layout[0] # // nrows
    row = index % subplots_layout[0]
    return row, col


if __name__ == "__main__":
    # configuration
    # load input genome and interactions file, not from config
    # CONFIG_LOADS: list for packed loading of variables
    CONFIG_LOADS = ["./input/WSN_fasta.txt", "A/WSN/1933[H1N1]", "./input/WSN_Dadonaite_isites.txt", "Dadonaite",
                   -1, 0, 0]
    #CONFIG_LOADS = ["./input/Udorn_fasta_H3N2.txt", "A/Udorn/1972[H3N2]", "./input/Udorn_rep1_Dadonaite_isites.txt", "Dadonaite",
    #                -1, 0, 0]
    #CONFIG_LOADS = ["./input/Udorn_fasta_H3N2_rep2.txt", "A/Udorn/1972[H3N2] (rep. 2)", "./input/Udorn_rep2_Dadonaite_isites.txt", "Dadonaite",
    #                -1, 0, 0]
    #CONFIG_LOADS = ["./input/PR8_rep1_fasta.txt", "A/Puerto Rico/8/1934[H1N1]", "./input/PR8_rep1_Dadonaite_isites.txt", "Dadonaite",
    #                -1, 0, 0]
    # CONFIG_LOADS = ["./input/WSN_fasta.txt", "A/WSN/1933[H1N1] rep. 1 (LeSage)", "./input/LeSage_isites_WSN_rep1.txt", "LeSage",
    #                 0, 0, -1]
    # CONFIG_LOADS = ["./input/WSN_fasta.txt", "A/WSN/1933[H1N1] rep. 2 (LeSage)", "./input/LeSage_isites_WSN_rep2.txt", "LeSage",
    #                 0, 0, -1]
    # CONFIG_LOADS = ["./input/WSN_fasta.txt", "A/WSN/1933[H1N1] NP-HSmut rep. 1 (LeSage)", "./input/LeSage_isites_WSN_HSmut_rep1.txt", "LeSage",
    #                 0, -1, 0]

    # packed loads from CONFIG_LOADS; not on one line for clarity purposes
    INPUT_GENOME = CONFIG_LOADS[0]
    STRAIN_NAME = CONFIG_LOADS[1]
    INTERACTIONS_FILE = CONFIG_LOADS[2]
    INTERACTIONS_FILE_FORMAT = CONFIG_LOADS[3]
    INDICES_FILE_ARRAY_OFFSET = CONFIG_LOADS[4]
    INDICES_FILE_END_OFFSET = CONFIG_LOADS[5]
    INDICES_FILE_START_OFFSET = CONFIG_LOADS[6]

    WEIGHTING_METHOD = "none" # "none" or "strength" or "log_strength" : weight applied to each interaction
    NORMALIZE = True # normalize the graphs by maximum coverage (maxed over ALL segments) # TODO
    SEGMENTS = ["PB2", "PB1", "PA", "HA", "NP", "NA", "MP", "NS"] # segments to consider
    COLOR_VMIN, COLOR_VMAX = 0, None # color minima and maxima used for heatmap
    # good colors: set to 0, 120% of max; for example
    HEATMAP_TXT_COLOR = '#111111' # heatmap cell text color

    # POTENTIAL TODO: add sliding window averaging

    # load interactions
    interactions_list, interactions_dict = get_interaction_sites.getInteractionSites(INTERACTIONS_FILE,
                                                                                     INTERACTIONS_FILE_FORMAT)
    if WEIGHTING_METHOD not in ["none", "strength", "log_strength"]:
        raise ValueError("@interaction_segments_analysis.py: unknown WEIGHTING_METHOD: " + WEIGHTING_METHOD)

    # load genome
    parse_ref = sequence_eda.parseFASTASequences(INPUT_GENOME, complement=False, reverse=False)
    segment_search_pattern = "\([A-Z0-9]+\)"  # must be in parentheses
    sequences = []  # sequence list with (segment name, fasta header, seq)
    segments_found = []
    sequences_list_pointers = {} # dict of indices of segments in sequences list
    for i, seq in enumerate(parse_ref):
        segment_search = re.search(segment_search_pattern, seq[0])
        if segment_search is None:
            raise ValueError("@interaction_segments_analysis.py: failed to find segment name in:" + seq[0]) # upgraded to error
        segment = segment_search.group()[1:-1]
        segments_found.append(segment) # auto scan segments; use SEGMENTS for config
        sequences.append([segment, seq[0], seq[1]])
        sequences_list_pointers[segment] = len(sequences) - 1

##### experiment 1: segment interaction coverage
    # main loop over interactions; scan for interaction coverage
    cov_per_segment = {segment : np.zeros((len(sequences[sequences_list_pointers[segment]][2]),)) # length of segment
                       for segment in SEGMENTS}
    num_interactions_per_segment = {segment : 0 for segment in SEGMENTS} # number of unique inters per segment
    max_inter_strength = max(interactions_list, key=lambda v: v.strength).strength # maximal strength interaction
    inter_segment_interactions = {seg1 : {seg2 : 0 for seg2 in SEGMENTS} for seg1 in SEGMENTS} # for heatmap
    for inter in interactions_list:
        weight = 1 # "none" WEIGHTING_METHOD; no weight applied
        if WEIGHTING_METHOD == "strength": # apply inter. 'strength' weight reduction here; i.e. Dadonaite RPM
            weight = inter.strength
        elif WEIGHTING_METHOD == "log_strength": # logarithmic strength weighting
            weight = math.log(inter.strength) / math.log(max_inter_strength)

        # calibrate interaction site: offset (pythonic vs. array start at 1)
        if inter.segment1 in SEGMENTS:
            calib_start1, calib_end1 = inter.start1 + INDICES_FILE_ARRAY_OFFSET + INDICES_FILE_START_OFFSET,\
                                       inter.end1 + INDICES_FILE_ARRAY_OFFSET + INDICES_FILE_END_OFFSET
            weights = np.ones((calib_end1 - calib_start1,)) * weight
            cov_per_segment[inter.segment1][calib_start1:calib_end1] += weights

        if inter.segment2 in SEGMENTS:
            calib_start2, calib_end2 = inter.start2 + INDICES_FILE_ARRAY_OFFSET + INDICES_FILE_START_OFFSET,\
                                       inter.end2 + INDICES_FILE_ARRAY_OFFSET + INDICES_FILE_END_OFFSET
            weights = np.ones((calib_end2 - calib_start2,)) * weight
            cov_per_segment[inter.segment2][calib_start2:calib_end2] += weights

        num_interactions_per_segment[inter.segment1] += 1
        num_interactions_per_segment[inter.segment2] += 1
        inter_segment_interactions[inter.segment1][inter.segment2] += 1
        inter_segment_interactions[inter.segment2][inter.segment1] += 1 # symmetry along diagonal

    # normalization
    if NORMALIZE: # normalize to max over ALL segments
        maximum_coverage = -1
        for segment in SEGMENTS:
            v = np.amax(cov_per_segment[segment])
            if v > maximum_coverage:
                maximum_coverage = v
        if maximum_coverage < 0: # should never happen
            raise ValueError("@interaction_segments_analysis.py: maximum coverage is somehow below 0")
        for segment in SEGMENTS:
            cov_per_segment[segment] /= maximum_coverage

    # print numbers of interactions per segment
    print("Numbers of interactions per segment:")
    print("Total:")
    for segment in SEGMENTS:
        print(segment, " : ", num_interactions_per_segment[segment])
    print("Ratio over segment length:")
    ratios = []
    for segment in SEGMENTS:
        ratio = num_interactions_per_segment[segment] / len(sequences[sequences_list_pointers[segment]][2])
        print(segment, " : ", ratio)
        ratios.append(ratio)
    print("Ratio over segment length normalized to maximum:")
    max_ratio = max(ratios)
    for segment in SEGMENTS:
        ratio = num_interactions_per_segment[segment] / len(sequences[sequences_list_pointers[segment]][2])
        print(segment, " : ", ratio / max_ratio)
    print("Segment lengths:")
    for segment in SEGMENTS:
        print(segment, " : ", len(sequences[sequences_list_pointers[segment]][2]))
    print("\n")

    # plotting
    plot_layout = (math.ceil(len(SEGMENTS) / math.ceil(len(SEGMENTS) / 4)), math.ceil(len(SEGMENTS) / 4)) # subplots layout
    fig_cov, ax_cov = plt.subplots(nrows=plot_layout[0], ncols=plot_layout[1])
    fig_cov.subplots_adjust(hspace=0.35, top=0.90)  # better spacing

    for i, segment in enumerate(SEGMENTS):
        pi = getIndexInPlot(i, plot_layout)
        cov = cov_per_segment[segment]
        ax_cov[pi[0], pi[1]].scatter(np.arange(cov.shape[0]), cov, s=plt.rcParams['lines.markersize'] * 1)
        ax_cov[pi[0], pi[1]].set_title("Interaction coverage plot for segment: " + segment)
        if NORMALIZE:
            ax_cov[pi[0], pi[1]].set_ylim((0, 1))

    fig_cov.suptitle("Interaction coverage plots per segment for strain: " + STRAIN_NAME)
    plt.show()

    # make heatmap of interactions between segments
    data_matrix = np.array([[inter_segment_interactions[seg1][seg2] for seg2 in SEGMENTS] for seg1 in SEGMENTS])
    hmp1, ax1 = ira.makeHeatmap(data_matrix, SEGMENTS, SEGMENTS, 'Blues',
                                cell_data_text_format="{}", cell_text_color=HEATMAP_TXT_COLOR,
                                color_vmin=COLOR_VMIN, color_vmax=COLOR_VMAX)
    ax1.set_title("Number of unique interactions between each segment for strain: " + STRAIN_NAME)
    plt.show()
