"""Sequence loading and alignment formation via ClustalW."""

import re
import Bio.Seq as bseq
import Bio.SeqRecord as bsrc
import Bio.Align as balgn
import Bio.Align.Applications as balgn_app
import os

import tools.config as config

COMPLEMENT_DICT_DNA = {"A" : "T", "C" : "G", "T" : "A", "G" : "C",
                       "R" : "Y", "Y" : "R", "K" : "M", "M" : "K",
                       "S" : "W", "W" : "S", "B" : "V", "D" : "H",
                       "H" : "D", "V" : "B" , "N" : "N", "X" : "X"}
COMPLEMENT_DICT_RNA = {"A" : "U", "C" : "G", "U" : "A", "G" : "C",
                       "R" : "Y", "Y" : "R", "K" : "M", "M" : "K",
                       "S" : "W", "W" : "S", "B" : "V", "D" : "H",
                       "H" : "D", "V" : "B" , "N" : "N", "X" : "X"}


def alphabetComplement(string, complement_dict, complement=True, reverse=False):
    """Get complement of string using complement_dict for alphabet.
        Reverse (bool): whether to also reverse output string."""
    nstring = ""
    if complement:
        if not reverse:
            for s in string:
                nstring += complement_dict[s]
        else:
            for s in reversed(string):
                nstring += complement_dict[s]
        return nstring
    else: # don't complement
        if not reverse:
            return string
        else:
            return string[::-1]


def parseFASTASequences(filename, reverse=False, complement=False,
                        complement_dict=COMPLEMENT_DICT_DNA):
    """Parse a FASTA list to (header, sequence) list."""
    sequences = [] # [fasta header, fasta sequence]
    seq_index = -1
    with open(filename) as f:
        for line in f.readlines():
            if line == "":
                pass
            elif line[0] == ">":
                seq_index += 1
                sequences.append([line[0:-1], ""])
            else:
                if seq_index == -1:
                    raise ValueError("@sequence_eda.py: FASTA file does not start with header")
                else:
                    sequences[seq_index][1] += line[0:-1]
        for i, seq in enumerate(sequences): # complementing and reversing
            sequences[i][1] = alphabetComplement(seq[1], complement_dict=complement_dict,
                                                 complement=complement, reverse=reverse)
    return sequences


def newStrainNameParser(string, influenza_prefix="A/", segment_length_limit=None):
    """New (slow but more exact?) parser for influenza strain names.
    This should catch weird strain names, e.g. A/duck/.../blabla/3235235/...
    Returns only the first such occurrence."""
    # TODO: segment length limits (DONE)? segment number ranges?
    forbidden_chars = ["\n", "\t"]
    start = string.find(influenza_prefix)
    if start == -1:
        return None

    strain_name = "" # find only first occurence
    tstring = "" # temp string
    segments = 1 # A/ = 1
    for c in string[start:]:
        if c in forbidden_chars or (segment_length_limit is not None and len(tstring) > segment_length_limit):
            break # instant break

        tstring += c
        if c == "/":
            strain_name += tstring
            tstring = ""
            segments += 1

    t2_string = ""
    for c in tstring: # now look in the remaining tstring
        if c == " " or (segment_length_limit is not None and len(tstring) > segment_length_limit):
            break
        t2_string += c
    strain_name += t2_string

    return strain_name


def parseFASTAHeadersFromSeqlist(sequences, use_new_parser=True, search_segments=True):
    """Parse sequences list (see previous) to dict of {strain: {segment: index in sequences list}}
        Segment parser recognizes e.g. : (PB1)
        Strain parser recognizes e.g. : A/Brisbane/1/1999
        use_new_parser (bool): new style name parser, non regex, but should be more flexible."""
    segment_search_pattern = "\([A-Z0-9]+\)" # must be in parentheses
    seq_name_search_pattern_1 = "A/[^/]+/[0-9]+/[0-9]+" # infl A only TODO: strain ID may not be numeric
    seq_name_search_pattern_2 = "A/[^/]+/[0-9]+" # infl A only; e.g. A/WSN/1933 (no further specifier)
    strain_dict = {} # keep strains, segments
    n_sequences = [] # sequence list with (strain name, segment name, fasta header, seq)
    for i, seq in enumerate(sequences):
        segment = None # else segment = None
        if search_segments:
            segment_search = re.search(segment_search_pattern, seq[0])
            if segment_search is None:
                print("@parseFASTAHeadersFromSeqlist: failed to find segment name in:", seq[0])
            segment = segment_search.group()[1:-1]

        if not use_new_parser: # regex style
            strain_search_1 = re.search(seq_name_search_pattern_1, seq[0])  # first possible pattern
            strain_search_2 = re.search(seq_name_search_pattern_2, seq[0])  # second possible pattern
            if strain_search_1 is None and strain_search_2 is None:
                print("@parseFASTAHeadersFromSeqlist: (old parser) failed to find strain name in:", seq[0])
            if strain_search_1 is not None:
                strain = strain_search_1.group()
            else:
                strain = strain_search_2.group()
        else: # new style
            strain = newStrainNameParser(seq[0])
            if strain is None:
                print("@parseFASTAHeadersFromSeqlist: (new parser) failed to find strain name in:", seq[0])

        n_sequences.append((strain, segment, seq[0], seq[1])) # (strain name, segment name, fasta header, seq)
        if strain not in strain_dict.keys():
            strain_dict[strain] = {}
        strain_dict[strain][segment] = i # segment: seq. index
    return strain_dict, n_sequences


def limitStringLength(string, length, insert_dots=0):
    """Return string if len < length; else cut to length from start
        insert_dots (int): whether to substitute last n chars for dots if len > length"""
    if length < insert_dots:
        raise ValueError("@limitStringLength: length must be larger than insert_dots")

    if len(string) <= length:
        return string
    else:
        n_string = string[0:(length - insert_dots)]
        for _ in range(insert_dots):
            n_string += "."
        return n_string


def writeFASTA(file, headers, sequences, extra_space=False, final_newline=False):
    """Write a FASTA file from list of headers and sequences. Headers without '>'."""
    if len(headers) != len(sequences):
        raise ValueError("@sequence_eda.writeFASTA: headers must match sequences!")

    with open(file, 'w') as f:
        for i, (head, seq) in enumerate(zip(headers, sequences)):
            f.write(">" + head + "\n")
            f.write(seq)
            if final_newline or i < len(headers) - 1:
                f.write("\n")
                if extra_space:
                    f.write("\n")
            

if __name__ == "__main__": # do stuff here
    # Config extraction
    CONFIG_FILE = "./input/config_H1N1.txt"
    #CONFIG_FILE = "./input/config_H3N2.txt"
    #CONFIG_FILE = "./input/config_various_WSNref.txt"
    #CONFIG_FILE = "./input/config_H5N1_WSN_ref.txt"
    PARAMS, PARAM_TYPES = config.loadConfig(CONFIG_FILE)

    SEQUENCES_FILE = PARAMS["SEQUENCES_FILE"] # contains target sequences
    TARGET_COMPLEMENT = PARAMS["TARGET_COMPLEMENT"] # complement sequences
    TARGET_REVERSE = PARAMS["TARGET_REVERSE"] # reverse sequences

    REF_GENOME_FILE = PARAMS["REF_GENOME_FILE"]
    REF_COMPLEMENT = PARAMS["REF_COMPLEMENT"] # complement sequences
    REF_REVERSE = PARAMS["REF_REVERSE"] # reverse sequences

    VERBOSE_PARSER = PARAMS["VERBOSE_PARSER"]
    SEGMENTS = PARAMS["SEGMENTS_SEQUENCES"]

    CLUSTAL_BINARY_FOLDER = PARAMS["CLUSTAL_BINARY_FOLDER"] # point to CLUSTAL binary
    CLUSTAL_OUT_PREFIX = PARAMS["CLUSTAL_OUT_PREFIX"] # prefix for MSA output without "./". Stored from curr_folder level

    # Extract segment sequences
    sequences = parseFASTASequences(SEQUENCES_FILE, complement=TARGET_COMPLEMENT,
                                    reverse=TARGET_REVERSE) # (header, seq)
    # Parse segment/strain names
    strain_dict, _ = parseFASTAHeadersFromSeqlist(sequences)
    if VERBOSE_PARSER:
        for strain in strain_dict.keys():
            for segment in strain_dict[strain].keys():
                seq = sequences[strain_dict[strain][segment]]
                print(">>>")
                print(seq[0])
                print("Segment:", segment)
                print("Strain:", strain)

    # Report on segment coverage
    print("Strain label\t" + "\t".join(SEGMENTS) + "\tCompl.")
    for strain in strain_dict.keys():
        print(limitStringLength(strain, 12, insert_dots=2), end="\t")
        compl = 0
        for segm in SEGMENTS:
            if segm in strain_dict[strain].keys():
                print("✓", end="\t")
                compl += 1
            else:
                print("✗", end="\t")
        print("%d/8" % (compl), end="\n")

    # Load reference genome
    print("Do CLUSTAL Omega")
    ref_sequences = parseFASTASequences(REF_GENOME_FILE, complement=REF_COMPLEMENT,
                                        reverse=REF_REVERSE) # get sequences
    ref_strain_dict, _ = parseFASTAHeadersFromSeqlist(ref_sequences) # map segments (if out of order)
    ref_strain_name = list(ref_strain_dict.keys())[0]
    ref_strain_dict = ref_strain_dict[ref_strain_name] # only need ref strain

    # Segment-wise multiple sequence alignment/pairwise to reference
    curr_folder = os.getcwd() + "/" # get current working folder
    for segment in SEGMENTS:
        print("-Parsing segment:", segment)

        ref_segment = ref_sequences[ref_strain_dict[segment]][1] # non-Biopython
        target_segments = []
        for strain in strain_dict.keys():
            if segment in strain_dict[strain]:
                seq = sequences[strain_dict[strain][segment]][1] # non-Biopython
                target_segments.append((strain, seq)) # (strain, sequences tuple)

        TEMP_FASTA = "intermediate/temp_in.fasta" # without "./"
        with open("./" + TEMP_FASTA, 'w') as f:
            f.write(">" + "REF:" + ref_strain_name + " (" + segment + ")" + "\n")
            f.write(ref_sequences[ref_strain_dict[segment]][1] + "\n")
            for seq in target_segments:
                f.write(">" + seq[0] + " (" + segment + ")" + "\n")
                f.write(seq[1] + "\n")


        # Call CLUSTAL OMEGA
        outfile = curr_folder + CLUSTAL_OUT_PREFIX + "_" + segment + ".fasta"
        os.chdir(CLUSTAL_BINARY_FOLDER)
        clustalomega_cline = balgn_app.ClustalOmegaCommandline(infile=curr_folder + TEMP_FASTA, outfile=outfile,
                                                               verbose=True, auto=True, force=True)
        clustalomega_cline()
        #res = subprocess.Popen(clustalomega_cline) # better practice?
        os.chdir(curr_folder)

    os.chdir(curr_folder)

