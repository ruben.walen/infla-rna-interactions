"""Analysis of interaction results .csv file over all interactions, plus potential per-strain analysis."""

import sequence_eda
import get_interaction_sites
import tools.config as config
import tools.readcsv as readcsv
from interaction_results_analysis import * # for .csv reading

import numpy as np


def mapNoneToZero(v):
    """Return 0 if v == None."""
    if v is None:
        return 0
    else:
        return v


if __name__ == "__main__":
    # Config extraction
    #CONFIG_FILE = "./input/config_various_WSNref_all.txt"
    CONFIG_FILE = "./input/config_H1N1_all.txt"
    #CONFIG_FILE = "./input/config_H5N1_WSN_ref.txt"
    #CONFIG_FILE = "./input/config_H5N1_WSN_ref_all.txt"
    PARAMS, PARAM_TYPES = config.loadConfig(CONFIG_FILE)

    INTERACTIONS_RESULTS_FILE = PARAMS["INTERACTIONS_RESULTS_FILE"]
    INTERACTIONS_AVERAGES_FILE = PARAMS["INTERACTIONS_AVERAGES_FILE"]

    # extra options
    #INTERACTIONS_STATS_SAVE_FILE = "./results/global_analysis_WSN_var1.csv"  # None for no saving
    INTERACTIONS_STATS_SAVE_FILE = "./results/global_analysis_WSN_H1N1.csv"
    #INTERACTIONS_STATS_SAVE_FILE = "./results/global_analysis_WSN_H5Nx_sel.csv"
    #INTERACTIONS_STATS_SAVE_FILE = "./results/global_analysis_WSN_H5Nx_all.csv"
    REF_STRAIN_INDEX = 0 # index of reference strain in .csv blocks, excluding first (info) block

    ref_strain_index = REF_STRAIN_INDEX + 1 # compensate info block

    # Load results .csv
    csv_read = readcsv.readCsv(INTERACTIONS_RESULTS_FILE)
    # block converters try to convert to correct type for each .csv block
    # block1: index, seg1, start1, end1, seg2, start2, end2
    # strain blocks: strain, sign. interaction found?, seq1, coverage ratio 1, seq2, coverage ratio 2,
    # cont.: interaction gapped sequence, istart1, iend1, interaction gapped sequence 2, istart2, iend2,
    # cont.: binding pattern, free energy
    block1_converter = [int, str, int, int, str, int, int]  # ATTN: final str?
    strain_block_converter = [str, strToBool, strOrNone, floatOrNone, strOrNone, floatOrNone, strOrNone,
                              intOrNone, intOrNone, strOrNone, intOrNone, intOrNone, strOrNone, floatOrNone]
    csv_interpreted = readcsv.csvBlockInterpret(csv_read, [0], block1_converter)
    num_strain_blocks = len(csv_interpreted[0]) - 1  # = number of non-block1 csv blocks = num of strains
    csv_interpreted = readcsv.csvBlockInterpret(csv_interpreted, range(1, num_strain_blocks + 1),
                                                strain_block_converter)

    # scan strains
    strains = []  # get strains from csv
    for strain_block in csv_interpreted[0][1:]:
        strains.append(strain_block[0])

    # keep measures on list of interactions; mostly over free energies
    # indexed over csv_interpreted
    interaction_measures = {"average" : [], "stdev" : [], "ref" : [], "new_strains_average" : [], "nsa_diff" : [],
                            "min_fe" : [], "min_fe_name" : [],
                            "nums" : {"interaction" : [], "no_interaction" : [], "not_covered" : []}}
    for line in csv_interpreted:
        free_energies = []
        corresponding_strains = []
        new_strains_energies = []
        interaction_measures["nums"]["interaction"].append(0) # interaction found
        interaction_measures["nums"]["no_interaction"].append(0) # sign. interaction not found
        interaction_measures["nums"]["not_covered"].append(0) # not enough genomic coverage
        for i, block in enumerate(line[1:]):
            # fe add to lists
            fe = block[13]  # can be None
            if i == REF_STRAIN_INDEX: # not offset due to line[1:]
                if fe is not None:
                    free_energies.append(fe)
                    corresponding_strains.append(block[0])
                interaction_measures["ref"].append(fe) # can be None
            else: # new strain
                if fe is not None:
                    free_energies.append(fe)
                    corresponding_strains.append(block[0])
                    new_strains_energies.append(fe)

            # sign. interaction
            if block[1] is True:
                interaction_measures["nums"]["interaction"][-1] += 1
            elif block[1] is False:
                interaction_measures["nums"]["no_interaction"][-1] += 1
            else:
                assert block[1] is None
                interaction_measures["nums"]["not_covered"][-1] += 1

        # append measures
        avg, stdev, nsa, nsa_diff = None, None, None, None
        if len(free_energies) > 0:
            avg = np.average(free_energies)
            stdev = np.std(free_energies)
        if len(new_strains_energies) > 0:
            nsa = np.average(new_strains_energies)
            if len(free_energies) > 0:
                nsa_diff = nsa - avg
        interaction_measures["average"].append(avg)
        interaction_measures["stdev"].append(stdev)
        interaction_measures["new_strains_average"].append(nsa)
        interaction_measures["nsa_diff"].append(nsa_diff)

        # minimum free energy in any strain
        min_fe, min_fe_name = None, None
        if len(free_energies) > 0:
            min_fe = 0
        for i, fe in enumerate(free_energies):
            if fe < min_fe:
                min_fe = fe
                min_fe_name = corresponding_strains[i]
            # no stream sampling, first encounter is saved
        interaction_measures["min_fe"].append(min_fe)
        interaction_measures["min_fe_name"].append(min_fe_name)

    if INTERACTIONS_STATS_SAVE_FILE:
        with open(INTERACTIONS_STATS_SAVE_FILE, 'w') as f:
            f.write("interaction,average FE,stdev,reference FE,new strains average,new strains average diff,")
            f.write(",".join(strains)) # all strains
            for i in range(len(csv_interpreted)):
                csv_line = csv_interpreted[i]
                f.write("\n")
                f.write("{} {} {}-{} {} {}-{},".format(*[csv_line[0][i] for i in range(7)]))
                f.write("{},{},{},{},{}".format(interaction_measures["average"][i],
                                                interaction_measures["stdev"][i],
                                                interaction_measures["ref"][i],
                                                interaction_measures["new_strains_average"][i],
                                                interaction_measures["nsa_diff"][i]))

                for block in csv_line[1:]:
                    fe = block[13]
                    if fe is not None:
                        if fe is not False:
                            fe_string = str(fe)
                        else:
                            fe_string = "NI"
                    else:
                        fe_string = "N/A"
                    f.write("," + fe_string)

##### experiment 1: print sorted lists of e.g. highest energy interactions, highest averages, highest increase, etc.
    MAX_RANGE = 20 # maximum number of interactions to print
    indices_range = list(range(len(csv_interpreted))) # all indices of interactions in csv_interpreted, for sorting

    # highest ref strain energy interactions
    sortlist = sorted(indices_range, key=lambda v : mapNoneToZero(interaction_measures["ref"][v]), reverse=False)
    # reverse = False because lower free energy is better

    string = "List of interactions by lowest reference strain free energy\n"
    for s in sortlist[0:MAX_RANGE]:
        tempstring = "Interaction\t{} {} {}-{} {} {}-{}\twith reference strain free energy:\t"
        string += tempstring.format(*[csv_interpreted[s][0][i] for i in range(7)])
        string += "{0:.2f}\tkcal/mol".format(interaction_measures["ref"][s])
        string += "\n"
    print(string + "\n")

    # highest average interactions
    sortlist = sorted(indices_range, key=lambda v: mapNoneToZero(interaction_measures["average"][v]), reverse=False)

    string = "List of interactions by lowest average free energy\n"
    for s in sortlist[0:MAX_RANGE]:
        tempstring = "Interaction\t{} {} {}-{} {} {}-{}\twith average free energy:\t"
        string += tempstring.format(*[csv_interpreted[s][0][i] for i in range(7)])
        string += "{0:.2f}\tkcal/mol".format(interaction_measures["average"][s])
        string += "\n"
    print(string + "\n")

    # lowest stdev interactions
    sortlist = sorted(indices_range, key=lambda v: mapNoneToZero(interaction_measures["stdev"][v]), reverse=False)

    string = "List of interactions by lowest free energy standard deviation\n"
    for s in sortlist[0:MAX_RANGE]:
        tempstring = "Interaction\t{} {} {}-{} {} {}-{}\twith stdev:\t"
        string += tempstring.format(*[csv_interpreted[s][0][i] for i in range(7)])
        string += "{0:.2f}\tkcal/mol".format(interaction_measures["stdev"][s])
        string += "\taverage free energy:\t"
        string += "{0:.2f}\tkcal/mol".format(interaction_measures["average"][s])
        string += "\n"
    print(string)

    # highest stdev interactions
    sortlist = sorted(indices_range, key=lambda v: mapNoneToZero(interaction_measures["stdev"][v]), reverse=True)

    string = "List of interactions by highest free energy standard deviation\n"
    for s in sortlist[0:MAX_RANGE]:
        tempstring = "Interaction\t{} {} {}-{} {} {}-{}\twith stdev:\t"
        string += tempstring.format(*[csv_interpreted[s][0][i] for i in range(7)])
        string += "{0:.2f}\tkcal/mol".format(interaction_measures["stdev"][s])
        string += "\taverage free energy:\t"
        string += "{0:.2f}\tkcal/mol".format(interaction_measures["average"][s])
        string += "\n"
    print(string)

    # highest average decrease of FE from ref
    sortlist = sorted(indices_range, key=lambda v: mapNoneToZero(interaction_measures["nsa_diff"][v]), reverse=False)

    string = "List of interactions by largest FE decrease from ref\n"
    for s in sortlist[0:MAX_RANGE]:
        tempstring = "Interaction\t{} {} {}-{} {} {}-{}\twith average free energy change:\t"
        string += tempstring.format(*[csv_interpreted[s][0][i] for i in range(7)])
        string += "{0:.2f}\tkcal/mol".format(interaction_measures["nsa_diff"][s])
        string += "\treference free energy:\t"
        string += "{0:.2f}\tkcal/mol".format(interaction_measures["ref"][s])
        string += "\n"
    print(string)

    # highest average INCREASE of FE from ref
    sortlist = sorted(indices_range, key=lambda v: mapNoneToZero(interaction_measures["nsa_diff"][v]), reverse=True)

    string = "List of interactions by largest FE increase from ref\n"
    for s in sortlist[0:MAX_RANGE]:
        tempstring = "Interaction\t{} {} {}-{} {} {}-{}\twith average free energy change:\t"
        string += tempstring.format(*[csv_interpreted[s][0][i] for i in range(7)])
        string += "{0:.2f}\tkcal/mol".format(interaction_measures["nsa_diff"][s])
        string += "\treference free energy:\t"
        string += "{0:.2f}\tkcal/mol".format(interaction_measures["ref"][s])
        string += "\n"
    print(string)

    # lowest any-strain free energy for all interactions
    sortlist = sorted(indices_range, key=lambda v: mapNoneToZero(interaction_measures["min_fe"][v]), reverse=False)

    string = "List of interactions by lowest interaction free energy in any predicted strain including reference\n"
    for s in sortlist[0:MAX_RANGE]:
        tempstring = "Interaction\t{} {} {}-{} {} {}-{}\twith free energy:\t"
        string += tempstring.format(*[csv_interpreted[s][0][i] for i in range(7)])
        string += "{0:.2f}\tkcal/mol".format(interaction_measures["min_fe"][s])
        string += "\tin optimal strain:\t"
        string += interaction_measures["min_fe_name"][s]
        string += "\tFE in reference:\t"
        string += "{0:.2f}\tkcal/mol".format(interaction_measures["ref"][s])
        string += "\n"
    print(string)

##### experiment 2: strain-specific reports
    for i, strain in enumerate(strains):
        strain_index = i + 1

##### experiment 3: clustering on strains based on FE

##### experiment 4: adding mutation w.r.t. refseq, and consensus building + consensus alignment scoring
