"""Recreate circos segment plots showing interactions."""

import sys
import numpy as np
import re
import math
import matplotlib.patches as patches

import sequence_eda
import get_interaction_sites

sys.path.append("./misc/pyCircos-master/")
from pycircos import *


def RGBtoHex(rgb):
    return '#%02x%02x%02x' % rgb


def hexToRGB(hex):
    h = hex.lstrip("#")
    return tuple(int(h[i:i+2], 16) for i in (0, 2, 4))


def mixRGB(*args, weights=None):
    rgb = [0, 0, 0]
    if weights is None:
        for col in args:
            rgb[0] += col[0]
            rgb[1] += col[1]
            rgb[2] += col[2]
        rgb = [c / len(args) for c in rgb]
    else:
        for i, col in enumerate(args):
            rgb[0] += weights[i] * col[0]
            rgb[1] += weights[i] * col[1]
            rgb[2] += weights[i] * col[2]
        rgb = [c / sum(weights) for c in rgb]
    return tuple([int(c) for c in rgb])


def determineCenterPosition(circle, locus):
    start, end = circle.locus_dict[locus]["positions"][0], circle.locus_dict[locus]["positions"][-1]
    center_ang = (end + start) / 2
    radius = circle.locus_dict[locus]["bottom"] + (circle.locus_dict[locus]["height"] / 2)
    return center_ang, radius


def circosDrawText(circle, x, y, text, fontdict=None):
    txt = circle.ax.text(x, y, text, horizontalalignment='center', verticalalignment='center',
                         transform=circle.ax.transData, fontdict=fontdict)
    return txt


def circosPlotInteraction(circle, segm1, start1, end1, height1, segm2, start2, end2, height2, color):
    circle.chord_plot([segm1, start1, end1, height1], [segm2, start2, end2, height2], color=color)
    return


def _circosDrawTick(circle, locus, len_position, width, height, color, at_top=True):
    # length = circle.locus_dict[locus]["length"]
    start, end = circle.locus_dict[locus]["positions"][0], circle.locus_dict[locus]["positions"][-1]
    posx = start + ((end - start) * len_position)
    posy = circle.locus_dict[locus]["bottom"]
    if at_top:
        posy += circle.locus_dict[locus]["height"]

    rect = patches.Rectangle((posx - (width / 2), posy - (height / 2)), width, height,
                             linewidth=1, edgecolor=color, facecolor=color)
    circle.ax.add_patch(rect)
    return rect


def circosDrawTicks(circle, locus, len_spacing, width, height, color, absolute_spacing=False, at_top=True):
    if len_spacing <= 0:
        raise ValueError("@circosDrawTicks: len_spacing must be larger than 0.")

    if absolute_spacing:
        rel_len_spacing = len_spacing / circle.locus_dict[locus]["length"]
    else:
        rel_len_spacing = len_spacing

    positions = []
    p = 0
    while True:
        p += rel_len_spacing
        if p + (width / 2) > 1:
            break
        positions.append(p)

    for p in positions:
        _circosDrawTick(circle, locus, p, width, height, color, at_top=True)
    return


if __name__ == "__main__":
    #INPUT_GENOME = "./input/WSN_fasta.txt"
    INPUT_GENOME = "./input/Udorn_fasta_H3N2.txt"
    #INPUT_GENOME = "./input/PR8_rep1_fasta.txt"
    #STRAIN_NAME = "A/WSN/1933[H1N1]"
    STRAIN_NAME = "A/Udorn/1972[H3N2]"
    #STRAIN_NAME = "A/Puerto Rico/8/1934[H1N1]"

    # TODO: add number of interactions filter

    SEGMENT_COLORS = {"PB2" : "#56E2FF", "PB1" : "#6B81FF", "PA" : "#F08EFF", "HA" : "#FF6073",
                      "NP" : "#FFC26D", "NA" : "#FFF64F", "MP" : "#9DFF60", "NS" : "#B7FFCB"}
    BASE_INTERACTION_COLOR = "#22AADD"
    SPECIAL_INTERACTION_COLOR = "#AA3300"
    SAVE_NAME = "./misc/circos_plot_Udorn_2"
    SAVE_FORMAT = "pdf"
    MAJOR_TICK_SPACING = 200
    MAJOR_TICK_WH = (0.001, 30)
    MINOR_TICK_SPACING = 100
    MINOR_TICK_WH = (0.0005, 15)
    TICK_COLOR = "#000000"

    #INTERACTIONS_FILE = "./input/WSN_Dadonaite_isites.txt"
    INTERACTIONS_FILE = "./input/Udorn_rep1_Dadonaite_isites.txt"
    #INTERACTIONS_FILE = "./input/PR8_rep1_Dadonaite_isites.txt"
    INTERACTIONS_FILE_FORMAT = "Dadonaite"
    USE_INDICES_FILE_FOR_SAMPLING = True
    #INDICES_FILE_INTERACTIONS = "./input/most_conserved_WSN_Udorn_PR8.txt"
    INDICES_FILE_INTERACTIONS = "./input/most_conserved_WSN_Udorn_PR8_Udornref.txt"
    #INDICES_FILE_INTERACTIONS = "./input/most_conserved_WSN_Udorn_PR8_PR8_rep1_ref.txt" # TODO: add Udorn cons
    INDICES_FILE_ARRAY_OFFSET = -1

    STR_FILTER = 2 # log max strength/inter strength or None
    SEGMENT_FILTER = None # list of segments or None
    SPECIAL_INTERACTION_INDICES = None # list of indices or None
    SAMPLED_INTERACTIONS_SPECIAL = True # mark sampled (sample file) interactions as special
    FILTER_SAMPLED_INDICES = False # do not pass non-sampled interactions

    interactions_sampled_indices = []
    if USE_INDICES_FILE_FOR_SAMPLING:
        interactions_sampled_indices = get_interaction_sites.loadIndicesFile(INDICES_FILE_INTERACTIONS,
                                                                             array_start_factor=INDICES_FILE_ARRAY_OFFSET)
    interactions_list, interactions_dict = get_interaction_sites.getInteractionSites(INTERACTIONS_FILE,
                                                                                     INTERACTIONS_FILE_FORMAT)

    rgb_base_col, rgb_special_col = hexToRGB(BASE_INTERACTION_COLOR), hexToRGB(SPECIAL_INTERACTION_COLOR)
    parse_ref = sequence_eda.parseFASTASequences(INPUT_GENOME, complement=False, reverse=False)
    segment_search_pattern = "\([A-Z0-9]+\)"  # must be in parentheses
    sequences = []  # sequence list with (segment name, fasta header, seq)
    for i, seq in enumerate(parse_ref):
        segment_search = re.search(segment_search_pattern, seq[0])
        if segment_search is None:
            print("@circos.py: failed to find segment name in:", seq[0])
        segment = segment_search.group()[1:-1]
        sequences.append([segment, seq[0], seq[1]])

    segment_circle = Gcircle()
    for seq in sequences: # segment adding
        segment_circle.add_locus(seq[0], len(seq[2]), bottom=900, height=100, facecolor=SEGMENT_COLORS[seq[0]])
    segment_circle.set_locus()
    for seq in sequences: # annotation of segments
        x, y = determineCenterPosition(segment_circle, seq[0])
        circosDrawText(segment_circle, x, y, seq[0], fontdict={'size' : 'smaller'}) # segment name
        circosDrawTicks(segment_circle, seq[0], MAJOR_TICK_SPACING, MAJOR_TICK_WH[0], MAJOR_TICK_WH[1],
                        TICK_COLOR, absolute_spacing=True, at_top=True) # major ticks
        circosDrawTicks(segment_circle, seq[0], MINOR_TICK_SPACING, MINOR_TICK_WH[0], MINOR_TICK_WH[1],
                        TICK_COLOR, absolute_spacing=True, at_top=True) # minor ticks

    max_strength = max(interactions_list, key=lambda x : x.strength).strength
    plotted_count = 0 # number of interactions to plot
    for i_ind, inter in enumerate(interactions_list):
        str = math.log(max_strength / inter.strength)

        cont = False
        if STR_FILTER is None:
            cont = True
        elif str <= STR_FILTER:
            cont = True
        if SEGMENT_FILTER is not None:
            if inter.segment1 not in SEGMENT_FILTER and inter.segment2 not in SEGMENT_FILTER:
                cont = False
        if FILTER_SAMPLED_INDICES:
            if i_ind not in interactions_sampled_indices:
                cont = False

        special = False
        if SPECIAL_INTERACTION_INDICES is not None:
            if i_ind in SPECIAL_INTERACTION_INDICES:
                special = True
        if SAMPLED_INTERACTIONS_SPECIAL:
            if i_ind in interactions_sampled_indices:
                special = True

        if cont:
            plotted_count += 1
            col = [rgb_base_col, rgb_special_col][special]
            str_color = RGBtoHex(mixRGB(col, (255, 255, 255), weights=[1, str]))

            try:
                circosPlotInteraction(segment_circle, inter.segment1, inter.start1, inter.end1, 900,
                                      inter.segment2, inter.start2, inter.end2, 900, str_color)
            except IndexError: # not pixel-perfect
                circosPlotInteraction(segment_circle, inter.segment1, inter.start1, inter.end1 - 1, 900,
                                      inter.segment2, inter.start2, inter.end2 - 1, 900, str_color)

    segment_circle.ax.set_title("Interactions for strain: " + STRAIN_NAME)
    segment_circle.save(file_name=SAVE_NAME, format=SAVE_FORMAT)
    print("Saved plot,", plotted_count, "interactions")


    """
    gcircle = Gcircle()
    gcircle.add_locus("1", 1000, bottom=900,
                      height=100)  # name, length, bottom (0<=bottom<=1000), height (0<=bottom<=1000)
    gcircle.add_locus("2", 2000, bottom=900, height=100, facecolor="#ED665D")
    gcircle.add_locus("3", 3000, bottom=900, height=100, facecolor="#6DCCDA")
    gcircle.add_locus("4", 2000, bottom=800, height=100, facecolor="#ED97CA")
    gcircle.add_locus("5", 5000, bottom=950, height=50, facecolor="#EDC948")
    gcircle.set_locus()  # Creat figure object

    data = np.random.rand(100)
    gcircle.scatter_plot("1", data, bottom=900, height=-100)

    data = np.random.rand(100)
    gcircle.line_plot("2", data, bottom=600, height=100)

    data = np.random.rand(100)
    gcircle.line_plot("2", data, bottom=600, height=100)

    data = np.random.rand(50)
    gcircle.heatmap("3", data, bottom=600, height=100)

    data = np.random.rand(50)
    gcircle.bar_plot("4", data, bottom=600, height=-200)

    data = np.random.rand(50)
    gcircle.bar_plot("4", data, bottom=600, height=200)

    gcircle.chord_plot(["4", 1000, 1100], ["1", 200, 400], bottom=400)
    gcircle.chord_plot(["5", 1000, 1100, 950], ["3", 1000, 2000, 600], color="#FF0000")
    gcircle.chord_plot(["5", 4000, 4500, 950], ["2", 500, 1000, 400], color="#F2BE2B")

    gcircle.save("./misc/gcircle")
    """
