"""Processing results from the interaction finding (process_alignments_sites.py). Mostly individual interactions."""

from Bio import AlignIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
import matplotlib.pyplot as plt
import matplotlib.cm
import matplotlib.colors

from intarna_functions import *
from process_alignments_sites import *
import sequence_eda
import tools.config as config
import tools.readcsv as readcsv
import sequence_functions as seqf
import mutation_analysis

import copy
import os


def strToBool(string):
    if string == "0" or string == "False" or string == "false":
        return False
    if string == "1" or string == "True" or string == "true":
        return True
    elif string == "None":
        return None
    else:
        raise ValueError("@strToBool: cannot convert to bool: " + string)


def strOrNone(string):
    if string == "None":
        return None
    else:
        return string


def intOrNone(string):
    if string == "None":
        return None
    else:
        return int(string)


def floatOrNone(string):
    if string == "None":
        return None
    else:
        return float(string)


def typeOrNone(string, type):
    if string == "None":
        return None
    else:
        return type(string)


def fornaFastaFromInter(strain, sequence, structure):
    """Copy-able FASTA adaptable to forna from strain, sequence & structure of interaction."""
    string = ""
    string += ">" + strain + "\n"
    string += sequence.replace("&", "") + "\n"
    string += structure.replace("&", "")
    return string


def writeAndPrint(string, save_to_file, end="\n"):
    """Print string, and write it to save_to_file if it is not None."""
    print(string)
    if save_to_file is not None:
        save_to_file.write(str(string))
        if end is not None:
            save_to_file.write(end)
    return


def makeHeatmap(data_matrix, column_labels, row_labels, color_scheme,
                write_data_in_cells=True, cell_data_text_format="{:.1e}", cell_texts=None, cell_text_color='w',
                color_vmin=None, color_vmax=None):
    """Make a matplotlib.pyplot heatmap."""

    # TODO
    # adapted: https://matplotlib.org/3.1.0/gallery/images_contours_and_fields/image_annotated_heatmap.html
    fig, ax = plt.subplots()
    cnorm = matplotlib.colors.Normalize(vmin=color_vmin, vmax=color_vmax)
    hmp = ax.imshow(data_matrix, cmap=color_scheme, norm=cnorm)

    ax.set_xticks(range(len(column_labels)))
    ax.set_yticks(range(len(row_labels)))
    ax.set_xticklabels(column_labels)
    ax.set_yticklabels(row_labels)
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    for i in range(len(column_labels)):
        for j in range(len(row_labels)):
            if write_data_in_cells:
                txt = cell_data_text_format.format(data_matrix[j, i])
            else:
                txt = cell_texts[j][i] # cell_texts cannot be array (ndarray does not accept strings)
            text = ax.text(i, j, txt, ha="center", va="center", color=cell_text_color)

    fig.tight_layout()
    return fig, ax


if __name__ == "__main__":
    # Config extraction # TODO: remove unnecessary
    CONFIG_FILE = "./input/config_H1N1.txt"
    #CONFIG_FILE = "./input/config_H1N1_irdef.txt"
    #CONFIG_FILE = "./input/config_H3N2.txt"
    #CONFIG_FILE = "./input/config_H3N2_Udorn_ref.txt"
    #CONFIG_FILE = "./input/config_various_WSNref.txt"
    #CONFIG_FILE = "./input/config_H1N1_all.txt"
    #CONFIG_FILE = "./input/config_H5N1_WSN_ref.txt"
    #CONFIG_FILE = "./input/config_various_WSNref_all.txt"
    PARAMS, PARAM_TYPES = config.loadConfig(CONFIG_FILE)

    ALIGNMENTS_LOCATION = PARAMS["ALIGNMENTS_LOCATION"]
    ALIGNMENTS_PREFIX = PARAMS["ALIGNMENTS_PREFIX"]
    SEGMENTS = PARAMS["SEGMENTS_PAS"] # TODO: replace?
    INTERACTIONS_FILE = PARAMS["INTERACTIONS_FILE"]
    INTERACTIONS_FILE_FORMAT = PARAMS["INTERACTIONS_FILE_FORMAT"]
    INTA_RNA_FOLDER = PARAMS["INTA_RNA_FOLDER"]
    INTARNA_PARAM_FILE = PARAMS["INTARNA_PARAM_FILE"]

    INDICES_FILE_INTERACTIONS = PARAMS["INDICES_FILE_INTERACTIONS"]  # for taking subset of interactions
    USE_INDICES_FILE_FOR_SAMPLING = PARAMS[
        "USE_INDICES_FILE_FOR_SAMPLING"]  # whether to use file for taking a subset of interactions
    INDICES_FILE_ARRAY_OFFSET = PARAMS[
        "INDICES_FILE_ARRAY_OFFSET"]  # start offset (currently -1 due to use of inter.index "native index" (offset by 1) later)

    REF_STRAIN = PARAMS["REF_STRAIN"]  # index of reference strain in strains
    INTERACTIONS_RESULTS_FILE = PARAMS["INTERACTIONS_RESULTS_FILE"]
    INTERACTIONS_AVERAGES_FILE = PARAMS["INTERACTIONS_AVERAGES_FILE"]

    # additional parameters for interaction experiments
    # experiments: free energy plot (global), interaction specific computations, structural imposition, (co-)variation
    DISABLE_EXPERIMENTS = [True, False, False, False, False] # disable/enable experiments 1 thru 4 (by index)
    STRAIN_INDEX = 0  # select ref strain index in block for computations; block index - 1
    LINE_INDEX = 0  # index of interaction in .csv
    LINE_INDEX_FROM_NATIVE = False # if True - map strain index to native strain index (e.g. from Dadonaite)
    # if False: line index = index in results.csv file
    SAVE_TO_FOLDER = "./results/interaction" # or None for no saving
    PLT_SHOW = False # whether to plt.show()

    plt.rcParams["figure.figsize"] = (18, 12) # figure size

    # Load results .csv
    csv_read = readcsv.readCsv(INTERACTIONS_RESULTS_FILE)
    # block converters try to convert to correct type for each .csv block
    # block1: index, seg1, start1, end1, seg2, start2, end2
    # strain blocks: strain, sign. interaction found?, seq1, coverage ratio 1, seq2, coverage ratio 2,
    # cont.: interaction gapped sequence, istart1, iend1, interaction gapped sequence 2, istart2, iend2,
    # cont.: binding pattern, free energy
    block1_converter = [int, str, int, int, str, int, int] # ATTN: final str?
    strain_block_converter = [str, strToBool, strOrNone, floatOrNone, strOrNone, floatOrNone, strOrNone,
                              intOrNone, intOrNone, strOrNone, intOrNone, intOrNone, strOrNone, floatOrNone]
    csv_interpreted = readcsv.csvBlockInterpret(csv_read, [0], block1_converter)
    num_strain_blocks = len(csv_interpreted[0]) - 1 # = number of non-block1 csv blocks = num of strains
    csv_interpreted = readcsv.csvBlockInterpret(csv_interpreted, range(1, num_strain_blocks+1), strain_block_converter)

    strains = [] # get strains from csv
    for strain_block in csv_interpreted[0][1:]:
        strains.append(strain_block[0])
    ref_strain_name = csv_interpreted[0][STRAIN_INDEX + 1][0] # first strain = refstrain
    #ref_strain_index = 0

    ##### experiment 1: free energy changes per interaction (plotting)
    if not DISABLE_EXPERIMENTS[0]:
        reference_energies = readcsv.extractColumn(csv_interpreted, 1, -1) # last column of first strain block
        free_energy_per_strain_per_inter = {strain : [] for strain in strains} # dict of interaction energies (list)
        interaction_indices = [] # interaction indices list
        for line in csv_interpreted:
            interaction_indices.append(line[0][0])
            for strain_block in line[1:]:
                strain = strain_block[0]
                energy = strain_block[-1]
                free_energy_per_strain_per_inter[strain].append(energy)

        fig1, ax1 = plt.subplots()
        fig2, ax2 = plt.subplots()
        cmap = matplotlib.cm.get_cmap('nipy_spectral')
        for strain_i, strain in enumerate(strains):
            x, y = [], []  # x = interaction index, y = free energy
            x2, y2 = [], []  # for another plot: y2 = free energy diff w.r.t. reference
            for i, fe in enumerate(free_energy_per_strain_per_inter[strain]):
                if fe is not None:
                    x.append(i)
                    y.append(fe)
                    if reference_energies[i] is not None and strain != ref_strain_name:
                        x2.append(i)
                        y2.append(fe - reference_energies[i])  # FE difference
            if strain != ref_strain_name:
                legend_name = strain
            else:
                legend_name = "REF: " + strain
            color = cmap(strain_i / len(strains))  # RGBA color
            ax1.plot(x, y, label=legend_name, color=color, linestyle='', marker='.', markersize=9)
            if strain != ref_strain_name:  # otherwise no sense in plotting
                ax2.plot(x2, y2, label=legend_name, color=color, linestyle='', marker='.', markersize=9)

        ax1.set_title("Free energies for (sampled) RNA-RNA interactions for different strains")
        ax1.set_xlabel("Interaction index")
        ax1.set_ylabel("Free energy (kcal/mol)")
        ax1.grid()
        ax1.legend()

        ax2.set_title("Free energy differences for (sampled) RNA-RNA interactions for different strains" +
                      " with respect to reference strain: " + strains[STRAIN_INDEX])
        ax2.set_xlabel("Interaction index")
        ax2.set_ylabel("Free energy difference (kcal/mol)")
        ax2.axhline(y=0, ls='dashed')  # horizontal line
        ax2.legend()
        ax2.grid()
        plt.show()

##### experiment 2: information about specific interaction
    # first load line and strain
    if LINE_INDEX_FROM_NATIVE: # get 'native' line indices and look for LINE_INDEX
        line_index_dict = {}
        for i, l in enumerate(csv_interpreted):
            native = int(l[0][0]) # just to make sure
            line_index_dict[native] = i
        line_index = line_index_dict.get(LINE_INDEX, None)
        print("Remapping index", LINE_INDEX, "to in-file", line_index)
        if line_index is None:
            raise KeyError("@interaction_results_analysis.py: line index "
                           + str(LINE_INDEX) + " not found in results.csv file!")
        del line_index_dict
    else:
        line_index = LINE_INDEX
    strain_index = STRAIN_INDEX + 1
    line = csv_interpreted[line_index]

    ###
    #SAVE_TO_FOLDER = "./results/H3N2_WSN_" + str(line[0][0]) + "_ "+ line[0][1] + "_" + line[0][4] # folder save hack
    ###
    if SAVE_TO_FOLDER is not None:
        if not os.path.isdir(SAVE_TO_FOLDER):
            os.mkdir(SAVE_TO_FOLDER)

    # - some structure computations
    if not DISABLE_EXPERIMENTS[1]:
        print(line)
        seq1, seq2, iseq1, iseq2, binding = line[strain_index][2], line[strain_index][4], line[strain_index][6],\
                                            line[strain_index][9], line[strain_index][12]
        iseq1_ref_ext, iseq2_ref_ext, ebp_ref = extendBindingPattern(seq1, line[strain_index][7],
                                                                     line[strain_index][8], iseq1,
                                                                     seq2, line[strain_index][10],
                                                                     line[strain_index][11], iseq2, binding)
        cut_bp_ref = cutBindingPatternAtGappedSites(iseq1_ref_ext, iseq2_ref_ext, ebp_ref)
        recov_seq1_ref, struct_per_seq1_ref, \
            recov_seq2_ref, struct_per_seq2_ref = extractBindingPatternForSequences(iseq1_ref_ext, iseq2_ref_ext, ebp_ref)

        reverse_1, reverse_2 = False, False
        if line[strain_index][7] > line[strain_index][8]: # sequence 1 binding form reversed from stored genome
            reverse_1 = True
        if line[strain_index][10] > line[strain_index][11]:
            reverse_2 = True

        print("Ref strain: " + line[strain_index][0])

        # all strains
        fe_base = line[strain_index][-1]
        bracket_ref = getBracketNotation(iseq1, iseq2, binding, include_weak=False)
        seqs1, seqs2, structs = [], [], []
        structs_per_seq1, structs_per_seq2 = [], []
        iseqs1_mut_ext, iseqs2_mut_ext = [], []
        ext_seq1_dict, ext_seq2_dict, ext_bp_dict = {}, {}, {} # dicted by strain name
        forna_copylist = ""
        for si in range(1, num_strain_blocks + 1): # main gathering loop
            print("\n#################\nStrain:", line[si][0])

            if line[si][1]: # sign interaction found
                seq1_mut, seq2_mut = line[si][2].replace("-", "d"), line[si][4].replace("-", "d") # replace gap with "d" char
                # TODO: evaluate effects of this "d" character substitution
                iseq1_mut, iseq2_mut, binding_mut = line[si][6], line[si][9], line[si][12]

                print("Free energy:", str(line[si][-1]))
                print("Free energy difference (ref):", str(line[si][-1] - fe_base), "\n")

                print("\nInteraction sites")
                #print(getBracketNotation(iseq1, iseq2, binding, include_weak=True))
                # is this even necessary?
                _, _, nogap_bp_mut = cutBindingPatternAtGappedSites(iseq1_mut, iseq2_mut,
                                                                    binding_mut, remove_bonds_only=True)
                bracket_mutated = getBracketNotation(iseq1_mut, iseq2_mut, nogap_bp_mut, include_weak=False)
                print(bracket_mutated[2])  # mutation
                print(bracket_mutated[3])

                #print("\nforna copy-able:")
                #print(fornaFastaFromInter(line[si][0], bracket_mutated[2], bracket_mutated[3]))
                forna_fasta_line = line[si][0] + " " + str(line[si][7]) + "-" + str(line[si][8]) \
                    + " " + str(line[si][10]) + "-" + str(line[si][11])
                forna_copylist += fornaFastaFromInter(forna_fasta_line, bracket_mutated[2], bracket_mutated[3]) + "\n"

                print("\nMutations")
                print(seq1 + "&" + seq2)
                print(seq1_mut + "&" + seq2_mut)
                print(mutation_analysis.mutationString(seq1, seq1_mut) + "&" +
                      mutation_analysis.mutationString(seq2, seq2_mut))

                iseq1_mut_ext, iseq2_mut_ext, ebp_mut = extendBindingPattern(seq1_mut, line[si][7], line[si][8],
                                                                             iseq1_mut, seq2_mut, line[si][10],
                                                                             line[si][11], iseq2_mut, binding_mut)
                #ebp_cut_mut = cutBindingPatternAtGappedSites(iseq1_mut_ext, iseq2_mut_ext, ebp_mut)
                _, struct_per_seq1, _, struct_per_seq2 = extractBindingPatternForSequences(iseq1_mut_ext, iseq2_mut_ext, ebp_mut)

                print("\nBinding")
                print(iseq1_mut_ext)
                print(ebp_mut)
                print(iseq2_mut_ext)

                seqs1.append(seq1_mut.replace("T", "U"))
                seqs2.append(seq2_mut.replace("T", "U"))
                iseqs1_mut_ext.append(iseq1_mut_ext.replace("T", "U"))
                iseqs2_mut_ext.append(iseq2_mut_ext.replace("T", "U"))
                structs.append(bindingPatternVisualiseNoInter(ebp_mut))
                structs_per_seq1.append(struct_per_seq1)
                structs_per_seq2.append(struct_per_seq2)
                ext_seq1_dict[line[si][0]] = iseq1_mut_ext
                ext_bp_dict[line[si][0]] = ebp_mut
                ext_seq2_dict[line[si][0]] = iseq2_mut_ext
            else: # if no interaction is found: append sequence and no structure, or append no sequence, no structure
                print("\nNo significant interaction found.")
                if line[si][1] is False:
                    seqs1.append(seq1_mut.replace("T", "U"))
                    seqs2.append(seq2_mut.replace("T", "U"))
                else:
                    seqs1.append("")
                    seqs2.append("")
                iseqs1_mut_ext.append("")
                iseqs2_mut_ext.append("")
                structs.append("")
                structs_per_seq1.append("") # else run into trouble later
                structs_per_seq2.append("")
                ext_seq1_dict[line[si][0]] = None
                ext_bp_dict[line[si][0]] = None
                ext_seq2_dict[line[si][0]] = None

            print("\n")

        # save extended structures to file
        if SAVE_TO_FOLDER is not None:
            structsfile = open(SAVE_TO_FOLDER + "/structs.txt", 'w')
            for k in ext_seq1_dict.keys():
                structsfile.write(">" + k)
                if ext_seq1_dict[k] is not None and ext_bp_dict[k] is not None and ext_seq2_dict[k] is not None: # TODO: make better
                    structsfile.write("\n" + seqf.toRNA(ext_seq1_dict[k]) + "\n" + seqf.toRNA(ext_bp_dict[k]) +
                                      "\n" + seqf.toRNA(ext_seq2_dict[k]) + "\n\n")
                else:
                    structsfile.write("\nNo sign. structure\n\n")
            structsfile.close()

        # - FORNA COPYLIST & BLOCKS
        inter_info = csv_interpreted[line_index][0]
        print("Interaction:", str(inter_info[0]), str(inter_info[1]), str(inter_info[2]) + "-" + str(inter_info[3]),
              str(inter_info[4]), str(inter_info[5]) + "-" + str(inter_info[6]))
        print("Strains found:\n" + "\n".join(strains))
        print("\nforna copy-able:")
        print(forna_copylist)

        if SAVE_TO_FOLDER is not None:
            blocksfile = open(SAVE_TO_FOLDER + "/blocks.txt", 'w')
            blocksfile.write(str(line[0][0]) + " " + line[0][1] + " " + line[0][4] + "\n")
            blocksfile.write("\nStrains found:\n" + "\n".join(strains))
            blocksfile.write("\n" + "\nforna copy-able:\n" + forna_copylist)
            blocksfile.write("\n")

        # - mutation block computation
        NUM_CHARS_STRAIN_NAMES = 15  # shortening of strain names
        print("\nMutation block")

        string = line[strain_index][0] + " " + str(line[0][0]) + " " + line[0][1] + " " + \
                 str(line[0][2]) + "-" + str(line[0][3]) + " " + line[0][4] + " " + \
                 str(line[0][5]) + "-" + str(line[0][6])
        print(string)
        if SAVE_TO_FOLDER is not None:
            blocksfile.write("Mutation block\n")
            blocksfile.write(string)

        seq1_ref, seq2_ref = seq1, seq2 # reversal of ref sequence?
        rev1, rev2 = False, False
        if line[strain_index][7] > line[strain_index][8]:
            rev1 = True
            seq1_ref = seq1[::-1]
        if line[strain_index][10] > line[strain_index][11]:
            rev2 = True
            seq2_ref = seq2[::-1]

        chr1_1, chr1_2, chr2_1, chr2_2 = "v", "$", "v", "$"
        if rev1: # mark beginning, end of genome segment given reversal
            chr1_1, chr1_2 = "$", "v"
        if rev2:
            chr2_1, chr2_2 = "$", "v"

        string = "v = genomic start position; $ = genomic end position\n"
        string += "".join([" " for _ in range(NUM_CHARS_STRAIN_NAMES)]) + "\t" + chr1_1 + \
                  "".join([" " for _ in range(len(seq1_ref) - 2)]) + chr1_2 + \
                  " " + chr2_1 + "".join([" " for _ in range(len(seq2_ref) - 2)]) + chr2_2 + "\n" # place marks
        string += sequence_eda.limitStringLength(line[strain_index][0], NUM_CHARS_STRAIN_NAMES, insert_dots=2) + \
                  "\t" + seq1_ref + "&" + seq2_ref # print reference sequence
        print(string)
        if SAVE_TO_FOLDER is not None:
            blocksfile.write("\n")
            blocksfile.write(string)

        for si in range(1, num_strain_blocks + 1):
            string = sequence_eda.limitStringLength(line[si][0], NUM_CHARS_STRAIN_NAMES, insert_dots=2) + "\t" # shortened strain name
            seq1_mut, seq2_mut = line[si][2], line[si][4]
            rev1_mut, rev2_mut = False, False # reverse sequences due to binding site reversal?
            if line[si][1] is not None:
                if line[si][1] is not False: # else there is no sequence for potential reversal
                    if line[si][7] > line[si][8]:
                        rev1_mut = True
                    if line[si][10] > line[si][11]:
                        rev2_mut = True
                else:
                    rev1_mut, rev2_mut = rev1, rev2

                if rev1_mut:
                    seq1_mut = seq1_mut[::-1]
                if rev2_mut:
                    seq2_mut = seq2_mut[::-1]

                for i, c in enumerate(seq1_mut):
                    if c != seq1_ref[i]:
                        string += c
                    else:
                        string += " "
                string += "&"
                for i, c in enumerate(seq2_mut):
                    if c != seq2_ref[i]:
                        string += c
                    else:
                        string += " "

                if rev1 != rev1_mut: # in case of strain binding pattern strand reversal w.r.t. reference
                    string += "\tSTRAND 1 REVERSAL MISMATCH" # NOTE: this is probably useless given how intaRNA works
                if rev2 != rev2_mut:
                    string += "\tSTRAND 2 REVERSAL MISMATCH"
            else:
                string += "".join(["x" for _ in range(len(seq1_ref))]) + \
                          "&" + "".join(["x" for _ in range(len(seq2_ref))])
            print(string)
            if SAVE_TO_FOLDER is not None:
                blocksfile.write("\n")
                blocksfile.write(string)

        # - computation of structure blocks
        string = "\nPer-nucleotide structure block\n"
        struct_per_seq1_ref_vis = bindingPatternVisualiseNoInter(struct_per_seq1_ref)
        struct_per_seq2_ref_vis = bindingPatternVisualiseNoInter(struct_per_seq2_ref)
        string += sequence_eda.limitStringLength(line[strain_index][0], NUM_CHARS_STRAIN_NAMES, insert_dots=2) + \
                  "\t" + seqf.toRNA(recov_seq1_ref) + "&" + seqf.toRNA(recov_seq2_ref) + "\n"
        string += sequence_eda.limitStringLength(line[strain_index][0], NUM_CHARS_STRAIN_NAMES, insert_dots=2) + \
                  "\t" + struct_per_seq1_ref_vis + "&" + struct_per_seq2_ref_vis
        print(string)
        if SAVE_TO_FOLDER is not None:
            blocksfile.write("\n")
            blocksfile.write(string)

        for si in range(1, num_strain_blocks + 1):
            string = sequence_eda.limitStringLength(line[si][0], NUM_CHARS_STRAIN_NAMES, insert_dots=2) + "\t"
            seq1_mut, seq2_mut = line[si][2], line[si][4]
            seq1_nt_mut = iseqs1_mut_ext[si - 1]
            seq2_nt_mut = iseqs2_mut_ext[si - 1]
            struct1_nt_mut = bindingPatternVisualiseNoInter(structs_per_seq1[si - 1])
            struct2_nt_mut = bindingPatternVisualiseNoInter(structs_per_seq2[si - 1])
            if line[si][1] is not None and line[si][1] is not False:
                for i, c in enumerate(struct1_nt_mut):
                    if c != struct_per_seq1_ref_vis[i]:
                        string += c
                    else:
                        string += " "
                string += "&"
                for i, c in enumerate(struct2_nt_mut):
                    if c != struct_per_seq2_ref_vis[i]:
                        string += c
                    else:
                        string += " "
                string += "\t{0:.2f}".format(line[si][13]) + " kcal/mol" # free energy
            else:
                string += "".join(["x" for _ in range(len(struct_per_seq1_ref))]) + "&" + \
                          "".join(["x" for _ in range(len(struct_per_seq2_ref))])
                string += "\tN/A" # free energy not applicable

            print(string)
            if SAVE_TO_FOLDER is not None:
                blocksfile.write("\n")
                blocksfile.write(string)

        if SAVE_TO_FOLDER is not None:
            blocksfile.close()

        # - write some basic information to file (information table)
        print("\nWriting table.txt")
        if SAVE_TO_FOLDER is not None:
            tablefile = open(SAVE_TO_FOLDER + "/table.txt", 'w')
            span = (abs(line[0][2] - line[0][3]), abs(line[0][5] - line[0][6]))
            tablefile.write(str(line[0][0]) + " " + line[0][1] + " " + str(line[0][2]) + "-" + str(line[0][3]) +
                            " ({})".format(span[0]) + " " + line[0][4] + " " + str(line[0][5]) + "-" + str(line[0][6])
                            + " ({})".format(span[1]))
            tablefile.write("\nReference strain: " + line[STRAIN_INDEX + 1][0])
            tablefile.write("\nStrain\tFE/interaction found\tSeg. 1 span\tSeg. 2 span\tFull bonds/half bonds/no bonds")
            tablefile.write("\tMutations in whole site seg.1 / seg. 2")
            for si in range(1, num_strain_blocks + 1): # strains
                tablefile.write("\n" + line[si][0] + "\t") # strain name
                tablefile.write(["Incomplete sequence",
                                 ["No sign. interaction found",
                                  str(line[si][13])][line[si][1] == True]][line[si][1] is not None]) # FE or interaction msg
                if line[si][1] is True:
                    span = (abs(line[si][7] - line[si][8]), abs(line[si][10] - line[si][11]))
                    # span, seg. 1 & seg. 2
                    tablefile.write("\t" + str(line[si][7]) + "-" + str(line[si][8]) + " ({})".format(span[0]))
                    tablefile.write("\t" + str(line[si][10]) + "-" + str(line[si][11]) + " ({})".format(span[1]))
                    binding_count = (line[si][12].count("|"), line[si][12].count(":"), line[si][12].count(" "))
                    tablefile.write("\t" + "/".join([str(v) for v in binding_count])) # number of bonds in-site
                    hamming = (seqf.multiHammingDistance(line[si][2], line[STRAIN_INDEX + 1][2], normalize=False),
                               seqf.multiHammingDistance(line[si][4], line[STRAIN_INDEX + 1][4], normalize=False))
                    tablefile.write("\t" + "/".join([str(v) for v in hamming])) # hamming distance vs. ref strain
                else:
                    tablefile.write("\tN/A\tN/A\tN/A") # three N/A fields
                    if line[si][1] is not None: # sequence available
                        hamming = (seqf.multiHammingDistance(line[si][2], line[STRAIN_INDEX + 1][2], normalize=False),
                                   seqf.multiHammingDistance(line[si][4], line[STRAIN_INDEX + 1][4], normalize=False))
                    else:
                        hamming = ("N/A", "N/A")
                    tablefile.write("\t" + "/".join([str(v) for v in hamming])) # hamming distance should be fine
            tablefile.close()

        # - consensus in sequences and structure
        print("\nConsensus blocks")
        seqs1_cs = seqf.ConsensusSequence(seqs1, alphabet=["A", "C", "U", "G", "d"], enforce_alphabet=True)
        seqs2_cs = seqf.ConsensusSequence(seqs2, alphabet=["A", "C", "U", "G", "d"], enforce_alphabet=True)
        structs_cs = seqf.ConsensusSequence(structs, alphabet=["|", ":", "."], enforce_alphabet=True)
        print(seqs1_cs)
        print(seqs2_cs)
        print(structs_cs)

        structs_seq1_cs = seqf.ConsensusSequence([bindingPatternVisualiseNoInter(s) for s in structs_per_seq1],
                                                 alphabet=["|", ":", "."], enforce_alphabet=True)
        structs_seq2_cs = seqf.ConsensusSequence([bindingPatternVisualiseNoInter(s) for s in structs_per_seq2],
                                                 alphabet=["|", ":", "."], enforce_alphabet=True)

        fig1, ax1 = plt.subplots()
        fig2, ax2 = plt.subplots()
        fig3, ax3 = plt.subplots()
        fig1.subplots_adjust(bottom=0.33, top=0.63)
        fig2.subplots_adjust(bottom=0.33, top=0.63)
        fig3.subplots_adjust(bottom=0.33, top=0.63)

        CONSENSUS_METHOD = "differential"
        seq1_cons = seqs1_cs.consensusPerPosition(method=CONSENSUS_METHOD)
        seq2_cons = seqs2_cs.consensusPerPosition(method=CONSENSUS_METHOD)
        struct_cons = structs_cs.consensusPerPosition(method=CONSENSUS_METHOD)
        structs_seq1_cons = structs_seq1_cs.consensusPerPosition(method=CONSENSUS_METHOD)
        structs_seq2_cons = structs_seq2_cs.consensusPerPosition(method=CONSENSUS_METHOD)

        # - making consensus plots
        # -- plot 1: sequence consensus
        xticks1 = []
        # maximal walks on sequences for comparison
        seq1_cons_mw = seqs1_cs.maximalWalk(return_walk_probability=False, randomize_on_multiple_maxima=False)
        seq2_cons_mw = seqs2_cs.maximalWalk(return_walk_probability=False, randomize_on_multiple_maxima=False)
        isite1_left = [line[strain_index][7],
                       len(seq1) - line[strain_index][7] + 1][reverse_1] # interaction site seq1 start left
        isite1_right = [line[strain_index][8],
                        len(seq1) - line[strain_index][8] + 1][reverse_1]
        isite2_left = [line[strain_index][10],
                       len(seq2) - line[strain_index][10] + 1][reverse_2]
        isite2_right = [line[strain_index][11],
                        len(seq2) - line[strain_index][11] + 1][reverse_2]
        # determine xticks
        for i, x in enumerate([seq1, seq1[::-1]][reverse_1].replace("T", "U")):
            xticks1.append([" ", str(i + 1), "@", "@", " "]) # debug symbol @
            xticks1[-1][2] = x
            xticks1[-1][3] = " "
        for i, x in enumerate([seq2, seq2[::-1]][reverse_2].replace("T", "U")):
            if i < len(xticks1):
                xticks1[i][3] = x
            else:
                xticks1.append(["@", str(i + 1), "@", "@", "@"])
                xticks1[-1][2] = " "
                xticks1[-1][3] = x
        for i, c in enumerate(seq1_cons_mw): # check maximal walk for seq. 1
            symbol = " "
            if c != xticks1[i][2] and xticks1[i][2] != " ": # second check should not be necessary
                symbol = "*" # maximum != ref strain
            xticks1[i][0] = symbol
        for i, c in enumerate(seq2_cons_mw): # check maximal walk for seq. 2
            symbol = " "
            if c != xticks1[i][3] and xticks1[i][3] != " ":
                symbol = "*" # maximum != ref strain
            xticks1[i][4] = symbol
        for i, x in enumerate(xticks1):
            xticks1[i] = "\n".join(x) # newline join

        # potential issue with xticks indices for differing length sequences --> resolved?

        ax1.plot(range(1, len(seq1_cons)+1), [seq1_cons, seq1_cons[::-1]][reverse_1], label="Sequence 1")
        ax1.plot(range(1, len(seq2_cons)+1), [seq2_cons, seq2_cons[::-1]][reverse_2], label="Sequence 2")
        ax1.set_title("Sequence consensus plot for interaction: " + str(line[0][0]) + " " + line[0][1] + " " + line[0][4])
        #ax1.set_xlabel("Position")
        ax1.set_ylabel("Consensus score")
        ax1.legend()
        ax1.set_xticks(range(1, len(xticks1)+1))
        ax1.set_xticklabels(xticks1)
        ax1.axvline(x=isite1_left, ls='dashed', color='tab:blue') # seq1 isite left (start)
        ax1.axvline(x=isite1_right, ls='dashed', color='tab:blue')
        ax1.axvline(x=isite2_left, ls='dashed', color='tab:orange')
        ax1.axvline(x=isite2_right, ls='dashed', color='tab:orange')
        ax1.axvspan(isite1_left, isite1_right, alpha=0.2, color='tab:blue')
        ax1.axvspan(isite2_left, isite2_right, alpha=0.2, color='tab:orange')
        ax1.grid()

        # iseq1_ref_ext, iseq2_ref_ext, ebp_ref or cut_bp_ref?
        # -- plot 2: structural consensus
        xticks2 = []
        struct_cons_mw = structs_cs.maximalWalk(return_walk_probability=False,
                                                randomize_on_multiple_maxima=False) # structure maximal walk
        for i, x in enumerate(iseq1_ref_ext.replace("T", "U")):
            xticks2.append([" ", "@", "@", "@"])  # debug symbol @
            xticks2[-1][1] = x
            xticks2[-1][2] = " " # init
            xticks2[-1][3] = " " # init
        for i, x in enumerate(ebp_ref):
            if i < len(xticks2):
                xticks2[i][2] = x
            else:
                xticks2.append([" ", "@", "@", "@"])
                xticks2[-1][1] = " " # cannot ever bond
                xticks2[-1][2] = x
                xticks2[-1][3] = " " # init seq 2
        for i, x in enumerate(iseq2_ref_ext.replace("T", "U")):
            if i < len(xticks2):
                xticks2[i][3] = x
            else:
                xticks2.append([" ", "@", "@", "@"])
                xticks2[-1][1] = " "
                xticks2[-1][2] = " " # cannot ever bond
                xticks2[-1][3] = x
        if len(xticks2) < len(struct_cons): # line overrun
            for _ in range(len(struct_cons) - len(xticks2)):
                xticks2.append([" ", "@", "@", "@"])
                xticks2[-1][1] = "X"
                xticks2[-1][2] = " "
                xticks2[-1][3] = "X"
        for i, c in enumerate(struct_cons_mw): # check maximal walk for structure
            symbol = " "
            if c != xticks2[i][2].replace(" ", "."): # " " visualised to dot in consensus structure
                symbol = "*" # maximum != ref strain
            xticks2[i][0] = symbol
        for i, x in enumerate(xticks2):
            xticks2[i] = "\n".join(x) # newline join

        ax2.plot(range(1, len(struct_cons) + 1), struct_cons, label="Structure")
        ax2.set_title("Structural consensus plot for interaction: " + str(line[0][0]) + " " + line[0][1] + " " + line[0][4])
        #ax2.set_xlabel("Position")
        ax2.set_ylabel("Consensus score")
        ax2.grid()
        ax2.set_xticks(range(1, len(xticks2)+1))
        ax2.set_xticklabels(xticks2)

        # -- plot 3: per-nucleotide sequence consensus
        xticks3 = []
        # consensus maximal walks for per-nucleotide structures
        struct1_cons_mw = structs_seq1_cs.maximalWalk(return_walk_probability=False, randomize_on_multiple_maxima=False)
        struct2_cons_mw = structs_seq2_cs.maximalWalk(return_walk_probability=False, randomize_on_multiple_maxima=False)
        for i, x in enumerate(struct_per_seq1_ref): # pn structure 1
            xticks3.append([" ", "@", "@", "@", "@", " "])
            xticks3[-1][1] = x
            xticks3[-1][2], xticks3[-1][3], xticks3[-1][4] = " ", " ", " "
        for i, x in enumerate(recov_seq1_ref.replace("T", "U")): # pn sequence 1
            if i < len(xticks2):
                xticks3[i][2] = x
            else:
                xticks3.append([" ", "@", "@", "@", "@", " "])
                xticks3[-1][2] = x # this should not be possible with [1] = " "
                xticks3[-1][1], xticks3[-1][3], xticks3[-1][4] = " ", " ", " "
        for i, x in enumerate(recov_seq2_ref.replace("T", "U")): # pn sequence 2
            if i < len(xticks3):
                xticks3[i][3] = x
            else:
                xticks3.append([" ", "@", "@", "@", "@", " "])
                xticks3[-1][3] = x
                xticks3[-1][1], xticks3[-1][2], xticks3[-1][4] = " ", " ", " "
        for i, x in enumerate(struct_per_seq2_ref): # pn structure 2
            if i < len(xticks3):
                xticks3[i][4] = x
            else:
                xticks3.append([" ", "@", "@", "@", "@", " "])
                xticks3[-1][4] = x
                xticks3[-1][1], xticks3[-1][2], xticks3[-1][3] = " ", " ", " "
        if len(xticks3) < max(len(structs_seq1_cons), len(structs_seq2_cons)): # line overrun
            for _ in range(max(len(structs_seq1_cons), len(structs_seq2_cons)) - len(xticks2)):
                xticks3.append([" ", "@", "@", "@", "@", " "])
                xticks3[-1][1], xticks3[-1][2], xticks3[-1][3], xticks3[-1][4] = " ", "X", "X", " "
        for i, c in enumerate(struct1_cons_mw): # check maximal walk for pn structure 1
            symbol = " "
            if c != xticks3[i][1].replace(" ", "."):
                symbol = "*" # maximum != ref strain
            xticks3[i][0] = symbol
        for i, c in enumerate(struct2_cons_mw): # check maximal walk for pn structure 2
            symbol = " "
            if c != xticks3[i][4].replace(" ", "."):
                symbol = "*" # maximum != ref strain
            xticks3[i][5] = symbol
        for i, x in enumerate(xticks3):
            xticks3[i] = "\n".join(x) # newline join

        ax3.plot(range(1, len(structs_seq1_cons) + 1), structs_seq1_cons, label="Seq. 1 Structure")
        ax3.plot(range(1, len(structs_seq2_cons) + 1), structs_seq2_cons, label="Seq. 2 Structure")
        ax3.set_title("Per-nucleotide structural consensus plot for interaction: " + str(line[0][0]) + " " + line[0][1] + " " + line[0][4])
        # ax2.set_xlabel("Position")
        #ax3.legend()
        ax3.set_ylabel("Consensus score")
        ax3.grid()
        ax3.set_xticks(range(1, len(xticks3) + 1))
        ax3.set_xticklabels(xticks3)

        if SAVE_TO_FOLDER is not None:
            fig1.savefig(SAVE_TO_FOLDER + "/seqc.png")
            fig2.savefig(SAVE_TO_FOLDER + "/sc.png")
            fig3.savefig(SAVE_TO_FOLDER + "/pnsc.png")
        if PLT_SHOW:
            plt.show()

##### experiment 3: structural imposition experiment: impone structures from one seq. to other seqs, measure FE
    if not DISABLE_EXPERIMENTS[2]:
        import vienna_rna_functions as vienna
        import numpy as np
        from matplotlib.patches import Rectangle

        # FIX: deal with gapped interaction sites --> done?

        # index of interaction (line): moved to before previous exp.
        MIDDLE_INSERTION = "GGGGG" # middle insert to reduce strain glitch
        VIENNA_RNA_FOLDER = "C:/Program Files (x86)/ViennaRNA Package/"
        TEMP_FILE = "./intermediate/temp_workable.txt"
        RECOMPATIBILIZE_INTERACTIONS = True # rescan binding pattern within structure seed for optimization
        INCLUDE_GU_BASEPAIR = True # include G-U global base pair
        STRAIN_IGNORE_LIST = ["A/Bayern/7/1995"] # ignore these strains for computation
        PLOT_COLOR_VMIN, PLOT_COLOR_VMAX = -40, 20 # heatmap color range
        COVERAGE_LIMIT = 0.40 # site nucleotide coverage required for inclusion

        # load line again/first time
        line = csv_interpreted[line_index]
        print("\nStructural imposition experiment\n", line)
        sign_interaction = {}
        imposition_free_energy_dict = {}
        isites_substitutions_dict = {}
        nucleotides_found = {} # strains for which sufficient nucleotides exist to impose structure
        legal_bonds = RNA_LEGAL_BONDS_STRICT
        if INCLUDE_GU_BASEPAIR:
            legal_bonds.update(RNA_GU_BP)

        # main loop: site structure vs. new nucleotides
        for si1, block in enumerate(line[1:]): # uses line_index line above
            strain = block[0]
            imposition_free_energy_dict[strain] = {}
            isites_substitutions_dict[strain] = {}
            if block[1] is True and strain not in STRAIN_IGNORE_LIST:
                sign_interaction[strain] = True
                seq1, seq2 = block[2], block[4]
                iseq1, iseq2, binding_pattern = block[6], block[9], block[12]
                i1l, i1u, i2l, i2u = block[7], block[8], block[10], block[11] # bounds of interaction sites

                for si2, block2 in enumerate(line[1:]): # impose unto this (1: structure --> 2: sequence)
                    strain2 = block2[0]

                    # ATTN: if no interaction is found, nucleotides could still be substituted in as donor sequence
                    # calculate interaction site coverage
                    nuc_coverage = 0
                    if block2[3] is not None:
                        nuc_coverage += block2[3]
                    if block2[5] is not None:
                        nuc_coverage += block2[5]
                    nuc_coverage /= 2

                    # if block2[1] is True and strain2 not in STRAIN_IGNORE_LIST:
                    if strain2 not in STRAIN_IGNORE_LIST and nuc_coverage >= COVERAGE_LIMIT:
                        nucleotides_found[strain2] = True
                        seq1_2, seq2_2 = block2[2], block2[4] # sites loading
                        if i1l <= i1u:
                            l, r = i1l - 1, i1u
                            site1 = seq1_2[l:r]
                        else:
                            l, r = i1u - 1, i1l
                            site1 = seq1_2[l:r]
                            site1 = site1[::-1]

                        if i2l <= i2u:
                            l, r = i2l - 1, i2u
                            site2 = seq2_2[l:r]
                        else:
                            l, r = i2u - 1, i2l
                            site2 = seq2_2[l:r]
                            site2 = site2[::-1]

                        # ATTN: iseq1, iseq2, iseq1_new, iseq2_new will have "T" replaced with "U"
                        iseq1, iseq2 = seqf.toRNA(iseq1, iseq2)

                        iseq1_new, iseq2_new, _ = imposeStructure(iseq1, iseq2, binding_pattern, site1, site2)
                        iseq1_new, iseq2_new = seqf.toRNA(iseq1_new, iseq2_new) # T --> U

                        if RECOMPATIBILIZE_INTERACTIONS:
                            scrub_bp = reoptimizeBinding(iseq1_new, iseq2_new, legal=legal_bonds, weak={})
                        else:
                            scrub_bp = scrubImpossibleBindingNucleotides(iseq1_new, iseq2_new, binding_pattern,
                                                                         legal=legal_bonds)

                        # include_weak = True: scrub 'weak' binding before bracketing with INCLUDE_GU_BASEPAIR
                        _, _, bracket_seq, bracket_pattern = getBracketNotation(iseq1_new, iseq2_new, scrub_bp,
                                                                                include_weak=True,
                                                                                ampersand_split=False,
                                                                                middle_insert=MIDDLE_INSERTION)

                        # call viennaRNA for evaluation of folded structure: NOTE: not same as intaRNA!
                        rna_eval = vienna.callRNAevalWithTempFile(VIENNA_RNA_FOLDER, TEMP_FILE, bracket_seq, bracket_pattern)
                        eval_values = vienna.processRNAevalOutput(rna_eval)[0]
                        fe = eval_values[0][2]
                        imposition_free_energy_dict[strain][strain2] = fe

                        # mutation analysis
                        mut1 = mutation_analysis.mutationStringCount(mutation_analysis.mutationString(iseq1, iseq1_new))
                        mut2 = mutation_analysis.mutationStringCount(mutation_analysis.mutationString(iseq2, iseq2_new))
                        # ATTN: now handles ins and del as well as sub
                        muts_sum = ((mut1["sub"] + mut1["ins"] + mut1["del"]) / mut2["len"]) +\
                                   ((mut2["sub"] + mut2["ins"] + mut2["del"]) / mut2["len"]) # normalized
                        muts_sum /= 2
                        # FIX: this can go over 1 --> normalize by 2
                        isites_substitutions_dict[strain][strain2] = muts_sum

                        print(strain, strain2)
                        print(iseq1_new)
                        print(scrub_bp)
                        print(iseq2_new)
                        print(fe)
                        print(bracket_seq, bracket_pattern)

                    else:
                        imposition_free_energy_dict[strain][strain2] = None
                        isites_substitutions_dict[strain][strain2] = None # actually, even though no-
                        # sign. interaction may exist, there may still be a valid mutation profile
            else:
                sign_interaction[strain] = False

        # - plotting of the heatmaps and file generation
        strains_with_structure = [s for s in sign_interaction.keys() if sign_interaction[s]]
        strains_with_nucleotides = copy.copy(strains_with_structure) # should be ordered to preserve diagonal
        strains_with_nucleotides.extend([s for s in nucleotides_found.keys() if s not in strains_with_structure])

        imposition_data_matrix = np.array([[imposition_free_energy_dict[strain][strain2]
                                            for strain2 in strains_with_nucleotides] for strain in strains_with_structure])
        mutation_data_matrix = np.array([[isites_substitutions_dict[strain][strain2]
                                          for strain2 in strains_with_nucleotides] for strain in strains_with_structure])

        interaction_title = str(line[0][0]) + " " + line[0][1] + " " + line[0][4]
        hmp1_fig, hmp1_ax = makeHeatmap(imposition_data_matrix, strains_with_nucleotides, strains_with_structure, 'bwr',
                                        cell_data_text_format="{0:.1f}", cell_text_color='black',
                                        color_vmin=PLOT_COLOR_VMIN, color_vmax=PLOT_COLOR_VMAX)
        hmp2_fig, hmp2_ax = makeHeatmap(mutation_data_matrix, strains_with_nucleotides, strains_with_structure, 'Blues',
                                        cell_data_text_format="{0:.2f}", cell_text_color='black',
                                        color_vmin=0, color_vmax=1)
        hmp1_ax.set_title("Structural imposition free energies for interaction: " + interaction_title)
        hmp1_ax.set_xlabel("Sequence")
        hmp1_ax.set_ylabel("Structure imposed")
        hmp2_ax.set_title("Strain-pair mutation rates in (imposed) interaction sites for interaction: "
                          + interaction_title)
        hmp2_ax.set_xlabel("Sequence 1")
        hmp2_ax.set_ylabel("Sequence 2 (site-yielding)")
        for i in range(len(strains_with_structure)):
            hmp1_ax.add_patch(Rectangle((i - 0.5, i - 0.5), 1, 1, fill=False, edgecolor='chartreuse', lw=2))
        for i in range(len(strains_with_structure)):
            hmp2_ax.add_patch(Rectangle((i - 0.5, i - 0.5), 1, 1, fill=False, edgecolor='green', lw=2))

        hmp1_fig.subplots_adjust(bottom=0.25, top=0.85)
        hmp2_fig.subplots_adjust(bottom=0.25, top=0.85)

        if SAVE_TO_FOLDER is not None:
            hmp1_fig.savefig(SAVE_TO_FOLDER + "/imp_fe.png")
            hmp2_fig.savefig(SAVE_TO_FOLDER + "/imp_mut.png")
        if PLT_SHOW:
            plt.show()

##### experiment 4: co-variance computations
    STRAIN_IGNORE_LIST = [] # add strans here to uninclude in experiment
    MATCH_TABLE = {1 : PVC_BOND_GAIN, 2 : PVC_BOND_RETAIN_DOUBLE_MUT,
                   "*" : PVC_ANY_MUT} # paired variance type matching table --> interesting positions marked with symbol

    if not DISABLE_EXPERIMENTS[3]:
        line = csv_interpreted[line_index] # load line again/fiirst time
        ref_block = line[STRAIN_INDEX + 1]
        if ref_block[1] is not True:
            raise ValueError("@main: ref_block has no valid interaction!")

        covar_file = None
        if SAVE_TO_FOLDER is not None:
            covar_file = open(SAVE_TO_FOLDER + "/covariation.txt", 'w')
            covar_file.write("Covariation report for: " + str(line[0][0]) + " " + line[0][1] + " " + line[0][4] + "\n")

        # load reference strain sequences/structure
        refiseq1, refiseq2, refbp = seqf.toRNA(ref_block[6]), seqf.toRNA(ref_block[9]), ref_block[12]
        refseq1, refseq2 = replaceGaps(seqf.toRNA(ref_block[2])), replaceGaps(seqf.toRNA(ref_block[4]))
        refseq1_ext, refseq2_ext, refbp_ext = extendBindingPattern(refseq1, ref_block[7], ref_block[8], refiseq1,
                                                                   refseq2, ref_block[10], ref_block[11], refiseq2, refbp)
        # loop over strains
        for new_index, new_block in enumerate(line[1:]):
            if new_index != STRAIN_INDEX: # no +1
                if new_block[1] is True and new_block[0] not in STRAIN_IGNORE_LIST:
                    newiseq1, newiseq2, newbp = seqf.toRNA(new_block[6]), seqf.toRNA(new_block[9]), new_block[12]
                    newseq1, newseq2 = replaceGaps(seqf.toRNA(new_block[2])), replaceGaps(seqf.toRNA(new_block[4]))
                    newseq1_ext, newseq2_ext, newbp_ext = extendBindingPattern(newseq1, new_block[7], new_block[8],
                                                                               newiseq1, newseq2, new_block[10],
                                                                               new_block[11], newiseq2, newbp)

                    writeAndPrint("\nReference strain:" + "\n" + ref_block[0], covar_file)
                    writeAndPrint(refseq1_ext + "\n" + refbp_ext + "\n" + refseq2_ext, covar_file)
                    writeAndPrint("Comparison strain:" + "\n" + new_block[0], covar_file)
                    writeAndPrint(newseq1_ext + "\n" + newbp_ext + "\n" + newseq2_ext, covar_file)

                    tables = getBindingVarianceTables(refseq1_ext, refseq2_ext, refbp_ext,
                                                      newseq1_ext, newseq2_ext,newbp_ext) # get variance code tables
                    writeAndPrint("\nStrand 1 variance codes:", covar_file)
                    writeAndPrint(tables[0], covar_file)
                    writeAndPrint("Strand 2 variance codes:", covar_file)
                    writeAndPrint(tables[1], covar_file)

                    mtable_1 = matchVarianceCodeTable(tables[0], MATCH_TABLE) # paired variance codes match table
                    mtable_2 = matchVarianceCodeTable(tables[1], MATCH_TABLE)

                    string_layers_ref = ["", "", "", "", ""]
                    i1, i2 = 0, 0
                    for i, (c1, cbp, c2) in enumerate(zip(refseq1_ext, refbp_ext, refseq2_ext)):
                        m1, m2 = 0, 0
                        if c1 != "-":
                            m1 = mtable_1[i1]
                        if c2 != "-":
                            m2 = mtable_2[i2]

                        string_layers_ref[0] += [" ", str(m1)][m1 != 0]
                        string_layers_ref[4] += [" ", str(m2)][m2 != 0]
                        string_layers_ref[1] += c1
                        string_layers_ref[2] += cbp
                        string_layers_ref[3] += c2

                        if c1 != "-":
                            i1 += 1
                        if c2 != "-":
                            i2 += 1

                    string_layers_new = ["", "", "", "", ""]
                    i1, i2 = 0, 0
                    for i, (c1, cbp, c2) in enumerate(zip(newseq1_ext, newbp_ext, newseq2_ext)):
                        m1, m2 = 0, 0
                        if c1 != "-":
                            m1 = mtable_1[i1]
                        if c2 != "-":
                            m2 = mtable_2[i2]

                        string_layers_new[0] += [" ", str(m1)][m1 != 0]
                        string_layers_new[4] += [" ", str(m2)][m2 != 0]
                        string_layers_new[1] += c1
                        string_layers_new[2] += cbp
                        string_layers_new[3] += c2

                        if c1 != "-":
                            i1 += 1
                        if c2 != "-":
                            i2 += 1

                    writeAndPrint("\nCovariance layer complex:", covar_file)
                    for layer in string_layers_ref:
                        writeAndPrint(layer, covar_file)
                    for layer in string_layers_new:
                        writeAndPrint(layer, covar_file)

                    print("\n")

        if SAVE_TO_FOLDER is not None:
            covar_file.close()

##### experiment 5: sequence/interaction cross-compatibility between strains
    STRAIN_IGNORE_LIST = set([]) # ignore these strains in the computation
    UNGAPPED_SEQ_REJECTION_THRESHOLD = 0.3 # ratio of non-gap chars in seq to accept
    BATCH_SIZE = 10 # intaRNA calls batch size, to reduce number of system calls
    REDO_SAME_STRAIN_COMPUTATIONS = False # whether to redo computations for segments from same strain
    INTA_RNA_CONFIG_EXP4 = PARAMS["INTARNA_PARAM_FILE"] # parameters for intaRNA
    INTA_RNA_FOLDER = PARAMS["INTA_RNA_FOLDER"] # location of intaRNA
    PLOT_COLOR_VMIN, PLOT_COLOR_VMAX = -40, 20 # for the heatmap

    line = csv_interpreted[line_index]
    segment1, segment2 = line[0][1], line[0][4]
    cross_interaction_dict = {}
    cross_interaction_segments_accepted = {strain : [False, False] for strain in strains}
    queries_to_consider = [] # for batching
    if not DISABLE_EXPERIMENTS[4]:
        print("\nCross-computation experiment")
        from tools.batch import *
        import intarna_functions as irf
        import numpy as np
        from matplotlib.patches import Rectangle

        for block1 in line[1:]:
            strain1 = block1[0]
            seq1 = block1[2] # first segment set
            if strain1 not in STRAIN_IGNORE_LIST and block1[1] is not None:
                if block1[3] > UNGAPPED_SEQ_REJECTION_THRESHOLD:
                    cross_interaction_segments_accepted[strain1][0] = True
                    cross_interaction_dict[strain1] = {}

                    for block2 in line[1:]:
                        strain2 = block2[0]
                        seq2 = block2[4]
                        if strain2 not in STRAIN_IGNORE_LIST and block2[1] is not None:
                            if block2[5] > UNGAPPED_SEQ_REJECTION_THRESHOLD:
                                cross_interaction_segments_accepted[strain2][1] = True

                                if strain1 != strain2 or REDO_SAME_STRAIN_COMPUTATIONS:
                                    cross_interaction_dict[strain1][strain2] = [] # information about interaction (cross-inter)
                                    queries_to_consider.append([strain1, strain2, seq1, seq2])
                                else: # same strain and don't redo computation
                                    cross_interaction_dict[strain1][strain2] = \
                                        [block1[1], block1[13], block1[6], block1[9], block1[12],
                                         (block1[7], block1[8], block1[10], block1[11])] # (interaction found, free energy, ...)

        print("Starting intaRNA computations")
        num_batches = numBatches(queries_to_consider, BATCH_SIZE)
        for ibatch in range(num_batches):
            print("-Batch %d / %d" % (ibatch + 1, num_batches))
            batch = takeBatch(queries_to_consider, ibatch, BATCH_SIZE)
            res, err, lines, info_dict = irf.formatIntaRNABatchOutput(
                *irf.callIntaRNABatch(INTA_RNA_FOLDER, [b[2] for b in batch], [b[3] for b in batch],
                                      flag_dict={"parameterFile": INTARNA_PARAM_FILE}))

            # loop over interactions in the batch
            for block_index, fe in enumerate(info_dict["free_energy"]):
                # batch_results tuple contents: [interaction found, free energy, seq1 gapped, seq2 gapped,
                # binding pattern, alignment positions]
                batch_results = []
                batch_results.append(info_dict["interaction_found"][block_index])  # False, True or None
                batch_results.append(fe) # fe can still be "None"

                block_start, block_end = irf.blockStartEnd(block_index, info_dict["block_starts"])
                if info_dict["interaction_found"][block_index]:  # if a significant interaction was found for block
                    block = lines[block_start:block_end]
                    seq1_gapped, seq2_gapped, binding_pattern, alignment_positions \
                        = irf.IntaRNAOutput_getAlignment(block)  # read the actual intaRNA interaction
                    batch_results.extend([seq1_gapped, seq2_gapped, binding_pattern, alignment_positions])
                else:
                    batch_results.extend([None, None, None, None])

                # map back to strains
                query_index = ibatch * BATCH_SIZE + block_index
                strain1, strain2 = queries_to_consider[query_index][0], queries_to_consider[query_index][1]
                cross_interaction_dict[strain1][strain2] = copy.copy(batch_results)

        # - plotting of the heatmaps and file generation
        strains_to_plot1, strains_to_plot2 = [], [] # currently, only plot strains which have intact sequences
        for k, v in cross_interaction_segments_accepted.items():
            if v[0] and v[1]:
                strains_to_plot1.append(k)
                strains_to_plot2.append(k)

        cross_compat_data_matrix = [[999 for _ in range(len(strains_to_plot2))] for _ in range(len(strains_to_plot1))]
        cross_compat_cell_texts = [["@@@" for _ in range(len(strains_to_plot2))] for _ in range(len(strains_to_plot1))]

        for i1, strain1 in enumerate(strains_to_plot1):
            for i2, strain2 in enumerate(strains_to_plot2):
                result = cross_interaction_dict[strain1][strain2]
                #print(strain1, strain2) # print the cross-strain computations
                #print(result)
                if result[0] is True: # interaction found
                    cross_compat_data_matrix[i1][i2] = result[1]
                    cross_compat_cell_texts[i1][i2] = "{0:.1f}".format(result[1])
                elif result[0] is False: # no interaction found
                    cross_compat_data_matrix[i1][i2] = np.nan
                    cross_compat_cell_texts[i1][i2] = "NI" # no interaction
                else: # incomplete sequences
                    cross_compat_data_matrix[i1][i2] = np.nan
                    cross_compat_cell_texts[i1][i2] = "N/A"
        cross_compat_data_matrix = np.array(cross_compat_data_matrix)

        interaction_title = str(line[0][0]) + " " + line[0][1] + " " + line[0][4]
        cchmp_fig, cchmp_ax = makeHeatmap(cross_compat_data_matrix, strains_to_plot2, strains_to_plot1,
                                        'bwr', write_data_in_cells=False,
                                        cell_texts=cross_compat_cell_texts, cell_text_color='black',
                                        color_vmin=PLOT_COLOR_VMIN, color_vmax=PLOT_COLOR_VMAX)
        cchmp_ax.set_title("Cross-strain interaction energies for interaction: " + interaction_title + "; in kcal/mol")
        cchmp_ax.set_xlabel("Strain for segment 2 (" + line[0][4] + ")")
        cchmp_ax.set_ylabel("Strain for segment 1 (" + line[0][1] + ")")

        for i in range(len(strains_to_plot1)): # add diagonal patches TODO: rework if needed
            cchmp_ax.add_patch(Rectangle((i - 0.5, i - 0.5), 1, 1, fill=False, edgecolor='chartreuse', lw=2))

        cchmp_fig.subplots_adjust(bottom=0.25, top=0.85)

        if SAVE_TO_FOLDER is not None:
            cchmp_fig.savefig(SAVE_TO_FOLDER + "/cross_compatibility.png")
        if PLT_SHOW:
            plt.show()
