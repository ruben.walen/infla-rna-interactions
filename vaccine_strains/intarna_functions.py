"""Functions for dealing with the intaRNA binary, and useful sequence and structure processing."""

import subprocess
import os
import re
import copy


def callIntaRNA(install_folder, target, query, flag_dict={}):
    """Call IntaRNA with target and query (Python string sequences)."""
    intaRNA_binary = install_folder + "IntaRNA.exe"
    if not os.path.isfile(intaRNA_binary):
        raise ValueError("@callIntaRNA: IntaRNA.exe not found in: " + install_folder)

    command = [intaRNA_binary, "-t", target, "-q", query]
    for flag in flag_dict.keys():
        command.append("--" + flag + "=" + flag_dict[flag])

    out = subprocess.run(command, capture_output=True)
    return out


def callIntaRNABatch(install_folder, target_list, query_list, flag_dict={}, os_shell="cmd.exe"):
    """Call IntaRNA with target and query (Python string sequences) in list format.
        Aims to reduce overhead of opening/closing shells."""
    intaRNA_binary = install_folder + "IntaRNA.exe"
    intaRNA_binary_abs = "\"" + os.path.abspath(intaRNA_binary) + "\""
    if not os.path.isfile(intaRNA_binary):
        raise ValueError("@callIntaRNABatch: IntaRNA.exe not found in: " + install_folder)

    process = subprocess.Popen(os_shell, shell=False,
                               stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)  # call to shell! universal_newlines=True?
    for query, target in zip(query_list, target_list):
        command = intaRNA_binary_abs + " -t " + target + " -q " + query
        for flag in flag_dict.keys():
            command += " --" + flag + "=" + flag_dict[flag]
        command += "\n"
        command_enc = command.encode()

        process.stdin.write(command_enc)
    process.stdin.close()

    out, err = process.stdout.read(), process.stderr.read()
    return out, err


def formatIntaRNAOutput(process_output, decode_format='utf-8'):
    """Format the output of callIntaRNA."""
    output = process_output.stdout.decode(decode_format)
    linesplit = output.split("\n")
    info_dict = {}

    fe_search = None
    if re.search("interaction energy", linesplit[-2]) is not None:
        fe_search = re.search("[-]?[0-9]+\.[0-9]+", linesplit[-2])
    if fe_search is not None:
        free_energy = float(fe_search.group())
        interaction_found = True
    else:
        free_energy = None
        interaction_found = False  # no relevant interaction
    # target = --> see IntaRNAOuput_getAlignment
    # query = ''
    info_dict["free_energy"] = free_energy
    info_dict["interaction_found"] = interaction_found
    return output, linesplit, info_dict


def formatIntaRNABatchOutput(out, err, decode_format='utf-8'):
    """Format the output of callIntaRNABatch."""
    output = out.decode(decode_format)
    error = err.decode(decode_format)
    linesplit = output.split("\n")
    info_dict = {"free_energy": [],
                 "interaction_found": []}

    block_starts = []  # starts of blocks (indices in list)
    for i, line in enumerate(linesplit):
        if "IntaRNA" in line:
            if "-t" in line and "-q" in line:
                block_starts.append(i)
    for i, start in enumerate(block_starts):
        if i != len(block_starts) - 1:
            end = block_starts[i + 1]
        else:
            end = len(linesplit)
        # queries here
        free_energy = None
        interaction_found = False
        for li in range(start, end):
            fe_search = None
            if re.search("interaction energy", linesplit[li]) is not None:
                free_energy = None  # just to be sure
                fe_search = re.search("[-]?[0-9]+\.[0-9]+", linesplit[li])
            if fe_search is not None:
                free_energy = float(fe_search.group())
                interaction_found = True
                break
        info_dict["free_energy"].append(free_energy)
        info_dict["interaction_found"].append(interaction_found)
    info_dict["block_starts"] = block_starts  # starts of each block = each alignment (if found)

    return output, error, linesplit, info_dict


def blockStartEnd(list_index, block_starts):
    """Get block start and end indices from list_index index."""
    if len(block_starts) - 1 > list_index >= 0:
        return block_starts[list_index], block_starts[list_index + 1]
    elif list_index == len(block_starts) - 1:
        return block_starts[list_index], -1  # end of list
    else:
        raise IndexError("@blockStartEnd: list_index out of bounds: " + str(list_index))


# the next 3 functions work only on a single block with valid alignment
def IntaRNAOutput_determineAlignmentStart(output_list):
    """Determine start index of alignment part in intaRNA output (non-batch).
        The alignment is 5 lines long but other information surrounds it.
        Input: output_list (list): the output linesplit for interaction"""
    index = None
    for i, line in enumerate(output_list):
        if "5'-" in line:
            index = i

    if index is None:
        print(output_list)
        raise ValueError("@intaRNAOutput_determineAlignmentStart: start line not found.""")
    else:
        return index


def IntaRNAOutput_getAlignment(output_list):
    """Get IntaRNA alignment from output_list starting at start_index (see determineAlignmentStart).
        Input: output_list (list): the output linesplit for interaction
        Output: sequence 1; sequence 2; binding pattern; positions: (start1, end1, start2, end2)"""
    # add sequence starts/ends
    # reversing?
    # at alignment starts: start reading
    start_index = IntaRNAOutput_determineAlignmentStart(output_list)
    sublist = output_list[start_index:start_index + 5]

    align_start_top_line = output_list[start_index - 2]  # this line contains starting positions for seq1
    align_start_bottom_line = output_list[start_index + 6]  # this line contains starting positions for seq1
    align_start_top, align_end_top = None, None  # alignment start and end position for intaRNA interaction found
    align_start_bottom, align_end_bottom = None, None
    for strand in range(2):
        prev_was_digit = False
        num = None
        line = [align_start_top_line, align_start_bottom_line][strand == 1]
        for i, c in enumerate(line):
            if c != " ":
                if prev_was_digit:
                    num += c
                else:
                    num = c
                    prev_was_digit = True
            else:  # is space
                if prev_was_digit:
                    if strand == 0:  # top
                        if align_start_top == None:
                            align_start_top = int(num)
                            num = None
                        else:
                            align_end_top = int(num)
                            num = None
                    else:  # bottom
                        if align_start_bottom == None:
                            align_start_bottom = int(num)
                            num = None
                        else:
                            align_end_bottom = int(num)
                            num = None
                prev_was_digit = False
        if prev_was_digit:  # still True and unprocessed
            if strand == 0:
                align_end_top = int(num)  # set end to that value
            else:
                align_end_bottom = int(num)

    delimiters_line = output_list[start_index - 1]  # this line contains | to delimit start/end of sequences
    al_left, al_right = None, None
    for i, c in enumerate(delimiters_line):
        if c == "|":
            if al_left == None:
                al_left = i
            else:
                al_right = i
    if al_right is None:
        # TODO: does this even work?
        print(">IntaRNAOutput_getAlignment: ATTN: al_right is None; setting to al_left + 1")
        al_right = al_left + 1

    length = al_right - al_left + 1
    seq1, seq2 = "", ""  # gapped sequences
    binding = ""  # binding symbols
    for i, s in enumerate(sublist):  # stripping
        s = s[al_left:al_right + 1]  # strip to | | boundaries
        sublist[i] = s

    for i in range(length):  # find alignment
        symbol1 = "-"  # gap
        if len(sublist[0]) > i:
            if sublist[0][i] != " ":
                symbol1 = sublist[0][i]
        if len(sublist[1]) > i:
            if sublist[1][i] != " ":
                symbol1 = sublist[1][i]
        symbol2 = "-"
        if len(sublist[3]) > i:
            if sublist[3][i] != " ":
                symbol2 = sublist[3][i]
        if len(sublist[4]) > i:
            if sublist[4][i] != " ":
                symbol2 = sublist[4][i]
        symbol_bind = " "  # symbol for binding
        if len(sublist[2]) > i:
            symbol_bind = sublist[2][i]
        seq1 += symbol1
        seq2 += symbol2
        binding += symbol_bind
    return seq1, seq2, binding, \
           (align_start_top, align_end_top, align_start_bottom, align_end_bottom)


def getBracketNotation(seq1, seq2, binding, include_weak=False, ampersand_split=True, middle_insert=""):
    """Get bracket notation for two sequences and a binding pattern.
        include_weak (bool): whether to include ":" type bonds in bracketing.
        seq1 is pasted in front of seq2 here for the bracketing.
        "&" is used for appending sequences/structures.
        The bracket structures are over ungapped nucleotides.
        ampersand_split (bool): whether to use ampersand character to split seqs/structs in output
        middle_insert (string): non-binding sequence to insert between seq1, seq2 (inserted at end of seq1)
        Output: bracket pattern 1; bracket pattern 2; pasted sequences; pasted bracket patterns"""
    if len(seq1) != len(seq2) or len(seq2) != len(binding):
        raise ValueError("@getBracketNotation: seq1, seq2 and binding pattern must be of same length.")

    # ATTN: bracket_pattern2, seq_ungapped2 are inverted w.r.t. to first seq.!
    bracket_pattern1 = ""
    bracket_pattern2 = ""
    seq_ungapped = ["", ""]  # doubled sequence; ungapped
    for i in range(len(binding)):
        if binding[i] == "|" or (binding[i] == ":" and include_weak):
            bracket_pattern1 += "("
            bracket_pattern2 = ")" + bracket_pattern2
            seq_ungapped[0] += seq1[i]  # ATTN: should never be gapped
            seq_ungapped[1] = seq2[i] + seq_ungapped[1]
        else:
            if seq1[i] != "-":  # not gapped
                bracket_pattern1 += "."
                seq_ungapped[0] += seq1[i]
            if seq2[i] != "-":
                bracket_pattern2 = "." + bracket_pattern2
                seq_ungapped[1] = seq2[i] + seq_ungapped[1]

    seq_ungapped[0] += middle_insert
    bracket_pattern1 += "".join(["." for _ in range(len(middle_insert))])

    if ampersand_split:
        return bracket_pattern1, bracket_pattern2, seq_ungapped[0] + "&" + seq_ungapped[1], \
            bracket_pattern1 + "&" + bracket_pattern2
    else:
        return bracket_pattern1, bracket_pattern2, seq_ungapped[0] + seq_ungapped[1], \
               bracket_pattern1 + bracket_pattern2


def extendBindingPattern(seq1, start1, end1, intarna_seq1,
                         seq2, start2, end2, intarna_seq2, pattern,
                         check_inversions=True):
    """Extend a binding pattern to whole seq1 and seq2 as padding.
    start-end features as the core where the binding pattern is realized.
    Return sequences may be gapped and inverted.
    seq1 = full context sequence (ungapped); intarna_seq1 = gapped intaRNA interaction site within seq1, etc.
    The sequences may also contain gapped parts in the non-binding parts due to length differences.
    start1 and end1 index starting at 1, as given in intaRNA. end1 includes that nucleotide.
    check_inversions (bool) to check whether seq1 and seq2 should be inverted if the intaRNA subsequence was inverted.
    Returns: seq1_gapped, seq2_gapped, extended_pattern"""

    # TODO: sequences may contain T, U
    # may still be broken?

    seq1_inverted, seq2_inverted = False, False  # intaRNA inverts some sequences for optimal interaction
    len1, len2 = len(seq1), len(seq2)
    left1, right1 = start1 - 1, end1 - 1
    left2, right2 = start2 - 1, end2 - 1
    if check_inversions:
        if end1 < start1:
            seq1_inverted = True
            left1, right1 = len1 - start1, len1 - end1
        if end2 < start2:
            seq2_inverted = True
            left2, right2 = len2 - start2, len2 - end2
    front1, front2 = left1, left2  # size of left non-interacting sequence (ungapped)
    tail1, tail2 = len(seq1) - right1, len(seq2) - right2  # size of right non-interacting sequence (ungapped)
    # left, right always point to the left and right sides resp. of the aligned sequences in intaRNA alignment

    extended_binding_pattern = copy.copy(pattern)
    if check_inversions: # else don't check for inversions
        seq1_gapped = [seq1, seq1[::-1]][seq1_inverted]
        seq2_gapped = [seq2, seq2[::-1]][seq2_inverted]
    seq1_gapped = seq1_gapped[0:left1] + intarna_seq1 \
                  + seq1_gapped[right1+1:] # including nucleotide at right1, nucleotide counting starts at 1
    seq2_gapped = seq2_gapped[0:left2] + intarna_seq2 \
                  + seq2_gapped[right2+1:]  # intaRNA sequence should be reversed already if appropriate
    # intaRNA binding alignment may contain gaps

    gapping_left = front2 - front1  # how many gaps should be inserted in front (left) position
    if gapping_left < 0:  # insert in seq2
        seq2_gapped = "".join(["-" for _ in range(abs(gapping_left))]) + seq2_gapped
    elif gapping_left > 0:  # insert in seq1
        seq1_gapped = "".join(["-" for _ in range(gapping_left)]) + seq1_gapped
    gapping_right = tail2 - tail1  # how many gaps should be inserted in tail (right) position
    if gapping_right < 0:  # insert in seq2
        seq2_gapped = seq2_gapped + "".join(["-" for _ in range(abs(gapping_right))])
    elif gapping_right > 0:  # insert in seq1
        seq1_gapped = seq1_gapped + "".join(["-" for _ in range(gapping_right)])

    # extend the binding pattern with gaps (spaces)
    left_max = max(front1, front2)
    right_max = max(tail1, tail2) - 1
    if left_max > 0:
        extended_binding_pattern = "".join([" " for _ in range(left_max)]) + extended_binding_pattern
    if right_max > 0:
        extended_binding_pattern = extended_binding_pattern + "".join([" " for _ in range(right_max)])

    return seq1_gapped, seq2_gapped, extended_binding_pattern


def bindingPatternScrubWeak(binding_pattern, replace_with_strong=False):
    """Scrub weak interactions (":") from a binding pattern, or replace with strong interaction "|" """
    if not replace_with_strong:
        return binding_pattern.replace(":", " ")
    else:
        return binding_pattern.replace(":", "|")


def bindingPatternVisualiseNoInter(binding_pattern, char="."):
    """Visualise no-interaction character (" ") using char in a binding pattern."""
    return binding_pattern.replace(" ", char)


def cutBindingPatternAtGappedSites(seq1, seq2, binding_pattern, remove_bonds_only=False):
    """Cut characters from a binding pattern and sequences at sites that are gapped in seq1 and/or seq2.
    If remove_bonds_only: does not cut binding pattern or sequences, instead removes bonds at gapped positions.
    Also returns seq1, seq2 with/without gap cuts."""
    if len(seq1) != len(seq2) or len(seq2) != len(binding_pattern):
        raise ValueError("@cutBindingPatternAtGappedSites: sequences and binding pattern must be of equal length")

    cut_seq1, cut_seq2, cut_bp = "", "", ""
    for c1, c2, cbp in zip(seq1, seq2, binding_pattern):
        if c1 != "-" and c2 != "-":
            cut_seq1 += c1
            cut_seq2 += c2
            cut_bp += cbp
        elif remove_bonds_only:
            cut_seq1 += c1
            cut_seq2 += c2
            cut_bp += " " # remove bond

    return cut_seq1, cut_seq2, cut_bp


def getStructuralConservation(binding_pattern1, binding_pattern2, include_weak=False, ignore_length_diff=False):
    """Get structural conservation values for two binding patterns.
        Does not include weak interactions by default.
        pattern1 = reference; pattern2 = 'new'
        Input: two binding patterns (containing space, | or :)
            include_weak (bool): whether to include weak ":" interactions
            ignore_length_diff (bool): whether to ignore string lengths differences in counting
        Returns: gain_string, bonds_p1, bonds_p2, bond_gains, bond_losses, bonds_conserved
            gain_string (string): string of positional changes/conservations: . o - + for each possible change/cons"""
    if not ignore_length_diff:
        if len(binding_pattern1) != len(binding_pattern2):
            raise ValueError("@getStructuralConservation: binding patterns must be of equal length")

    binding_pattern1 = bindingPatternScrubWeak(binding_pattern1, replace_with_strong=include_weak)
    binding_pattern2 = bindingPatternScrubWeak(binding_pattern2, replace_with_strong=include_weak)

    gain_string = ""  # "." : . --> . ; "o" : | --> | ; "+" : . --> | ; "-" : | --> .
    bonds_p1, bonds_p2 = 0, 0  # bonds per pattern
    bond_gains, bond_losses, bonds_conserved = 0, 0, 0  # number of bond gains and bond losses in pattern 2 vs. pattern 1
    # if len exceeds len of other sequence, the zip function stops looping, i.e. length exceeding part is ignored
    for c1, c2 in zip(binding_pattern1, binding_pattern2):
        if c1 == c2:
            if c1 == " ":
                gain_string += "."
            else:  # = |
                bonds_p1 += 1
                bonds_p2 += 1
                bonds_conserved += 1
                gain_string += "o"
        else:
            if c1 == "|":  # | --> .
                bonds_p2 += 1
                bond_losses += 1
                gain_string += "-"
            else:  # . --> |
                bonds_p1 += 1
                bond_gains += 1
                gain_string += "+"

    return gain_string, bonds_p1, bonds_p2, bond_gains, bond_losses, bonds_conserved


def extractBindingPatternForSequences(bind_seq1, bind_seq2, binding_pattern):
    """Map binding pattern to ungapped sequences from bind_seq1, bind_seq2.
    Returns ungapped sequence 1 and 2, and binding patterns mapped to these sequences.
    Useful for structure consensus formation per nucleotide."""
    ug_seq1, ug_seq2 = "", ""
    bp_seq1, bp_seq2 = "", ""
    for i, b in enumerate(binding_pattern):
        if bind_seq1[i] != "-":
            ug_seq1 += bind_seq1[i]
            bp_seq1 += b
        if bind_seq2[i] != "-":
            ug_seq2 += bind_seq2[i]
            bp_seq2 += b

    return ug_seq1, bp_seq1, ug_seq2, bp_seq2


def cutToBindingSubsequences(seq1, seq2, binding_pattern, include_weak=True):
    """Cuts the three input seqs/struct to the part where binding occurs, without deleting inner nucleotides.
    Assuming seq1, seq2, binding_pattern are aligned."""
    start, stop = 0, max(len(seq1), len(seq2)) # just in case, seq1 should be same length as seq2
    for i, b in enumerate(binding_pattern):
        if b == "|" or (b == ":" and include_weak):
            start = b
    for i, b in enumerate(binding_pattern[start:]):
        if b == "|" or (b == ":" and include_weak):
            stop = b + start

    return seq1[start:stop], seq2[start:stop], binding_pattern[start:stop]


def extendString(string, left, right, char):
    """Extend a string on left and right using char"""
    return "".join([char for _ in range(left)]) + string + "".join([char for _ in range(right)])


def ungappedLen(string, gap_char="-"):
    """Measure ungapped length of string using gap char."""
    length = 0
    for c in string:
        if c != gap_char:
            length += 1
    return length


def reinsertGaps(insert_seq, gap_seq, gap_char_insert, gap_char_gap):
    """Reinsert gaps from gap_seq into insert_seq using gap_char. E.g. ..|| + AA---TT + . ==> .....||.
    Assume aligned over gaps."""
    print("aaa")
    print(insert_seq)
    print(gap_seq)
    gapped = ""
    ii = 0
    for ig, cg in enumerate(gap_seq):
        if cg != gap_char_gap:
            gapped += insert_seq[ii]
            ii += 1
        else: # insert gap
            gapped += gap_char_insert
    print(gapped)
    assert len(gap_seq) == len(gapped) # these must now be equal
    return gapped


def imposeStructure(iseq1_native, iseq2_native, bp_native,
                    iseq1_transfer, iseq2_transfer):
    """Impose structure of bp_native on transfer sequences, using gapping patterns from native sequences.
    bp_native is not used, it is only passed for convenience."""
    if not (ungappedLen(iseq1_native) == len(iseq1_transfer) and ungappedLen(iseq2_native) == len(iseq2_transfer)):
        raise ValueError("@imposeStructure: iseq native ungapped length must be equal to len of transfer seq.")
    iseq1_new, iseq2_new = "", ""
    i = -1
    for c in iseq1_native:
        if c != "-":
            i += 1
            iseq1_new += iseq1_transfer[i]
        else:
            iseq1_new += "-"
    i = -1
    for c in iseq2_native:
        if c != "-":
            i += 1
            iseq2_new += iseq2_transfer[i]
        else:
            iseq2_new += "-"
    return iseq1_new, iseq2_new, bp_native


RNA_LEGAL_BONDS_STRICT = {"AU", "UA", "CG", "GC"}
RNA_DNA_LEGAL_BONDS_STRICT = {"AT", "TA", "AU", "UA", "CG", "GC"}
DNA_LEGAL_BONDS_STRICT = {"AT", "TA", "CG", "GC"}
DNA_GT_BP = {"GT", "TG"} # potential weak pairing
RNA_GU_BP = {"GU", "UG"}


def scrubImpossibleBindingNucleotides(iseq1, iseq2, binding_pattern, legal=RNA_LEGAL_BONDS_STRICT):
    """Scrub RNA-RNA bonds that cannot bind at all. Given input set of legal bonds. Assuming inputs are aligned."""
    if len(iseq1) != len(iseq2) != len(binding_pattern):
        raise ValueError("@scrubImpossibleBindingNucleotides: all inputs strings must be of same length.")

    scrub_bp = ""
    for c1, b, c2 in zip(iseq1, binding_pattern, iseq2):
        if c1 + c2 in legal:
            scrub_bp += b
        else:
            scrub_bp += " "
    return scrub_bp


def reoptimizeBinding(iseq1, iseq2, legal=RNA_LEGAL_BONDS_STRICT, weak=RNA_GU_BP):
    """Re-optimize binding for two (gapped) interaction site sequences. Does not shift gaps.
    This should also scrub as in scrubImpossibleBindingNucleotides."""
    if len(iseq1) != len(iseq2):
        raise ValueError("@reoptimizeBinding: all inputs strings must be of same length.")

    reopt_bp = ""
    for c1, c2 in zip(iseq1, iseq2):
        if not (c1 == "-" or c2 == "-"): # gapped = no binding possible
            if c1 + c2 in legal:
                reopt_bp += "|"
            elif c1 + c2 in weak:
                reopt_bp += ":"
            else:
                reopt_bp += " "
        else:
            reopt_bp += " "
    return reopt_bp


def replaceGaps(string, replace_char="d", gap_char="-"):
    """Replace gaps in string with replace_char.
    Useful for distinguishing genomic alignment gaps from interaction gaps"""
    return string.replace(gap_char, replace_char)


def getBindingPositionTables(iseq1, iseq2, binding_pattern, include_weak=True, interaction_gap_char="-",
                             alignment_gap_char="d"):
    """Get per-strand tables of binding positions (indices in alignment, no interaction gaps), in dict form.
    Must pass iseq1, iseq2, binding_pattern in interaction-aligned form (binding paired positions).
    Returns: btable_seq1, btable_seq2: contains index 1 : (char 1, index 2, char 2), last two can be None."""
    # FIX: problem distinguishing genomic alignment gap from interaction gap --> use replaceGaps
    # TODO: include binding type (full bond or half bond)?
    if len(iseq1) != len(iseq2) != len(binding_pattern):
        raise ValueError("@getBindingPositionTables: all inputs strings must be of same length.")

    btable_seq1, btable_seq2 = {}, {}
    ind_s1, ind_s2 = 0, 0
    for c1, c2, cbp in zip(iseq1, iseq2, binding_pattern):
        if cbp == "|" or (cbp == ":" and include_weak):
            if c1 == interaction_gap_char or c1 == alignment_gap_char \
                    or c2 == interaction_gap_char or c2 == alignment_gap_char: # this is slower but more safe
                raise ValueError("@getBindingPositionTables: binding position contains gapped nucleotide(s): "
                                 + c1 + cbp + c2)

            btable_seq1[ind_s1] = (c1, ind_s2, c2) # index 1: (char 1, index 2, char 2)
            btable_seq2[ind_s2] = (c2, ind_s1, c1)
        else:
            btable_seq1[ind_s1] = (c1, None, None)
            btable_seq2[ind_s2] = (c2, None, None)

        if c1 != interaction_gap_char:
            ind_s1 += 1
        if c2 != interaction_gap_char:
            ind_s2 += 1
    if iseq1[-1] == interaction_gap_char: # table end correction
        del btable_seq1[ind_s1]
    if iseq2[-1] == interaction_gap_char:
        del btable_seq2[ind_s2]

    return btable_seq1, btable_seq2


def _getVarianceCode(i1_ref, c1_ref, i2_ref, c2_ref,
                     i1_new, c1_new, i2_new, c2_new, alignment_gap_char="d"):
    """Private method, see below."""
    code = ""
    if i2_ref is None: # no binding 1
        code += "n"
    else:
        code += "b"
    if i2_new is None: # no binding 2
        code += "n"
    else:
        code += "b"
    if c1_ref != c1_new:
        if c1_ref == alignment_gap_char:
            code += "i"  # insertion
        elif c1_new == alignment_gap_char:
            code += "d"  # deletion
        else:
            code += "1"  # substitution
    else:
        if c1_ref == alignment_gap_char:  # gap position in both
            code += "x"
        else:
            code += "0"  # no mutation
    if i2_ref is not None and i2_new is not None and i2_ref != i2_new: # shift position
        code += "s"
    return code


# variance codes:
# nucleotide position: compare binding ref vs. new, mutation? e.g. nn0 = no binding --> no binding, no mutations, etc.
# variation code system
def getBindingVarianceTables(iseq1_ref, iseq2_ref, bp_ref, iseq1_new, iseq2_new, bp_new, interaction_gap_char="-",
                             alignment_gap_char="d", include_weak=True):
    """Get variance codes per nucleotide position (with genomic alignment indices) of ref structure compared to new
    Binding pattern extension may be used to make sure both strands in both sources have equal nucleotide numbers."""
    if len(iseq1_ref) != len(iseq2_ref) != len(bp_ref):
        raise ValueError("@getBindingVarianceTables: reference inputs strings must be of same length.")
    if len(iseq1_new) != len(iseq2_new) != len(bp_new):
        raise ValueError("@getBindingVarianceTables: comparison inputs strings must be of same length.")

    # get nucleotide binding tables
    btable1_ref, btable2_ref = getBindingPositionTables(iseq1_ref, iseq2_ref, bp_ref, include_weak=include_weak,
                                                        interaction_gap_char=interaction_gap_char,
                                                        alignment_gap_char=alignment_gap_char)
    btable1_new, btable2_new = getBindingPositionTables(iseq1_new, iseq2_new, bp_new, include_weak=include_weak,
                                                        interaction_gap_char=interaction_gap_char,
                                                        alignment_gap_char=alignment_gap_char)
    # tables to contain variance codes
    code_table1, code_table2 = {}, {}

    # start scan of tables
    for i1_ref, v1_ref in btable1_ref.items():
        c1_ref, i2_ref, c2_ref = v1_ref # unpack
        try:
            v1_new = btable1_new[i1_ref]
            i1_new = i1_ref
        except KeyError:
            raise KeyError("@getBindingVarianceTables: index " + str(i1_ref) + " does not exist in new binding table!")
        c1_new, i2_new, c2_new = v1_new

        # code making
        code_table1[i1_ref] = _getVarianceCode(i1_ref, c1_ref, i2_ref, c2_ref,
                                               i1_new, c1_new, i2_new, c2_new, alignment_gap_char=alignment_gap_char)

    for i1_ref, v1_ref in btable2_ref.items(): # NOTE: now strands are switched in perspective
        c1_ref, i2_ref, c2_ref = v1_ref # unpack
        try:
            v1_new = btable2_new[i1_ref]
            i1_new = i1_ref
        except KeyError:
            raise KeyError("@getBindingVarianceTables: index " + str(i1_ref) + " does not exist in new binding table!")
        c1_new, i2_new, c2_new = v1_new

        # code making
        code_table2[i1_ref] = _getVarianceCode(i1_ref, c1_ref, i2_ref, c2_ref,
                                               i1_new, c1_new, i2_new, c2_new, alignment_gap_char=alignment_gap_char)

    # paired codes
    paired_code_table1, paired_code_table2 = {}, {} # these code tables contain covariance information
    for i1_ref, v1_ref in btable1_ref.items():
        paired_code = code_table1[i1_ref]
        if paired_code[0:2] == "bn": # bond loss scenario
            i2_compl = v1_ref[1]
        elif paired_code[0:2] == "bb" or paired_code[0:2] == "nb": # bond retention or bond gain
            try:
                v1_new = btable1_new[i1_ref]
                i2_compl = v1_new[1]
            except KeyError:
                raise KeyError("@getBindingVarianceTables: index "
                               + str(i1_ref) + " does not exist in new binding table!")
        else:
            i2_compl = None # throw error later

        if i2_compl is not None: # paired code adds information about variance of binding opposite nucleotide
            paired_code += ">" + code_table2[i2_compl]
        paired_code_table1[i1_ref] = paired_code

    for i1_ref, v1_ref in btable2_ref.items(): # NOTE: now strands are switched in perspective
        paired_code = code_table2[i1_ref]
        if paired_code[0:2] == "bn": # bond loss scenario
            i2_compl = v1_ref[1]
        elif paired_code[0:2] == "bb" or paired_code[0:2] == "nb": # bond retention or bond gain
            try:
                v1_new = btable2_new[i1_ref]
                i2_compl = v1_new[1]
            except KeyError:
                raise KeyError("@getBindingVarianceTables: index "
                               + str(i1_ref) + " does not exist in new binding table!")
        else:
            i2_compl = None # throw error later

        if i2_compl is not None:
            paired_code += ">" + code_table1[i2_compl]
        paired_code_table2[i1_ref] = paired_code

    return paired_code_table1, paired_code_table2, code_table1, code_table2, \
           btable1_ref, btable2_ref, btable1_new, btable2_new


# paired variance code regex defs that may be useful
PVC_BOND_GAIN = "nb" # bond gain
PVC_DOUBLE_BOND_GAIN = "nb[01i]>nb[01i]" # bond gain with any mutation type, including insertion
PVC_BOND_RETAIN_DOUBLE_MUT = "bb1>bb1" # strict retention with double mutation
PVC_BOND_RETAIN_ANY_MUT = "bb[01]>bb[01]" # bond retention with any mutation profile
PVC_BOND_RETAIN_SHIFT = "bb[01]s" # bond retention with complementary site shift
PVC_ANY_MUT = "..1" # any mutation occuring at pos
PVC_ANY_BOND = "b" # any initial bond


def compileCodesToSet(*args):
    """Make set of combination of PVC regex strings."""
    codes = set([])
    for arg in args:
        codes.add(arg)
    return codes


def matchVarianceCodeTable(table, match_library, default_symbol=0):
    """Match a table of paired- or non-paired variance codes at every index position using regex search patterns
    in match_library. match_library is a dictionary with match symbol : search pattern(s), with search pattern(s)
    a string or a set or a list. Default match code is 0 for no match."""
    # TODO: compile regex strings?
    match_table = {}
    for i, v in table.items():
        matched_one = False
        for ms, mv in match_library.items():
            match = False
            if type(mv) == str:
                res = re.match(mv, v)
                if res is not None:
                    match = True
            else:
                for p in mv: # assume iterable
                    res = re.match(p, v)
                    if res is not None:
                        match = True
                        break
            if match:
                match_table[i] = ms
                matched_one = True
                break # skip rest
        if not matched_one:
            match_table[i] = default_symbol
    return match_table



