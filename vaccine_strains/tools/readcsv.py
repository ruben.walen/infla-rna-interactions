def readCsv(filename, sep1=";", sep2=","):
    """Read a .csv file, given two levels of separation.
    Returns: read (list(list(str)))"""
    read = []
    with open(filename) as f:
        for line in f:
            line = line.strip() # strip endline, spaces
            split = line.split(sep1)
            split = [s.split(sep2) for s in split]
            read.append(split)
    return read

def csvBlockInterpret(csv_list, block_indices, converters_per_element):
    """Convert elements in readCsv result to Python types. Used on all blocks in block_indices,
    with converters_per_element as the converter per index within block.
    Returns: interpreted_read (list(list(various)): contains whole .csv read with interpreted columns!"""
    interpreted_read = csv_list
    for li, line in enumerate(csv_list):
        for bi in block_indices:
            block = line[bi]
            interp_block = [converters_per_element[i](elem) for i, elem in enumerate(block)]
            interpreted_read[li][bi] = interp_block
    return interpreted_read

def extractColumn(csv_list, block_index, column_index):
    """Extract a column from (interpreted) csv list."""
    res = []
    for line in csv_list:
        res.append(line[block_index][column_index])
    return res
