import math


def numBatches(listlike, batch_size):
    """Just rounded up division. For use with takeBatch."""
    return int(math.ceil(len(listlike) / batch_size))


def takeBatch(listlike, batch_num, batch_size):
    """Get a smaller batch of list/tuple. batch_num starts at 0."""
    batch_start = batch_num * batch_size
    batch_end = batch_start + batch_size
    if batch_start > len(listlike):
        raise ValueError("@takeBatch: reached end of batching.""")
    if batch_end > len(listlike):
        batch_end = len(listlike) # final batch can be smaller

    return listlike[batch_start:batch_end]