"""Loading of interaction sites from files, e.g. in Dadonaite Excel format."""

class RNAInteraction:
    def __init__(self, segment1, segment2, start1, start2, end1, end2, strength=None, index=None, seq1=None, seq2=None):
        self.segment1 = segment1
        self.segment2 = segment2
        self.start1 = start1
        self.start2 = start2
        self.end1 = end1
        self.end2 = end2
        self.strength = strength
        self.index = index
        self.seq1 = seq1
        self.seq2 = seq2

    def len1(self):
        return self.end1 - self.start1

    def len2(self):
        return self.end2 - self.start2

    def coverageRatio(self):
        return self.len2() / self.len1()

    def __str__(self):
        prstring = ""
        prstring += self.segment1 +  " " + str(self.start1) + " " + str(self.end1)
        prstring += " | "
        prstring += self.segment2 + " " + str(self.start2) + " " + str(self.end2)
        prstring += " | "
        prstring += str(self.strength)
        return prstring


# ATTN: remove whitespace lines at end of interactions files
def getInteractionSites(filename, fileformat):
    """Get interacting RNA sites from Dadonaite or Le Sage file type format.
        Copy the data columns to a text file first (do not input raw excel)
        Valid fileformats: Dadonaite, LeSage, std-tsv"""
    if fileformat not in ["Dadonaite", "LeSage", "std-tsv"]:
        raise ValueError("@getInteractionSites: invalid fileformat:" + fileformat)

    SEGMENTS = ["PB2", "PB1", "PA", "HA", "NP", "NA", "MP", "NS"]
    interaction_list = [] # [RNAInteraction]
    interaction_dict = {} # {segment1 : {segment2 : [RNAInteraction]}}
    with open(filename) as f:
        for segm in SEGMENTS:
            interaction_dict[segm] = {}
            for segm2 in SEGMENTS:
                interaction_dict[segm][segm2] = []
                
        if fileformat == "Dadonaite":
            segment_dict = {1 : "PB2", 2 : "PB1", 3 : "PA",
                            4 : "HA", 5 : "NP", 6 : "NA",
                            7 : "MP", 8 : "NS"}

            data = f.read()
            lines = data.split("\n")
            for i in range(len(lines)):
                lines[i] = lines[i].split("\t")

            for i, l in enumerate(lines): # i: native index
                segm1, segm2 = segment_dict[int(l[0])], segment_dict[int(l[3])]
                start1, start2 = int(l[1]), int(l[4])
                end1, end2 = int(l[2]), int(l[5])
                rpm_string = l[6]
                if "," in rpm_string:
                    rpm_string = rpm_string.replace(",", "")
                rpm = int(rpm_string)
                inter = RNAInteraction(segm1, segm2, start1, start2, end1, end2, strength=rpm, index=i)
                interaction_list.append(inter)
                interaction_dict[segm1][segm2].append(inter)
        elif fileformat == "LeSage":
            data = f.read()
            lines = data.split("\n")
            for i in range(len(lines)):
                lines[i] = lines[i].split("\t")

            for i, l in enumerate(lines):
                segm1, segm2 = l[0], l[3]
                if segm1 == "M":
                    segm1 = "MP"
                if segm2 == "M":
                    segm2 = "MP"
                start1, start2 = int(l[1]), int(l[4])
                end1, end2 = int(l[2]), int(l[5])
                num = int(l[6])
                inter = RNAInteraction(segm1, segm2, start1, start2, end1, end2, strength=num, index=i)
                interaction_list.append(inter)
                interaction_dict[segm1][segm2].append(inter)
        elif fileformat == "std-tsv":
            data = f.read()
            lines = data.split("\n")
            for i in range(len(lines)):
                lines[i] = lines[i].split("\t")

            for i, l in enumerate(lines):
                segm1, segm2 = l[0], l[3]
                start1, start2 = int(l[1]), int(l[4])
                end1, end2 = int(l[2]), int(l[5])
                strength = int(l[6])
                inter = RNAInteraction(segm1, segm2, start1, start2, end1, end2, strength=strength, index=i)
                interaction_list.append(inter)
                interaction_dict[segm1][segm2].append(inter)
                
    return interaction_list, interaction_dict

def loadIndicesFile(filename, array_start_factor=0):
    """Load list of indices for subset sampling of interactions from a txt file.
        Numerical indices separated by "\n".
        array_start_factor (int): addition factor to compensate array start differences"""
    indices = []
    with open(filename) as f:
        for line in f.readlines():
            if len(line) > 1: # not whitespace
                if line[-1] == "\n":
                    string = line[0:-1]
                else:
                    string = line
                ind = int(string) + array_start_factor
                indices.append(ind)

    return indices

if __name__ == "__main__":
    ind = loadIndicesFile("./input/most_conserved_WSN_Udorn_PR8.txt", array_start_factor=-1)
    print(ind)
