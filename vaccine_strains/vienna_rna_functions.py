"""Some Python bindings for ViennaRNA programs."""

import os
import subprocess
import re

# TODO: global flag for install location?


def callViennaRNAProgram(vienna_rna_folder, program_name, inputs, flag_dict={}):
    """Call a ViennaRNA program in install location with list of inputs, optional dictionary of flags."""
    program_com = vienna_rna_folder + program_name
    if not os.path.isfile(program_com):
        raise ValueError("@callViennaRNAProgram: program \"" + program_name + "\" not found in: " + vienna_rna_folder)

    command = [program_com]
    for flag in flag_dict.keys():
        if flag_dict[flag] is not None:
            command.append(" --" + flag + "=" + flag_dict[flag])
        else:
            command.append(" --" + flag)
    command.extend(inputs)

    out = subprocess.run(command, capture_output=True)
    return out


def callViennaRNAProgramBatch(vienna_rna_folder, program_name, inputs_lists, flag_dict={}, os_shell="cmd.exe"):
    """Call a ViennaRNA program in install location with multiple list of inputs, optional dictionary of flags.
        Aims to reduce overhead of opening/closing shells."""
    program_com = vienna_rna_folder + program_name
    if not os.path.isfile(program_com):
        raise ValueError("@callViennaRNAProgramBatch: program \"" + program_name +
                         "\" not found in: " + vienna_rna_folder)

    process = subprocess.Popen(os_shell, shell=False,
                               stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)  # call to shell! universal_newlines=True?
    for inputs in inputs_lists:
        command = program_com
        for flag in flag_dict.keys():
            if flag_dict[flag] is not None:
                command += " --" + flag + "=" + flag_dict[flag]
            else:
                command += " --" + flag
        command += " ".join(inputs)
        command += "\n"
        command_enc = command.encode()

        process.stdin.write(command_enc)
    process.stdin.close()

    out, err = process.stdout.read(), process.stderr.read()
    return out, err


def callRNAevalWithTempFile(vienna_rna_folder, temp_file, seq, struct):
    """Call ViennaRNA RNAeval.exe program with temp_file for seq, struct storage.
    Seq, struct in string or list format (for multi input)."""
    
    with open(temp_file, 'w') as f:
        if type(seq) == str: # single seq + struct
            f.write(seq + "\n" + struct)
        elif type(seq) == list or type(seq) == tuple: # iterable --> multiple seqs and structs
            for i, sq in enumerate(seq):
                f.write(sq + "\n" + struct[i] + "\n")

    return callViennaRNAProgram(vienna_rna_folder, "RNAeval.exe", [temp_file])


def processRNAevalOutput(out):
    """Process output of ViennaRNA RNAeval.exe.
    Returns: processed output, stdout, stderr, error_found"""
    derr = out.stderr.decode()
    dout = out.stdout.decode()

    evals = [] # (seq, struct, energy)
    if derr != '':
        # TODO: problem: this works only with non-batched mode
        if "WARNING: bases" not in derr: # base pair warning; can still get free energy?
            return evals, dout, derr, True
    
    count = 0
    for line in dout.split("\n"):
        line = line.strip()
        if count % 2 == 0: # seq line
            evals.append([None, None, None])
            evals[-1][0] = line
        else: # struct + energy line
            firstspace = line.find(" ")
            evals[-1][1] = line[0:firstspace]
            energy = line[firstspace+1:][1:-1]
            if energy[0] == " ":
                energy = energy[1:]
            evals[-1][2] = float(energy)
        count += 1

    return evals, dout, derr, False # derr may contain warnings

