"""EARLYSTAGE - More precise mutation analysis and global covariation analysis."""

from intarna_functions import *
from process_alignments_sites import *
from interaction_results_analysis import *
import sequence_eda
import get_interaction_sites
import tools.config as config
import tools.readcsv as readcsv
import sequence_functions as seqf
import mutation_analysis

import matplotlib.pyplot as plt
import Bio.Align as bpalign
import numpy as np

def determineAlignmentProperStart(alignment, consecutive=5, acceptable_gap_ratio=0.90, backwards=False, gap_char="-"):
    """TODO: this function should determine the proper start/end of an alignment, ignoring heaviliy gapped initial/
    final sites."""
    indices = {} # column : number of acceptable rows preceding it so far
    num_rows = len(alignment)
    index_found = None
    iter = [range(alignment.get_alignment_length()), range(alignment.get_alignment_length() - 1, 0, -1)][backwards]
    for j in iter:
        kill_indices = [] # wipe
        indices[j] = 0
        ng = sum([alignment[i, j] != gap_char for i in range(num_rows)]) # vectorize?
        if ng / num_rows >= acceptable_gap_ratio:
            inc = 1
        else:
            inc = -1

        for k in indices.keys():
            indices[k] += inc
            if indices[k] < 0: # below threshold --> kill
                kill_indices.append(k)
            elif indices[k] >= consecutive: # found consecutive nucleotides
                index_found = k
                break

        if index_found is not None:
            break
        for k in kill_indices:
            del indices[k]

    return index_found


def alignmentToConsensus(alignment, consensus_method, alphabet=seqf.Alphabet.fromPreset("RNA"), enforce_alphabet=True,
                         conv_T_U=False):
    """Get consensus per position in alignment via seqf.ConsensusSequence."""
    cs = seqf.ConsensusSequence([], alphabet=alphabet, enforce_alphabet=enforce_alphabet) # TODO: "d" symbol? for later
    for row in alignment:
        if conv_T_U:
            cs.addSequence(seqf.toRNA(str(row.seq)), alphabet=alphabet, enforce_alphabet=enforce_alphabet)
        else:
            cs.addSequence(str(row.seq), alphabet=alphabet, enforce_alphabet=enforce_alphabet)

    return cs, cs.consensusPerPosition(method=consensus_method)


def rollingAverage(vec, k):
    return np.convolve(vec, np.ones(k), mode='same') / k


def determineCoverage(alignment, row, gap_char="-"):
    ngaps = str(alignment[row, :].seq).count(gap_char)
    return 1 - (ngaps / alignment.get_alignment_length())


def vecSumAndStdev(vec):
    """Get average of list of (average, stdev) and also compute new stdev."""
    var = 0
    sum = 0
    for v in vec:
        sum += v[0]
        var += v[1]**2
    return sum / len(vec), np.sqrt(var / len(vec))


if __name__ == "__main__":
    # Config extraction
    #CONFIG_FILE = "./input/config_H1N1.txt"
    CONFIG_FILE = "./input/config_H3N2.txt"
    #CONFIG_FILE = "./input/config_H3N2_Udorn_ref.txt"
    #CONFIG_FILE = "./input/config_various_WSNref.txt"
    PARAMS, PARAM_TYPES = config.loadConfig(CONFIG_FILE)

    ALIGNMENTS_LOCATION = PARAMS["ALIGNMENTS_LOCATION"]
    ALIGNMENTS_PREFIX = PARAMS["ALIGNMENTS_PREFIX"]
    INTERACTIONS_FILE = PARAMS["INTERACTIONS_FILE"]
    INTERACTIONS_FILE_FORMAT = PARAMS["INTERACTIONS_FILE_FORMAT"]
    INDICES_FILE_INTERACTIONS = PARAMS["INDICES_FILE_INTERACTIONS"]  # for taking subset of interactions
    INDICES_FILE_ARRAY_OFFSET = PARAMS[
        "INDICES_FILE_ARRAY_OFFSET"]  # start offset (currently -1 due to use of inter.index "native index" (offset by 1) later)
    SEGMENTS = PARAMS["SEGMENTS_PAS"]  # TODO: replace?

    REF_STRAIN = PARAMS["REF_STRAIN"]  # index of reference strain in strains
    INTERACTIONS_RESULTS_FILE = PARAMS["INTERACTIONS_RESULTS_FILE"]
    INTERACTIONS_AVERAGES_FILE = PARAMS["INTERACTIONS_AVERAGES_FILE"]

    # Load results .csv
    csv_read = readcsv.readCsv(INTERACTIONS_RESULTS_FILE)
    block1_converter = [int, str, int, int, str, int, int] # ATTN: final str?
    strain_block_converter = [str, strToBool, strOrNone, floatOrNone, strOrNone, floatOrNone, strOrNone,
                              intOrNone, intOrNone, strOrNone, intOrNone, intOrNone, strOrNone, floatOrNone]
    csv_interpreted = readcsv.csvBlockInterpret(csv_read, [0], block1_converter)
    num_strain_blocks = len(csv_interpreted[0]) - 1 # = number of non-block1 csv blocks = num of strains
    csv_interpreted = readcsv.csvBlockInterpret(csv_interpreted, range(1, num_strain_blocks+1), strain_block_converter)

    # additional config items
    STRAIN_BLACKLIST = [] # strains to not include
    CONSENSUS_METHOD = "entropy" # consensus method for determination, see sequence_functions.py
    COVERAGE_THRESHOLD = 0.5 # sequence coverage threshold for inclusion, set to 0.0 for include all
    INCLUDE_WEAK = True # include weak binding positions in binding position consensus determination

    # load alignments per segment
    alignments_dict = {}
    strains_mapped_per_segment = {}
    strains_coverage_per_segment = {}
    for segment in SEGMENTS:
        strains_mapped_per_segment[segment] = []
        strains_coverage_per_segment[segment] = {}
        fname = ALIGNMENTS_LOCATION + ALIGNMENTS_PREFIX + "_" + segment + ".fasta"
        alignment = AlignIO.read(fname, "fasta")
        if STRAIN_BLACKLIST != []:
            red_alignment = bpalign.MultipleSeqAlignment([])
            for rind, row in enumerate(alignment):
                covg = determineCoverage(alignment, rind)
                if row.id not in STRAIN_BLACKLIST and covg >= COVERAGE_THRESHOLD:
                    red_alignment.add_sequence(row.id, str(row.seq))
                    strains_mapped_per_segment[segment].append(row.id)
                    strains_coverage_per_segment[segment][row.id] = covg
        else:
            red_alignment = bpalign.MultipleSeqAlignment([])
            for rind, row in enumerate(alignment):
                covg = determineCoverage(alignment, rind)
                if covg >= COVERAGE_THRESHOLD:
                    red_alignment.add_sequence(row.id, str(row.seq))
                    strains_mapped_per_segment[segment].append(row.id)
                    strains_coverage_per_segment[segment][row.id] = covg
        alignments_dict[segment] = red_alignment

    avg_per_segment = {}
    csna_per_segment = {}
    cseq_per_segment = {}
    for segment in SEGMENTS:
        print(segment)
        alignment = alignments_dict[segment]
        ind_start = determineAlignmentProperStart(alignment)
        ind_end = determineAlignmentProperStart(alignment, backwards=True)
        cs, cons = alignmentToConsensus(alignment[:, ind_start:ind_end], CONSENSUS_METHOD,
                                        enforce_alphabet=False, conv_T_U=True)
        print("Indices:", ind_start, ind_end, alignment.get_alignment_length())
        csna = np.array(cons)
        avg, stdev = np.average(csna), np.std(csna)
        avg_per_segment[segment] = (avg, stdev)
        csna_per_segment[segment] = csna
        cseq_per_segment[segment] = cs
        print(avg, stdev)
    print("\n")

    #cs, cons = alignmentToConsensus(alignments_dict["NS"], "entropy", enforce_alphabet=False, conv_T_U=True)
    #fig, ax = plt.subplots()
    #ax.plot(range(len(cons)), rollingAverage(np.array(cons), 10))
    #ax.plot(range(len(cons)), rollingAverage(np.array(cs.consensusPerPosition("differential")), 10))
    #ax.plot(range(len(cons)), rollingAverage(np.array(cs.consensusPerPosition("average")), 10))
    #ax.plot(range(len(cons)), [v / cs.coverage[i] for i, v in enumerate(cs.getSymbolCounts("-", invert=True))])
    #plt.show()

    # every line in interaction output file
    ref_strain_index = REF_STRAIN + 1
    whole_site_cons_per_line = []
    specific_site_cons_per_line = []
    for line in csv_interpreted:
        id = line[0][0]
        seg1, seg2 = line[0][1], line[0][4]
        start1, end1, start2, end2 = line[0][2], line[0][3], line[0][5], line[0][6]
        isite_start1, isite_end1, isite_start2, isite_end2 = line[ref_strain_index][7], line[ref_strain_index][8], \
                                                             line[ref_strain_index][10], line[ref_strain_index][11]
        binding_pattern = line[ref_strain_index][12]

        # consensus per whole site
        csna1, csna2 = csna_per_segment[seg1][start1:end1+1], csna_per_segment[seg2][start2:end2+1]
        site1_avg, site1_stdev = np.average(csna1), np.std(csna1)
        site2_avg, site2_stdev = np.average(csna2), np.std(csna2)
        print(id, seg1, seg2)
        print("Full site:", round(site1_avg, 3), "±", round(site1_stdev, 3), "|",
              round(site2_avg, 3), "±", round(site2_stdev, 3))
        whole_site_cons_per_line.append((site1_avg, site1_stdev, site2_avg, site2_stdev))

        # consensus for binding positions
        vec1, vec2 = [], []
        r1, r2 = 0, 0
        for c1, c2, cbp in zip(line[ref_strain_index][6], line[ref_strain_index][9], line[ref_strain_index][12]):
            if cbp == "|" or (cbp == ":" and INCLUDE_WEAK):
                vec1.append(csna1[r1])
                vec2.append(csna2[r2])
            if c1 != "-":
                r1 += 1
            if c2 != "-":
                r2 += 1

        vec1, vec2 = np.array(vec1), np.array(vec2)
        isite1_avg, isite1_stdev = np.average(vec1), np.std(vec1)
        isite2_avg, isite2_stdev = np.average(vec2), np.std(vec2)
        print("Binding only:", round(isite1_avg, 3), "±", round(isite1_stdev, 3), "|",
              round(isite2_avg, 3), "±", round(isite2_stdev, 3))
        specific_site_cons_per_line.append((isite1_avg, isite1_stdev, isite2_avg, isite2_stdev))

        print("vs. segments:", round(avg_per_segment[seg1][0], 3), "±", round(avg_per_segment[seg1][1], 3), "|",
              round(avg_per_segment[seg2][0], 3), "±", round(avg_per_segment[seg2][1], 3)) # segment averages

    sg_avg, sg_stdev = vecSumAndStdev([avg_per_segment[s] for s in SEGMENTS])
    wh_avg, wh_stdev = vecSumAndStdev(whole_site_cons_per_line)
    sp_avg, sp_stdev = vecSumAndStdev(specific_site_cons_per_line)
    print("\nAll segments average and standard deviation (start to end with cut):", round(sg_avg, 3), "±", round(sg_stdev, 3))
    print("All interactions average and standard deviation (full site):", round(wh_avg, 3), "±", round(wh_stdev, 3))
    print("All interactions average and standard deviation (binding positions):", round(sp_avg, 3), "±", round(sp_stdev, 3))

    fig, ax = plt.subplots()
    ax.hist([v[0] for v in specific_site_cons_per_line])
    ax.set_title("Distribution of consensus averages over interactions binding sites (binding nucleotides)")
    ax.set_ylabel("Number of interactions")
    ax.set_xlabel("Average consensus score")
    plt.show()