# for use with config.loadConfig()
# UNDER CONSTRUCTION

# sequence_eda.py

SEQUENCES_FILE : string = ./input/H3N2_vaccine_strains_complete.fa # contains target sequences
TARGET_COMPLEMENT : bool = True # complement sequences
TARGET_REVERSE : bool = True # reverse sequences

REF_GENOME_FILE : string = ./input/Udorn_fasta_H3N2.txt
REF_COMPLEMENT : bool = False # complement sequences
REF_REVERSE : bool = False # reverse sequences

VERBOSE_PARSER : bool = False # verbose parser for sequence_eda.py
SEGMENTS_SEQUENCES : list(string) = [HA, NA, NP, MP, NS, PA, PB1, PB2] # segments to extract

CLUSTAL_BINARY_FOLDER : string = ./clustal-omega-1.2.2-win64/ # point to CLUSTAL binary
CLUSTAL_OUT_PREFIX : string = alignments/MSA_H3N2_Udorn # prefix for MSA output without "./". Stored from curr_folder level

# process_alignments_sites.py

ALIGNMENTS_LOCATION : string = ./alignments/
ALIGNMENTS_PREFIX : string = MSA_H3N2_Udorn
SEGMENTS_PAS : list(string) = [HA, NA, NP, MP, NS, PA, PB1, PB2] # for this script
INTERACTIONS_FILE : string = ./input/Udorn_rep1_Dadonaite_isites.txt
INTERACTIONS_FILE_FORMAT : string = Dadonaite
INTA_RNA_FOLDER : string = ./IntaRNA-3.2.0-windows-64bit/

INDICES_FILE_INTERACTIONS : string = ./input/most_conserved_WSN_Udorn_PR8_Udornref.txt # for taking subset of interactions
USE_INDICES_FILE_FOR_SAMPLING : bool = True # whether to use file for taking a subset of interactions
# if False: random sampling (or whole batch) is used instead (see below)
INDICES_FILE_ARRAY_OFFSET : int = -1 # start offset (currently -1 due to use of inter.index "native index" (offset by 1) later)

QUERY_FASTA_FILE : string = ./intermediate/inta_temp_query.fasta # batch processing DEFUNCT
TARGET_FASTA_FILE : string = ./intermediate/inta_temp_target.fasta # DEFUNCT
INTARNA_PARAM_FILE : string = ./intaRNA_config.txt
RANDOM_SEED : int = 333 # random seed for sampling. Set to "random" for random
SIZE_BATCH : int = 10 # number of samples per batch (keep unchanged)
NUM_BATCH_SAMPLES : int = 5 # number of batches. Use "all" for all interactions (change to string = all)
UNGAPPED_SEQ_REJECTION_THRESHOLD : int = 3 # threshold ungapped length to not reject interaction sequences (for new strains)

REF_STRAIN : int = 0 # index of reference strain in strains
INTERACTIONS_RESULTS_FILE : string = ./results/interaction_results_H3N2_Udorn.csv # some information on the interactions
INTERACTIONS_AVERAGES_FILE : string = ./results/interaction_averages_H3N2_Udorn.csv # averages of each interaction and stdev etc.

# mutation_analysis.py

# alignments see above
# interactions files see above
# random seed see above NOTE : defunct
# reference strain see above
MUTATIONS_RESULTS_FILE : string = ./results/mutations_results_H3N2_Udorn.csv # store results here
SEGMENTS_MUTATION_ANALYSIS : list(string) = [HA, NA, NP, MP, NS, PA, PB1, PB2] # for the mutation analysis code