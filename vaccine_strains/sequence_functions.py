"""Some tools for working with sequence strings."""
import random
import matplotlib.pyplot as plt
import matplotlib.ticker as pltticker
import math
from functools import partial

# alphabets and complements
COMPLEMENT_DICT_DNA = {"A": "T", "C": "G", "T": "A", "G": "C",
                       "R": "Y", "Y": "R", "K": "M", "M": "K",
                       "S": "W", "W": "S", "B": "V", "D": "H",
                       "H": "D", "V": "B", "N": "N", "X": "X",
                       "-": "-"}
COMPLEMENT_DICT_RNA = {"A": "U", "C": "G", "U": "A", "G": "C",
                       "R": "Y", "Y": "R", "K": "M", "M": "K",
                       "S": "W", "W": "S", "B": "V", "D": "H",
                       "H": "D", "V": "B", "N": "N", "X": "X",
                       "-": "-"}

DNA_ALPHABET_FULL = ['A', 'C', 'T', 'G', 'R', 'Y', 'K', 'M', 'S', 'W', 'B', 'D', 'H', 'V', 'N', 'X', '-']
DNA_ALPHABET_FULL_D = ['A', 'C', 'T', 'G', 'R', 'Y', 'K', 'M', 'S', 'W', 'B', 'D', 'H', 'V', 'N', 'X', 'd']
RNA_ALPHABET_FULL = ['A', 'C', 'U', 'G', 'R', 'Y', 'K', 'M', 'S', 'W', 'B', 'D', 'H', 'V', 'N', 'X', '-']
RNA_ALPHABET_FULL_D = ['A', 'C', 'U', 'G', 'R', 'Y', 'K', 'M', 'S', 'W', 'B', 'D', 'H', 'V', 'N', 'X', 'd']
PROTEIN_ALPHABET = ['A','R','N','D','B','C','E','Q','Z','G','H','I','L','K','M','F','P','S','T','W','Y','V','-']
PROTEIN_ALPHABET_D = ['A','R','N','D','B','C','E','Q','Z','G','H','I','L','K','M','F','P','S','T','W','Y','V','d']

# TODO : add substitution matrices? (BLOSUM62, PAM100, etc.)

class Alphabet:
    def __init__(self, alphabet, complement_dict=None):
        """An alphabet of symbols."""
        self.alphabet = alphabet
        self.complement_dict = complement_dict

    @classmethod
    def fromPreset(cls, preset):
        """Initiate with preset."""
        if preset not in ["DNA", "RNA", "dna", "rna"]:
            raise ValueError("@Alphabet.__init__: invalid preset: " + preset)

        if preset in ["DNA", "dna"]:  # with gaps
            return cls(["A", "C", "T", "G", "-"], complement_dict=COMPLEMENT_DICT_DNA)
        elif preset in ["RNA", "rna"]:  # with gaps
            return cls(["A", "C", "U", "G", "-"], complement_dict=COMPLEMENT_DICT_RNA)

    def __contains__(self, item):
        """Item in alphabet?"""
        return item in self.alphabet

    def __iter__(self):
        """Iterator over own alphabet."""
        return self.alphabet.__iter__()

    def adheresAlphabet(self, iterable):
        """Whether symbols in iterable adhere to alphabet.
            Slow list iteration method.
            Returns: adherence (bool), non-adhering (str or None)"""
        for c in iterable:
            if c not in self.alphabet:
                return False, c
        return True, None

    def complement(self, iterable):
        if self.complement_dict is None:
            raise ValueError("@Alphabet.complement: Alphabet has no complement_dict set")
        cstring = ""
        for c in iterable:
            cstring += self.complement_dict[c]  # raises KeyError
        return cstring


def toDNA(*strings, reverse=False):
    """Replace U with T. Should be compatible with multiple args."""
    returns = []
    if not reverse:
        for string in strings:
            returns.append(string.replace("U", "T"))
    else:
        for string in strings:
            returns.append(string.replace("U", "T")[::-1])

    if len(returns) == 1:
        return returns[0]
    else:
        return tuple(returns)


def toRNA(*strings, reverse=False):
    """Replace T with U. Should be compatible with multiple args."""
    returns = []
    if not reverse:
        for string in strings:
            returns.append(string.replace("T", "U"))
    else:
        for string in strings:
            returns.append(string.replace("T", "U")[::-1])

    if len(returns) == 1:
        return returns[0]
    else:
        return tuple(returns)


def differentialConsensus(values, normalize=True):
    """Differential consensus score. If normalize = True: normalize over sum(values)."""
    sort = sorted(values)
    score = 0.0
    if len(values) > 1:
        for i in range(1, len(values)):
            score += sort[i] - sort[i - 1]
        if normalize:
            score /= sum(values)
    elif len(values) == 1:
        return 1.0
    # elif 0 return 0
    return score


def averageConsensus(values, normalize=True):
    avg = sum(values) / len(values)
    score = sum([abs(v - avg) for v in values])
    if normalize:
        if sum(values) > 0:
            score /= ((2 * sum(values)) - (2 * sum(values) / len(values)))
        else:
            score = 0.0
    return score


def entropyConsensus(values, normalize=True, normalize_bins=True, base=math.e):
    """Consensus score based on distribution entropy."""
    # TODO: test; return -1 for invalid histograms?
    if normalize_bins:
        sumv = sum(values)
        if sumv > 0:
            nvalues = [v / sumv for v in values]
        else:
            return 0.0
    else:
        nvalues = values

    entropy = 0.0
    for v in nvalues:
        if v != 0: # else lim (n log n) --> 0
            entropy -= v * math.log(v, base) # entropy should be negative
    if normalize:
        N, M = sum(values), len(values)
        if normalize_bins:
            if M > 1: # more than 1 bin
                ratio = entropy / math.log(M, base)
            elif M == 1: # 1 bin so full consensus
                ratio = 0.0
            else: # no bins
                ratio = 1.0
        else:
            if M > 1 and N > 0:
                ratio = (entropy + N * math.log(N, base)) / (N * math.log(M, base))
            elif M == 1: # 1 bin so full consensus
                ratio = 0.0
            else: # no bins or no data
                ratio = 1.0
        return 1 - ratio # if normalise return entropy ratio
    return entropy # else just return entropy


def pairwiseDistance(values, normalize=True, invert=True):
    """(Mean) pairwise distance score: = (average/summed) Hamming distance of pairs of entered sequences."""
    pd = 0.0
    if len(values) == 1:
        return [0.0, 1.0][invert]
    elif len(values) == 0:
        return [1.0, 0.0][invert]
    
    l = [v for v in values] # inefficient but need list
    for i, v in enumerate(l):
        pd += sum([v * l[j] for j in range(i + 1, len(l))])

    if normalize:
        N = sum(l)
        norm = (N**2 - N) / 2 # normalization = all unique pairs
        pd /= norm

    if invert:
        return 1 - pd
    else:
        return pd


class ConsensusSequence:
    """Sequence with multiple symbols per position representing consensus."""
    # TODO: vectorize this class? --> this will take much effort using numpy

    def __init__(self, sequences, alphabet=None, enforce_alphabet=False, ignore_characters=frozenset([])):
        """Form consensus from list of sequences. If alphabet not None: all characters in alphabet are
        initialized to each position."""
        self.sequence = []  # num of each character per pos.
        self.coverage = []  # num of sequences per pos.
        for seq in sequences:
            for i, c in enumerate(seq):
                if i >= len(self.sequence):
                    self.coverage.append(0)
                    if alphabet is not None:
                        self.sequence.append({a: 0 for a in alphabet})
                    else:
                        self.sequence.append({})
                        
                if c in ignore_characters: # ignore the character and don't add coverage
                    continue
                
                try:
                    self.sequence[i][c] += 1
                except KeyError:
                    if not enforce_alphabet:
                        self.sequence[i][c] = 1
                    else:
                        raise KeyError("@ConsensusSequence.__init__: non-alphabet symbol: " + c)
                self.coverage[i] += 1

    def __repr__(self):
        return self.formattedString(consensus_method='differential')

    def formattedString(self, consensus_method, index_offset=0):
        string = ""
        cons = self.consensusPerPosition(method=consensus_method)
        for i, sym in enumerate(self.sequence):
            string += str(i + index_offset) + ": | "
            for c in sym.keys():
                string += c
                string += " : "
                string += str(sym[c])
                string += " | "
            string += "(" + str(self.coverage[i]) + ")"
            string += " [" + str(round(cons[i], 2)) + "]"
            string += "\n"
        return string

    def addSequence(self, sequence, start_index=0, alphabet=None, enforce_alphabet=False, ignore_characters=frozenset([])):
        """Add sequence to the consensus base."""
        for i, c in enumerate(sequence):
            if c in ignore_characters: # ignore the character and don't add coverage
                continue
            
            i_st = i + start_index
            if i_st >= len(self.sequence):
                self.coverage.extend([0 for _ in range(i_st - len(self.sequence) + 1)])
                if alphabet is not None:
                    self.sequence.extend([{a: 0 for a in alphabet}
                                          for _ in range(i_st - len(self.sequence) + 1)])
                else:
                    self.sequence.extend([{} for _ in range(i_st - len(self.sequence) + 1)])
            try:
                self.sequence[i_st][c] += 1
            except KeyError:
                if not enforce_alphabet:
                    self.sequence[i_st][c] = 1
                else:
                    raise KeyError("@ConsensusSequence.addSequence: non-alphabet symbol: " + c)
            self.coverage[i_st] += 1

    def maximalWalk(self, return_walk_probability=True, laplace_smoothing=False, randomize_on_multiple_maxima=True,
                    start_index=0, end_index=None):
        """Get maximum probability sequence, under independence of positions assumption.
        FIX: should now randomize on multiple maxima."""
        walk = ""
        p = 1.0 # probability of walk given consensus
        for i, sym in enumerate(self.sequence[start_index:end_index]):
            max_key, max_val = None, -1
            n_max = 0 # for stream sampling
            for c in sym.keys():
                if max_key is None:
                    max_key = c
                    max_val = sym[max_key]
                    n_max = 1
                elif sym[c] > max_val:
                    max_key = c
                    max_val = sym[max_key]
                    n_max = 1
                elif sym[c] == sym[max_key]:
                    if randomize_on_multiple_maxima: # stream sampling for replacement if another max val detected
                        draw = random.random()
                        if draw >= n_max / (n_max + 1): # stream sampling approach
                            max_key = c
                            n_max += 1
                            # no need to update max_val
            walk += max_key

            if return_walk_probability:
                if laplace_smoothing:
                    p *= (sym.get(max_key, 0) + 1) / (self.coverage[i] + len(sym.keys()))
                else:
                    p *= sym.get(max_key, 0) / self.coverage[i]

        if return_walk_probability:
            return walk, p
        else:
            return walk

    def randomWalk(self, return_walk_probability=True, laplace_smoothing=False,
                   start_index=0, end_index=None):
        """Get probability-based random walk under independence of positions assumption."""
        walk = ""
        p = 1.0 # probability of walk given consensus
        for i, sym in enumerate(self.sequence[start_index:end_index]):
            max_key = None
            draw = random.random()
            cumul = 0
            cov = self.coverage[i]
            c = None
            for c in sym.keys():
                cumul += sym[c] / cov
                if cumul >= draw:
                    walk += c
                    break

            if return_walk_probability:
                if laplace_smoothing:
                    p *= (sym.get(c, 0) + 1) / (self.coverage[i] + len(sym.keys()))
                else:
                    p *= sym.get(c, 0) / self.coverage[i]

        if return_walk_probability:
            return walk, p
        else:
            return walk

    def walkProbability(self, walk, start_index=0, laplace_smoothing=False):
        """Measure probability of walk given consensus.
        laplace_smoothing (bool): see Wikipedia > additive smoothing."""
        p_cumul = 1.0
        if laplace_smoothing:
            for i, (w, c) in enumerate(zip(walk, self.sequence[start_index:])):
                p_cumul *= (c.get(w, 0) + 1) / (self.coverage[i] + len(c.keys()))
        else:
            for i, (w, c) in enumerate(zip(walk, self.sequence[start_index:])):
                p_cumul *= c.get(w, 0) / self.coverage[i]
        return p_cumul

    def walkProbabilityRelativeToMax(self, walk, start_index=0, laplace_smoothing=False):
        """Walk probability relative to maximum walk. Tries to avoid potential float underflows."""
        ratio = 1.0
        if laplace_smoothing: # coverage terms cancel out
            for i, (w, c) in enumerate(zip(walk, self.sequence[start_index:])):
                p = c.get(w, 0) + 1
                hi = max(c.values()) + 1
                ratio *= p / hi # assume max > 0
        else:
            for i, (w, c) in enumerate(zip(walk, self.sequence[start_index:])):
                p = c.get(w, 0)
                hi = max(c.values())
                ratio *= p / hi  # assume max > 0
        return ratio

    def consensusPerPosition(self, method="differential", start_index=0, end_index=None):
        """Measure consensus per sequence position using method function."""
        func = None
        if method == "differential":
            func = differentialConsensus
        elif method == "average":
            func = averageConsensus
        elif method == "entropy":
            func = entropyConsensus
        elif method == "mpd" or method == "pairwise_distance":
            func = pairwiseDistance
        else:
            raise ValueError("@ConsensusSequence.consensusPerPosition: invalid method: " + method)

        cons = []
        for sym in self.sequence[start_index:end_index]:
            cons.append(func(sym.values()))
        return cons

    def occupancyPerPosition(self, k, start_index=0, end_index=None, use_ratio=True):
        """q_1 ... q_k for every position,
        where q_i = occurence ratio of rank i symbol at position.
        Rank 1 symbol = most common symbol at position, etc.
        use_ratio: divide by coverage at position."""
        occ = [[] for _ in range(k)] # first dim: occurence rank i, second dim: positions
        for sym in self.sequence[start_index:end_index]:
            col = [] # column of occ
            queue = list(sym.values())
            deficit = 0
            if len(queue) < k: # k > len(queue): too many symbols for k, have to pad with 0s
                deficit = k - len(queue)
            divisor = [1, max(1, sum(queue))][use_ratio] # normaliser, prevent div by zero
            for i in range(k - deficit):
                max_it = max(range(len(queue)), key=queue.__getitem__)
                col.append(queue.pop(max_it) / divisor)
            col.extend([0 for _ in range(deficit)]) # add in case too few symbols (< k)
            
            for i in range(k): # append as column
                occ[i].append(col[i])
        return occ

    def getSymbolCounts(self, symbol, invert=False):
        """Get count per index of symbol.
        invert (bool): get count of all !symbol instead."""
        counts = []
        if invert:
            for i, sym in enumerate(self.sequence):
                counts.append(self.coverage[i] - sym.get(symbol, 0)) # rely on self.coverage
        else:
            for sym in self.sequence:
                counts.append(sym.get(symbol, 0))
        return counts

    def getConsensusPlot(self, normalize=True, index_offset=0, start_index=0, end_index=None, cmap=None,
                         scatter=False, scatter_size=12, annotate_characters=True,
                         x_major=5.0, x_minor=1.0):
        """Get a matplotlib.pyplot plot of the consensus sequence."""
        # prepare char lists
        char_yranges = []
        char_xranges = []
        chars = {}
        ind = 0
        for sym in self.sequence[start_index:end_index]:
            for k in sym.keys():
                if k not in chars:
                    chars[k] = ind
                    char_xranges.append([])
                    char_yranges.append([])
                    ind += 1
        nchars = ind

        # main loop
        for iu, sym in enumerate(self.sequence[start_index:end_index]):
            i = iu + start_index
            for v in range(nchars): # append defaults else plot glitches
                char_xranges[v].append(i + index_offset)
                char_yranges[v].append(0)
            for k in sym.keys():
                ind = chars[k]
                if normalize:
                    char_yranges[ind][-1] = sym[k] / self.coverage[i]  # throw div. by zero impossible?
                else:
                    char_yranges[ind][-1] = sym[k]
                    
        # colors
        fig, ax = plt.subplots()
        if cmap:
            import matplotlib.cm as pltcm
            cmap_obj = pltcm.get_cmap(cmap)

        # ticks
        ax.xaxis.set_major_locator(pltticker.IndexLocator(base=x_major, offset=0))
        ax.xaxis.set_major_formatter(pltticker.StrMethodFormatter('{x:.0f}'))
        ax.xaxis.set_minor_locator(pltticker.IndexLocator(base=x_minor, offset=0))
        ax.tick_params(which='major', length=5.0)
        ax.tick_params(which='minor', length=4.0)

        # plot
        if normalize:  # necessary?
            ax.set_ylim((0, 1))
        plot_func = [ax.plot, partial(ax.scatter, s=scatter_size)][scatter]
        for k in chars.keys():
            ind = chars[k]
            if cmap:
                plot_func(char_xranges[ind], char_yranges[ind], label=k, color=cmap_obj(ind / nchars))
            else:
                plot_func(char_xranges[ind], char_yranges[ind], label=k)
        rlim = index_offset + [end_index, len(self.sequence) - 1][end_index is None] # correct for None arg
        ax.set_xlim(left=index_offset+start_index, right=rlim)

        # annotate max. characters above plot
        if annotate_characters:
            max_char = self.maximalWalk(return_walk_probability=False, laplace_smoothing=True, randomize_on_multiple_maxima=False,
                                        start_index=start_index, end_index=end_index)
            if len(max_char) > 0: # else div. by zero
                for i, c in enumerate(max_char):
                    ax.annotate(c, xy=(i / len(max_char), 1.01), xycoords='axes fraction')

        return fig, ax

    def getCoveragePlot(self, normalize=True, index_offset=0, start_index=0, end_index=None,
                        x_major=5.0, x_minor=1.0):
        """Get a plot of the sequence coverage at every position."""
        # coverage plot
        fig, ax = plt.subplots()
        cov = self.coverage[start_index:end_index]
        if normalize:
            mcov = max(1, max(self.coverage)) # across whole sequence
            cov = [v / mcov for v in cov]
            ax.set_ylim((0, 1))
        ax.plot([i + start_index + index_offset for i in range(len(cov))], cov)

        # ticks
        ax.xaxis.set_major_locator(pltticker.IndexLocator(base=x_major, offset=0))
        ax.xaxis.set_major_formatter(pltticker.StrMethodFormatter('{x:.0f}'))
        ax.xaxis.set_minor_locator(pltticker.IndexLocator(base=x_minor, offset=0))
        ax.tick_params(which='major', length=5.0)
        ax.tick_params(which='minor', length=4.0)
        
        return fig, ax

    def consensusMatch(self, string, start_index=0, normalize=False):
        """Match a string to ConsensusSequence: at every position: add num. of occurences of symbol / coverage."""
        if len(string) == 0:
            raise ValueError("@ConsensusSequence.consensusMatch: string must be longer than 0 characters.")
        
        value = 0
        for i, (s, cs) in enumerate(zip(string, self.sequence[start_index:])):
            inc = cs.get(s, 0) / self.coverage[i + start_index]
            value += inc

        if normalize:
            value /= len(string)

        return value


def bilateralSlidingAverage(iterable, window_size):
    """Bilateral sliding window averaging, window_size on one side (e.g. windows_size = 3 --> left 3 and right 3.
    (moved from sequence_consensus_plot.py)."""
    if window_size < 0:
        raise ValueError("@interaction_segment_analysis.bilateralSlidingAverage: window_size must be larger than 0.")
    elif window_size == 0: # just return the original (but iterated)
        return [v for v in iterable]

    averaged = []
    for i, v in enumerate(iterable):
        i1 = max(0, i - window_size)
        i2 = min(len(iterable), i + window_size)
        averaged.append(sum(iterable[i1:i2]) / (i2 - i1)) # iterable must support indexing
    return averaged


def overlapIndices(start1, len1, start2, len2):
    """For two sequences: get indices (overlap_start1, overlap_end1, os2, oe2) of overlap. Return None if N/A."""
    s1, e1, s2, e2 = None, None, None, None
    if start2 < len1:
        s1, e1 = start2, len1
        s2, e2 = 0, len1 - start2
    elif start1 < len2:
        s1, e1 = 0, len2 - start1
        s2, e2 = start1, len2
    else:
        return None
    return s1, e1, s2, e2


class SequenceEnsemble:
    """Ensemble of multiple sequences"""

    def __init__(self, sequences):
        self.sequences = sequences

    def __repr__(self):
        string = "Ensemble"
        for s in self.sequences:
            string += "\n"
            string += s
        return string

    def addSequence(self, sequence):
        """Add a sequence to the ensemble."""
        self.sequences.append(sequence)

    def getSequence(self, index):
        return self.sequences[index]

    def toConsensusSequence(self, alphabet=None, enforce_alphabet=False):
        """Create consensus sequence from ensemble."""
        cs = ConsensusSequence([])
        for s in self.sequences:
            cs.addSequence(s, start_index=0, alphabet=alphabet, enforce_alphabet=enforce_alphabet)
        return cs

    @classmethod
    def __doubleDeleteDictKey(cls, dictionary, key): # passed by reference?
        for k in dictionary.keys():
            dictionary[k].pop(key, None)
        dictionary.pop(key, None)
        return

    @classmethod
    def __doubleDictMin(cls, dictionary):
        min_keys = []
        min_val = float('inf')
        for k1, v1 in dictionary.items():
            for k2, v2 in v1.items():
                if v2 < min_val:
                    min_keys = [(k1, k2)]
                    min_val = v2
                elif v2 == min_val:
                    min_keys.append((k1, k2))
        return min_keys

    def getHierarchy(self, distance_function):
        """Hierarchical clustering using averaging method."""
        dist_dict = {} # dist per index
        level_dict = {} # dist per hierarchy level
        for i, s1 in enumerate(self.sequences):
            dist_dict[i] = {}
            for j in range(i + 1, len(self.sequences)):
                s2 = self.sequences[j]
                dist_dict[i][j] = distance_function(s1, s2)
        hierarchy = set(range(len(self.sequences)))
        while True:
            min_keys = self.__doubleDictMin(dist_dict)
            mk1, mk2 = random.choice(min_keys)
            min_val = dist_dict[mk1][mk2]
            new_key = frozenset([mk1, mk2])
            new_column = {}
            for k in dist_dict.keys():
                if k != mk1 and k != mk2:
                    if mk1 in dist_dict.get(k, {}):
                        val1 = dist_dict[k][mk1]
                    else:
                        val1 = dist_dict[mk1][k]
                    if mk2 in dist_dict.get(k, {}):
                        val2 = dist_dict[k][mk2]
                    else:
                        val2 = dist_dict[mk2][k]
                    new_column[k] = (val1 + val2) / 2
            dist_dict[new_key] = new_column
            self.__doubleDeleteDictKey(dist_dict, mk1)
            self.__doubleDeleteDictKey(dist_dict, mk2)
            hierarchy.remove(mk1)
            hierarchy.remove(mk2)
            hierarchy.add(new_key)
            level_dict[new_key] = min_val

            if len(hierarchy) == 1:
                break
        return hierarchy, level_dict


def multiHammingDistance(string1, string2, normalize=False):
    """Hamming distance between strings."""
    val = 0
    for s1, s2 in zip(string1, string2):
        if s1 != s2:
            val += 1
    if normalize:
        length = min(len(string1), len(string2))
        if length != 0:
            return val / length
        else:
            raise ValueError("@multiHammingDistance: minimum sequence has length zero.")
    else:
        return val


class PhylogeneticNode:
    """Node for a sequence tree."""
    def __init__(self, name, sequence=None, parent_distance=None):
        self.name = name
        self.sequence = sequence
        self.parent_distance = parent_distance
        self.children = []

    def addChild(self, node):
        self.children.append(node)

    def getChild(self, index):
        return self.children[index]

    def removeChild(self, index):
        child = self.children.pop(index)
        return child

    def _toString(self, level=0):
        """Recursive repr for class PhylogeneticNode."""
        # TODO: make frozenset look nicer
        string = ""
        string += "".join(["-" for _ in range(level)])
        string += " " + str(self.name)
        if self.parent_distance is not None:
            string += " " + str(self.parent_distance)
        if self.sequence is not None:
            string += " " + str(self.sequence)
        string += "\n"

        for cnode in self.children:
            string += cnode._toString(level=level + 1)
        return string

    def __repr__(self):
        return self._toString(level=0)


def _formNodesHierarchyTree(hierarchy, level_dist_dict, names_dict=None, sequences_dict=None):
    node = PhylogeneticNode(hierarchy)
    if type(hierarchy) == frozenset:
        for h in hierarchy:
            cnode = _formNodesHierarchyTree(h, level_dist_dict, names_dict=names_dict, sequences_dict=sequences_dict)
            cnode.parent_distance = level_dist_dict[hierarchy]
            node.addChild(cnode)
        return node
    else:
        name, seq = hierarchy, None
        if names_dict is not None:
            name = names_dict[hierarchy]
        if sequences_dict is not None:
            seq = sequences_dict[hierarchy]
        node.name = name
        node.sequence = seq
        return node


def hierarchyToTree(hierarchy, level_dist_dict, names_dict=None, sequences_dict=None):
    """Convert SequenceEnsemble.getHierarchy to PhylogeneticNode tree."""
    root_elem = hierarchy.pop()
    root = _formNodesHierarchyTree(root_elem, level_dist_dict, names_dict=names_dict, sequences_dict=sequences_dict)
    return root


if __name__ == "__main__":
    seq1 = "AAAATTTCGGG"
    seq2 = "CCAATCGCGAAAC"
    seq3 = "ACACTTCCGTA"
    cs1 = ConsensusSequence([seq1, seq2], alphabet=["A", "C", "T", "G"])
    print(cs1)
    print(cs1.maximalWalk())
    print(cs1.randomWalk())

    struct1 = "............|||||.....:|||||.|||||..............."
    struct2 = "..............|||||||.....:||||||..................."
    struct3 = "|||||::||...:||||||.|:||.....||"
    cs2 = ConsensusSequence([struct1, struct2, struct3])
    print(cs2.maximalWalk())
    print(cs2.randomWalk())

    seqs = []
    for _ in range(0, 10):
        seq_rand = ""
        for _ in range(0, 30):
            seq_rand += random.choice(["A", "C", "T", "G"])
        seqs.append(seq_rand)

    cs_rand = ConsensusSequence(seqs, alphabet=["A", "C", "T", "G"], enforce_alphabet=True)
    print(cs_rand)
    print(cs_rand.consensusPerPosition())
    print(cs_rand.maximalWalk())
    for _ in range(0, 5):
        print(cs_rand.randomWalk())

    fig, ax = cs_rand.getConsensusPlot(normalize=True, index_offset=1)
    ax.legend()
    ax.grid()
    plt.show()

    ens1 = SequenceEnsemble(seqs)
    print(ens1)
    hier1, hierlvl1 = ens1.getHierarchy(distance_function=multiHammingDistance)
    print(hier1)

    htree1 = hierarchyToTree(hier1, hierlvl1)
    print(htree1)
    print(ens1.toConsensusSequence())
