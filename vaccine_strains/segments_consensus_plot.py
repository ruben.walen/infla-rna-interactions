"""Plotting of consensus along segments, given input genomes."""
from Bio import AlignIO

import sequence_eda
import get_interaction_sites
import process_alignments_sites as pas
import tools.config as config
import interaction_segments_analysis
import sequence_functions as seqf

import math
import matplotlib.pyplot as plt


if __name__ == "__main__":
    # setup
    # load config
    CONFIG_FILE = "./input/config_H1N1.txt"
    #CONFIG_FILE = "./input/config_H5N1_WSN_ref.txt"
    PARAMS, PARAM_TYPES = config.loadConfig(CONFIG_FILE)

    ALIGNMENTS_LOCATION = PARAMS["ALIGNMENTS_LOCATION"]
    ALIGNMENTS_PREFIX = PARAMS["ALIGNMENTS_PREFIX"]

    # params
    SEGMENTS = ["PB2", "PB1", "PA", "HA", "NP", "NA", "MP", "NS"]  # segments to consider

##### - experiment 1: segment consensus plots
    # config
    CONSENSUS_METHOD = "mpd"  # consensus computation method
    SLIDING_WINDOW_SIZE = 10  # size of averaging window (bilateral window)
    STRAIN_BLACKLIST_ID = [] # strains to exclude, by INDEX in alignment
    STRAIN_BLACKLIST_NAME = ["A/Bayern/7/1995", "A/Solomon"] # strains to exclude by alignment id (may have issues with space bar char)

    # alignments loading
    alignments_dict = {}
    strain_coverage_per_segment = {}
    for segment in SEGMENTS:
        print("Reading", segment)
        fname = ALIGNMENTS_LOCATION + ALIGNMENTS_PREFIX + "_" + segment + ".fasta"
        alignment = AlignIO.read(fname, "fasta")
        alignments_dict[segment] = alignment

        # Parse segment/strain names
        sequences = sequence_eda.parseFASTASequences(fname, complement=False, reverse=False)  # (header, seq)
        _, seq_parsed = sequence_eda.parseFASTAHeadersFromSeqlist(sequences)  # index 0 = strain name
        strain_coverage_per_segment[segment] = [seq[0] for seq in seq_parsed]  # in order of occurence
    strains_found = set([]) # strain names found in alignment
    for segment, strains in strain_coverage_per_segment.items():
        for strain in strains:
            strains_found.add(strain)


    # get consensus per segment
    consensus_dict = {}
    for segment in SEGMENTS:
        print("Segment", segment)
        alignment = alignments_dict[segment]
        consensus = seqf.ConsensusSequence([], alphabet=seqf.RNA_ALPHABET_FULL, enforce_alphabet=True)
        for row in range(len(alignment)):
            alignment_strain_name = alignment[row, :].id
            if row in STRAIN_BLACKLIST_ID or alignment_strain_name in STRAIN_BLACKLIST_NAME:
                print("\tRejected", alignment_strain_name)
                continue

            print("\tStrain", alignment_strain_name)
            seq = seqf.toRNA(str(alignment[row, :].seq))
            consensus.addSequence(seq, alphabet=seqf.RNA_ALPHABET_FULL, enforce_alphabet=True)
        consensus_dict[segment] = consensus

    # plotting
    plot_layout = (math.ceil(len(SEGMENTS) / math.ceil(len(SEGMENTS) / 4)), math.ceil(len(SEGMENTS) / 4))
    fig_cons, ax_cons = plt.subplots(nrows=plot_layout[0], ncols=plot_layout[1])
    fig_cons.subplots_adjust(hspace=0.35, top=0.90)  # better spacing

    for i, segment in enumerate(SEGMENTS):
        pi = interaction_segments_analysis.getIndexInPlot(i, plot_layout)
        cons = seqf.bilateralSlidingAverage(consensus_dict[segment].consensusPerPosition(method=CONSENSUS_METHOD),
                                       window_size=SLIDING_WINDOW_SIZE)
        ax_cons[pi[0], pi[1]].scatter([v for v in range(len(cons))], cons, s=plt.rcParams['lines.markersize'] * 0.5)
        ax_cons[pi[0], pi[1]].set_title("Sequence consensus for segment: " + segment)
        ax_cons[pi[0], pi[1]].set_ylim((0, 1))

    fig_cons.suptitle("Sequence consensus plot per segment with " +
                      "bilateral sliding window ({} nt ea.) averaging".format(SLIDING_WINDOW_SIZE))
    plt.show()
